import 'package:flutter/material.dart';

import 'package:gt_test/models/insurance_details_model.dart';
import 'package:gt_test/services/customer_service.dart';
import '../widgets/common/circular_progress_bar.dart';
import '../widgets/insurance_details_widget.dart';

class InsuranceDetailsContainer extends StatefulWidget {
  final int? transactionId;
  final String? status;

  InsuranceDetailsContainer({this.transactionId, this.status});
  @override
  _InsuranceDetailsContainerState createState() =>
      _InsuranceDetailsContainerState();
}

class _InsuranceDetailsContainerState extends State<InsuranceDetailsContainer> {
  late Future<InsuranceDetailsModel?> insuranceData;
  late InsuranceDetailsModel insuranceDetails;
  @override
  void initState() {
    super.initState();
    insuranceData = CustomerService().getInsurerPlanDetails(
      transactionId: widget.transactionId.toString(),
      context: context,
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: insuranceData,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(child: CircularProgressBar());
        }
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.hasData) {
          insuranceDetails = snapshot.data;
          return InsuranceDetailsWidget(
              insuranceDetails: insuranceDetails, status: widget.status!);
        }
        return Center(child: CircularProgressBar());
      },
    );
  }
}
