import '../common/pref_status.dart';
import '../common/shared_prefs.dart';
import '../models/user_model.dart';

class AuthData {

  Future<UserModel> getUserDetails() async {
    UserModel userData = UserModel();
    PrefsStatus items = await SharedPrefs().getMapPrefs('userdetails');
    if (items.status == true) {
      userData = UserModel.fromJson(items.value);
    }
    return userData;
  }
}
