import 'package:flutter/material.dart';

import 'package:gt_test/models/payment_body_model.dart';

class PaymentsProvider extends ChangeNotifier {
  PaymentBodyModel? paymentBody = PaymentBodyModel();

  set setPaymentBody(PaymentBodyModel payment) {
    paymentBody = payment;
    notifyListeners();
  }

  void resetPaymentBody() {
    paymentBody = null;
  }
}
