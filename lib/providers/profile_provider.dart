import 'package:flutter/material.dart';

import 'package:gt_test/models/profile_model.dart';

class ProfileProvider extends ChangeNotifier {
  ProfileModel? profileData = ProfileModel();

  set setProfileData(ProfileModel? profile) {
    profileData = profile;
    notifyListeners();
  }

  void resetProfileData() {
    profileData = null;
  }
}
