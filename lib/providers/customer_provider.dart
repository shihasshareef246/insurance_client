import 'package:flutter/material.dart';
import 'package:gt_test/models/additional_travel_insurance_model.dart';
import 'package:gt_test/models/claim_docs_model.dart';

import 'package:gt_test/models/make_model.dart';
import 'package:gt_test/models/customer_claims_model.dart';
import 'package:gt_test/models/customer_insurance_model.dart';
import '../models/additional_motor_insurance_model.dart';
import '../models/discount_plan_model.dart';
import '../models/insurance_details_model.dart';
import '../models/motor_insurance_group_model.dart';
import '../models/motor_insurance_model.dart';
import '../models/travel_insurance_group_model.dart';
import '../models/travel_insurance_model.dart';

class CustomerProvider extends ChangeNotifier {
  List<CustomerInsuranceModel?> insuranceList = [];
  List<CustomerClaimsModel?> claimsList = [];
  List<ClaimDocsModel?> claimDocs = [];
  List<PersonalDetailsModel?> insuranceDetails = [];
  String insuranceType = '';
  InsurerMotorDetailsModel? insuranceMotorDetails = InsurerMotorDetailsModel();
  List<AdditionalMotorInsuranceModel?> additionalMotorInsuranceList = [];
  List<MotorInsuranceModel?> selectedMotorInsuranceList = [];
  List<AdditionalMotorInsuranceModel?> selectedAdditionalMotorInsuranceList =
      [];
  InsurerTravelDetailsModel? insuranceTravelDetails =
      InsurerTravelDetailsModel();
  List<AdditionalTravelInsuranceModel?> additionalTravelInsuranceList = [];
  List<TravelInsuranceModel?> selectedTravelInsuranceList = [];
  List<AdditionalTravelInsuranceModel?> selectedAdditionalTravelInsuranceList =
      [];
  set setInsuranceList(List<CustomerInsuranceModel?> insList) {
    insuranceList = insList;
    notifyListeners();
  }

  void resetInsuranceList() {
    insuranceList = [];
  }

  set setInsuranceType(String insType) {
    insuranceType = insType;
    notifyListeners();
  }

  void resetInsuranceType() {
    insuranceType = '';
  }

  set setClaimsList(List<CustomerClaimsModel?> claimList) {
    claimsList = claimList;
    notifyListeners();
  }

  void resetClaimsList() {
    claimsList = [];
  }

  set setClaimDocuments(List<ClaimDocsModel?> docs) {
    claimDocs = docs;
    notifyListeners();
  }

  void resetClaimDocuments() {
    claimDocs = [];
  }

  set setInsuranceDetails(List<PersonalDetailsModel?> details) {
    insuranceDetails = details;
    notifyListeners();
  }

  void resetInsuranceDetails() {
    insuranceDetails = [];
  }

  set setInsurerMotorDetails(InsurerMotorDetailsModel? details) {
    insuranceMotorDetails = details;
    notifyListeners();
  }

  void resetInsurerMotorDetails() {
    insuranceMotorDetails = InsurerMotorDetailsModel();
  }

  set setSelectedMotorInsuranceList(List<MotorInsuranceModel?> selectedList) {
    selectedMotorInsuranceList = selectedList;
    notifyListeners();
  }

  void resetSelectedMotorInsuranceList() {
    selectedMotorInsuranceList = [];
  }

  set setSelectedAdditionalMotorInsuranceList(
      List<AdditionalMotorInsuranceModel?> selectedList) {
    selectedAdditionalMotorInsuranceList = selectedList;
    notifyListeners();
  }

  void resetSelectedAdditionalMotorInsuranceList() {
    selectedAdditionalMotorInsuranceList = [];
  }

  set setInsurerTravelDetails(InsurerTravelDetailsModel? details) {
    insuranceTravelDetails = details;
    notifyListeners();
  }

  void resetInsurerTravelDetails() {
    insuranceTravelDetails = InsurerTravelDetailsModel();
  }

  set setSelectedTravelInsuranceList(List<TravelInsuranceModel?> selectedList) {
    selectedTravelInsuranceList = selectedList;
    notifyListeners();
  }

  void resetSelectedTravelInsuranceList() {
    selectedTravelInsuranceList = [];
  }

  set setSelectedAdditionalTravelInsuranceList(
      List<AdditionalTravelInsuranceModel?> selectedList) {
    selectedAdditionalTravelInsuranceList = selectedList;
    notifyListeners();
  }

  void resetSelectedAdditionalTravelInsuranceList() {
    selectedAdditionalTravelInsuranceList = [];
  }
}
