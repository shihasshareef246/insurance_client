import 'package:flutter/material.dart';
import 'package:gt_test/models/additional_travel_insurance_model.dart';

import 'package:gt_test/models/make_model.dart';
import 'package:gt_test/models/travel_insurance_model.dart';
import '../models/additional_motor_insurance_model.dart';
import '../models/discount_plan_model.dart';
import '../models/motor_insurance_model.dart';

class DashboardProvider extends ChangeNotifier {
  List<MakeModel?> brandList = [];
  List<DiscountPlanModel?> discountPlanList = [];
  List<MotorInsuranceModel?> motorInsuranceList = [];
  List<AdditionalMotorInsuranceModel?> additionalMotorInsuranceList = [];
  List<TravelInsuranceModel?> travelInsuranceList = [];
  List<AdditionalTravelInsuranceModel?> additionalTravelInsuranceList = [];

  set setBrandsList(List<MakeModel?> brandsList) {
    brandList = brandsList;
    notifyListeners();
  }

  void resetBrandsList() {
    brandList = [];
  }

  set setDiscountPlanList(List<DiscountPlanModel?> planList) {
    discountPlanList = planList;
    notifyListeners();
  }

  void resetDiscountPlanList() {
    discountPlanList = [];
  }

  set setMotorInsuranceList(List<MotorInsuranceModel?> insuranceList) {
    motorInsuranceList = insuranceList;
    notifyListeners();
  }

  void resetMotorInsuranceList() {
    motorInsuranceList = [];
  }

  set setAdditionalMotorInsuranceModel(
      List<AdditionalMotorInsuranceModel?> insuranceList) {
    additionalMotorInsuranceList = insuranceList;
    notifyListeners();
  }

  void resetAdditionalMotorInsuranceModel() {
    additionalMotorInsuranceList = [];
  }

  set setTravelInsuranceList(List<TravelInsuranceModel?> insuranceList) {
    travelInsuranceList = insuranceList;
    notifyListeners();
  }

  void resetTravelInsuranceList() {
    travelInsuranceList = [];
  }

  set setAdditionalTravelInsuranceList(
      List<AdditionalTravelInsuranceModel?> insuranceList) {
    additionalTravelInsuranceList = insuranceList;
    notifyListeners();
  }

  void resetAdditionalTravelInsuranceList() {
    additionalTravelInsuranceList = [];
  }
}
