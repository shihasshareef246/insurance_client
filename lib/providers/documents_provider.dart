import 'package:flutter/material.dart';
import 'package:gt_test/models/file_model.dart';

import '../models/motor_insurance_group_model.dart';

class DocumentsProvider extends ChangeNotifier {
  List<InsurerDocumentDetailsModel?> docsList = [];
  List<FileModel?> documentsList = [];

  set setDocs(List<InsurerDocumentDetailsModel?> filesList) {
    docsList = filesList;
    notifyListeners();
  }

  void resetDocs() {
    docsList = [];
  }

  set setDocuments(List<FileModel?> filesList) {
    documentsList = filesList;
    notifyListeners();
  }

  void resetDocuments() {
    documentsList = [];
  }
}
