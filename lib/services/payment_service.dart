import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:uuid/uuid.dart';

import '../common/environment.dart';
import '../common/logger.dart';
import '../common/navigator/navigations.dart';
import '../common/routes/routes.dart';
import '../common/shared_prefs.dart';
import 'package:gt_test/models/status_model.dart';
import '../models/payment_model.dart';
import 'package:gt_test/models/payment_status_model.dart';

class PaymentService {
  var uuid = Uuid();

  Future<PaymentModel?> postPaymentToGateway({
    required BuildContext context,
    required String amount,
    required String? name,
    required String? mobile,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}PostPaymentToGateway');
    var client = http.Client();
    try {
      var response = await client
          .post(
            uri,
            headers: {
              "content-type": "application/json",
              "accept": "application/json",
            },
            body: json.encode({
              "Do_PyrDtl": {"Pyr_MPhone": mobile, "Pyr_Name": name},
              "Do_TxnDtl": [
                {"Txn_AMT": amount}
              ],
              "Do_TxnHdr": {
                "Merch_Txn_UID": uuid.v1().substring(0, 6),
              }
            }),
          )
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        var paymentData;
        Map paymentDetails = jsonDecode(response.body);
        paymentData = PaymentModel.fromJson(paymentDetails);
        return paymentData;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<PaymentStatusModel?> paymentStatus({
    required BuildContext context,
    required String txnRefNo,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}PaymentStatus');
    var client = http.Client();
    try {
      var response = await client
          .post(
            uri,
            headers: {
              "content-type": "application/json",
              "accept": "application/json",
            },
            body: json.encode({
              "MerchantTxnRefNo": ["654"]
            }),
          )
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        var paymentData;
        Map paymentDetails = jsonDecode(response.body);
        paymentData = PaymentStatusModel.fromJson(paymentDetails);
        return paymentData;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<StatusModel?> postPaymentToDb({
    required BuildContext context,
    String paymentreferenceno = '',
    required String PaymentMode,
    required String paymentdate,
    required String premiumAmount,
    required String? transactionid,
    required String status,
    required String transactiondate,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}PostPaymentToDbv2');
    var client = http.Client();
    try {
      var response;
      if (PaymentMode == 'Cash') {
        response = await client
            .post(
              uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "PaymentMode": PaymentMode,
                "paymentdate": paymentdate, //"01-06-2000",//paymentdate,
                "premiumAmount": premiumAmount,
                "transactionid": transactionid,
                "status": status,
                "transactionDate":
                    transactiondate //"01-06-2000 10:10:10",//transactiondate,
              }),
            )
            .timeout(const Duration(seconds: 30));
      } else {
        response = await client
            .post(
              uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "paymentreferenceno": paymentreferenceno,
                "PaymentMode": PaymentMode,
                "paymentdate": paymentdate, //"01-06-2000",//paymentdate,
                "premiumAmount": premiumAmount,
                "transactionid": transactionid,
                "status": status,
                "transactionDate":
                    transactiondate //"01-06-2000 10:10:10",//transactiondate,
              }),
            )
            .timeout(const Duration(seconds: 30));
      }
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        var paymentData;
        Map paymentDetails = jsonDecode(response.body);
        paymentData = StatusModel.fromJson(paymentDetails);
        return paymentData;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<StatusModel?> applyPromoCode({
    required BuildContext context,
    required String code,
    required String? planId,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}PromoCode');
    var client = http.Client();
    try {
      var response = await client
          .post(
            uri,
            headers: {
              "content-type": "application/json",
              "accept": "application/json",
            },
            body: json.encode({
              "InputBody": {
                "RootNode": {
                  "promocode": code,
                  "planId": planId,
                  "insuranceId": "1"
                }
              }
            }),
          )
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        var promoData;
        Map promoDetails = jsonDecode(response.body);
        promoData = StatusModel.fromJson(promoDetails);
        return promoData;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }
}
