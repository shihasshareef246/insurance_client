import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:gt_test/models/additional_travel_insurance_model.dart';
import 'package:gt_test/models/travel_insurance_model.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../common/environment.dart';
import '../common/logger.dart';
import '../common/navigator/navigations.dart';
import '../common/routes/routes.dart';
import '../common/shared_prefs.dart';
import '../models/additional_model.dart';
import '../models/motor_insurance_model.dart';
import 'package:gt_test/models/additional_motor_insurance_model.dart';
import 'package:gt_test/models/make_model.dart';
import 'package:gt_test/models/discount_plan_model.dart';

import '../models/transaction_model.dart';
import '../providers/customer_provider.dart';
import '../providers/documents_provider.dart';
import '../providers/profile_provider.dart';

class TravelService {
  List<MakeModel> brandList = [];
  List<DiscountPlanModel> discountPlanList = [];
  List<TravelInsuranceModel> travelInsuranceList = [];
  List<AdditionalTravelInsuranceModel> additionalTravelInsuranceList = [];
  CustomerProvider customerProvider = CustomerProvider();
  ProfileProvider profileProvider = ProfileProvider();
  DocumentsProvider documentsProvider = DocumentsProvider();

  final dateFormat = DateFormat("dd-MM-yyyy");
  final timeFormat = DateFormat("dd-MM-yyyy HH:mm:ss");

  Future<List<DiscountPlanModel?>> getDiscountPlans({
    required BuildContext context,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}getDiscountPlans');
    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({}))
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        List planDetails = jsonDecode(response.body);
        if (planDetails.isNotEmpty) {
          for (var data in planDetails) {
            discountPlanList.add(
              DiscountPlanModel.fromJson(data),
            );
          }
        }
        return discountPlanList;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return [];
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return [];
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return [];
    } on SocketException {
      print("You are not connected to internet");
      return [];
    } catch (e) {
      Logger.d(e.toString(), '');
      return [];
    } finally {
      client.close();
    }
  }

  Future<List<TravelInsuranceModel?>> getTravelPlans({
    required BuildContext context,
    String travelFromDate = "",
    String travelToDate = "",
    String dob = "",
    String planType = "",
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}GetTravelPlans');
    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "travelFromDate": travelFromDate,
                "travelToDate": travelToDate,
                "dob": dob,
                "planType": planType
              }))
          .timeout(const Duration(seconds: 30));

      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        List insuranceDetails = jsonDecode(response.body);
        if (insuranceDetails.isNotEmpty) {
          for (var data in insuranceDetails) {
            travelInsuranceList.add(
              TravelInsuranceModel.fromJson(data),
            );
          }
        }
        return travelInsuranceList;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return [];
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return [];
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return [];
    } on SocketException {
      print("You are not connected to internet");
      return [];
    } catch (e) {
      Logger.d(e.toString(), '');
      return [];
    } finally {
      client.close();
    }
  }

  Future<List<AdditionalTravelInsuranceModel?>> getAdditionalTravelDetails({
    required String premiumAmount,
    required BuildContext context,
  }) async {
    var uri =
        Uri.parse('${AppEnvironment.serverUrl}GetAdditionalTravelDetails');
    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InputBody": {
                  "RootNode": {"premiumValue": premiumAmount}
                }
              }))
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        List planDetails = jsonDecode(response.body);
        if (planDetails.isNotEmpty) {
          for (var data in planDetails) {
            additionalTravelInsuranceList.add(
              AdditionalTravelInsuranceModel.fromJson(data),
            );
          }
        }
        return additionalTravelInsuranceList;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return [];
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return [];
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return [];
    } on SocketException {
      print("You are not connected to internet");
      return [];
    } catch (e) {
      Logger.d(e.toString(), '');
      return [];
    } finally {
      client.close();
    }
  }

  Future<TransactionModel?> postInsurerInsuranceDetails({
    required BuildContext context,
  }) async {
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    documentsProvider = Provider.of<DocumentsProvider>(context, listen: false);
    String additionalCoverID = '';
    List<AdditionalModel> addList = [];
    customerProvider?.selectedAdditionalTravelInsuranceList.forEach((element) {
      addList.add(AdditionalModel(
          additionalCoverID: int.parse(element?.additionalCoverID ?? '0')));
    });
    // for (int i = 0;
    //     i < customerProvider.selectedAdditionalInsuranceList.length;
    //     i++) {
    //   if (i == 0) {
    //     additionalCoverID =
    //         '{"additionalCoverID": ${customerProvider?.selectedAdditionalInsuranceList[i]?.additionalCoverID}}';
    //   } else {
    //     additionalCoverID = additionalCoverID +
    //         ',' +
    //         '{"additionalCoverID": ${customerProvider?.selectedAdditionalInsuranceList[i]?.additionalCoverID}}';
    //   }
    // }
    var uri =
        Uri.parse('${AppEnvironment.serverUrl}PostInsurerInsuranceDetails');
    var client = http.Client();
    try {
      var response;
      if (addList.isEmpty) {
        response = await client
            .post(
              uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InsurerMotorAdditionalDetails": [],
                "InsurerTravelDetails": {
                  "travelFromDate": customerProvider
                              .insuranceTravelDetails?.travelFromDate !=
                          ''
                      ? customerProvider.insuranceTravelDetails?.travelFromDate
                      : "01-01-1990",
                  "totalTravelDays": customerProvider
                              .insuranceTravelDetails?.totalTravelDays !=
                          ''
                      ? customerProvider.insuranceTravelDetails?.totalTravelDays
                      : '1',
                  "travelToDate": customerProvider
                              .insuranceTravelDetails?.travelToDate !=
                          ''
                      ? customerProvider.insuranceTravelDetails?.travelToDate
                      : "01-01-1990"
                },
                "InsurerDocumentDetails": documentsProvider.docsList,
                "InsurerPersonalDetails": [
                  {
                    "insurerName":
                        customerProvider.insuranceDetails[0]?.insurerName ?? '',
                    "dob": //"01-01-1990",
                        customerProvider.insuranceDetails[0]?.dob != ''
                            ? customerProvider.insuranceDetails[0]?.dob ?? ''
                            : "01-01-1990",
                    "passportNo":
                        customerProvider.insuranceDetails[0]?.passportNo != ''
                            ? customerProvider.insuranceDetails[0]?.passportNo
                            : '000',
                    "civilIdNo":
                        customerProvider.insuranceDetails[0]?.civilIdNo != ''
                            ? customerProvider.insuranceDetails[0]?.civilIdNo
                            : '000',
                    "relation":
                        customerProvider.insuranceDetails[0]?.relation ?? '',
                  }
                ],
                "InsurerPlans": {
                  "customerId": profileProvider.profileData?.customerId,
                  "insuranceId": 2,
                  "planId":
                      customerProvider.selectedTravelInsuranceList[0]?.planId,
                  "transactionDate":
                      //    "12-10-2022 10:10:10", //
                      timeFormat.format(DateTime.now()).toString(),
                  // "transactionId": 1,"tranType": "Edit",
                  //  "tranType": "Save"
                }
              }),
            )
            .timeout(const Duration(seconds: 30));
      } else {
        response = await client
            .post(
              uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InsurerMotorAdditionalDetails": addList,
                "InsurerTravelDetails": {
                  "travelFromDate": customerProvider
                              .insuranceTravelDetails?.travelFromDate !=
                          ''
                      ? customerProvider.insuranceTravelDetails?.travelFromDate
                      : "01-01-1990",
                  "totalTravelDays": customerProvider
                              .insuranceTravelDetails?.totalTravelDays !=
                          ''
                      ? customerProvider.insuranceTravelDetails?.totalTravelDays
                      : '1',
                  "travelToDate": customerProvider
                              .insuranceTravelDetails?.travelToDate !=
                          ''
                      ? customerProvider.insuranceTravelDetails?.travelToDate
                      : "01-01-1990"
                },
                "InsurerDocumentDetails": documentsProvider.docsList,
                "InsurerPersonalDetails": [
                  {
                    "insurerName":
                        customerProvider.insuranceDetails[0]?.insurerName ?? '',
                    "dob": //"01-01-1990",
                        customerProvider.insuranceDetails[0]?.dob != ''
                            ? customerProvider.insuranceDetails[0]?.dob ?? ''
                            : "01-01-1990",
                    "passportNo":
                        customerProvider.insuranceDetails[0]?.passportNo != ''
                            ? customerProvider.insuranceDetails[0]?.passportNo
                            : '000',
                    "civilIdNo":
                        customerProvider.insuranceDetails[0]?.civilIdNo != ''
                            ? customerProvider.insuranceDetails[0]?.civilIdNo
                            : '000',
                    "relation":
                        customerProvider.insuranceDetails[0]?.relation ?? '',
                  }
                ],
                "InsurerPlans": {
                  "customerId": profileProvider.profileData?.customerId,
                  "insuranceId": 2,
                  "planId":
                      customerProvider.selectedTravelInsuranceList[0]?.planId,
                  "transactionDate":
                      //    "12-10-2022 10:10:10", //
                      timeFormat.format(DateTime.now()).toString(),
                  // "transactionId": 1,"tranType": "Edit",
                  //  "tranType": "Save"
                }
              }),
            )
            .timeout(const Duration(seconds: 30));
      }
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        Map statusDetails = jsonDecode(response.body);
        TransactionModel statusModel = TransactionModel();
        if (statusDetails.isNotEmpty) {
          statusModel = TransactionModel.fromJson(statusDetails);
        }
        return statusModel;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }
}
