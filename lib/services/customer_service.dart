import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:gt_test/models/insurance_details_model.dart';
import 'package:gt_test/providers/documents_provider.dart';
import 'package:gt_test/providers/profile_provider.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../common/environment.dart';
import '../common/logger.dart';
import '../common/navigator/navigations.dart';
import '../common/routes/routes.dart';
import '../common/shared_prefs.dart';
import 'package:gt_test/models/customer_model.dart';
import '../models/additional_model.dart';
import '../models/claim_docs_model.dart';
import '../models/ocr_model.dart';
import '../models/register_model.dart';
import '../models/status_model.dart';
import 'package:gt_test/models/customer_claims_model.dart';
import 'package:gt_test/models/customer_insurance_model.dart';

import '../models/transaction_model.dart';
import '../providers/customer_provider.dart';

class CustomerService {
  List<CustomerInsuranceModel> insuranceList = [];
  List<InsuranceDetailsModel> insuranceDetailsList = [];
  List<CustomerClaimsModel> claimsList = [];
  List<ClaimDocsModel> claimDocs = [];
  CustomerProvider customerProvider = CustomerProvider();
  ProfileProvider profileProvider = ProfileProvider();
  DocumentsProvider documentsProvider = DocumentsProvider();

  final dateFormat = DateFormat("dd-MM-yyyy");
  final timeFormat = DateFormat("dd-MM-yyyy HH:mm:ss");

  Future<List<CustomerInsuranceModel?>> getInsurerInsurancePlans({
    required BuildContext context,
    required String? customerId,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}GetInsurerInsurancePlans');
    var client = http.Client();
    try {
      var response = await client
          .post(
            uri,
            headers: {
              "content-type": "application/json",
              "accept": "application/json",
            },
            body: json.encode({
              "InputBody": {
                "RootNode": {"customerId": customerId}
              }
            }),
          )
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        List brandDetails = jsonDecode(response.body);
        if (brandDetails.isNotEmpty) {
          for (var data in brandDetails) {
            insuranceList.add(
              CustomerInsuranceModel.fromJson(data),
            );
          }
        }
        return insuranceList;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return [];
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return [];
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return [];
    } on SocketException {
      print("You are not connected to internet");
      return [];
    } catch (e) {
      Logger.d(e.toString(), '');
      return [];
    } finally {
      client.close();
    }
  }

  Future<InsuranceDetailsModel?> getInsurerPlanDetails({
    required BuildContext context,
    required String? transactionId,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}GetInsurerPlanDetailsv2');
    var client = http.Client();
    try {
      var response = await client
          .post(
            uri,
            headers: {
              "content-type": "application/json",
              "accept": "application/json",
            },
            body:
                json.encode({"insuranceId": 1, "transactionId": transactionId}),
          )
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        var insuranceData;
        Map insuranceDetails = jsonDecode(response.body);
        if (insuranceDetails.isNotEmpty) {
          insuranceData = InsuranceDetailsModel.fromJson(insuranceDetails);
        }
        return insuranceData;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), 'Error');
      return null;
    } finally {
      client.close();
    }
  }

  Future<List<CustomerClaimsModel?>> getInsurerClaimDetails({
    required BuildContext context,
    required String? customerId,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}GetInsurerClaimDetails');
    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InputBody": {
                  "RootNode": {"customerId": int.parse(customerId!)}
                }
              }))
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        List claimDetails = jsonDecode(response.body);
        if (claimDetails.isNotEmpty) {
          for (var data in claimDetails) {
            claimsList.add(
              CustomerClaimsModel.fromJson(data),
            );
          }
        }
        return claimsList;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return [];
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return [];
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return [];
    } on SocketException {
      print("You are not connected to internet");
      return [];
    } catch (e) {
      Logger.d(e.toString(), '');
      return [];
    } finally {
      client.close();
    }
  }

  Future<List<ClaimDocsModel?>> getClaimDocument({
    required BuildContext context,
    required String? claimId,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}GetClaimDocument');
    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InputBody": {
                  "RootNode": {"ClaimId": claimId}
                },
              }))
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        List claimDetails = jsonDecode(response.body);
        if (claimDetails.isNotEmpty) {
          for (var data in claimDetails) {
            claimDocs.add(
              ClaimDocsModel.fromJson(data),
            );
          }
        }
        return claimDocs;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return [];
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return [];
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return [];
    } on SocketException {
      print("You are not connected to internet");
      return [];
    } catch (e) {
      Logger.d(e.toString(), 'Error');
      return [];
    } finally {
      client.close();
    }
  }

  Future<StatusModel?> updateClaim({
    required BuildContext context,
    required int insuranceId,
    required int? planId,
    required String claimDate,
    required String claimReason,
    required String lossDate,
    required String customerId,
    required int? claimId,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}UpdateClaim');
    var client = http.Client();
    try {
      var response = await client
          .post(
            uri,
            headers: {
              "content-type": "application/json",
              "accept": "application/json",
            },
            body: json.encode({
              "insuranceId": insuranceId,
              "planID": planId,
              "claimDate": claimDate,
              "claimReason": claimReason,
              "lossDate": lossDate,
              "customerId": customerId,
              "claimId": claimId
            }),
          )
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        Map claimDetails = jsonDecode(response.body);
        var claimModel;
        if (claimDetails.isNotEmpty) {
          claimModel = StatusModel.fromJson(claimDetails);
        }
        return claimModel;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<int?> registerClaims({
    required BuildContext context,
    required int insuranceId,
    required int? planId,
    required String claimDate,
    required String claimReason,
    required String lossDate,
    required String customerId,
    required List<ClaimDocsModel> claimDocs,
  }) async {
    // String additionalCoverID = '';
    // for (int i = 0;
    // i < claimDocs.length;
    // i++) {
    //   if (i == 0) {
    //     additionalCoverID =
    //     '{\"additionalCoverID\": ${claimDocs[i]?.DocumentName}}';
    //   } else {
    //     additionalCoverID = additionalCoverID +
    //         ',' +
    //         '{\"additionalCoverID\": ${customerProvider?.selectedAdditionalInsuranceList[i]?.additionalCoverID}}';
    //   }
    // }
    var uri = Uri.parse('${AppEnvironment.serverUrl}PostClaims');
    var client = http.Client();
    try {
      var response = await client
          .post(
            uri,
            headers: {
              "content-type": "application/json",
              "accept": "application/json",
            },
            body: json.encode({
              "insuranceId": insuranceId,
              "planId": planId,
              "claimDate": claimDate,
              "claimReason": claimReason,
              "lossDate": lossDate,
              "customerId": customerId,
              "DocumentDetails": claimDocs
            }),
          )
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        return response.statusCode;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<RegisterModel?> validateAndRegisterCustomer({
    required BuildContext context,
    required String emailId,
    required String mobileNo,
    required String customerName,
  }) async {
    var uri =
        Uri.parse('${AppEnvironment.serverUrl}ValidateAndRegisterCustomer');
    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InputBody": {
                  "RootNode": {
                    'mobileNo': mobileNo,
                    'emailId': emailId,
                    'customerName': customerName
                  }
                }
              }))
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        Map regDetails = jsonDecode(response.body);
        RegisterModel regModel = RegisterModel();
        if (regDetails.isNotEmpty) {
          regModel = RegisterModel.fromJson(regDetails);
        }
        return regModel;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<CustomerModel?> validateCustomer({
    required BuildContext context,
    required String mobileNo,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}validateCustomer');
    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InputBody": {
                  "RootNode": {"mobileNo": mobileNo}
                }
              }))
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        Map brandDetails = jsonDecode(response.body);
        CustomerModel customerModel = CustomerModel();
        if (brandDetails.isNotEmpty) {
          customerModel = CustomerModel.fromJson(brandDetails);
        }
        return customerModel;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<TransactionModel?> postInsurerInsuranceDetails({
    required BuildContext context,
  }) async {
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    documentsProvider = Provider.of<DocumentsProvider>(context, listen: false);
    String additionalCoverID = '';
    List<AdditionalModel> addList = [];
    customerProvider?.selectedAdditionalMotorInsuranceList.forEach((element) {
      addList.add(
          AdditionalModel(additionalCoverID: element?.additionalCoverID ?? 0));
    });
    // for (int i = 0;
    //     i < customerProvider.selectedAdditionalInsuranceList.length;
    //     i++) {
    //   if (i == 0) {
    //     additionalCoverID =
    //         '{"additionalCoverID": ${customerProvider?.selectedAdditionalInsuranceList[i]?.additionalCoverID}}';
    //   } else {
    //     additionalCoverID = additionalCoverID +
    //         ',' +
    //         '{"additionalCoverID": ${customerProvider?.selectedAdditionalInsuranceList[i]?.additionalCoverID}}';
    //   }
    // }
    var uri =
        Uri.parse('${AppEnvironment.serverUrl}PostInsurerInsuranceDetails');
    var client = http.Client();
    try {
      var response;
      if (addList.isEmpty) {
        response = await client
            .post(
              uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InsurerMotorAdditionalDetails": [],
                "InsurerMotorDetails": {
                  "licenseNo":
                      customerProvider.insuranceMotorDetails?.licenseNo ??
                          '000',
                  "licenseExpiry":
                      customerProvider.insuranceMotorDetails?.licenseExpiry !=
                              ''
                          ? customerProvider
                                  ?.insuranceMotorDetails?.licenseExpiry ??
                              ''
                          : "01-01-1990",
                  "vinNo":
                      customerProvider.insuranceMotorDetails?.vinNo ?? '000',
                  "plateNo":
                      customerProvider.insuranceMotorDetails?.plateNo ?? '000',
                  "makeModel":
                      customerProvider.insuranceMotorDetails?.makeModel != ''
                          ? customerProvider.insuranceMotorDetails?.makeModel
                          : '0',
                  "carValue":
                      customerProvider.insuranceMotorDetails?.carValue != ''
                          ? customerProvider.insuranceMotorDetails?.carValue
                          : '0',
                  "noOfSeats":
                      customerProvider.insuranceMotorDetails?.noOfSeats != ''
                          ? customerProvider.insuranceMotorDetails?.noOfSeats
                          : '1',
                  "makeYear":
                      customerProvider.insuranceMotorDetails?.makeYear != ''
                          ? customerProvider.insuranceMotorDetails?.makeYear
                          : '0'
                },
                "InsurerDocumentDetails": documentsProvider.docsList,
                "InsurerPersonalDetails": [
                  {
                    "insurerName":
                        customerProvider.insuranceDetails[0]?.insurerName ?? '',
                    "dob": //"01-01-1990",
                        customerProvider.insuranceDetails[0]?.dob != ''
                            ? customerProvider.insuranceDetails[0]?.dob ?? ''
                            : "01-01-1990",
                    "emailId":
                        customerProvider.insuranceDetails[0]?.emailId ?? '',
                    "passportNo":
                        customerProvider.insuranceDetails[0]?.passportNo != ''
                            ? customerProvider.insuranceDetails[0]?.passportNo
                            : '000',
                    "passportExpiry": //"01-06-2000",
                        customerProvider.insuranceDetails[0]?.passportExpDate !=
                                ''
                            ? customerProvider
                                    .insuranceDetails[0]?.passportExpDate ??
                                '01-01-1990'
                            : "01-01-1990",
                    "civilIdNo":
                        customerProvider.insuranceDetails[0]?.civilIdNo != ''
                            ? customerProvider.insuranceDetails[0]?.civilIdNo
                            : '000',
                    "civilIdExpiry":
                        customerProvider.insuranceDetails[0]?.civilIDExpDate !=
                                ''
                            ? customerProvider
                                    .insuranceDetails[0]?.civilIDExpDate ??
                                '01-01-1990'
                            : "01-01-1990",
                    "nationality":
                        customerProvider.insuranceDetails[0]?.nationality ?? '',
                    "gender":
                        customerProvider.insuranceDetails[0]?.gender == 'Female'
                            ? 'F'
                            : 'M' ?? '',
                    "unitType":
                        customerProvider.insuranceDetails[0]?.unitType ?? '',
                    "area": customerProvider.insuranceDetails[0]?.area ?? '',
                    "blockNo":
                        customerProvider.insuranceDetails[0]?.blockNo ?? '',
                    "street":
                        customerProvider.insuranceDetails[0]?.street ?? '',
                    "bnoOrHno":
                        customerProvider.insuranceDetails[0]?.bNoOrHouseNo ??
                            '',
                    "floorNo":
                        customerProvider.insuranceDetails[0]?.floorNo ?? ''
                  }
                ],
                "InsurerPlans": {
                  "customerId": profileProvider.profileData?.customerId,
                  "insuranceId": 1,
                  "planId":
                      customerProvider.selectedMotorInsuranceList[0]?.planid,
                  "transactionDate":
                      //    "12-10-2022 10:10:10", //
                      timeFormat.format(DateTime.now()).toString(),
                  // "transactionId": 1,"tranType": "Edit",
                  //  "tranType": "Save"
                }
              }),
            )
            .timeout(const Duration(seconds: 30));
      } else {
        response = await client
            .post(
              uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InsurerMotorAdditionalDetails": addList,
                "InsurerMotorDetails": {
                  "licenseNo":
                      customerProvider.insuranceMotorDetails?.licenseNo ??
                          '000',
                  "licenseExpiry":
                      customerProvider.insuranceMotorDetails?.licenseExpiry !=
                              ''
                          ? customerProvider
                                  ?.insuranceMotorDetails?.licenseExpiry ??
                              ''
                          : "01-01-1990",
                  "vinNo":
                      customerProvider.insuranceMotorDetails?.vinNo ?? '000',
                  "plateNo":
                      customerProvider.insuranceMotorDetails?.plateNo ?? '000',
                  "makeModel":
                      customerProvider.insuranceMotorDetails?.makeModel != ''
                          ? customerProvider.insuranceMotorDetails?.makeModel
                          : '0',
                  "carValue":
                      customerProvider.insuranceMotorDetails?.carValue != ''
                          ? customerProvider.insuranceMotorDetails?.carValue
                          : '0',
                  "noOfSeats":
                      customerProvider.insuranceMotorDetails?.noOfSeats != ''
                          ? customerProvider.insuranceMotorDetails?.noOfSeats
                          : '1',
                  "makeYear":
                      customerProvider.insuranceMotorDetails?.makeYear != ''
                          ? customerProvider.insuranceMotorDetails?.makeYear
                          : '0'
                },
                "InsurerDocumentDetails": documentsProvider.docsList,
                "InsurerPersonalDetails": [
                  {
                    "insurerName":
                        customerProvider.insuranceDetails[0]?.insurerName ?? '',
                    "dob": //"01-01-1990",
                        customerProvider.insuranceDetails[0]?.dob != ''
                            ? customerProvider.insuranceDetails[0]?.dob ?? ''
                            : "01-01-1990",
                    "emailId":
                        customerProvider.insuranceDetails[0]?.emailId ?? '',
                    "passportNo":
                        customerProvider.insuranceDetails[0]?.passportNo != ''
                            ? customerProvider.insuranceDetails[0]?.passportNo
                            : '000',
                    "passportExpiry": //"01-06-2000",
                        customerProvider.insuranceDetails[0]?.passportExpDate !=
                                ''
                            ? customerProvider
                                .insuranceDetails[0]?.passportExpDate
                            : "01-01-1990",
                    "civilIdNo":
                        customerProvider.insuranceDetails[0]?.civilIdNo != ''
                            ? customerProvider.insuranceDetails[0]?.civilIdNo
                            : '000',
                    "civilIdExpiry": customerProvider
                                .insuranceDetails[0]?.civilIDExpDate !=
                            ''
                        ? customerProvider.insuranceDetails[0]?.civilIDExpDate
                        : "01-01-1990",
                    "nationality":
                        customerProvider.insuranceDetails[0]?.nationality ?? '',
                    "gender":
                        customerProvider.insuranceDetails[0]?.gender == 'Female'
                            ? 'F'
                            : 'M' ?? '',
                    "unitType":
                        customerProvider.insuranceDetails[0]?.unitType ?? '',
                    "area": customerProvider.insuranceDetails[0]?.area ?? '',
                    "blockNo":
                        customerProvider.insuranceDetails[0]?.blockNo ?? '',
                    "street":
                        customerProvider.insuranceDetails[0]?.street ?? '',
                    "bnoOrHno":
                        customerProvider.insuranceDetails[0]?.bNoOrHouseNo ??
                            '',
                    "floorNo":
                        customerProvider.insuranceDetails[0]?.floorNo ?? ''
                  }
                ],
                "InsurerPlans": {
                  "customerId": profileProvider.profileData?.customerId,
                  "insuranceId": 1,
                  "planId":
                      customerProvider.selectedMotorInsuranceList[0]?.planid,
                  "transactionDate":
                      //    "12-10-2022 10:10:10",
                      timeFormat.format(DateTime.now()).toString(),
                  // "transactionId": 1,"tranType": "Edit",
                  //  "tranType": "Save"
                }
              }),
            )
            .timeout(const Duration(seconds: 30));
      }
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        Map statusDetails = jsonDecode(response.body);
        TransactionModel statusModel = TransactionModel();
        if (statusDetails.isNotEmpty) {
          statusModel = TransactionModel.fromJson(statusDetails);
        }
        return statusModel;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<DlOcrModel?> dlOcr({
    required BuildContext context,
    required String? url,
  }) async {
    var uri = Uri.parse(
        "http://gtocr.fusionezee.tech:5002/fz-api/parse-driving-license");
    var client = http.Client();
    try {
      var response = await client
          .post(
            uri,
            headers: {
              "content-type": "application/json",
              "accept": "application/json",
            },
            body: json.encode({
              "InputBody": {
                "request": {"url": url}
              }
            }),
          )
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        Map docDetails = jsonDecode(response.body);
        var data;
        if (docDetails.isNotEmpty) {
          data = DlOcrModel.fromJson(docDetails);
          return data;
        }
        return null;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<CrOcrModel?> crOcr({
    required BuildContext context,
    required String? url,
  }) async {
    var uri = Uri.parse(
        "http://gtocr.fusionezee.tech:5002/fz-api/parse-vehicle-details");

    var client = http.Client();
    try {
      var response = await client
          .post(
            uri,
            headers: {
              "content-type": "application/json",
              "accept": "application/json",
            },
            body: json.encode({
              "InputBody": {
                "Request": {"url_front": url, "url_back": ""}
              }
            }),
          )
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        Map docDetails = jsonDecode(response.body);
        var data;
        if (docDetails.isNotEmpty) {
          data = CrOcrModel.fromJson(docDetails);
          return data;
        }
        return null;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<CidOcrModel?> cidOcr({
    required BuildContext context,
    required String? url,
  }) async {
    var uri =
        Uri.parse("http://gtocr.fusionezee.tech:5002/fz-api/parse-civil-id");
    var client = http.Client();
    try {
      var response = await client
          .post(
            uri,
            headers: {
              "content-type": "application/json",
              "accept": "application/json",
            },
            body: json.encode({
              "InputBody": {
                "Request": {"url_front": url, "url_back": ""}
              }
            }),
          )
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        Map docDetails = jsonDecode(response.body);
        var data;
        if (docDetails.isNotEmpty) {
          data = CidOcrModel.fromJson(docDetails);
          return data;
        }
        return null;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }
}
