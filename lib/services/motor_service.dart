import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../common/environment.dart';
import '../common/logger.dart';
import '../common/navigator/navigations.dart';
import '../common/routes/routes.dart';
import '../common/shared_prefs.dart';
import '../models/motor_insurance_model.dart';
import 'package:gt_test/models/additional_motor_insurance_model.dart';
import 'package:gt_test/models/make_model.dart';
import 'package:gt_test/models/discount_plan_model.dart';

class MotorService {
  List<MakeModel> brandList = [];
  List<DiscountPlanModel> discountPlanList = [];
  List<MotorInsuranceModel> motorInsuranceList = [];
  List<AdditionalMotorInsuranceModel> additionalMotorInsuranceList = [];

  Future<List<MakeModel?>> getMakeModel({
    required BuildContext context,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}GetMakeModel');
    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({}))
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        List brandDetails = jsonDecode(response.body);
        if (brandDetails.isNotEmpty) {
          for (var data in brandDetails) {
            brandList.add(
              MakeModel.fromJson(data),
            );
          }
        }
        return brandList;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return [];
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return [];
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return [];
    } on SocketException {
      print("You are not connected to internet");
      return [];
    } catch (e) {
      Logger.d(e.toString(), '');
      return [];
    } finally {
      client.close();
    }
  }

  Future<List<DiscountPlanModel?>> getDiscountPlans({
    required BuildContext context,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}getDiscountPlans');
    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({}))
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        List planDetails = jsonDecode(response.body);
        if (planDetails.isNotEmpty) {
          for (var data in planDetails) {
            discountPlanList.add(
              DiscountPlanModel.fromJson(data),
            );
          }
        }
        return discountPlanList;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return [];
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return [];
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return [];
    } on SocketException {
      print("You are not connected to internet");
      return [];
    } catch (e) {
      Logger.d(e.toString(), '');
      return [];
    } finally {
      client.close();
    }
  }

  Future<List<MotorInsuranceModel?>> getMotorInsurancePlans(
      {required BuildContext context,
      String planType = "TPL",
      String makeModel = " ",
      int value = 0,
      int makeYear = 2022,
      int noOfSeats = 5}) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}getMotorInsurancePlans');
    var client = http.Client();
    try {
      var response;
      if (planType == "TPL") {
        response = await client
            .post(uri,
                headers: {
                  "content-type": "application/json",
                  "accept": "application/json",
                },
                body: json.encode({
                  "InputBody": {
                    "RootNode": {
                      "planType": planType,
                      "makeYear": makeYear,
                      "noOfSeats": noOfSeats
                    }
                  }
                }))
            .timeout(const Duration(seconds: 30));
      } else {
        response = await client
            .post(uri,
                headers: {
                  "content-type": "application/json",
                  "accept": "application/json",
                },
                body: json.encode({
                  "InputBody": {
                    "RootNode": {
                      "planType": planType,
                      "makeModel": makeModel,
                      "value": value,
                      "makeYear": makeYear,
                    }
                  }
                }))
            .timeout(const Duration(seconds: 30));
      }
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        List insuranceDetails = jsonDecode(response.body);
        if (insuranceDetails.isNotEmpty) {
          for (var data in insuranceDetails) {
            motorInsuranceList.add(
              MotorInsuranceModel.fromJson(data),
            );
          }
        }
        return motorInsuranceList;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return [];
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return [];
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return [];
    } on SocketException {
      print("You are not connected to internet");
      return [];
    } catch (e) {
      Logger.d(e.toString(), '');
      return [];
    } finally {
      client.close();
    }
  }

  Future<List<AdditionalMotorInsuranceModel?>> getAdditionalMotorPlans({
    required String productType,
    required BuildContext context,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}getAdditionalMotorPlans');
    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InputBody": {
                  "RootNode": {"productType": productType}
                }
              }))
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        List planDetails = jsonDecode(response.body);
        if (planDetails.isNotEmpty) {
          for (var data in planDetails) {
            additionalMotorInsuranceList.add(
              AdditionalMotorInsuranceModel.fromJson(data),
            );
          }
        }
        return additionalMotorInsuranceList;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return [];
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return [];
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return [];
    } on SocketException {
      print("You are not connected to internet");
      return [];
    } catch (e) {
      Logger.d(e.toString(), '');
      return [];
    } finally {
      client.close();
    }
  }
}
