import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:gt_test/models/status_model.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import '../common/environment.dart';
import '../common/logger.dart';
import '../common/navigator/navigations.dart';
import '../common/routes/routes.dart';
import '../common/shared_prefs.dart';
import '../common/utils.dart';
import '../models/otp_model.dart';

class OTPService {
  Future<OTPModel?> requestOTP({
    required String mobileNo,
    required BuildContext context,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}SendSMSGTInsurance');

    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({"PhoneNo": "965$mobileNo"}))
          .timeout(const Duration(seconds: 30));
      var otpRefId;
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        Map otpData = jsonDecode(response.body);
        if (otpData.isNotEmpty) {
          otpRefId = OTPModel.fromJson(otpData);
          return otpRefId;
        }
        return null;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<StatusModel?> validateOTP({
    required String otp,
    required String otpRefId,
    required BuildContext context,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}ValidateLogin');

    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InputBody": {
                  "RootNode": {"messageReferenceID": otpRefId, "otp": otp}
                }
              }))
          .timeout(const Duration(seconds: 30));
      var userData;
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response. statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        Map otpData = jsonDecode(response.body);
        if (otpData.isNotEmpty) {
          userData = StatusModel.fromJson(otpData);
          return userData;
        }
        return null;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }
}
