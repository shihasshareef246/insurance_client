import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../common/environment.dart';
import '../common/logger.dart';
import '../common/navigator/navigations.dart';
import '../common/routes/routes.dart';
import '../common/shared_prefs.dart';
import '../models/profile_model.dart';
import '../models/register_model.dart';
import '../models/status_model.dart';

class ProfileService {
  Future<ProfileModel?> postCustomerDetails({
    required BuildContext context,
    String customerName = '',
    String dob = '',
    String emailId = '',
    String civilIdNo = '',
    String phoneNo = '',
    String mobileNo = '',
    String? customerId = '',
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}Postcustomerdetails');
    var client = http.Client();
    try {
      var response;
      if (customerId != '') {
        if (dob != '') {
          response = await client
              .post(uri,
                  headers: {
                    "content-type": "application/json",
                    "accept": "application/json",
                  },
                  body: json.encode({
                    "tranType": "Edit",
                    "customerName": customerName,
                    "dob": "01-06-2000", // dob,
                    "emailId": emailId,
                    "civilIdNo": civilIdNo != '' ? civilIdNo : '0',
                    "mobileNo": mobileNo,
                    "phoneNo": phoneNo != '' ? phoneNo : '0',
                    "customerId": customerId
                  }))
              .timeout(const Duration(seconds: 30));
        } else {
          response = await client
              .post(uri,
                  headers: {
                    "content-type": "application/json",
                    "accept": "application/json",
                  },
                  body: json.encode({
                    "tranType": "Edit",
                    "customerName": customerName,
                    "emailId": emailId,
                    "civilIdNo": civilIdNo != '' ? civilIdNo : '0',
                    "mobileNo": mobileNo,
                    "phoneNo": phoneNo != '' ? phoneNo : '0',
                    "customerId": customerId
                  }))
              .timeout(const Duration(seconds: 30));
        }
      } else {
        response = await client
            .post(uri,
                headers: {
                  "content-type": "application/json",
                  "accept": "application/json",
                },
                body: json.encode({
                  "tranType": "Save",
                  "customerName": customerName,
                  "emailId": emailId,
                  "mobileNo": mobileNo,
                }))
            .timeout(const Duration(seconds: 30));
      }
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        var profileData;
        Map profileDetails = jsonDecode(response.body);
        profileData = ProfileModel.fromJson(profileDetails);
        return profileData;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<ProfileModel?> getCustomerDetails({
    required BuildContext context,
    String customerId = '',
    String mobileNo = '',
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}GetCustomerDetails');
    var client = http.Client();
    try {
      var response;
      if (mobileNo != '') {
        response = await client
            .post(uri,
                headers: {
                  "content-type": "application/json",
                  "accept": "application/json",
                },
                body: json.encode({
                  "InputBody": {
                    "RootNode": {"mobileNo": mobileNo}
                  }
                }))
            .timeout(const Duration(seconds: 30));
      } else {
        response = await client
            .post(uri,
                headers: {
                  "content-type": "application/json",
                  "accept": "application/json",
                },
                body: json.encode({
                  "InputBody": {
                    "RootNode": {"customerId": customerId}
                  }
                }))
            .timeout(const Duration(seconds: 30));
      }
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        var profileData;
        Map profileDetails = jsonDecode(response.body);
        profileData = ProfileModel.fromJson(profileDetails);
        return profileData;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<StatusModel?> postCustomerPhoto({
    required BuildContext context,
    required String photoUrl,
    required String customerId,
  }) async {
    var uri = Uri.parse('${AppEnvironment.serverUrl}PostCustomerPhoto');
    var client = http.Client();
    try {
      var response = await client
          .post(uri,
              headers: {
                "content-type": "application/json",
                "accept": "application/json",
              },
              body: json.encode({
                "InputBody": {
                  "RootNode": {"PhotoUrl": photoUrl, "CustomerId": customerId}
                }
              }))
          .timeout(const Duration(seconds: 30));
      Logger.d('Statuscode ', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.body.toString()}');
        var profileData;
        Map profileDetails = jsonDecode(response.body);
        profileData = StatusModel.fromJson(profileDetails);
        return profileData;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }
}
