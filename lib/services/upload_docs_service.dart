import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import '../auth/auth_data.dart';
import '../common/environment.dart';
import '../common/logger.dart';
import '../common/navigator/navigations.dart';
import '../common/routes/routes.dart';
import '../common/shared_prefs.dart';
import '../models/profile_pic_model.dart';
import '../models/upload_docs_model.dart';


class UploadDocsService {
  Future<UploadDocsModel?> uploadDocs({
    required String docType,
    required String docPath,
    required String debitorId,
    required  BuildContext context,
  }) async {
    var uri = Uri.parse(
        '${AppEnvironment.serverUrl}v0/khata/transaction/app/upload-documents');
    var client = http.Client();
    try {
      var userDetails = await AuthData().getUserDetails();
      var request = http.MultipartRequest('POST', uri);
      request.files.add(await http.MultipartFile.fromPath('file', docPath));
      request.headers.addAll({
        "content-type": "application/json",
        "accept": "application/json",
        "token": userDetails.authToken,
      });
      request.fields.addAll({
        'creditorId': userDetails.agrifiId,
        'debitorId': debitorId,
        'prefix': docType,
      });
      var streamedResponse =
          await request.send().timeout(const Duration(seconds: 30));
      var response = await http.Response.fromStream(streamedResponse);
      Logger.d('Statuscode', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.toString()}');
        UploadDocsModel docs = UploadDocsModel(docs: []);
        var docsList =
            Map<String, dynamic>.from(jsonDecode(response.body.toString()));
        if (docsList.isNotEmpty) {
          docs = UploadDocsModel.fromJson(docsList);
          return docs;
        }
        return docs;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        // Utils.showErrorPopup(
        //     context: context,
        //     message: 'Something went wrong. Please try again');
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }

  Future<ProfilePicModel?> uploadProfilePic({
    required String docPath,
    required int flag,
    required String userId,
    required BuildContext context,
  }) async {
    var uri = Uri.parse(
        '${AppEnvironment.serverUrl}v0/khata/transaction/app/update-profile-picture');
    var client = http.Client();
    try {
      var userDetails = await AuthData().getUserDetails();
      var request = http.MultipartRequest('POST', uri);
      request.files.add(await http.MultipartFile.fromPath('file', docPath));
      request.headers.addAll({
        "content-type": "application/json",
        "accept": "application/json",
        "token": userDetails.authToken,
      });
      request.fields.addAll({
        'userId': userId,
        'flag': flag.toString(),
      });
      var streamedResponse =
          await request.send().timeout(const Duration(seconds: 30));
      var response = await http.Response.fromStream(streamedResponse);
      Logger.d('Statuscode', '${response.statusCode}');
      if (response.statusCode == 200) {
        Logger.d('Response', '${response.toString()}');
        ProfilePicModel docs = ProfilePicModel(message: '', documentLink: '');
        var docsList =
            Map<String, dynamic>.from(jsonDecode(response.body.toString()));
        if (docsList.isNotEmpty) {
          docs = ProfilePicModel.fromJson(docsList);
          return docs;
        }
        return docs;
      } else if (response.statusCode == 403) {
        Logger.d('Failure Response', '${response.body.toString()}');
        Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen);
        await SharedPrefs().removePrefs(
          'userdetails',
        );
        return null;
      } else {
        Logger.d('Failure Response', '${response.body.toString()}');
        return null;
      }
    } on TimeoutException {
      print("Connection timed out. Please try again");
      return null;
    } on SocketException {
      print("You are not connected to internet");
      return null;
    } catch (e) {
      Logger.d(e.toString(), '');
      return null;
    } finally {
      client.close();
    }
  }
}
