class StatusModel {
  String status;

  StatusModel({
    this.status = '',
  });

  factory StatusModel.fromJson(Map<dynamic, dynamic> json) =>
      StatusModel(
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
      };
}
