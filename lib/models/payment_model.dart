class PaymentModel {
  String PayUrl;
  String PaymentGateway;
  String ErrorMessage;
  String Do_WltDtl;

  PaymentModel({
    this.PayUrl = '',
    this.PaymentGateway = '',
    this.ErrorMessage = '',
    this.Do_WltDtl = '',
  });

  factory PaymentModel.fromJson(Map<dynamic, dynamic> json) => PaymentModel(
        PayUrl: json["PayUrl"] ?? '',
        PaymentGateway: json["PaymentGateway"] ?? '',
        ErrorMessage: json["ErrorMessage"] ?? '',
        Do_WltDtl: json["Do_WltDtl"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "PayUrl": PayUrl,
        "PaymentGateway": PaymentGateway,
        "ErrorMessage": ErrorMessage,
        "Do_WltDtl": Do_WltDtl,
      };
}
