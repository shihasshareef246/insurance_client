class DlOcrModel {
  String Status;
  String Name;
  String IssuanceDate;
  String ExpiryDate;
  String Dob;
  String Designation;
  String LicenseNo;
  String BloodGroup;
  String Gender;
  String LicenseType;
  String Nationality;

  DlOcrModel({
    this.Status = '',
    this.Name = '',
    this.IssuanceDate = '',
    this.ExpiryDate = '',
    this.Dob = '',
    this.Designation = '',
    this.LicenseNo = '',
    this.BloodGroup = '',
    this.Gender = '',
    this.LicenseType = '',
    this.Nationality = '',
  });

  factory DlOcrModel.fromJson(Map<dynamic, dynamic> json) => DlOcrModel(
        Status: json["Status"] ?? '',
        Name: json["Name"] ?? '',
        IssuanceDate: json["IssuanceDate"] ?? '',
        ExpiryDate: json["ExpiryDate"] ?? '',
        Dob: json["Dob"] ?? '',
        Designation: json["Designation"] ?? '',
        LicenseNo: json["LicenseNo"] ?? '',
        BloodGroup: json["BloodGroup"] ?? '',
        Gender: json["Gender"] ?? '',
        LicenseType: json["LicenseType"] ?? '',
        Nationality: json["Nationality"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "Status": Status,
        "Name": Name,
        "IssuanceDate": IssuanceDate,
        "ExpiryDate": ExpiryDate,
        "Dob": Dob,
        "Designation": Designation,
        "LicenseNo": LicenseNo,
        "BloodGroup": BloodGroup,
        "Gender": Gender,
        "LicenseType": LicenseType,
        "Nationality": Nationality,
      };
}

class CrOcrModel {
  String CivilId;
  String ExpiryDate;
  String PlateNumber;
  String VinNumber;
  String YearOfManufacture;
  String Weight;
  String Height;

  CrOcrModel({
    this.CivilId = '',
    this.ExpiryDate = '',
    this.PlateNumber = '',
    this.VinNumber = '',
    this.YearOfManufacture = '',
    this.Weight = '',
    this.Height = '',
  });

  factory CrOcrModel.fromJson(Map<dynamic, dynamic> json) => CrOcrModel(
        CivilId: json["CivilId"] ?? '',
        ExpiryDate: json["ExpiryDate"] ?? '',
        PlateNumber: json["PlateNumber"] ?? '',
        VinNumber: json["VinNumber"] ?? '',
        YearOfManufacture: json["YearOfManufacture"] ?? '',
        Weight: json["Weight"] ?? '',
        Height: json["Height"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "CivilId": CivilId,
        "ExpiryDate": ExpiryDate,
        "PlateNumber": PlateNumber,
        "VinNumber": VinNumber,
        "YearOfManufacture": YearOfManufacture,
        "Weight": Weight,
        "Height": Height,
      };
}

class CidOcrModel {
  String Name;
  String CivilId;
  String ResidencyExpiry;
  String Dob;
  String Gender;
  String Nationality;
  String PassportNo;
  String CovidVaccStatus;
  String Block;
  String UnitNo;
  String FloorNo;
  String PaciNo;
  String BuildingNo;
  String ArticleNo;
  String MobileIdSlNo;
  CidOcrModel({
    this.Name = '',
    this.CivilId = '',
    this.ResidencyExpiry = '',
    this.Dob = '',
    this.Gender = '',
    this.Nationality = '',
    this.PassportNo = '',
    this.CovidVaccStatus = '',
    this.Block = '',
    this.UnitNo = '',
    this.FloorNo = '',
    this.PaciNo = '',
    this.BuildingNo = '',
    this.ArticleNo = '',
    this.MobileIdSlNo = '',
  });

  factory CidOcrModel.fromJson(Map<dynamic, dynamic> json) => CidOcrModel(
        Name: json["Name"] ?? '',
        CivilId: json["CivilId"] ?? '',
        ResidencyExpiry: json["ResidencyExpiry"] ?? '',
        Dob: json["Dob"] ?? '',
        Gender: json["Gender"] ?? '',
        Nationality: json["Nationality"] ?? '',
        PassportNo: json["PassportNo"] ?? '',
        CovidVaccStatus: json["CovidVaccStatus"] ?? '',
        Block: json["Block"] ?? '',
        UnitNo: json["UnitNo"] ?? '',
        FloorNo: json["FloorNo"] ?? '',
        PaciNo: json["PaciNo"] ?? '',
        BuildingNo: json["BuildingNo"] ?? '',
        ArticleNo: json["ArticleNo"] ?? '',
        MobileIdSlNo: json["MobileIdSlNo"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "Name": Name,
        "CivilId": CivilId,
        "ResidencyExpiry": ResidencyExpiry,
        "Dob": Dob,
        "Gender": Gender,
        "Nationality": Nationality,
        "PassportNo": PassportNo,
        "CovidVaccStatus": CovidVaccStatus,
        "Block": Block,
        "UnitNo": UnitNo,
        "FloorNo": FloorNo,
        "PaciNo": PaciNo,
        "BuildingNo": BuildingNo,
        "ArticleNo": ArticleNo,
        "MobileIdSlNo": MobileIdSlNo,
      };
}
