import 'package:intl/intl.dart';

final dateFormat = DateFormat("dd-MM-yyyy");

class ProfileModel {
  ProfileModel({
    this.customerId = '',
    this.customerName = '',
    this.dob = '',
    this.civilidNo = '',
    this.emailId = '',
    this.phoneNo = '',
    this.mobileNo = '',
    this.photoUrl = '',
    this.otpVerified = false,
  });

  String customerId;
  String customerName;
  String dob;
  String civilidNo;
  String emailId;
  String phoneNo;
  String mobileNo;
  String photoUrl;
  bool otpVerified;

  factory ProfileModel.fromJson(Map<dynamic, dynamic> json) => ProfileModel(
        customerId: json["customerId"] ?? '',
        customerName: json["customerName"] ?? '',
        dob: json["dob"]?? '',
        civilidNo: json["civilidno"] ?? '',
        emailId: json["emailid"] ?? '',
        phoneNo: json["phoneno"] ?? '',
        mobileNo: json["mobileno"] ?? '',
        photoUrl: json["photoUrl"] ?? '',
        otpVerified: json["otpVerified"] ?? false,
      );

  Map<String, dynamic> toJson() => {
        "customerId": customerId,
        "customerName": customerName,
        "dob": dob,
        "civilidNo": civilidNo,
        "emailId": emailId,
        "phoneNo": phoneNo,
        "mobileNo": mobileNo,
        "photoUrl": photoUrl,
        "otpVerified": otpVerified,
      };
}
