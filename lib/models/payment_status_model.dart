class PaymentStatusModel {
  String MerchantTxnRefNo;
  String PaymentId;
  String ProcessDate;
  String StatusDescription;
  String BookeeyTrackId;
  String BankRefNo;
  String PaymentType;
  String ErrorCode;
  String ProductType;
  String finalStatus;

  PaymentStatusModel({
    this.MerchantTxnRefNo = '',
    this.PaymentId = '',
    this.ProcessDate = '',
    this.StatusDescription = '',
    this.BookeeyTrackId = '',
    this.BankRefNo = '',
    this.PaymentType = '',
    this.ErrorCode = '',
    this.ProductType = '',
    this.finalStatus = '',
  });

  factory PaymentStatusModel.fromJson(Map<dynamic, dynamic> json) =>
      PaymentStatusModel(
        MerchantTxnRefNo: json["MerchantTxnRefNo"] ?? '',
        PaymentId: json["PaymentId"] ?? '',
        ProcessDate: json["ProcessDate"] ?? '',
        StatusDescription: json["StatusDescription"] ?? '',
        BookeeyTrackId: json["BookeeyTrackId"] ?? '',
        BankRefNo: json["BankRefNo"] ?? '',
        PaymentType: json["PaymentType"] ?? '',
        ErrorCode: json["ErrorCode"] ?? '',
        ProductType: json["ProductType"] ?? '',
        finalStatus: json["finalStatus"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "MerchantTxnRefNo": MerchantTxnRefNo,
        "PaymentId": PaymentId,
        "ProcessDate": ProcessDate,
        "StatusDescription": StatusDescription,
        "BookeeyTrackId": BookeeyTrackId,
        "BankRefNo": BankRefNo,
        "PaymentType": PaymentType,
        "ErrorCode": ErrorCode,
        "ProductType": ProductType,
        "finalStatus": finalStatus,
      };
}
