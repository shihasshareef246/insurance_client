class ProductModel {
  ProductModel({
    this.productId = '',
    this.productName = '',
    this.isSelected = false,
  });

  String productId;
  String productName;
  bool isSelected;

  factory ProductModel.fromJson(Map<dynamic, dynamic> json) => ProductModel(
        productId: json["productId"] ?? '',
        productName: json["productName"] ?? '',
        isSelected: json["isSelected"] ?? false,
      );

  Map<String, dynamic> toJson() => {
        "productId": productId,
        "productName": productName,
        "isSelected": isSelected,
      };
}
