class AdditionalTravelInsuranceModel {
  AdditionalTravelInsuranceModel({
    this.productType = '',
    this.additionalCoverID = '',
    this.coverCode = '',
    this.coverDetails = '',
    this.premiumAmount = '',
    this.type = '',
    this.insuranceType = '',
    this.isSelected = false,
  });

  String productType;
  String additionalCoverID;
  String coverCode;
  String coverDetails;
  String premiumAmount;
  String type;
  String insuranceType;
  bool isSelected;

  factory AdditionalTravelInsuranceModel.fromJson(Map<dynamic, dynamic> json) =>
      AdditionalTravelInsuranceModel(
        productType: json["productType"] ?? '',
        additionalCoverID: json["additionalCoverId"] ?? '',
        coverCode: json["coverCode"] ?? '',
        coverDetails: json["coverDetails"] ?? '',
        premiumAmount: json["premiumAmount"] ?? '',
        type: json["type"] ?? '',
        insuranceType: json["insuranceType"] ?? '',
        isSelected: json["isSelected"] ?? false,
      );

  Map<String, dynamic> toJson() => {
        "productType": productType,
        "additionalCoverId": additionalCoverID,
        "coverCode": coverCode,
        "coverDetails": coverDetails,
        "premiumAmount": premiumAmount,
        "type": type,
        "insuranceType": insuranceType,
        "isSelected": isSelected,
      };
}
