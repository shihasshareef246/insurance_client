class CustomerInsuranceModel {
  CustomerInsuranceModel({
    this.insuranceType = '',
    this.insuranceId = 0,
    this.planType = '',
    this.premiumAmount = '',
    this.planId = 0,
    this.status = '',
    this.transactionId = 0,
    this.transactionDate = '',
    this.productType = '',
    this.paymentMode = '',
  });

  String insuranceType;
  int insuranceId;
  String planType;
  String premiumAmount;
  int planId;
  String status;
  int transactionId;
  String productType;
  String transactionDate;
  String paymentMode;

  factory CustomerInsuranceModel.fromJson(Map<dynamic, dynamic> json) =>
      CustomerInsuranceModel(
        insuranceType: json["insuranceType"] ?? '',
        insuranceId: json["insuranceId"] ?? 0,
        planType: json["planType"] ?? '',
        premiumAmount: json["premiumAmount"] ?? '',
        planId: json["planId"] ?? 0,
        status: json["status"] ?? '',
        transactionId: json["transactionId"] ?? 0,
        productType: json["productType"] ?? '',
        transactionDate: json["transactionDate"] ?? '',
        paymentMode: json["paymentMode"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "insuranceType": insuranceType,
        "insuranceId": insuranceId,
        "planType": planType,
        "premiumAmount": premiumAmount,
        "planId": planId,
        "status": status,
        "transactionId": transactionId,
        "transactionDate": transactionDate,
        "productType": productType,
        "paymentMode": paymentMode,
      };
}
