class OTPModel {
  String messageId;

  OTPModel({
    required this.messageId,
  });

  factory OTPModel.fromJson(Map<dynamic, dynamic> json) => OTPModel(
        messageId: json["messageId"],
      );

  Map<String, dynamic> toJson() => {
        "messageId": messageId,
      };
}
