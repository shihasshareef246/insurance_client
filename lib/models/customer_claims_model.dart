class CustomerClaimsModel {
  CustomerClaimsModel({
    this.claimId = 0,
    this.insuranceId = 0,
    this.claimDate = '',
    this.claimReason = '',
    this.planId = 0,
    this.lossDate = '',
    this.claimStatus = '',
    this.productType = '',
    this.claimAmount = 0,
  });

  int claimId;
  int insuranceId;
  String claimDate;
  String claimReason;
  int planId;
  String lossDate;
  String claimStatus;
  String productType;
  int claimAmount;

  factory CustomerClaimsModel.fromJson(Map<dynamic, dynamic> json) =>
      CustomerClaimsModel(
        claimId: json["claimId"] ?? 0,
        insuranceId: json["insuranceId"] ?? 0,
        claimDate: json["claimDate"] ?? '',
        claimReason: json["claimReason"] ?? '',
        planId: json["planId"] ?? 0,
        lossDate: json["lossDate"] ?? '',
        claimStatus: json["claimStatus"] ?? '',
        productType: json["productType"] ?? '',
        claimAmount: json["claimAmount"] ?? 0,
      );

  Map<String, dynamic> toJson() => {
        "claimId": claimId,
        "insuranceId": insuranceId,
        "claimDate": claimDate,
        "claimReason": claimReason,
        "planId": planId,
        "lossDate": lossDate,
        "claimStatus": claimStatus,
        "productType": productType,
        "claimAmount": claimAmount,
      };
}
