class DocumentsModel {
  num docId;
  String docName;
  String docUrl;
  String comment;
  String status;

  DocumentsModel({
    required this.docId,
    required this.docName,
    required this.docUrl,
    required  this.comment,
    required this.status,
  });

  factory DocumentsModel.fromJson(Map<dynamic, dynamic> json) => DocumentsModel(
        docId: json["docId"],
        docName: json["docName"],
        docUrl: json["docUrl"],
        comment: json["comment"],
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "docId": docId,
        "docName": docName,
        "docUrl": docUrl,
        "comment": comment,
        "status": status,
      };
}
