class TransactionModel {
  TransactionModel({
    this.transactionId = 0,
  });

  int transactionId;

  factory TransactionModel.fromJson(Map<dynamic, dynamic> json) =>
      TransactionModel(
        transactionId: json["transactionId"] ?? 0,
      );

  Map<String, dynamic> toJson() => {
        "transactionId": transactionId,
      };
}
