class PaymentBodyModel {
  String merchantId;
  String username;
  String password;
  String apiKey;
  String orderId;
  String totalPrice;
  String currencyCode;
  String successUrl;
  String errorUrl;
  String testMode;
  String customerFName;
  String customerEmail;
  String customerMobile;
  String paymentGateway;
  String whitelabled;
  String productTitle;
  String productName;
  String productPrice;
  String productQty;
  String reference;
  String notifyURL;

  PaymentBodyModel({
    this.merchantId = '',
    this.username = '',
    this.password = '',
    this.apiKey = '',
    this.orderId = '',
    this.totalPrice = '',
    this.currencyCode = '',
    this.successUrl = '',
    this.errorUrl = '',
    this.testMode = '',
    this.customerFName = '',
    this.customerEmail = '',
    this.customerMobile = '',
    this.paymentGateway = '',
    this.whitelabled = '',
    this.productTitle = '',
    this.productName = '',
    this.productPrice = '',
    this.productQty = '',
    this.reference = '',
    this.notifyURL = '',
  });

  factory PaymentBodyModel.fromJson(Map<dynamic, dynamic> json) =>
      PaymentBodyModel(
        merchantId: json["merchantId"] ?? '',
        username: json["username"] ?? '',
        password: json["password"] ?? '',
        apiKey: json["apiKey"] ?? '',
        orderId: json["orderId"] ?? '',
        totalPrice: json["totalPrice"] ?? '',
        currencyCode: json["currencyCode"] ?? '',
        successUrl: json["successUrl"] ?? '',
        errorUrl: json["errorUrl"] ?? '',
        testMode: json["testMode"] ?? '',
        customerFName: json["customerFName"] ?? '',
        customerEmail: json["customerEmail"] ?? '',
        customerMobile: json["customerMobile"] ?? '',
        paymentGateway: json["paymentGateway"] ?? '',
        whitelabled: json["whitelabled"] ?? '',
        productTitle: json["productTitle"] ?? '',
        productName: json["productName"] ?? '',
        productPrice: json["productPrice"] ?? '',
        productQty: json["productQty"] ?? '',
        reference: json["reference"] ?? '',
        notifyURL: json["notifyURL"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "merchantId": merchantId,
        "username": username,
        "password": password,
        "apiKey": apiKey,
        "orderId": orderId,
        "totalPrice": totalPrice,
        "currencyCode": currencyCode,
        "successUrl": successUrl,
        "errorUrl": errorUrl,
        "testMode": testMode,
        "customerFName": customerFName,
        "customerEmail": customerEmail,
        "customerMobile": customerMobile,
        "paymentGateway": paymentGateway,
        "whitelabled": whitelabled,
        "productTitle": productTitle,
        "productName": productName,
        "productPrice": productPrice,
        "productQty": productQty,
        "reference": reference,
        "notifyURL": notifyURL,
      };
}
