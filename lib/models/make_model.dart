class MakeModel {
  String makeModel;

  MakeModel({
    required this.makeModel,
  });

  factory MakeModel.fromJson(Map<dynamic, dynamic> json) => MakeModel(
        makeModel: json["makeModel"],
      );

  Map<String, dynamic> toJson() => {
        "makeModel": makeModel,
      };
}
