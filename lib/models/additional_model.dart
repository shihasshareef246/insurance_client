class AdditionalModel {
  AdditionalModel({
    this.additionalCoverID = 0,
  });

  int additionalCoverID;

  factory AdditionalModel.fromJson(Map<dynamic, dynamic> json) =>
      AdditionalModel(
        additionalCoverID: json["additionalCoverID"] ?? 0,
      );

  Map<String, dynamic> toJson() => {
        "additionalCoverID": additionalCoverID,
      };
}
