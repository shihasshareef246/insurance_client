class ProfilePicModel {
  String documentLink;
  String message;
  ProfilePicModel({
    required this.documentLink,
    required this.message,
  });

  factory ProfilePicModel.fromJson(Map<dynamic, dynamic> json) =>
      ProfilePicModel(
        documentLink: json["documentLink"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "documentLink": documentLink,
        "message": message,
      };
}
