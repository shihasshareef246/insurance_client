import 'insurance_details_model.dart';

class TravelInsuranceGroupModel {
  TravelInsuranceGroupModel({
    this.InsurerTravelAdditionalDetails = const [],
    required this.InsurerTravelDetails,
    this.InsurerDocumentDetails = const [],
    required this.InsurerPlans,
    this.InsurerPersonalDetails = const [],
  });

  List<InsurerTravelAdditionalDetailsModel?> InsurerTravelAdditionalDetails;
  InsurerTravelDetailsModel? InsurerTravelDetails;
  List<InsurerDocumentDetailsModel?> InsurerDocumentDetails;
  InsurerPlansModel InsurerPlans;
  List<InsuranceDetailsModel?> InsurerPersonalDetails;

  factory TravelInsuranceGroupModel.fromJson(Map<dynamic, dynamic> json) =>
      TravelInsuranceGroupModel(
        InsurerTravelAdditionalDetails:
            json['InsurerTravelAdditionalDetails'] != null
                ? List<InsurerTravelAdditionalDetailsModel>.from(
                    json['InsurerTravelAdditionalDetails'].map(
                      (docs) =>
                          InsurerTravelAdditionalDetailsModel.fromJson(docs),
                    ),
                  )
                : [],
        InsurerTravelDetails: json["InsurerTravelDetails"],
        InsurerDocumentDetails: json['InsurerDocumentDetails'] != null
            ? List<InsurerDocumentDetailsModel>.from(
                json['InsurerDocumentDetails'].map(
                  (docs) => InsurerDocumentDetailsModel.fromJson(docs),
                ),
              )
            : [],
        InsurerPlans: json["InsurerPlans"],
        InsurerPersonalDetails: json['InsurerPersonalDetails'] != null
            ? List<InsuranceDetailsModel>.from(
                json['InsurerPersonalDetails'].map(
                  (docs) => InsuranceDetailsModel.fromJson(docs),
                ),
              )
            : [],
      );

  Map<String, dynamic> toJson() => {
        "InsurerTravelAdditionalDetails": InsurerTravelAdditionalDetails,
        "InsurerTravelDetails": InsurerTravelDetails,
        "InsurerDocumentDetails": InsurerDocumentDetails,
        "InsurerPlans": InsurerPlans,
        "InsurerPersonalDetails": InsurerPersonalDetails,
      };
}

class InsurerTravelDetailsModel {
  String travelFromDate;
  String totalTravelDays;
  String travelToDate;

  InsurerTravelDetailsModel({
    this.travelFromDate = '',
    this.totalTravelDays = '',
    this.travelToDate = '',
  });

  factory InsurerTravelDetailsModel.fromJson(Map<dynamic, dynamic> json) =>
      InsurerTravelDetailsModel(
        travelFromDate: json["travelFromDate"] ?? '',
        totalTravelDays: json["totalTravelDays"] ?? '',
        travelToDate: json["travelToDate"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "travelFromDate": travelFromDate,
        "totalTravelDays": totalTravelDays,
        "travelToDate": travelToDate,
      };
}

class InsurerTravelAdditionalDetailsModel {
  String? additionalCoverID;
  InsurerTravelAdditionalDetailsModel({
    this.additionalCoverID = '',
  });

  factory InsurerTravelAdditionalDetailsModel.fromJson(
          Map<dynamic, dynamic> json) =>
      InsurerTravelAdditionalDetailsModel(
        additionalCoverID: json["additionalCoverID"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "additionalCoverID": additionalCoverID,
      };
}

class InsurerDocumentDetailsModel {
  String? documentName;
  String? documentUrl;
  InsurerDocumentDetailsModel({
    this.documentName = '',
    this.documentUrl = '',
  });

  factory InsurerDocumentDetailsModel.fromJson(Map<dynamic, dynamic> json) =>
      InsurerDocumentDetailsModel(
        documentName: json["documentName"] ?? '',
        documentUrl: json["documentUrl"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "documentName": documentName,
        "documentUrl": documentUrl,
      };
}

class InsurerPlansModel {
  String? customerId;
  String insuranceId;
  int? planId;
  String transactionDate;
  int transactionId;
  String tranType;
  InsurerPlansModel({
    this.customerId = '',
    this.insuranceId = '',
    this.planId = 0,
    this.transactionDate = '',
    this.transactionId = 0,
    this.tranType = '',
  });

  factory InsurerPlansModel.fromJson(Map<dynamic, dynamic> json) =>
      InsurerPlansModel(
        customerId: json["customerId"] ?? '',
        insuranceId: json["insuranceId"] ?? '',
        planId: json["planId"] ?? '',
        transactionDate: json["transactionDate"] ?? '',
        transactionId: json["transactionId"] ?? 0,
        tranType: json["tranType"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "customerId": customerId,
        "insuranceId": insuranceId,
        "planId": planId,
        "transactionDate": transactionDate,
        "transactionId": transactionId,
        "tranType": tranType,
      };
}

// class InsurerPersonalDetailsModel {
//   String insurerName;
//   String dob;
//   String emailId;
//   String passportNo;
//   String passportExpiry;
//   String civilIdNo;
//   String civilIdExpiry;
//   String nationality;
//   String gender;
//   String unitType;
//   String area;
//   String blockNo;
//   String street;
//   String bnoOrHno;
//   String floorNo;
//
//   InsurerPersonalDetailsModel({
//     this.insurerName = '',
//     this.dob = '',
//     this.emailId = '',
//     this.passportNo = '',
//     this.passportExpiry = '',
//     this.civilIdNo = '',
//     this.civilIdExpiry = '',
//     this.nationality = '',
//     this.gender = '',
//     this.unitType = '',
//     this.area = '',
//     this.blockNo = '',
//     this.street = '',
//     this.bnoOrHno = '',
//     this.floorNo = '',
//   });
//
//   factory InsurerPersonalDetailsModel.fromJson(Map<dynamic, dynamic> json) =>
//       InsurerPersonalDetailsModel(
//         insurerName: json["insurerName"] ?? '',
//         dob: json["dob"] ?? '',
//         emailId: json["emailId"] ?? '',
//         passportNo: json["passportNo"] ?? '',
//         passportExpiry: json["passportExpiry"] ?? '',
//         civilIdNo: json["civilIdNo"] ?? '',
//         civilIdExpiry: json["civilIdExpiry"] ?? '',
//         nationality: json["nationality"] ?? '',
//         gender: json["gender"] ?? '',
//         unitType: json["unitType"] ?? '',
//         area: json["area"] ?? '',
//         blockNo: json["blockNo"] ?? '',
//         street: json["street"] ?? '',
//         bnoOrHno: json["bnoOrHno"] ?? '',
//         floorNo: json["floorNo"] ?? '',
//       );
//
//   Map<String, dynamic> toJson() => {
//         "insurerName": insurerName,
//         "dob": dob,
//         "emailId": emailId,
//         "passportNo": passportNo,
//         "passportExpiry": passportExpiry,
//         "civilIdNo": civilIdNo,
//         "civilIdExpiry": civilIdExpiry,
//         "nationality": nationality,
//         "gender": gender,
//         "unitType": unitType,
//         "area": area,
//         "blockNo": blockNo,
//         "street": street,
//         "bnoOrHno": bnoOrHno,
//         "floorNo": floorNo,
//       };
//}
