class ClaimDocsModel {
  String DocumentName;
  String DocumentUrl;
  ClaimDocsModel({
    this.DocumentName = '',
    this.DocumentUrl = '',
  });

  factory ClaimDocsModel.fromJson(Map<dynamic, dynamic> json) => ClaimDocsModel(
        DocumentName: json["DocumentName"],
        DocumentUrl: json["DocumentUrl"],
      );

  Map<String, dynamic> toJson() => {
        "DocumentName": DocumentName,
        "DocumentUrl": DocumentUrl,
      };
}
