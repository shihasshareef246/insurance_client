class TravelInsuranceModel {
  TravelInsuranceModel({
    this.productType = '',
    this.productSummary = '',
    this.productDescription = '',
    this.premiumAmount = '',
    this.promoCode = '',
    this.promoDiscount = '',
    this.promoDiscountText = '',
    this.totalPremium = '',
    this.otherFee = '',
    this.validity = 0,
    this.validityPeriod = '',
    this.planId = '',
    this.planType = '',
  });

  String productType;
  String productSummary;
  String productDescription;
  String premiumAmount;
  String promoCode;
  String promoDiscount;
  String promoDiscountText;
  String totalPremium;
  String otherFee;
  int validity;
  String validityPeriod;
  String planId;
  String planType;

  factory TravelInsuranceModel.fromJson(Map<dynamic, dynamic> json) =>
      TravelInsuranceModel(
        productType: json["productType"] ?? '',
        productSummary: json["productSummary"] ?? '',
        productDescription: json["productDescription"] ?? '',
        premiumAmount: json["premiumAmount"] ?? '0',
        promoCode: json["promoCode"] ?? '',
        promoDiscount: json["promoDiscount"] ?? '0',
        promoDiscountText: json["promoDiscountText"] ?? '',
        totalPremium: json["totalPremium"] ?? '0',
        otherFee: json["otherFee"] ?? '0',
        validity: json["validity"] ?? 0,
        validityPeriod: json["validityPeriod"] ?? '',
        planId: json["planId"] ?? '',
        planType: json["planType"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "productType": productType,
        "productSummary": productSummary,
        "productDescription": productDescription,
        "premiumAmount": premiumAmount,
        "promoCode": promoCode,
        "promoDiscount": promoDiscount,
        "promoDiscountText": promoDiscountText,
        "totalPremium": totalPremium,
        "otherFee": otherFee,
        "validity": validity,
        "validityPeriod": validityPeriod,
        "planId": planId,
        "planType": planType,
      };
}
