class UploadDocsModel {
  List<DocModel> docs;

  UploadDocsModel({
    required this.docs,
  });

  factory UploadDocsModel.fromJson(Map<dynamic, dynamic> json) =>
      UploadDocsModel(
        docs: json['uploadDocumentsId'] != null
            ? List<DocModel>.from(json['uploadDocumentsId']
                .map((docs) => DocModel.fromJson(docs)))
            : [],
      );

  Map<String, dynamic> toJson() => {
        "uploadDocumentsId": docs,
      };
}

class DocModel {
  String documentLink;
  String id;
  DocModel({
    required this.documentLink,
    required this.id,
  });

  factory DocModel.fromJson(Map<dynamic, dynamic> json) => DocModel(
        documentLink: json["documentLink"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "documentLink": documentLink,
        "id": id,
      };
}
