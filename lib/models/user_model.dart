class UserModel {
  UserModel({
    this.agrifiId = '',
    this.mobile = '',
    this.role = '',
    this.name = '',
    this.pin = '',
    this.address = '',
    this.company = '',
    this.authToken = '',
    this.appliedForLead = false,
    this.firstStep = false,
    this.secondStep = false,
    this.ordered = false,
    this.profilePhotoLink = '',
    this.language = '',
  });

  String agrifiId;
  String mobile;
  String role;
  String name;
  String pin;
  String address;
  String company;
  String authToken;
  bool appliedForLead;
  bool firstStep;
  bool secondStep;
  bool ordered;
  String profilePhotoLink;
  String language;

  factory UserModel.fromJson(Map<dynamic, dynamic> json) => UserModel(
        agrifiId: json["agrifiId"],
        mobile: json["mobile"],
        role: json["role"],
        name: json["name"],
        pin: json["pinCode"],
        address: json["address"],
        company: json["company"],
        authToken: json["authToken"],
        appliedForLead: json["appliedForLead"]??false,
        firstStep: json["firstStep"]??false,
        secondStep: json["secondStep"]??false,
        ordered: json["ordered"]??false,
        profilePhotoLink: json["profilePhotoLink"],
        language: json["language"],
      );

  Map<String, dynamic> toJson() => {
        "agrifiId": agrifiId,
        "mobile": mobile,
        "role": role,
        "name": name,
        "pin": pin,
        "address": address,
        "company": company,
        "authToken": authToken,
        "appliedForLead": appliedForLead,
        "firstStep": firstStep,
        "secondStep": secondStep,
        "ordered": ordered,
        "profilePhotoLink": profilePhotoLink,
        "language": language,
      };
}
