class AdditionalMotorInsuranceModel {
  AdditionalMotorInsuranceModel({
    this.productType = '',
    this.additionalCoverID = 0,
    this.coverCode = '',
    this.coverDetails = '',
    this.premiumAmount = 0.0,
    this.type = '',
    this.isSelected = false,
  });

  String productType;
  int additionalCoverID;
  String coverCode;
  String coverDetails;
  double premiumAmount;
  String type;
  bool isSelected;

  factory AdditionalMotorInsuranceModel.fromJson(Map<dynamic, dynamic> json) =>
      AdditionalMotorInsuranceModel(
        productType: json["productType"] ?? '',
        additionalCoverID: json["additionalCoverID"] ?? 0,
        coverCode: json["coverCode"] ?? '',
        coverDetails: json["coverDetails"] ?? '',
        premiumAmount: json["premiumAmount"] ?? 0.0,
        type: json["type"] ?? '',
        isSelected: json["isSelected"] ?? false,
      );

  Map<String, dynamic> toJson() => {
        "productType": productType,
        "additionalCoverID": additionalCoverID,
        "coverCode": coverCode,
        "coverDetails": coverDetails,
        "premiumAmount": premiumAmount,
        "type": type,
        "isSelected": isSelected,
      };
}
