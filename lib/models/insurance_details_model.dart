class InsuranceDetailsModel {
  InsuranceDetailsModel({
    this.PersonalDetails,
    this.MotorDetails,
  });

  PersonalDetailsModel? PersonalDetails;
  MotorDetailsModel? MotorDetails;

  factory InsuranceDetailsModel.fromJson(Map<dynamic, dynamic> json) =>
      InsuranceDetailsModel(
        PersonalDetails:
            PersonalDetailsModel.fromJson(json['PersonalDetails']) ?? null,
        MotorDetails: MotorDetailsModel.fromJson(json['Motor']) ?? null,
      );

  Map<String, dynamic> toJson() => {
        "PersonalDetails": PersonalDetails,
        "Motor": MotorDetails,
      };
}

class PersonalDetailsModel {
  PersonalDetailsModel({
    this.insurerName = '',
    this.insuranceId = 0,
    this.nationality = '',
    this.relation = '',
    this.gender = '',
    this.dob = '',
    this.emailId = '',
    this.mobileNo = '',
    // this.phoneNo = '',
    this.civilIdNo = '',
    this.civilIDExpDate = '',
    this.passportNo = '',
    this.passportExpDate = '',
    this.unitType = '',
    this.area = '',
    this.blockNo = '',
    this.street = '',
    this.bNoOrHouseNo = '',
    this.floorNo = '',
    this.policyDocument = '',
  });

  String insurerName;
  int insuranceId;
  String nationality;
  String relation;
  String gender;
  String mobileNo;
  // String phoneNo;
  String dob;
  String emailId;
  String civilIdNo;
  String civilIDExpDate;
  String passportNo;
  String passportExpDate;
  String unitType;
  String area;
  String blockNo;
  String street;
  String bNoOrHouseNo;
  String floorNo;
  String policyDocument;

  factory PersonalDetailsModel.fromJson(Map<dynamic, dynamic> json) =>
      PersonalDetailsModel(
        insurerName: json["insurerName"] ?? '',
        insuranceId: json["insuranceId"] ?? 0,
        nationality: json["nationality"] ?? '',
        relation: json["relation"] ?? '',
        gender: json["gender"] ?? '',
        dob: json["dob"] ?? '',
        emailId: json["emailId"] ?? '',
        mobileNo: json["mobileNo"] ?? '',
        // phoneNo: json["phoneNo"] ?? '',
        civilIdNo: json["civilIdNo"] ?? '',
        civilIDExpDate: json["civilIDExpDate"] ?? '',
        passportNo: json["passportNo"] ?? '',
        passportExpDate: json["passportExpDate"] ?? '',
        unitType: json["unitType"] ?? '',
        area: json["area"] ?? '',
        blockNo: json["blockNo"] ?? '',
        street: json["street"] ?? '',
        bNoOrHouseNo: json["bNoOrHouseNo"] ?? '',
        floorNo: json["floorNo"] ?? '',
        policyDocument: json["policyDocument"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "insurerName": insurerName,
        "insuranceId": insuranceId,
        "nationality": nationality,
        "relation": relation,
        "gender": gender,
        "dob": dob,
        "emailId": emailId,
        "mobileNo": mobileNo,
        // "phoneNo": phoneNo,
        "civilIdNo": civilIdNo,
        "civilIDExpDate": civilIDExpDate,
        "passportNo": passportNo,
        "passportExpDate": passportExpDate,
        "unitType": unitType,
        "area": area,
        "blockNo": blockNo,
        "street": street,
        "bNoOrHouseNo": bNoOrHouseNo,
        "floorNo": floorNo,
        "policyDocument": policyDocument,
      };
}

class MotorDetailsModel {
  String licenseNo;
  String licenseExpiry;
  String vinNo;
  String plateNo;
  String makeModel;
  double carValue;
  String noOfSeats;
  String makeYear;
  int transactionId;
  int customerId;
  int planId;

  MotorDetailsModel({
    this.licenseNo = '',
    this.licenseExpiry = '',
    this.vinNo = '',
    this.plateNo = '',
    this.makeModel = '',
    this.carValue = 0,
    this.noOfSeats = '',
    this.makeYear = '',
    this.transactionId = 0,
    this.customerId = 0,
    this.planId = 0,
  });

  factory MotorDetailsModel.fromJson(Map<dynamic, dynamic> json) =>
      MotorDetailsModel(
        licenseNo: json["LicenseNo"] ?? '',
        licenseExpiry: json["LicenseExpiry"] ?? '',
        transactionId: json["TransactionId"] ?? 0,
        plateNo: json["PlateNo"] ?? '',
        makeModel: json["MakeModel"] ?? '',
        carValue: json["CarValue"] ?? 0,
        noOfSeats: json["NoOfSeats"] ?? '',
        makeYear: json["MakeYear"] ?? '',
        customerId: json["CustomerId"] ?? 0,
        vinNo: json["VinNo"] ?? '',
        planId: json["PlanId"] ?? 0,
      );

  Map<String, dynamic> toJson() => {
        "LicenseNo": licenseNo,
        "LicenseExpiry": licenseExpiry,
        "TransactionId": transactionId,
        "PlateNo": plateNo,
        "MakeModel": makeModel,
        "CarValue": carValue,
        "NoOfSeats": noOfSeats,
        "MakeYear": makeYear,
        "CustomerId": customerId,
        "VinNo": vinNo,
        "PlanId": planId,
      };
}
