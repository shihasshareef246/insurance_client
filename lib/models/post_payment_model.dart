class PostPaymentModel {
  String paymentreferenceno;
  String PaymentMode;
  String paymentdate;
  String premiumAmount;
  String transactionid;
  String status;
  String transactiondate;
  List<AdditionalClaimsModel> AdditionalClaims;
  PostPaymentModel({
    this.paymentreferenceno = '',
    this.PaymentMode = '',
    this.paymentdate = '',
    this.premiumAmount = '',
    this.transactionid = '',
    this.status = '',
    this.transactiondate = '',
    this.AdditionalClaims = const [],
  });

  factory PostPaymentModel.fromJson(Map<dynamic, dynamic> json) => PostPaymentModel(
        paymentreferenceno: json["paymentreferenceno"] ?? '',
        PaymentMode: json["PaymentMode"] ?? '',
        paymentdate: json["paymentdate"] ?? '',
        premiumAmount: json["premiumAmount"] ?? '',
        transactionid: json["transactionid"] ?? '',
        status: json["status"] ?? '',
        transactiondate: json["transactiondate"] ?? '',
        AdditionalClaims: json['AdditionalClaims'] != null
            ? List<AdditionalClaimsModel>.from(json['AdditionalClaims']
                .map((docs) => AdditionalClaimsModel.fromJson(docs)))
            : [],
      );

  Map<String, dynamic> toJson() => {
        "paymentreferenceno": paymentreferenceno,
        "PaymentMode": PaymentMode,
        "paymentdate": paymentdate,
        "premiumAmount": premiumAmount,
        "transactionid": transactionid,
        "status": status,
        "transactiondate": transactiondate,
        "AdditionalClaims": AdditionalClaims,
      };
}

class AdditionalClaimsModel {
  String coverId;
  String transactionId;
  AdditionalClaimsModel({
    this.coverId = '',
    this.transactionId = '',
  });

  factory AdditionalClaimsModel.fromJson(Map<dynamic, dynamic> json) =>
      AdditionalClaimsModel(
        coverId: json["coverId"] ?? '',
        transactionId: json["transactionId"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "coverId": coverId,
        "transactionId": transactionId,
      };
}
