class CustomerModel {
  CustomerModel({
    this.errorCode = '',
    this.mobileNo = '',
    this.errorMessage = '',
  });

  String errorCode;
  String mobileNo;
  String errorMessage;

  factory CustomerModel.fromJson(Map<dynamic, dynamic> json) => CustomerModel(
        errorCode: json["errorCode"] ?? '',
        mobileNo: json["mobileNo"] ?? '',
        errorMessage: json["errorMessage"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "errorCode": errorCode,
        "mobileNo": mobileNo,
        "errorMessage": errorMessage,
      };
}
