class DiscountPlanModel {
  DiscountPlanModel({
    this.planType = '',
    this.insuranceId = '',
    this.promoDiscountText = '',
    this.promoDiscount = '',
    this.planId = 0,
    this.insuranceType = '',
    this.productType = '',
    this.productSummary = '',
    this.productDescription = '',
    this.premiumAmount = '',
    this.totalPremium = '',
    this.validity = '',
    this.validityPeriod = '',
    this.promoCode = '',
    this.otherDiscount = '',
    this.otherFee = '',
  });

  String planType;
  String insuranceId;
  String promoDiscountText;
  String promoDiscount;
  int planId;
  String insuranceType;
  String productType;
  String productSummary;
  String productDescription;
  String premiumAmount;
  String totalPremium;
  String validity;
  String validityPeriod;
  String promoCode;
  String otherDiscount;
  String otherFee;

  factory DiscountPlanModel.fromJson(Map<dynamic, dynamic> json) =>
      DiscountPlanModel(
        planType: json["planType"] ?? '',
        insuranceId: json["insuranceId"] ?? '',
        promoDiscountText: json["promoDiscountText"] ?? '',
        promoDiscount: json["promoDiscount"] ?? '',
        planId: json["planId"] ?? 0,
        insuranceType: json["insuranceType"] ?? '',
        productType: json["productType"] ?? '',
        productSummary: json["productSummary"] ?? '',
        productDescription: json["productDescription"] ?? '',
        premiumAmount: json["premiumAmount"] ?? '',
        totalPremium: json["totalPremium"] ?? '',
        validity: json["validity"] ?? '',
        validityPeriod: json["validityPeriod"] ?? '',
        promoCode: json["promoCode"] ?? '',
        otherDiscount: json["otherDiscount"] ?? '',
        otherFee: json["otherFee"] ?? '',
      );

  Map<String, dynamic> toJson() => {
        "planType": planType,
        "insuranceId": insuranceId,
        "promoDiscountText": promoDiscountText,
        "promoDiscount": promoDiscount,
        "planId": planId,
        "insuranceType": insuranceType,
        "productType": productType,
        "productSummary": productSummary,
        "productDescription": productDescription,
        "premiumAmount": premiumAmount,
        "totalPremium": totalPremium,
        "validity": validity,
        "validityPeriod": validityPeriod,
        "promoCode": promoCode,
        "otherDiscount": otherDiscount,
        "otherFee": otherFee,
      };
}
