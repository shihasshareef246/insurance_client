import 'dart:io';

class FileModel {
  File file;
  String type;
  String date;
  String time;
  String extension;
  FileModel({
    required this.file,
    required this.type,
    this.date = '',
    this.time = '',
    this.extension = '',
  });

  factory FileModel.fromJson(Map<dynamic, dynamic> json) => FileModel(
        file: json["file"],
        type: json["type"],
        date: json["date"],
        time: json["time"],
        extension: json["extension"],
      );

  Map<String, dynamic> toJson() => {
        "file": file,
        "type": type,
        "date": date,
        "time": time,
        "extension": extension,
      };
}
