class RegisterModel {
  String customerId;

  RegisterModel({
    this.customerId = '',
  });

  factory RegisterModel.fromJson(Map<dynamic, dynamic> json) => RegisterModel(
        customerId: json["customerId"],
      );

  Map<String, dynamic> toJson() => {
        "customerId": customerId,
      };
}
