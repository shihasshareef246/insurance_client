import 'dart:async';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';

import 'package:gt_test/providers/documents_provider.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/providers/customer_provider.dart';
import 'package:gt_test/providers/dashboard_provider.dart';
import 'package:gt_test/providers/profile_provider.dart';
import 'package:gt_test/providers/payments_provider.dart';
import 'common/constants.dart';
import 'common/environment.dart';
import 'common/responsiveness.dart';
import 'common/routes/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runZonedGuarded(() {
    runApp(InsuranceClientApp());
  }, (error, stackTrace) {});
}

final navigatorKey = GlobalKey<NavigatorState>();

class InsuranceClientApp extends StatefulWidget {
  @override
  _InsuranceClientAppState createState() => _InsuranceClientAppState();
}

class _InsuranceClientAppState extends State<InsuranceClientApp> {
  @override
  void initState() {
    super.initState();
    AppEnvironment().setEnvironment();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: AppColors.primaryBlue));
    Responsiveness.init();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => DashboardProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => CustomerProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => ProfileProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => DocumentsProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => PaymentsProvider(),
        ),
      ],
      child: MaterialApp(
        title: AppConstants.APP_TITLE,
        debugShowCheckedModeBanner: false,
        navigatorKey: navigatorKey,
        navigatorObservers: [],
        initialRoute: AppRouter.initScreen,
        routes: AppRouter.setUpRoutes,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        builder: (context, child) {
          return child!;
        },
      ),
    );
  }
}
