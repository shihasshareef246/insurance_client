import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:dropdown_search/dropdown_search.dart';

import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/models/motor_insurance_model.dart';
import 'package:gt_test/screens/motor_documents_upload_screen.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import '../common/buttons/primary_button.dart';
import '../common/images.dart';
import '../common/popup/menu_popup.dart';
import '../models/discount_plan_model.dart';
import '../providers/customer_provider.dart';
import '../providers/dashboard_provider.dart';
import '../services/motor_service.dart';
import '../widgets/motor_additional_cover_bottom_sheet_widget.dart';
import '../widgets/insurance_details_bottom_sheet_widget.dart';

class SearchInsurancesScreen extends StatefulWidget {
  final DiscountPlanModel? discountData;
  final String type;
  const SearchInsurancesScreen({
    this.discountData,
    required this.type,
  });
  @override
  _SearchInsurancesScreenState createState() => _SearchInsurancesScreenState();
}

class _SearchInsurancesScreenState extends State<SearchInsurancesScreen> {
  DashboardProvider dashboardProvider = DashboardProvider();
  CustomerProvider customerProvider = CustomerProvider();
  TextEditingController searchController = TextEditingController();
  FocusNode searchFocus = FocusNode();
  FocusNode yearDropdownFocus = FocusNode();
  bool searchValid = false;
  bool searchChanged = false;
  bool isLoading = false;
  bool isSorted = true;
  bool isFiltered = false;
  // bool isSearched = false;
  List<MotorInsuranceModel?> searchList = [];
  //List<MotorInsuranceModel?> filterList = [];
  String? planType = 'TPL';
  String? brand = 'TOYOTA COROLA 516';
  String? makeYear = DateTime.now().year.toInt().toString();
  FocusNode seatsDropdownFocus = FocusNode();
  TextEditingController seatsController = TextEditingController();
  FocusNode seatsFocus = FocusNode();
  TextEditingController yearController =
      TextEditingController(text: DateTime.now().year.toInt().toString());
  FocusNode yearFocus = FocusNode();
  TextEditingController valueController = TextEditingController(text: '6000');
  FocusNode valueFocus = FocusNode();
  String? seats = '5';
  String? selectedValue;
  int selectedIndex = -1;

  @override
  void initState() {
    dashboardProvider = Provider.of<DashboardProvider>(context, listen: false);
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    searchList = dashboardProvider.motorInsuranceList;
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      dashboardProvider.setAdditionalMotorInsuranceModel =
          await MotorService()
              .getAdditionalMotorPlans(context: context, productType: "Tazur");
      dashboardProvider.setBrandsList =
          await MotorService().getMakeModel(context: context);
    });
  }

  List<MotorInsuranceModel> searchProduct({
    required String qry,
    required BuildContext context,
  }) {
    List<MotorInsuranceModel> queryList = [];
    for (var insurance in dashboardProvider.motorInsuranceList) {
      if ((insurance?.productDescription.contains(qry) ?? false) ||
          (insurance?.productDescription.contains(qry.toUpperCase()) ??
              false) ||
          (insurance?.productDescription.contains(qry.toLowerCase()) ??
              false) ||
          (insurance?.productType.contains(qry) ?? false) ||
          (insurance?.productType.contains(qry.toUpperCase()) ?? false) ||
          (insurance?.productType.contains(qry.toLowerCase()) ?? false) ||
          (insurance?.validity.toString().contains(qry) ?? false) ||
          (insurance?.validity.toString().contains(qry.toUpperCase()) ??
              false) ||
          (insurance?.validity.toString().contains(qry.toLowerCase()) ??
              false) ||
          (insurance?.premiumAmount.contains(qry) ?? false) ||
          (insurance?.premiumAmount.contains(qry.toUpperCase()) ?? false) ||
          (insurance?.premiumAmount.contains(qry.toLowerCase()) ?? false) ||
          (insurance?.productSummary.contains(qry) ?? false) ||
          (insurance?.productSummary.contains(qry.toUpperCase()) ?? false) ||
          (insurance?.productSummary.contains(qry.toLowerCase()) ?? false) ||
          (insurance?.promoDiscountText.contains(qry) ?? false) ||
          (insurance?.promoDiscountText.contains(qry.toUpperCase()) ?? false) ||
          (insurance?.promoDiscountText.contains(qry.toLowerCase()) ?? false) ||
          (insurance?.promoDiscount.contains(qry) ?? false) ||
          (insurance?.promoDiscount.contains(qry.toUpperCase()) ?? false) ||
          (insurance?.promoDiscount.contains(qry.toLowerCase()) ?? false)) {
        queryList.add(insurance!);
      }
    }
    return queryList;
  }

  List<String?> getDropdownValues(String type) {
    List<String?> values = [];
    switch (type) {
      case 'planType':
        values = ['TPL', 'Comprehensive'];
        break;
      case 'brand':
        dashboardProvider.brandList.forEach((element) {
          values.add(element?.makeModel);
        });
        break;
      case 'makeYear':
        values = [
          '2023',
          '2022',
          '2021',
          '2020',
          '2019',
          '2018',
          '2017',
          '2016',
          '2015',
          '2014',
          '2013',
        ];
        break;
      case 'seats':
        values = ['1', '2', '3', '4', '5', '6', '7'];
        break;
    }
    return values;
  }

  // void resetFilters() async {
  //   setState(() {
  //     //  planType = 'TPL';
  //     widget.discountData?.premiumAmount = '';
  //     brand = null;
  //     makeYear = null;
  //     seats = null;
  //     valueController.clear();
  //     yearController.clear();
  //     // searchList = dashboardProvider.motorInsuranceList;
  //     selectedIndex = -1;
  //   });
  // }

  void resetFilters() async {
    setState(() {
      widget.discountData?.premiumAmount = '';
      if (planType == 'TPL') {
        yearController.text = DateTime.now().year.toInt().toString();
        seats = '5';
        brand = null;
        makeYear = null;
        valueController.clear();
      } else {
        valueController.text = '6000';
        brand = 'TOYOTA COROLA 516';
        makeYear = DateTime.now().year.toInt().toString();
        seats = null;
        yearController.clear();
      }
      selectedIndex = -1;
    });
  }

  void sortByPrices() async {
    setState(() {
      if (isSorted) {
        searchList
            .sort((a, b) => (b!.premiumAmount).compareTo(a!.premiumAmount));
      } else {
        searchList
            .sort((a, b) => (a!.premiumAmount).compareTo(b!.premiumAmount));
      }
      isSorted = !isSorted;
      selectedIndex = -1;
    });
  }

  void onFilter(String type) async {
    var result;
    customerProvider.resetSelectedMotorInsuranceList();
    if (type == 'TPL') {
      result = await MotorService().getMotorInsurancePlans(
          context: context,
          makeYear: int.parse(yearController.text ?? ''),
          noOfSeats: int.parse(seats ?? ''),
          planType: type);
    } else {
      result = await MotorService().getMotorInsurancePlans(
          context: context,
          value: valueController.text.isEmpty
              ? 0
              : int.parse(valueController.text),
          makeYear: int.parse(makeYear ?? ''),
          makeModel: brand ?? '',
          planType: planType ?? '');
    }
    setState(() {
      isFiltered = true;
      selectedIndex = -1;
      searchList = result;
    });
  }

  void onSelectedItem(String type, String value) async {
    var result;
    setState(() {
      isFiltered = true;
      switch (type) {
        case 'planType':
          this.planType = value;
          if (planType == 'TPL') {
            valueController.clear();
          } else {
            seatsController.clear();
          }
          break;
        case 'brand':
          this.brand = value;
          break;
        case 'makeYear':
          this.makeYear = value;
          break;
        case 'seats':
          this.seats = value;
          break;
      }
    });
    if (type == 'planType') {
      if (value == 'TPL') {
        result = await MotorService().getMotorInsurancePlans(
            context: context,
            makeYear: DateTime.now().year.toInt(),
            noOfSeats: 5,
            planType: value);
      } else {
        result = await MotorService().getMotorInsurancePlans(
            context: context,
            value: 6000,
            makeYear: DateTime.now().year.toInt(),
            makeModel: 'TOYOTA COROLA 516',
            planType: value);
      }
    } else {
      if (makeYear == null || makeYear == 'Year') {
        if (seats == null || seats == 'Seats') {
          result = await MotorService().getMotorInsurancePlans(
              context: context,
              value: valueController.text.isEmpty
                  ? 0
                  : int.parse(valueController.text),
              makeModel: brand ?? '',
              planType: planType ?? '');
        } else {
          result = await MotorService().getMotorInsurancePlans(
              context: context,
              value: valueController.text.isEmpty
                  ? 0
                  : int.parse(valueController.text),
              makeModel: brand ?? '',
              noOfSeats: int.parse(seats ?? ''),
              planType: planType ?? '');
        }
      } else {
        if (seats == null || seats == 'Seats') {
          result = await MotorService().getMotorInsurancePlans(
              context: context,
              value: valueController.text.isEmpty
                  ? 0
                  : int.parse(valueController.text),
              makeModel: brand ?? '',
              makeYear: int.parse(makeYear ?? ''),
              planType: planType ?? '');
        } else {
          result = await MotorService().getMotorInsurancePlans(
              context: context,
              value: valueController.text.isEmpty
                  ? 0
                  : int.parse(valueController.text),
              makeModel: brand ?? '',
              makeYear: int.parse(makeYear ?? ''),
              noOfSeats: int.parse(seats ?? ''),
              planType: planType ?? '');
        }
      }
    }
    setState(() {
      searchList = result;
    });
  }

  void showMenu({required String type, required BuildContext context}) async {
    List<String?> values = getDropdownValues(type);
    MenuPopUp(
      rightPadding: 0.0.s,
      bottomPadding: 20.0.s,
      crossIconRequired: false,
      backgroundColor: AppColors.white,
      childcontainer: StatefulBuilder(
        builder: (BuildContext orderContext, StateSetter setModalState) {
          return Container(
            height: (values.length * 50).h,
            padding: EdgeInsets.symmetric(horizontal: 16.0.s, vertical: 10.0.s),
            child: ListView.builder(
                padding: EdgeInsets.zero,
                itemCount: values.length,
                scrollDirection: Axis.vertical,
                itemBuilder: (context, index) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 40.0.h,
                        width: 40.0.w,
                        child: Radio(
                          value: values[index] ?? '',
                          groupValue: this.planType,
                          onChanged: (val) async {
                            setState(() {
                              setModalState(() {
                                switch (type) {
                                  case 'planType':
                                    this.planType = val.toString();
                                    if (planType == 'TPL') {
                                      valueController.clear();
                                    } else {
                                      seatsController.clear();
                                    }
                                    break;
                                  case 'brand':
                                    this.brand = val.toString();
                                    break;
                                  case 'makeYear':
                                    this.makeYear = val.toString();
                                    break;
                                  case 'seats':
                                    this.seats = val.toString();
                                    break;
                                }
                              });
                            });
                            var result;
                            if (makeYear == 'Year') {
                              if (seats == 'Seats') {
                                result = await MotorService()
                                    .getMotorInsurancePlans(
                                        context: context,
                                        value: valueController.text.isEmpty
                                            ? 0
                                            : int.parse(valueController.text),
                                        makeModel: brand!,
                                        planType: planType!);
                              } else {
                                result = await MotorService()
                                    .getMotorInsurancePlans(
                                        context: context,
                                        value: valueController.text.isEmpty
                                            ? 0
                                            : int.parse(valueController.text),
                                        makeModel: brand!,
                                        noOfSeats: int.parse(seats!),
                                        planType: planType!);
                              }
                            } else {
                              if (seats == 'Seats') {
                                result = await MotorService()
                                    .getMotorInsurancePlans(
                                        context: context,
                                        value: valueController.text.isEmpty
                                            ? 0
                                            : int.parse(valueController.text),
                                        makeModel: brand!,
                                        makeYear: int.parse(makeYear!),
                                        planType: planType!);
                              } else {
                                result = await MotorService()
                                    .getMotorInsurancePlans(
                                        context: context,
                                        value: valueController.text.isEmpty
                                            ? 0
                                            : int.parse(valueController.text),
                                        makeModel: brand!,
                                        makeYear: int.parse(makeYear!),
                                        noOfSeats: int.parse(seats!),
                                        planType: planType!);
                              }
                            }
                            setState(() {
                              searchList = result;
                            });
                            Navigations.pop(context);
                          },
                        ),
                      ),
                      SizedBox(
                        width: 3.0.w,
                      ),
                      Container(
                        width: 240.0.w,
                        child: Text(
                          values[index] ?? '',
                          style: buildAppTextTheme().headline6,
                          textAlign: TextAlign.start,
                          overflow: TextOverflow.fade,
                        ),
                      ),
                    ],
                  );
                }),
          );
        },
      ),
      actualWidth: 330.0.s,
      closeClick: () => {},
      iconColor: AppColors.white,
      barrierColor: Colors.transparent,
    ).showPopup(context);
  }

  @override
  Widget build(BuildContext context) {
    // filterList.clear();
    //  searchList.forEach((product) {
    //    if (product?.productType == ' ' || product?.productType == planType) {
    //      filterList.add(product);
    //    }
    //  });
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          bottomSheet: (widget.type == 'MOTOR' || widget.type == 'OFFER')
              ? Container(
                  color: AppColors.white,
                  padding: EdgeInsets.only(
                      left: 20.0.s, right: 20.0.s, top: 20.0.s, bottom: 30.0.s),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      if (planType == 'Comprehensive' &&
                          customerProvider
                              .selectedMotorInsuranceList.isNotEmpty)
                        Container(
                          padding: EdgeInsets.only(bottom: 30.0.s),
                          child: InkWell(
                            onTap: () => showModalBottomSheet(
                                backgroundColor:
                                    AppColors.primaryBlue.withOpacity(0.9),
                                barrierColor:
                                    AppColors.primaryBlue.withOpacity(0.9),
                                context: context,
                                builder: (BuildContext context) {
                                  return MotorAdditionalCoverBottomSheetWidget();
                                }),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'Additional Covers for you',
                                  style: buildAppTextTheme().headline4,
                                  textAlign: TextAlign.center,
                                ),
                                Image(
                                  image: AssetImage(AppImages.ADD_NEW),
                                  height: 32.0.h,
                                  width: 32.0.w,
                                ),
                              ],
                            ),
                          ),
                        ),
                      Center(
                        child: PrimaryButton(
                            height: 67.0.h,
                            width: 380.0.w,
                            color: AppColors.primaryBlue,
                            //  disabled: !(this.otpValid),
                            isLoading: isLoading,
                            onPressed: () async {
                              if (!isLoading) {
                                setState(() {
                                  isLoading = true;
                                });
                                Navigator.push(
                                  context,
                                  PageTransition(
                                    type: PageTransitionType.fade,
                                    child: MotorDocumentsUploadScreen(),
                                  ),
                                );
                                // Navigations.pushNamed(
                                //   context,
                                //   AppRouter.documentsUploadScreen,
                                // );
                                setState(() {
                                  isLoading = false;
                                });
                              }
                            },
                            text: 'Continue',
                            disabled: selectedIndex == -1,
                            borderRadius: 80.0.s),
                      ),
                    ],
                  ),
                )
              : SizedBox.shrink(),
          appBar: AppBar(
            backgroundColor: AppColors.primaryBlue,
            leading: InkWell(
              onTap: () => Navigations.pop(context),
              child: Icon(
                Icons.arrow_back_ios,
                color: AppColors.white,
                size: 20.0.ics,
              ),
            ),
            title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    widget.type == 'MOTOR'
                        ? 'Motor Insurance'
                        : widget.type == 'TRAVEL'
                            ? 'Travel Insurance'
                            : widget.type == 'MEDICAL'
                                ? 'Medical Insurance'
                                : 'Motor Insurance', //TODO
                    style: buildAppTextTheme()
                        .headline3
                        ?.copyWith(color: AppColors.white),
                    textAlign: TextAlign.start,
                  ),
                ]),
          ),
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: false,
          body: widget.type == 'OFFER'
              ? SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(
                        left: 8.0.s, right: 8.0.s, top: 20.0.s, bottom: 10.0.s),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 10.0.h,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 10.0.s),
                          child: Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  this.planType = 'TPL';
                                  resetFilters();
                                  onFilter('TPL');
                                  //  onSelectedItem('planType', 'TPL');
                                  valueController.clear();
                                },
                                child: Container(
                                  width: 194.0.w,
                                  height: 42.0.h,
                                  decoration: BoxDecoration(
                                    color: this.planType == 'TPL'
                                        ? AppColors.primaryBlue
                                        : AppColors.white,
                                    border: Border.all(
                                        width: 1.5.s,
                                        color: AppColors.primaryBlue),
                                    borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10.0.s),
                                      topLeft: Radius.circular(10.0.s),
                                    ),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'TPL',
                                      style: buildAppTextTheme()
                                          .headline6!
                                          .copyWith(
                                              color: this.planType == 'TPL'
                                                  ? AppColors.white
                                                  : AppColors.lightBlue),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  this.planType = 'Comprehensive';
                                  resetFilters();
                                  onFilter('Comprehensive');
                                  //    onSelectedItem('planType', 'Comprehensive');
                                  yearController.clear();
                                },
                                child: Container(
                                  width: 194.0.w,
                                  height: 42.0.h,
                                  decoration: BoxDecoration(
                                    color: this.planType == 'Comprehensive'
                                        ? AppColors.primaryBlue
                                        : AppColors.white,
                                    border: Border.all(
                                        width: 1.5.s,
                                        color: AppColors.primaryBlue),
                                    borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(10.0.s),
                                      topRight: Radius.circular(10.0.s),
                                    ),
                                  ),
                                  child: Center(
                                    child: Text(
                                      'Comprehensive',
                                      style: buildAppTextTheme()
                                          .headline6!
                                          .copyWith(
                                              color: this.planType ==
                                                      'Comprehensive'
                                                  ? AppColors.white
                                                  : AppColors.lightBlue),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 19.0.h,
                        ),
                        Container(
                          height: 90.0.h,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: [
                              SizedBox(
                                width: 12.0.w,
                              ),
                              planType == 'TPL'
                                  ? Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              'Enter Manufacture Year',
                                              style: buildAppTextTheme()
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: AppColors
                                                          .primaryBlue),
                                            ),
                                            SizedBox(height: 8.0.h),
                                            Container(
                                              height: 56.0.h,
                                              width: 120.0.w,
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 4.0.s,
                                                  horizontal: 10.0.s),
                                              decoration: BoxDecoration(
                                                // color: (yearFocus.hasFocus ||
                                                //         yearController.text.isNotEmpty)
                                                //     ? AppColors.white
                                                //     : AppColors.primaryBlue,
                                                color: AppColors.primaryBlue,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        67.0.s),
                                                border: Border.all(
                                                    width: 1.0.s,
                                                    color:
                                                        AppColors.primaryBlue),
                                              ),
                                              child: Center(
                                                child: TextFormField(
                                                  textAlign: TextAlign.center,
                                                  inputFormatters: [
                                                    FilteringTextInputFormatter
                                                        .digitsOnly,
                                                    LengthLimitingTextInputFormatter(
                                                        7),
                                                  ],
                                                  onFieldSubmitted: (_) =>
                                                      FocusScope.of(context)
                                                          .requestFocus(
                                                              seatsDropdownFocus),
                                                  // focus to next
                                                  // scrollPadding: EdgeInsets.only(bottom: 100),
                                                  focusNode: yearFocus,
                                                  // style: yearFocus.hasFocus
                                                  //     ? buildAppTextTheme().headline5
                                                  //     : buildAppTextTheme()
                                                  //         .subtitle1!
                                                  //         .copyWith(color: AppColors.black),
                                                  style: buildAppTextTheme()
                                                      .headline5!
                                                      .copyWith(
                                                          color:
                                                              AppColors.white),
                                                  keyboardType:
                                                      TextInputType.number,
                                                  controller: yearController,
                                                  obscureText: false,
                                                  decoration: InputDecoration(
                                                    isDense: true,
                                                    // contentPadding: EdgeInsets.only(
                                                    //   left: 2.0.s,
                                                    //   top: 2.0.s,
                                                    //   bottom: 2.0.s,
                                                    //   right: 2.0.s,
                                                    // ),
                                                    // labelText: 'Year',
                                                    // labelStyle: (yearFocus.hasFocus ||
                                                    //         yearController.text.isNotEmpty)
                                                    //     ? buildAppTextTheme().subtitle2
                                                    //     : buildAppTextTheme()
                                                    //         .subtitle2!
                                                    //         .copyWith(color: AppColors.white),
                                                    border: InputBorder.none,
                                                  ),
                                                  onChanged: (value) async {
                                                    if (value.length == 4) {
                                                      onFilter('TPL');
                                                    } else {
                                                      setState(() {
                                                        searchList.clear();
                                                        selectedIndex = -1;
                                                      });
                                                    }
                                                    // var result;
                                                    // if (seats == null ||
                                                    //     seats == 'Seats') {
                                                    //   result = await CategoryService()
                                                    //       .getMotorInsurancePlans(
                                                    //           context: context,
                                                    //           makeYear: int.parse(
                                                    //               yearController.text),
                                                    //           planType: planType!);
                                                    // } else {
                                                    //   result = await CategoryService()
                                                    //       .getMotorInsurancePlans(
                                                    //           context: context,
                                                    //           makeYear: int.parse(
                                                    //               yearController.text),
                                                    //           noOfSeats:
                                                    //               int.parse(seats!),
                                                    //           planType: planType!);
                                                    // }
                                                    // setState(() {
                                                    //   searchList = result;
                                                    // });
                                                    //}
                                                  },
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          width: 12.0.w,
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              'Select No of Seats',
                                              style: buildAppTextTheme()
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: AppColors
                                                          .primaryBlue),
                                            ),
                                            SizedBox(height: 8.0.h),
                                            Container(
                                              height: 56.0.h,
                                              width: 120.0.w,
                                              decoration: BoxDecoration(
                                                color: AppColors.primaryBlue,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        67.0.s),
                                                border: Border.all(
                                                    width: 1.0.s,
                                                    color:
                                                        AppColors.primaryBlue),
                                              ),
                                              child: Center(
                                                child:
                                                    DropdownButtonHideUnderline(
                                                  child: DropdownButton2(
                                                    focusNode:
                                                        seatsDropdownFocus,
                                                    isExpanded: true,
                                                    // hint: Row(
                                                    //   children: [
                                                    //     Expanded(
                                                    //       child: Text(
                                                    //         'Seats',
                                                    //         style: buildAppTextTheme()
                                                    //             .headline6!
                                                    //             .copyWith(
                                                    //                 color:
                                                    //                     AppColors.white),
                                                    //         overflow:
                                                    //             TextOverflow.ellipsis,
                                                    //       ),
                                                    //     ),
                                                    //   ],
                                                    // ),
                                                    items: getDropdownValues(
                                                            'seats')
                                                        .map((item) =>
                                                            DropdownMenuItem<
                                                                String>(
                                                              value: item,
                                                              child: Text(
                                                                item!,
                                                                style: buildAppTextTheme()
                                                                    .headline6!
                                                                    .copyWith(
                                                                        color: AppColors
                                                                            .white),
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              ),
                                                            ))
                                                        .toList(),
                                                    value: this.seats,
                                                    onChanged: (value) {
                                                      this.seats =
                                                          value.toString();
                                                      onFilter('TPL');
                                                      // onSelectedItem(
                                                      //     'seats', value.toString());
                                                    },
                                                    icon: Icon(
                                                      Icons
                                                          .keyboard_arrow_down_outlined,
                                                      color: AppColors.white,
                                                      size: 24.0.ics,
                                                    ),
                                                    // iconSize: 14,
                                                    // iconEnabledColor: Colors.yellow,
                                                    // iconDisabledColor: Colors.grey,
                                                    buttonHeight: 50,
                                                    buttonWidth: 160,
                                                    buttonPadding:
                                                        const EdgeInsets.only(
                                                            left: 14,
                                                            right: 14),
                                                    buttonDecoration:
                                                        BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              67.0.s),
                                                      border: Border.all(
                                                          width: 1.0.s,
                                                          color: AppColors
                                                              .primaryBlue),
                                                      color:
                                                          AppColors.primaryBlue,
                                                    ),
                                                    buttonElevation: 2,
                                                    itemHeight: 40,
                                                    itemPadding:
                                                        const EdgeInsets.only(
                                                            left: 14,
                                                            right: 14),
                                                    dropdownMaxHeight: 200,
                                                    dropdownWidth: 150,
                                                    dropdownPadding: null,
                                                    dropdownDecoration:
                                                        BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              9.0.s),
                                                      color:
                                                          AppColors.primaryBlue,
                                                    ),
                                                    dropdownElevation: 8,
                                                    scrollbarRadius:
                                                        const Radius.circular(
                                                            40),
                                                    scrollbarThickness: 6,
                                                    scrollbarAlwaysShow: true,
                                                    offset: const Offset(0, 0),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    )
                                  : Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              'Enter Car Value KWD',
                                              style: buildAppTextTheme()
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: AppColors
                                                          .primaryBlue),
                                            ),
                                            SizedBox(height: 8.0.h),
                                            Container(
                                              height: 56.0.h,
                                              width: 120.0.w,
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 4.0.s,
                                                  horizontal: 10.0.s),
                                              decoration: BoxDecoration(
                                                // color: (valueFocus.hasFocus ||
                                                //         valueController.text.isNotEmpty)
                                                //     ? AppColors.white
                                                //     : AppColors.primaryBlue,
                                                color: AppColors.primaryBlue,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        67.0.s),
                                                border: Border.all(
                                                    width: 1.0.s,
                                                    color:
                                                        AppColors.primaryBlue),
                                              ),
                                              child: Center(
                                                child: TextFormField(
                                                  textAlign: TextAlign.center,
                                                  inputFormatters: [
                                                    FilteringTextInputFormatter
                                                        .digitsOnly,
                                                    LengthLimitingTextInputFormatter(
                                                        7),
                                                  ], // focus to next
                                                  onFieldSubmitted: (_) =>
                                                      FocusScope.of(context)
                                                          .requestFocus(
                                                              yearDropdownFocus),

                                                  // scrollPadding:
                                                  //     EdgeInsets.only(bottom: 100),
                                                  focusNode: valueFocus,
                                                  // style: valueFocus.hasFocus
                                                  //     ? buildAppTextTheme().headline5
                                                  //     : buildAppTextTheme()
                                                  //         .subtitle1!
                                                  //         .copyWith(
                                                  //             color: AppColors.black),
                                                  style: buildAppTextTheme()
                                                      .subtitle1!
                                                      .copyWith(
                                                          color:
                                                              AppColors.white),
                                                  keyboardType:
                                                      TextInputType.number,
                                                  controller: valueController,
                                                  obscureText: false,
                                                  decoration: InputDecoration(
                                                    isDense: true,
                                                    // contentPadding: EdgeInsets.only(
                                                    //   left: 2.0.s,
                                                    //   top: 2.0.s,
                                                    //   bottom: 2.0.s,
                                                    //   right: 2.0.s,
                                                    // ),
                                                    // labelText: 'Value',
                                                    // labelStyle: (valueFocus.hasFocus ||
                                                    //         valueController
                                                    //             .text.isNotEmpty)
                                                    //     ? buildAppTextTheme().subtitle2
                                                    //     : buildAppTextTheme()
                                                    //         .subtitle2!
                                                    //         .copyWith(
                                                    //             color: AppColors.white),
                                                    border: InputBorder.none,
                                                  ),
                                                  onChanged: (value) async {
                                                    onFilter('Comprehensive');
                                                    // var result;
                                                    // if (makeYear == null ||
                                                    //     makeYear == 'Year') {
                                                    //   if (seats == null ||
                                                    //       seats == 'Seats') {
                                                    //     result = await CategoryService()
                                                    //         .getMotorInsurancePlans(
                                                    //             context: context,
                                                    //             value: valueController
                                                    //                     .text.isEmpty
                                                    //                 ? 0
                                                    //                 : int.parse(
                                                    //                     valueController
                                                    //                         .text),
                                                    //             makeModel: brand ?? '',
                                                    //             planType: planType!);
                                                    //   } else {
                                                    //     result = await CategoryService()
                                                    //         .getMotorInsurancePlans(
                                                    //             context: context,
                                                    //             value: valueController
                                                    //                     .text.isEmpty
                                                    //                 ? 0
                                                    //                 : int.parse(
                                                    //                     valueController
                                                    //                         .text),
                                                    //             makeModel: brand!,
                                                    //             noOfSeats:
                                                    //                 int.parse(seats!),
                                                    //             planType: planType!);
                                                    //   }
                                                    // } else {
                                                    //   if (seats == null ||
                                                    //       seats == 'Seats') {
                                                    //     result = await CategoryService()
                                                    //         .getMotorInsurancePlans(
                                                    //             context: context,
                                                    //             value: valueController
                                                    //                     .text.isEmpty
                                                    //                 ? 0
                                                    //                 : int.parse(
                                                    //                     valueController
                                                    //                         .text),
                                                    //             makeModel: brand!,
                                                    //             makeYear:
                                                    //                 int.parse(makeYear!),
                                                    //             planType: planType!);
                                                    //   } else {
                                                    //     result = await CategoryService()
                                                    //         .getMotorInsurancePlans(
                                                    //             context: context,
                                                    //             value: valueController
                                                    //                     .text.isEmpty
                                                    //                 ? 0
                                                    //                 : int.parse(
                                                    //                     valueController
                                                    //                         .text),
                                                    //             makeModel: brand!,
                                                    //             makeYear:
                                                    //                 int.parse(makeYear!),
                                                    //             noOfSeats:
                                                    //                 int.parse(seats!),
                                                    //             planType: planType!);
                                                    //   }
                                                    // }
                                                    // setState(() {
                                                    //   searchList = result;
                                                    // });
                                                  },
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          width: 8.0.w,
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              'Select Manufacture Year',
                                              style: buildAppTextTheme()
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: AppColors
                                                          .primaryBlue),
                                            ),
                                            SizedBox(height: 8.0.h),
                                            Container(
                                              height: 56.0.h,
                                              width: 120.0.w,
                                              decoration: BoxDecoration(
                                                color: AppColors.primaryBlue,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        67.0.s),
                                                border: Border.all(
                                                    width: 1.0.s,
                                                    color:
                                                        AppColors.primaryBlue),
                                              ),
                                              child: Center(
                                                child:
                                                    DropdownButtonHideUnderline(
                                                  child: DropdownButton2(
                                                    focusNode:
                                                        yearDropdownFocus,
                                                    isExpanded: true,
                                                    // hint: Row(
                                                    //   children: [
                                                    //     Expanded(
                                                    //       child: Text(
                                                    //         'Year',
                                                    //         style: buildAppTextTheme()
                                                    //             .headline6!
                                                    //             .copyWith(
                                                    //                 color: AppColors.white),
                                                    //         overflow: TextOverflow.ellipsis,
                                                    //       ),
                                                    //     ),
                                                    //   ],
                                                    // ),
                                                    items: getDropdownValues(
                                                            'makeYear')
                                                        .map((item) =>
                                                            DropdownMenuItem<
                                                                String>(
                                                              value: item,
                                                              child: Text(
                                                                item!,
                                                                style: buildAppTextTheme()
                                                                    .headline6!
                                                                    .copyWith(
                                                                        color: AppColors
                                                                            .white),
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              ),
                                                            ))
                                                        .toList(),
                                                    value: this.makeYear,
                                                    onChanged: (value) {
                                                      makeYear =
                                                          value.toString();
                                                      onFilter('Comprehensive');
                                                      // onSelectedItem(
                                                      //     'makeYear', value.toString());
                                                      FocusScope.of(context)
                                                          .requestFocus(
                                                              searchFocus);
                                                    },
                                                    icon: Icon(
                                                      Icons
                                                          .keyboard_arrow_down_outlined,
                                                      color: AppColors.white,
                                                      size: 24.0.ics,
                                                    ),
                                                    // iconSize: 14,
                                                    // iconEnabledColor: Colors.yellow,
                                                    // iconDisabledColor: Colors.grey,
                                                    buttonHeight: 50,
                                                    buttonWidth: 160,
                                                    buttonPadding:
                                                        const EdgeInsets.only(
                                                            left: 14,
                                                            right: 14),
                                                    buttonDecoration:
                                                        BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              67.0.s),
                                                      border: Border.all(
                                                          width: 1.0.s,
                                                          color: AppColors
                                                              .primaryBlue),
                                                      color:
                                                          AppColors.primaryBlue,
                                                    ),
                                                    buttonElevation: 2,
                                                    itemHeight: 40,
                                                    itemPadding:
                                                        const EdgeInsets.only(
                                                            left: 14,
                                                            right: 14),
                                                    dropdownMaxHeight: 200,
                                                    dropdownWidth: 100,
                                                    dropdownPadding: null,
                                                    dropdownDecoration:
                                                        BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              9.0.s),
                                                      color:
                                                          AppColors.primaryBlue,
                                                    ),
                                                    dropdownElevation: 8,
                                                    scrollbarRadius:
                                                        const Radius.circular(
                                                            40),
                                                    scrollbarThickness: 6,
                                                    scrollbarAlwaysShow: true,
                                                    offset: const Offset(0, 0),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          width: 8.0.w,
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'Enter Make & Model',
                                              style: buildAppTextTheme()
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: AppColors
                                                          .primaryBlue),
                                            ),
                                            SizedBox(height: 8.0.h),
                                            Container(
                                              height: 56.0.h,
                                              width: 250.0.w,
                                              padding: EdgeInsets.only(
                                                  left: 20.0.s, right: 20.0.s),
                                              decoration: BoxDecoration(
                                                color: AppColors.primaryBlue,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        24.0.s),
                                                border: Border.all(
                                                    width: 1.0.s,
                                                    color:
                                                        AppColors.primaryBlue),
                                              ),
                                              child: Center(
                                                child: Theme(
                                                  data: ThemeData(
                                                    textTheme: TextTheme(
                                                        subtitle1: TextStyle(
                                                            color: AppColors
                                                                .white)),
                                                  ),
                                                  child:
                                                      DropdownSearch<String?>(
                                                    mode: Mode.MENU,
                                                    popupBackgroundColor:
                                                        AppColors.primaryBlue,
                                                    dropdownButtonProps:
                                                        IconButtonProps(
                                                      focusNode: searchFocus,
                                                      icon: Icon(
                                                        Icons
                                                            .keyboard_arrow_down_outlined,
                                                        color: AppColors.white,
                                                        size: 24.0.ics,
                                                      ),
                                                    ),
                                                    showSearchBox: true,
                                                    searchFieldProps:
                                                        TextFieldProps(
                                                      decoration:
                                                          InputDecoration(
                                                              fillColor: AppColors
                                                                  .greyishBlack,
                                                              border:
                                                                  OutlineInputBorder(),
                                                              // contentPadding: EdgeInsets.fromLTRB(
                                                              //     12, 8, 12, 8),
                                                              labelText:
                                                                  "Search",
                                                              labelStyle: TextStyle(
                                                                  color: AppColors
                                                                      .white)),
                                                    ),
                                                    //showSelectedItems: true,
                                                    dropdownSearchDecoration:
                                                        InputDecoration(
                                                            // labelText: this.brand ?? 'Make & Model',
                                                            // labelStyle: buildAppTextTheme()
                                                            //     .headline4!
                                                            //     .copyWith(color: AppColors.white),
                                                            ),
                                                    items: getDropdownValues(
                                                            'brand')
                                                        .map((item) => item!)
                                                        .toList(),
                                                    selectedItem:
                                                        this.brand ?? '',
                                                    // value: this.seats,
                                                    onChanged: (value) {
                                                      this.brand = value;
                                                      onFilter('Comprehensive');
                                                      // onSelectedItem(
                                                      //     'brand', value.toString());
                                                    },
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20.0.h,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 10.0.h),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      resetFilters();
                                      onFilter(planType!);
                                    },
                                    child: Text(
                                      'Reset Filter',
                                      style: buildAppTextTheme()
                                          .headline6!
                                          .copyWith(
                                              color: AppColors.primaryBlue),
                                      overflow: TextOverflow.fade,
                                    ),
                                  ),
                                  SizedBox(width: 4.0.w),
                                  InkWell(
                                    onTap: () {
                                      resetFilters();
                                      onFilter(planType!);
                                    },
                                    child: Icon(
                                      Icons.replay,
                                      color: AppColors.primaryBlue,
                                      size: 18.0.ics,
                                    ),
                                  ),
                                ],
                              ),
                              InkWell(
                                onTap: () => sortByPrices(),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Sort on price',
                                      style: buildAppTextTheme()
                                          .headline6!
                                          .copyWith(
                                              color: AppColors.primaryBlue),
                                      overflow: TextOverflow.fade,
                                    ),
                                    SizedBox(width: 4.0.w),
                                    Image(
                                      image: AssetImage(AppImages.SORT),
                                      color: AppColors.primaryBlue,
                                      height: 24.0.h,
                                      width: 24.0.w,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10.0.h,
                        ),
                        // isSearched &&
                        (!isFiltered)
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 2.0.w),
                                    height: MediaQuery.of(context).size.height *
                                        0.45,
                                    width: MediaQuery.of(context).size.width,
                                    child: ListView.builder(
                                        physics:
                                            AlwaysScrollableScrollPhysics(),
                                        // shrinkWrap: true,
                                        itemCount: dashboardProvider
                                            .discountPlanList.length,
                                        scrollDirection: Axis.vertical,
                                        itemBuilder: (context, index) {
                                          return Container(
                                            padding:
                                                EdgeInsets.only(bottom: 14.0.s),
                                            child: Container(
                                              height: 115.0.h,
                                              child: InkWell(
                                                onTap: () async {
                                                  FocusScope.of(context)
                                                      .unfocus();
                                                  // searchList[index]
                                                  //     ?.planType =
                                                  // planType!;
                                                  dashboardProvider
                                                          .setAdditionalMotorInsuranceModel =
                                                      await MotorService()
                                                          .getAdditionalMotorPlans(
                                                              context: context,
                                                              productType: dashboardProvider
                                                                      .discountPlanList[
                                                                          index]
                                                                      ?.productType ??
                                                                  '');
                                                  setState(() {
                                                    selectedIndex = index;
                                                    customerProvider
                                                        .resetSelectedMotorInsuranceList();
                                                    customerProvider
                                                        .resetInsurerMotorDetails();
                                                    customerProvider
                                                        .selectedMotorInsuranceList
                                                        .add(
                                                            MotorInsuranceModel(
                                                      productType: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.productType ??
                                                          '',
                                                      productSummary: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.productSummary ??
                                                          '',
                                                      productDescription:
                                                          dashboardProvider
                                                                  .discountPlanList[
                                                                      index]
                                                                  ?.productDescription ??
                                                              '',
                                                      premiumAmount: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.premiumAmount ??
                                                          '0',
                                                      promoCode: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.promoCode ??
                                                          '',
                                                      promoDiscount: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.promoDiscount ??
                                                          '0',
                                                      promoDiscountText:
                                                          dashboardProvider
                                                                  .discountPlanList[
                                                                      index]
                                                                  ?.promoDiscountText ??
                                                              '',
                                                      totalPremium: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.totalPremium ??
                                                          '0',
                                                      otherFee: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.otherFee ??
                                                          '0',
                                                      validity: int.parse(
                                                          dashboardProvider
                                                                  .discountPlanList[
                                                                      index]
                                                                  ?.validity ??
                                                              '0'),
                                                      validityPeriod: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.validityPeriod ??
                                                          '',
                                                      planid: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.planId ??
                                                          0,
                                                      planType: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.planType ??
                                                          '',
                                                    ));
                                                    if (planType == 'TPL') {
                                                      customerProvider
                                                              ?.insuranceMotorDetails
                                                              ?.makeYear =
                                                          yearController.text;
                                                    } else if (makeYear !=
                                                        null) {
                                                      customerProvider
                                                          ?.insuranceMotorDetails
                                                          ?.makeYear = makeYear!;
                                                    }
                                                    if (seats != null) {
                                                      customerProvider
                                                          ?.insuranceMotorDetails
                                                          ?.noOfSeats = seats!;
                                                    }
                                                    customerProvider
                                                            ?.insuranceMotorDetails
                                                            ?.carValue =
                                                        valueController.text!;
                                                    if (brand != null) {
                                                      customerProvider
                                                          ?.insuranceMotorDetails
                                                          ?.makeModel = brand!;
                                                    }
                                                  });
                                                },
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      //  height: 96.0.h,
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.2,
                                                      decoration: BoxDecoration(
                                                        color: selectedIndex ==
                                                                index
                                                            ? AppColors.border
                                                                .withOpacity(
                                                                    0.5)
                                                            : AppColors.white,
                                                        border: Border(
                                                          top: BorderSide(
                                                              color: AppColors
                                                                  .border,
                                                              width: 1.0),
                                                          bottom: BorderSide(
                                                              color: AppColors
                                                                  .border,
                                                              width: 1.0),
                                                          left: BorderSide(
                                                              color: AppColors
                                                                  .border,
                                                              width: 1.0),
                                                          right: BorderSide(
                                                              color: AppColors
                                                                  .border,
                                                              width: 1.0),
                                                        ),
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  10.0.s),
                                                          topLeft:
                                                              Radius.circular(
                                                                  10.0.s),
                                                        ),
                                                      ),
                                                      child: Center(
                                                        child: Image(
                                                          image: AssetImage(
                                                              dashboardProvider
                                                                          .discountPlanList[
                                                                              index]
                                                                          ?.productType ==
                                                                      'Gulf Trust'
                                                                  ? AppImages
                                                                      .SPLASH_SCREEN
                                                                  : dashboardProvider
                                                                              .discountPlanList[
                                                                                  index]
                                                                              ?.productType ==
                                                                          'Tazur'
                                                                      ? AppImages
                                                                          .TAZUR
                                                                      : dashboardProvider.discountPlanList[index]?.productType ==
                                                                              'Burgan'
                                                                          ? AppImages
                                                                              .BURGAN
                                                                          : dashboardProvider.discountPlanList[index]?.productType == 'NLGIC'
                                                                              ? AppImages.NATIONAL
                                                                              : AppImages.WARBA
                                                              // widget.isSelected
                                                              // ? AppImages.SELECTED_PLAN
                                                              // : AppImages.PLAN
                                                              ),
                                                          height: 63.0.h,
                                                          width: 63.0.w,
                                                          fit: BoxFit.fill,
                                                        ),
                                                      ),
                                                    ),
                                                    Stack(
                                                      children: [
                                                        Container(
                                                          // height: 96.0.h,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.55,
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 8.0.s,
                                                                  top: 10.0.s,
                                                                  bottom: 6.0.s,
                                                                  right: 8.0.s),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: selectedIndex ==
                                                                    index
                                                                ? AppColors
                                                                    .border
                                                                    .withOpacity(
                                                                        0.5)
                                                                : AppColors
                                                                    .white,
                                                            border: Border(
                                                              top: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              bottom: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              left: BorderSide(
                                                                  color: selectedIndex == index
                                                                      ? AppColors
                                                                          .primaryBlue
                                                                      : AppColors
                                                                          .border,
                                                                  width: 0.0),
                                                              right: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                            ),
                                                          ),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Column(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Container(
                                                                    width:
                                                                        200.0.w,
                                                                    child: Text(
                                                                      dashboardProvider
                                                                              .discountPlanList[index]
                                                                              ?.productSummary
                                                                              .trim() ??
                                                                          '',
                                                                      style: buildAppTextTheme()
                                                                          .subtitle2!
                                                                          .copyWith(
                                                                              color: AppColors.primaryBlue),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .left,
                                                                      maxLines:
                                                                          1,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                        10.0.h,
                                                                  ),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        '${dashboardProvider.discountPlanList[index]?.validity} ${dashboardProvider.discountPlanList[index]?.validityPeriod}',
                                                                        style: buildAppTextTheme()
                                                                            .caption!
                                                                            .copyWith(color: AppColors.primaryBlue),
                                                                        textAlign:
                                                                            TextAlign.left,
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                      ),
                                                                      SizedBox(
                                                                          width:
                                                                              5.0.w),
                                                                      if (dashboardProvider.discountPlanList[index]?.productType !=
                                                                              null &&
                                                                          dashboardProvider.discountPlanList[index]?.productType.trim() !=
                                                                              '')
                                                                        Container(
                                                                          height:
                                                                              16.0.h,
                                                                          child:
                                                                              VerticalDivider(
                                                                            width:
                                                                                2.0.w,
                                                                            thickness:
                                                                                2.0.w,
                                                                            color:
                                                                                AppColors.border,
                                                                          ),
                                                                        ),
                                                                      SizedBox(
                                                                          width:
                                                                              7.0.w),
                                                                      Text(
                                                                        dashboardProvider.discountPlanList[index]?.productType ??
                                                                            '',
                                                                        style: buildAppTextTheme()
                                                                            .caption!
                                                                            .copyWith(color: AppColors.primaryBlue),
                                                                        textAlign:
                                                                            TextAlign.left,
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                        9.0.h,
                                                                  ),
                                                                  InkWell(
                                                                    onTap: () => showModalBottomSheet(
                                                                        isScrollControlled: true,
                                                                        backgroundColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                        barrierColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                        context: context,
                                                                        builder: (BuildContext context) {
                                                                          return InsuranceDetailsBottomSheetWidget(
                                                                              index: dashboardProvider.discountPlanList[index]?.productType == 'Tazur'
                                                                                  ? 0
                                                                                  : dashboardProvider.discountPlanList[index]?.productType == 'Burgan'
                                                                                      ? 1
                                                                                      : dashboardProvider.discountPlanList[index]?.productType == 'NLGIC'
                                                                                          ? 2
                                                                                          : dashboardProvider.discountPlanList[index]?.productType == 'Warba'
                                                                                              ? 3
                                                                                              : 4);
                                                                        }),
                                                                    child: Row(
                                                                      children: [
                                                                        Text(
                                                                          'Information',
                                                                          style: buildAppTextTheme()
                                                                              .caption!
                                                                              .copyWith(color: AppColors.orange),
                                                                          textAlign:
                                                                              TextAlign.right,
                                                                        ),
                                                                        SizedBox(
                                                                          width:
                                                                              8.0.w,
                                                                        ),
                                                                        Image(
                                                                          image:
                                                                              AssetImage(AppImages.INFO),
                                                                          height:
                                                                              18.0.h,
                                                                          width:
                                                                              18.0.w,
                                                                          color:
                                                                              AppColors.orange,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Positioned(
                                                          bottom: 0.0.s,
                                                          right: 0.0.s,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Text(
                                                                'Promo Code',
                                                                style: buildAppTextTheme()
                                                                    .bodyText1!
                                                                    .copyWith(
                                                                        color: AppColors
                                                                            .brownColor),
                                                                textAlign:
                                                                    TextAlign
                                                                        .start,
                                                              ),
                                                              SizedBox(
                                                                width: 40.0.w,
                                                              ),
                                                              Container(
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color: AppColors
                                                                      .skyBlue,
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    bottomRight:
                                                                        Radius.circular(
                                                                            6.0.s),
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            8.0.s),
                                                                  ),
                                                                ),
                                                                height: 40.0.h,
                                                                width: 50.0.w,
                                                                child: Center(
                                                                  child: Text(
                                                                    '${dashboardProvider.discountPlanList[index]?.promoDiscount}%',
                                                                    style: buildAppTextTheme()
                                                                        .caption,
                                                                    textAlign:
                                                                        TextAlign
                                                                            .start,
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Stack(
                                                      children: [
                                                        Container(
                                                          //height: 92.0.h,
                                                          //width: 94.0.w,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.2,
                                                          padding:
                                                              EdgeInsets.all(
                                                                  2.0.s),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: selectedIndex ==
                                                                    index
                                                                ? AppColors
                                                                    .primaryBlue
                                                                : AppColors
                                                                    .border,
                                                            border: Border.all(
                                                                color: selectedIndex ==
                                                                        index
                                                                    ? AppColors
                                                                        .primaryBlue
                                                                    : AppColors
                                                                        .border,
                                                                width: 1.0),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              bottomRight: Radius
                                                                  .circular(
                                                                      10.0.s),
                                                              topRight: Radius
                                                                  .circular(
                                                                      10.0.s),
                                                            ),
                                                          ),
                                                          child: Center(
                                                            child: Text(
                                                              dashboardProvider
                                                                          .discountPlanList[
                                                                              index]
                                                                          ?.premiumAmount !=
                                                                      ''
                                                                  ? '${num.parse(dashboardProvider.discountPlanList[index]?.premiumAmount ?? '0').toStringAsFixed(3)}'
                                                                  : '',
                                                              style: buildAppTextTheme()
                                                                  .headline4!
                                                                  .copyWith(
                                                                      color: selectedIndex ==
                                                                              index
                                                                          ? AppColors
                                                                              .white
                                                                          : AppColors
                                                                              .primaryBlue),
                                                              textAlign:
                                                                  TextAlign
                                                                      .right,
                                                            ),
                                                          ),
                                                        ),
                                                        Positioned(
                                                          top: 1.0.s,
                                                          right: 1.5.s,
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              color: selectedIndex ==
                                                                      index
                                                                  ? AppColors
                                                                      .white
                                                                  : AppColors
                                                                      .primaryBlue,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .only(
                                                                bottomLeft: Radius
                                                                    .circular(
                                                                        10.0.s),
                                                                topRight: Radius
                                                                    .circular(
                                                                        10.0.s),
                                                              ),
                                                            ),
                                                            height: 26.0.h,
                                                            width: 40.0.w,
                                                            child: Center(
                                                              child: Text(
                                                                'KWD',
                                                                style: buildAppTextTheme().bodyText1!.copyWith(
                                                                    color: selectedIndex ==
                                                                            index
                                                                        ? AppColors
                                                                            .primaryBlue
                                                                        : AppColors
                                                                            .white),
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        }),
                                  ),
                                  SizedBox(
                                    height: 300.0.h,
                                  ),
                                ],
                              )
                            : searchList.isEmpty
                                ? Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 17.0.s),
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.9,
                                        child: Text(
                                          planType == 'TPL'
                                              ? 'No data found on selected filter. Please check manufacture year or number of seats. For assistance call on 22217090.'
                                              : 'No data found on selected filter. Please check manufacture year, make & model or value. For assistance call on 22217090.',
                                          style: buildAppTextTheme().headline3,
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 30.0.h,
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.9,
                                        child: Text(
                                          planType == 'TPL'
                                              ? 'لا توجد بيانات في الفلتر المحدد. يرجى التحقق من سنة الصنع أو عدد المقاعد. للمساعدة اتصل على 22217090.'
                                              : 'لا توجد بيانات في الفلتر المحدد. يرجى التحقق من سنة الصنع ، والطراز أو القيمة. للمساعدة اتصل على 22217090.',
                                          style: buildAppTextTheme().headline3,
                                          textAlign: TextAlign.center,
                                          textDirection: TextDirection.rtl,
                                        ),
                                      ),
                                    ],
                                  )
                                : Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 2.0.w),
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.45,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: ListView.builder(
                                            physics:
                                                AlwaysScrollableScrollPhysics(),
                                            // shrinkWrap: true,
                                            itemCount: searchList.length,
                                            scrollDirection: Axis.vertical,
                                            itemBuilder: (context, index) {
                                              return Container(
                                                padding: EdgeInsets.only(
                                                    bottom: 14.0.s),
                                                child: Container(
                                                  height: 115.0.h,
                                                  child: InkWell(
                                                    onTap: () async {
                                                      FocusScope.of(context)
                                                          .unfocus();
                                                      searchList[index]
                                                              ?.planType =
                                                          planType!;
                                                      dashboardProvider
                                                              .setAdditionalMotorInsuranceModel =
                                                          await MotorService()
                                                              .getAdditionalMotorPlans(
                                                                  context:
                                                                      context,
                                                                  productType:
                                                                      searchList[index]
                                                                              ?.productType ??
                                                                          '');
                                                      setState(() {
                                                        selectedIndex = index;
                                                        customerProvider
                                                            .resetSelectedMotorInsuranceList();
                                                        customerProvider
                                                            .resetInsurerMotorDetails();
                                                        customerProvider
                                                            .selectedMotorInsuranceList
                                                            .add(searchList[
                                                                index]);
                                                        if (planType == 'TPL') {
                                                          customerProvider
                                                                  ?.insuranceMotorDetails
                                                                  ?.makeYear =
                                                              yearController
                                                                  .text;
                                                        } else if (makeYear !=
                                                            null) {
                                                          customerProvider
                                                                  ?.insuranceMotorDetails
                                                                  ?.makeYear =
                                                              makeYear!;
                                                        }
                                                        if (seats != null) {
                                                          customerProvider
                                                              ?.insuranceMotorDetails
                                                              ?.noOfSeats = seats!;
                                                        }
                                                        customerProvider
                                                                ?.insuranceMotorDetails
                                                                ?.carValue =
                                                            valueController
                                                                .text!;
                                                        if (brand != null) {
                                                          customerProvider
                                                              ?.insuranceMotorDetails
                                                              ?.makeModel = brand!;
                                                        }
                                                      });
                                                    },
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Container(
                                                          //  height: 96.0.h,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.2,
                                                          decoration:
                                                              BoxDecoration(
                                                            color: selectedIndex ==
                                                                    index
                                                                ? AppColors
                                                                    .border
                                                                    .withOpacity(
                                                                        0.5)
                                                                : AppColors
                                                                    .white,
                                                            border: Border(
                                                              top: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              bottom: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              left: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              right: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                            ),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              bottomLeft: Radius
                                                                  .circular(
                                                                      10.0.s),
                                                              topLeft: Radius
                                                                  .circular(
                                                                      10.0.s),
                                                            ),
                                                          ),
                                                          child: Center(
                                                            child: Image(
                                                              image: AssetImage(
                                                                  searchList[index]
                                                                              ?.productType ==
                                                                          'Gulf Trust'
                                                                      ? AppImages
                                                                          .SPLASH_SCREEN
                                                                      : searchList[index]?.productType ==
                                                                              'Tazur'
                                                                          ? AppImages
                                                                              .TAZUR
                                                                          : searchList[index]?.productType == 'Burgan'
                                                                              ? AppImages.BURGAN
                                                                              : searchList[index]?.productType == 'NLGIC'
                                                                                  ? AppImages.NATIONAL
                                                                                  : AppImages.WARBA
                                                                  // widget.isSelected
                                                                  // ? AppImages.SELECTED_PLAN
                                                                  // : AppImages.PLAN
                                                                  ),
                                                              height: 63.0.h,
                                                              width: 63.0.w,
                                                              fit: BoxFit.fill,
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          // height: 96.0.h,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.55,
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 8.0.s,
                                                                  top: 10.0.s,
                                                                  bottom: 6.0.s,
                                                                  right: 8.0.s),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: selectedIndex ==
                                                                    index
                                                                ? AppColors
                                                                    .border
                                                                    .withOpacity(
                                                                        0.5)
                                                                : AppColors
                                                                    .white,
                                                            border: Border(
                                                              top: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              bottom: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              left: BorderSide(
                                                                  color: selectedIndex == index
                                                                      ? AppColors
                                                                          .primaryBlue
                                                                      : AppColors
                                                                          .border,
                                                                  width: 0.0),
                                                              right: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                            ),
                                                          ),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Column(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Container(
                                                                    width:
                                                                        200.0.w,
                                                                    child: Text(
                                                                      searchList[index]
                                                                              ?.productSummary
                                                                              .trim() ??
                                                                          '',
                                                                      style: buildAppTextTheme()
                                                                          .subtitle2!
                                                                          .copyWith(
                                                                              color: AppColors.primaryBlue),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .left,
                                                                      maxLines:
                                                                          1,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                        10.0.h,
                                                                  ),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        '${searchList[index]?.validity} ${searchList[index]?.validityPeriod}',
                                                                        style: buildAppTextTheme()
                                                                            .caption!
                                                                            .copyWith(color: AppColors.primaryBlue),
                                                                        textAlign:
                                                                            TextAlign.left,
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                      ),
                                                                      SizedBox(
                                                                          width:
                                                                              5.0.w),
                                                                      if (searchList[index]?.productType !=
                                                                              null &&
                                                                          searchList[index]?.productType.trim() !=
                                                                              '')
                                                                        Container(
                                                                          height:
                                                                              16.0.h,
                                                                          child:
                                                                              VerticalDivider(
                                                                            width:
                                                                                2.0.w,
                                                                            thickness:
                                                                                2.0.w,
                                                                            color:
                                                                                AppColors.border,
                                                                          ),
                                                                        ),
                                                                      SizedBox(
                                                                          width:
                                                                              7.0.w),
                                                                      Text(
                                                                        searchList[index]?.productType ??
                                                                            '',
                                                                        style: buildAppTextTheme()
                                                                            .caption!
                                                                            .copyWith(color: AppColors.primaryBlue),
                                                                        textAlign:
                                                                            TextAlign.left,
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                        9.0.h,
                                                                  ),
                                                                  InkWell(
                                                                    onTap: () => showModalBottomSheet(
                                                                        isScrollControlled: true,
                                                                        backgroundColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                        barrierColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                        context: context,
                                                                        builder: (BuildContext context) {
                                                                          return InsuranceDetailsBottomSheetWidget(
                                                                              index: searchList[index]?.productType == 'Tazur'
                                                                                  ? 0
                                                                                  : searchList[index]?.productType == 'Burgan'
                                                                                      ? 1
                                                                                      : searchList[index]?.productType == 'NLGIC'
                                                                                          ? 2
                                                                                          : searchList[index]?.productType == 'Warba'
                                                                                              ? 3
                                                                                              : 4);
                                                                        }),
                                                                    child: Row(
                                                                      children: [
                                                                        Text(
                                                                          'Information',
                                                                          style: buildAppTextTheme()
                                                                              .caption!
                                                                              .copyWith(color: AppColors.orange),
                                                                          textAlign:
                                                                              TextAlign.right,
                                                                        ),
                                                                        SizedBox(
                                                                          width:
                                                                              8.0.w,
                                                                        ),
                                                                        Image(
                                                                          image:
                                                                              AssetImage(AppImages.INFO),
                                                                          height:
                                                                              18.0.h,
                                                                          width:
                                                                              18.0.w,
                                                                          color:
                                                                              AppColors.orange,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Stack(
                                                          children: [
                                                            Container(
                                                              //height: 92.0.h,
                                                              //width: 94.0.w,
                                                              width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width *
                                                                  0.2,
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(2.0
                                                                          .s),
                                                              decoration:
                                                                  BoxDecoration(
                                                                color: selectedIndex ==
                                                                        index
                                                                    ? AppColors
                                                                        .primaryBlue
                                                                    : AppColors
                                                                        .border,
                                                                border: Border.all(
                                                                    color: selectedIndex ==
                                                                            index
                                                                        ? AppColors
                                                                            .primaryBlue
                                                                        : AppColors
                                                                            .border,
                                                                    width: 1.0),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .only(
                                                                  bottomRight: Radius
                                                                      .circular(
                                                                          10.0.s),
                                                                  topRight: Radius
                                                                      .circular(
                                                                          10.0.s),
                                                                ),
                                                              ),
                                                              child: Center(
                                                                child: Text(
                                                                  '${num.parse(searchList[index]?.premiumAmount ?? '').toStringAsFixed(3)}',
                                                                  style: buildAppTextTheme()
                                                                      .headline4!
                                                                      .copyWith(
                                                                          color: selectedIndex == index
                                                                              ? AppColors.white
                                                                              : AppColors.primaryBlue),
                                                                  textAlign:
                                                                      TextAlign
                                                                          .right,
                                                                ),
                                                              ),
                                                            ),
                                                            Positioned(
                                                              top: 1.0.s,
                                                              right: 1.5.s,
                                                              child: Container(
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color: selectedIndex == index
                                                                      ? AppColors
                                                                          .white
                                                                      : AppColors
                                                                          .primaryBlue,
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    bottomLeft:
                                                                        Radius.circular(
                                                                            10.0.s),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            10.0.s),
                                                                  ),
                                                                ),
                                                                height: 26.0.h,
                                                                width: 40.0.w,
                                                                child: Center(
                                                                  child: Text(
                                                                    'KWD',
                                                                    style: buildAppTextTheme()
                                                                        .bodyText1!
                                                                        .copyWith(
                                                                            color: selectedIndex == index
                                                                                ? AppColors.primaryBlue
                                                                                : AppColors.white),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .center,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }),
                                      ),
                                      SizedBox(
                                        height: 300.0.h,
                                      ),
                                    ],
                                  ),
                      ],
                    ),
                  ),
                )
              : widget.type == 'MOTOR'
                  ? SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 8.0.s,
                            right: 8.0.s,
                            top: 20.0.s,
                            bottom: 10.0.s),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 10.0.h,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 10.0.s),
                              child: Row(
                                children: [
                                  InkWell(
                                    onTap: () {
                                      this.planType = 'TPL';
                                      resetFilters();
                                      onFilter('TPL');
                                      //  onSelectedItem('planType', 'TPL');
                                      valueController.clear();
                                    },
                                    child: Container(
                                      width: 194.0.w,
                                      height: 42.0.h,
                                      decoration: BoxDecoration(
                                        color: this.planType == 'TPL'
                                            ? AppColors.primaryBlue
                                            : AppColors.white,
                                        border: Border.all(
                                            width: 1.5.s,
                                            color: AppColors.primaryBlue),
                                        borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(10.0.s),
                                          topLeft: Radius.circular(10.0.s),
                                        ),
                                      ),
                                      child: Center(
                                        child: Text(
                                          'TPL',
                                          style: buildAppTextTheme()
                                              .headline6!
                                              .copyWith(
                                                  color: this.planType == 'TPL'
                                                      ? AppColors.white
                                                      : AppColors.lightBlue),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                    ),
                                  ),
                                  InkWell(
                                    onTap: () {
                                      this.planType = 'Comprehensive';
                                      resetFilters();
                                      onFilter('Comprehensive');
                                      //    onSelectedItem('planType', 'Comprehensive');
                                      yearController.clear();
                                    },
                                    child: Container(
                                      width: 194.0.w,
                                      height: 42.0.h,
                                      decoration: BoxDecoration(
                                        color: this.planType == 'Comprehensive'
                                            ? AppColors.primaryBlue
                                            : AppColors.white,
                                        border: Border.all(
                                            width: 1.5.s,
                                            color: AppColors.primaryBlue),
                                        borderRadius: BorderRadius.only(
                                          bottomRight: Radius.circular(10.0.s),
                                          topRight: Radius.circular(10.0.s),
                                        ),
                                      ),
                                      child: Center(
                                        child: Text(
                                          'Comprehensive',
                                          style: buildAppTextTheme()
                                              .headline6!
                                              .copyWith(
                                                  color: this.planType ==
                                                          'Comprehensive'
                                                      ? AppColors.white
                                                      : AppColors.lightBlue),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 19.0.h,
                            ),
                            Container(
                              height: 90.0.h,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                children: [
                                  SizedBox(
                                    width: 12.0.w,
                                  ),
                                  planType == 'TPL'
                                      ? Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  'Enter Manufacture Year',
                                                  style: buildAppTextTheme()
                                                      .bodyText1!
                                                      .copyWith(
                                                          color: AppColors
                                                              .primaryBlue),
                                                ),
                                                SizedBox(height: 8.0.h),
                                                Container(
                                                  height: 56.0.h,
                                                  width: 120.0.w,
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 4.0.s,
                                                      horizontal: 10.0.s),
                                                  decoration: BoxDecoration(
                                                    // color: (yearFocus.hasFocus ||
                                                    //         yearController.text.isNotEmpty)
                                                    //     ? AppColors.white
                                                    //     : AppColors.primaryBlue,
                                                    color:
                                                        AppColors.primaryBlue,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            67.0.s),
                                                    border: Border.all(
                                                        width: 1.0.s,
                                                        color: AppColors
                                                            .primaryBlue),
                                                  ),
                                                  child: Center(
                                                    child: TextFormField(
                                                      textAlign:
                                                          TextAlign.center,
                                                      inputFormatters: [
                                                        FilteringTextInputFormatter
                                                            .digitsOnly,
                                                        LengthLimitingTextInputFormatter(
                                                            7),
                                                      ],
                                                      onFieldSubmitted: (_) =>
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  seatsDropdownFocus),
                                                      // focus to next
                                                      // scrollPadding: EdgeInsets.only(bottom: 100),
                                                      focusNode: yearFocus,
                                                      // style: yearFocus.hasFocus
                                                      //     ? buildAppTextTheme().headline5
                                                      //     : buildAppTextTheme()
                                                      //         .subtitle1!
                                                      //         .copyWith(color: AppColors.black),
                                                      style: buildAppTextTheme()
                                                          .headline5!
                                                          .copyWith(
                                                              color: AppColors
                                                                  .white),
                                                      keyboardType:
                                                          TextInputType.number,
                                                      controller:
                                                          yearController,
                                                      obscureText: false,
                                                      decoration:
                                                          InputDecoration(
                                                        isDense: true,
                                                        // contentPadding: EdgeInsets.only(
                                                        //   left: 2.0.s,
                                                        //   top: 2.0.s,
                                                        //   bottom: 2.0.s,
                                                        //   right: 2.0.s,
                                                        // ),
                                                        // labelText: 'Year',
                                                        // labelStyle: (yearFocus.hasFocus ||
                                                        //         yearController.text.isNotEmpty)
                                                        //     ? buildAppTextTheme().subtitle2
                                                        //     : buildAppTextTheme()
                                                        //         .subtitle2!
                                                        //         .copyWith(color: AppColors.white),
                                                        border:
                                                            InputBorder.none,
                                                      ),
                                                      onChanged: (value) async {
                                                        if (value.length == 4) {
                                                          onFilter('TPL');
                                                        } else {
                                                          setState(() {
                                                            searchList.clear();
                                                            selectedIndex = -1;
                                                          });
                                                        }
                                                        // var result;
                                                        // if (seats == null ||
                                                        //     seats == 'Seats') {
                                                        //   result = await CategoryService()
                                                        //       .getMotorInsurancePlans(
                                                        //           context: context,
                                                        //           makeYear: int.parse(
                                                        //               yearController.text),
                                                        //           planType: planType!);
                                                        // } else {
                                                        //   result = await CategoryService()
                                                        //       .getMotorInsurancePlans(
                                                        //           context: context,
                                                        //           makeYear: int.parse(
                                                        //               yearController.text),
                                                        //           noOfSeats:
                                                        //               int.parse(seats!),
                                                        //           planType: planType!);
                                                        // }
                                                        // setState(() {
                                                        //   searchList = result;
                                                        // });
                                                        //}
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 12.0.w,
                                            ),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  'Select No of Seats',
                                                  style: buildAppTextTheme()
                                                      .bodyText1!
                                                      .copyWith(
                                                          color: AppColors
                                                              .primaryBlue),
                                                ),
                                                SizedBox(height: 8.0.h),
                                                Container(
                                                  height: 56.0.h,
                                                  width: 120.0.w,
                                                  decoration: BoxDecoration(
                                                    color:
                                                        AppColors.primaryBlue,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            67.0.s),
                                                    border: Border.all(
                                                        width: 1.0.s,
                                                        color: AppColors
                                                            .primaryBlue),
                                                  ),
                                                  child: Center(
                                                    child:
                                                        DropdownButtonHideUnderline(
                                                      child: DropdownButton2(
                                                        focusNode:
                                                            seatsDropdownFocus,
                                                        isExpanded: true,
                                                        // hint: Row(
                                                        //   children: [
                                                        //     Expanded(
                                                        //       child: Text(
                                                        //         'Seats',
                                                        //         style: buildAppTextTheme()
                                                        //             .headline6!
                                                        //             .copyWith(
                                                        //                 color:
                                                        //                     AppColors.white),
                                                        //         overflow:
                                                        //             TextOverflow.ellipsis,
                                                        //       ),
                                                        //     ),
                                                        //   ],
                                                        // ),
                                                        items: getDropdownValues(
                                                                'seats')
                                                            .map((item) =>
                                                                DropdownMenuItem<
                                                                    String>(
                                                                  value: item,
                                                                  child: Text(
                                                                    item!,
                                                                    style: buildAppTextTheme()
                                                                        .headline6!
                                                                        .copyWith(
                                                                            color:
                                                                                AppColors.white),
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                  ),
                                                                ))
                                                            .toList(),
                                                        value: this.seats,
                                                        onChanged: (value) {
                                                          this.seats =
                                                              value.toString();
                                                          onFilter('TPL');
                                                          // onSelectedItem(
                                                          //     'seats', value.toString());
                                                        },
                                                        icon: Icon(
                                                          Icons
                                                              .keyboard_arrow_down_outlined,
                                                          color:
                                                              AppColors.white,
                                                          size: 24.0.ics,
                                                        ),
                                                        // iconSize: 14,
                                                        // iconEnabledColor: Colors.yellow,
                                                        // iconDisabledColor: Colors.grey,
                                                        buttonHeight: 50,
                                                        buttonWidth: 160,
                                                        buttonPadding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 14,
                                                                right: 14),
                                                        buttonDecoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      67.0.s),
                                                          border: Border.all(
                                                              width: 1.0.s,
                                                              color: AppColors
                                                                  .primaryBlue),
                                                          color: AppColors
                                                              .primaryBlue,
                                                        ),
                                                        buttonElevation: 2,
                                                        itemHeight: 40,
                                                        itemPadding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 14,
                                                                right: 14),
                                                        dropdownMaxHeight: 200,
                                                        dropdownWidth: 150,
                                                        dropdownPadding: null,
                                                        dropdownDecoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      9.0.s),
                                                          color: AppColors
                                                              .primaryBlue,
                                                        ),
                                                        dropdownElevation: 8,
                                                        scrollbarRadius:
                                                            const Radius
                                                                .circular(40),
                                                        scrollbarThickness: 6,
                                                        scrollbarAlwaysShow:
                                                            true,
                                                        offset:
                                                            const Offset(0, 0),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        )
                                      : Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  'Enter Car Value KWD',
                                                  style: buildAppTextTheme()
                                                      .bodyText1!
                                                      .copyWith(
                                                          color: AppColors
                                                              .primaryBlue),
                                                ),
                                                SizedBox(height: 8.0.h),
                                                Container(
                                                  height: 56.0.h,
                                                  width: 120.0.w,
                                                  padding: EdgeInsets.symmetric(
                                                      vertical: 4.0.s,
                                                      horizontal: 10.0.s),
                                                  decoration: BoxDecoration(
                                                    // color: (valueFocus.hasFocus ||
                                                    //         valueController.text.isNotEmpty)
                                                    //     ? AppColors.white
                                                    //     : AppColors.primaryBlue,
                                                    color:
                                                        AppColors.primaryBlue,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            67.0.s),
                                                    border: Border.all(
                                                        width: 1.0.s,
                                                        color: AppColors
                                                            .primaryBlue),
                                                  ),
                                                  child: Center(
                                                    child: TextFormField(
                                                      textAlign:
                                                          TextAlign.center,
                                                      inputFormatters: [
                                                        FilteringTextInputFormatter
                                                            .digitsOnly,
                                                        LengthLimitingTextInputFormatter(
                                                            7),
                                                      ], // focus to next
                                                      onFieldSubmitted: (_) =>
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  yearDropdownFocus),

                                                      // scrollPadding:
                                                      //     EdgeInsets.only(bottom: 100),
                                                      focusNode: valueFocus,
                                                      // style: valueFocus.hasFocus
                                                      //     ? buildAppTextTheme().headline5
                                                      //     : buildAppTextTheme()
                                                      //         .subtitle1!
                                                      //         .copyWith(
                                                      //             color: AppColors.black),
                                                      style: buildAppTextTheme()
                                                          .subtitle1!
                                                          .copyWith(
                                                              color: AppColors
                                                                  .white),
                                                      keyboardType:
                                                          TextInputType.number,
                                                      controller:
                                                          valueController,
                                                      obscureText: false,
                                                      decoration:
                                                          InputDecoration(
                                                        isDense: true,
                                                        // contentPadding: EdgeInsets.only(
                                                        //   left: 2.0.s,
                                                        //   top: 2.0.s,
                                                        //   bottom: 2.0.s,
                                                        //   right: 2.0.s,
                                                        // ),
                                                        // labelText: 'Value',
                                                        // labelStyle: (valueFocus.hasFocus ||
                                                        //         valueController
                                                        //             .text.isNotEmpty)
                                                        //     ? buildAppTextTheme().subtitle2
                                                        //     : buildAppTextTheme()
                                                        //         .subtitle2!
                                                        //         .copyWith(
                                                        //             color: AppColors.white),
                                                        border:
                                                            InputBorder.none,
                                                      ),
                                                      onChanged: (value) async {
                                                        onFilter(
                                                            'Comprehensive');
                                                        // var result;
                                                        // if (makeYear == null ||
                                                        //     makeYear == 'Year') {
                                                        //   if (seats == null ||
                                                        //       seats == 'Seats') {
                                                        //     result = await CategoryService()
                                                        //         .getMotorInsurancePlans(
                                                        //             context: context,
                                                        //             value: valueController
                                                        //                     .text.isEmpty
                                                        //                 ? 0
                                                        //                 : int.parse(
                                                        //                     valueController
                                                        //                         .text),
                                                        //             makeModel: brand ?? '',
                                                        //             planType: planType!);
                                                        //   } else {
                                                        //     result = await CategoryService()
                                                        //         .getMotorInsurancePlans(
                                                        //             context: context,
                                                        //             value: valueController
                                                        //                     .text.isEmpty
                                                        //                 ? 0
                                                        //                 : int.parse(
                                                        //                     valueController
                                                        //                         .text),
                                                        //             makeModel: brand!,
                                                        //             noOfSeats:
                                                        //                 int.parse(seats!),
                                                        //             planType: planType!);
                                                        //   }
                                                        // } else {
                                                        //   if (seats == null ||
                                                        //       seats == 'Seats') {
                                                        //     result = await CategoryService()
                                                        //         .getMotorInsurancePlans(
                                                        //             context: context,
                                                        //             value: valueController
                                                        //                     .text.isEmpty
                                                        //                 ? 0
                                                        //                 : int.parse(
                                                        //                     valueController
                                                        //                         .text),
                                                        //             makeModel: brand!,
                                                        //             makeYear:
                                                        //                 int.parse(makeYear!),
                                                        //             planType: planType!);
                                                        //   } else {
                                                        //     result = await CategoryService()
                                                        //         .getMotorInsurancePlans(
                                                        //             context: context,
                                                        //             value: valueController
                                                        //                     .text.isEmpty
                                                        //                 ? 0
                                                        //                 : int.parse(
                                                        //                     valueController
                                                        //                         .text),
                                                        //             makeModel: brand!,
                                                        //             makeYear:
                                                        //                 int.parse(makeYear!),
                                                        //             noOfSeats:
                                                        //                 int.parse(seats!),
                                                        //             planType: planType!);
                                                        //   }
                                                        // }
                                                        // setState(() {
                                                        //   searchList = result;
                                                        // });
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 8.0.w,
                                            ),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Text(
                                                  'Select Manufacture Year',
                                                  style: buildAppTextTheme()
                                                      .bodyText1!
                                                      .copyWith(
                                                          color: AppColors
                                                              .primaryBlue),
                                                ),
                                                SizedBox(height: 8.0.h),
                                                Container(
                                                  height: 56.0.h,
                                                  width: 120.0.w,
                                                  decoration: BoxDecoration(
                                                    color:
                                                        AppColors.primaryBlue,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            67.0.s),
                                                    border: Border.all(
                                                        width: 1.0.s,
                                                        color: AppColors
                                                            .primaryBlue),
                                                  ),
                                                  child: Center(
                                                    child:
                                                        DropdownButtonHideUnderline(
                                                      child: DropdownButton2(
                                                        focusNode:
                                                            yearDropdownFocus,
                                                        isExpanded: true,
                                                        // hint: Row(
                                                        //   children: [
                                                        //     Expanded(
                                                        //       child: Text(
                                                        //         'Year',
                                                        //         style: buildAppTextTheme()
                                                        //             .headline6!
                                                        //             .copyWith(
                                                        //                 color: AppColors.white),
                                                        //         overflow: TextOverflow.ellipsis,
                                                        //       ),
                                                        //     ),
                                                        //   ],
                                                        // ),
                                                        items: getDropdownValues(
                                                                'makeYear')
                                                            .map((item) =>
                                                                DropdownMenuItem<
                                                                    String>(
                                                                  value: item,
                                                                  child: Text(
                                                                    item!,
                                                                    style: buildAppTextTheme()
                                                                        .headline6!
                                                                        .copyWith(
                                                                            color:
                                                                                AppColors.white),
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                  ),
                                                                ))
                                                            .toList(),
                                                        value: this.makeYear,
                                                        onChanged: (value) {
                                                          makeYear =
                                                              value.toString();
                                                          onFilter(
                                                              'Comprehensive');
                                                          // onSelectedItem(
                                                          //     'makeYear', value.toString());
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  searchFocus);
                                                        },
                                                        icon: Icon(
                                                          Icons
                                                              .keyboard_arrow_down_outlined,
                                                          color:
                                                              AppColors.white,
                                                          size: 24.0.ics,
                                                        ),
                                                        // iconSize: 14,
                                                        // iconEnabledColor: Colors.yellow,
                                                        // iconDisabledColor: Colors.grey,
                                                        buttonHeight: 50,
                                                        buttonWidth: 160,
                                                        buttonPadding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 14,
                                                                right: 14),
                                                        buttonDecoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      67.0.s),
                                                          border: Border.all(
                                                              width: 1.0.s,
                                                              color: AppColors
                                                                  .primaryBlue),
                                                          color: AppColors
                                                              .primaryBlue,
                                                        ),
                                                        buttonElevation: 2,
                                                        itemHeight: 40,
                                                        itemPadding:
                                                            const EdgeInsets
                                                                    .only(
                                                                left: 14,
                                                                right: 14),
                                                        dropdownMaxHeight: 200,
                                                        dropdownWidth: 100,
                                                        dropdownPadding: null,
                                                        dropdownDecoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      9.0.s),
                                                          color: AppColors
                                                              .primaryBlue,
                                                        ),
                                                        dropdownElevation: 8,
                                                        scrollbarRadius:
                                                            const Radius
                                                                .circular(40),
                                                        scrollbarThickness: 6,
                                                        scrollbarAlwaysShow:
                                                            true,
                                                        offset:
                                                            const Offset(0, 0),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              width: 8.0.w,
                                            ),
                                            Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  'Enter Make & Model',
                                                  style: buildAppTextTheme()
                                                      .bodyText1!
                                                      .copyWith(
                                                          color: AppColors
                                                              .primaryBlue),
                                                ),
                                                SizedBox(height: 8.0.h),
                                                Container(
                                                  height: 56.0.h,
                                                  width: 250.0.w,
                                                  padding: EdgeInsets.only(
                                                      left: 20.0.s,
                                                      right: 20.0.s),
                                                  decoration: BoxDecoration(
                                                    color:
                                                        AppColors.primaryBlue,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            24.0.s),
                                                    border: Border.all(
                                                        width: 1.0.s,
                                                        color: AppColors
                                                            .primaryBlue),
                                                  ),
                                                  child: Center(
                                                    child: Theme(
                                                      data: ThemeData(
                                                        textTheme: TextTheme(
                                                            subtitle1: TextStyle(
                                                                color: AppColors
                                                                    .white)),
                                                      ),
                                                      child: DropdownSearch<
                                                          String?>(
                                                        mode: Mode.MENU,
                                                        popupBackgroundColor:
                                                            AppColors
                                                                .primaryBlue,
                                                        dropdownButtonProps:
                                                            IconButtonProps(
                                                          focusNode:
                                                              searchFocus,
                                                          icon: Icon(
                                                            Icons
                                                                .keyboard_arrow_down_outlined,
                                                            color:
                                                                AppColors.white,
                                                            size: 24.0.ics,
                                                          ),
                                                        ),
                                                        showSearchBox: true,
                                                        searchFieldProps:
                                                            TextFieldProps(
                                                          decoration:
                                                              InputDecoration(
                                                                  fillColor:
                                                                      AppColors
                                                                          .greyishBlack,
                                                                  border:
                                                                      OutlineInputBorder(),
                                                                  // contentPadding: EdgeInsets.fromLTRB(
                                                                  //     12, 8, 12, 8),
                                                                  labelText:
                                                                      "Search",
                                                                  labelStyle:
                                                                      TextStyle(
                                                                          color:
                                                                              AppColors.white)),
                                                        ),
                                                        //showSelectedItems: true,
                                                        dropdownSearchDecoration:
                                                            InputDecoration(
                                                                // labelText: this.brand ?? 'Make & Model',
                                                                // labelStyle: buildAppTextTheme()
                                                                //     .headline4!
                                                                //     .copyWith(color: AppColors.white),
                                                                ),
                                                        items:
                                                            getDropdownValues(
                                                                    'brand')
                                                                .map((item) =>
                                                                    item!)
                                                                .toList(),
                                                        selectedItem:
                                                            this.brand ?? '',
                                                        // value: this.seats,
                                                        onChanged: (value) {
                                                          this.brand = value;
                                                          onFilter(
                                                              'Comprehensive');
                                                          // onSelectedItem(
                                                          //     'brand', value.toString());
                                                        },
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 10.0.h),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          resetFilters();
                                          onFilter(planType!);
                                        },
                                        child: Text(
                                          'Reset Filter',
                                          style: buildAppTextTheme()
                                              .headline6!
                                              .copyWith(
                                                  color: AppColors.primaryBlue),
                                          overflow: TextOverflow.fade,
                                        ),
                                      ),
                                      SizedBox(width: 4.0.w),
                                      InkWell(
                                        onTap: () {
                                          resetFilters();
                                          onFilter(planType!);
                                        },
                                        child: Icon(
                                          Icons.replay,
                                          color: AppColors.primaryBlue,
                                          size: 18.0.ics,
                                        ),
                                      ),
                                    ],
                                  ),
                                  InkWell(
                                    onTap: () => sortByPrices(),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Sort on price',
                                          style: buildAppTextTheme()
                                              .headline6!
                                              .copyWith(
                                                  color: AppColors.primaryBlue),
                                          overflow: TextOverflow.fade,
                                        ),
                                        SizedBox(width: 4.0.w),
                                        Image(
                                          image: AssetImage(AppImages.SORT),
                                          color: AppColors.primaryBlue,
                                          height: 24.0.h,
                                          width: 24.0.w,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 10.0.h,
                            ),
                            // isSearched &&
                            (widget.discountData != null &&
                                    widget.discountData?.premiumAmount !=
                                        null &&
                                    !isFiltered)
                                ? Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 2.0.w),
                                    height: 130.0.h,
                                    width: MediaQuery.of(context).size.width,
                                    child: InkWell(
                                      onTap: () {
                                        FocusScope.of(context).unfocus();
                                        setState(() {
                                          selectedIndex = 0;
                                          customerProvider
                                              .resetSelectedMotorInsuranceList();
                                          customerProvider
                                              .resetInsurerMotorDetails();
                                          customerProvider
                                              .selectedMotorInsuranceList
                                              .add(MotorInsuranceModel(
                                            productType: widget.discountData
                                                    ?.productType ??
                                                '',
                                            productSummary: widget.discountData
                                                    ?.productSummary ??
                                                '',
                                            productDescription: widget
                                                    .discountData
                                                    ?.productDescription ??
                                                '',
                                            premiumAmount: widget.discountData
                                                    ?.premiumAmount ??
                                                '0',
                                            promoCode: widget
                                                    .discountData?.promoCode ??
                                                '',
                                            promoDiscount: widget.discountData
                                                    ?.promoDiscount ??
                                                '0',
                                            promoDiscountText: widget
                                                    .discountData
                                                    ?.promoDiscountText ??
                                                '',
                                            totalPremium: widget.discountData
                                                    ?.totalPremium ??
                                                '0',
                                            otherFee:
                                                widget.discountData?.otherFee ??
                                                    '0',
                                            validity: int.parse(
                                                widget.discountData?.validity ??
                                                    '0'),
                                            validityPeriod: widget.discountData
                                                    ?.validityPeriod ??
                                                '',
                                            planid:
                                                widget.discountData?.planId ??
                                                    0,
                                            planType:
                                                widget.discountData?.planType ??
                                                    '',
                                          ));
                                          if (planType == 'TPL') {
                                            customerProvider
                                                    ?.insuranceMotorDetails
                                                    ?.makeYear =
                                                yearController.text;
                                          } else if (makeYear != null) {
                                            customerProvider
                                                ?.insuranceMotorDetails
                                                ?.makeYear = makeYear!;
                                          }
                                          if (seats != null) {
                                            customerProvider
                                                ?.insuranceMotorDetails
                                                ?.noOfSeats = seats!;
                                          }
                                          customerProvider
                                                  ?.insuranceMotorDetails
                                                  ?.carValue =
                                              valueController.text!;
                                          if (brand != null) {
                                            customerProvider
                                                ?.insuranceMotorDetails
                                                ?.makeModel = brand!;
                                          }
                                        });
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            //  height: 96.0.h,
                                            width: 85.0.w,
                                            decoration: BoxDecoration(
                                              color: selectedIndex == 0
                                                  ? AppColors.border
                                                      .withOpacity(0.5)
                                                  : AppColors.white,
                                              border: Border(
                                                top: BorderSide(
                                                    color: AppColors.border,
                                                    width: 1.0),
                                                bottom: BorderSide(
                                                    color: AppColors.border,
                                                    width: 1.0),
                                                left: BorderSide(
                                                    color: AppColors.border,
                                                    width: 1.0),
                                                right: BorderSide(
                                                    color: AppColors.border,
                                                    width: 1.0),
                                              ),
                                              borderRadius: BorderRadius.only(
                                                bottomLeft:
                                                    Radius.circular(10.0.s),
                                                topLeft:
                                                    Radius.circular(10.0.s),
                                              ),
                                            ),
                                            child: Center(
                                              child: Image(
                                                image: AssetImage(widget
                                                                .discountData
                                                                ?.productType ==
                                                            'Gulf Trust'
                                                        ? AppImages
                                                            .SPLASH_SCREEN
                                                        : widget.discountData
                                                                    ?.productType ==
                                                                'Tazur'
                                                            ? AppImages.TAZUR
                                                            : widget.discountData
                                                                        ?.productType ==
                                                                    'Burgan'
                                                                ? AppImages
                                                                    .BURGAN
                                                                : widget.discountData
                                                                            ?.productType ==
                                                                        'NLGIC'
                                                                    ? AppImages
                                                                        .NATIONAL
                                                                    : AppImages
                                                                        .WARBA
                                                    // widget.isSelected
                                                    // ? AppImages.SELECTED_PLAN
                                                    // : AppImages.PLAN
                                                    ),
                                                height: 63.0.h,
                                                width: 63.0.w,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                          Stack(
                                            children: [
                                              Container(
                                                // height: 96.0.h,
                                                padding: EdgeInsets.only(
                                                    left: 8.0.s,
                                                    top: 10.0.s,
                                                    bottom: 6.0.s,
                                                    right: 8.0.s),
                                                decoration: BoxDecoration(
                                                  color: selectedIndex == 0
                                                      ? AppColors.border
                                                          .withOpacity(0.5)
                                                      : AppColors.white,
                                                  border: Border(
                                                    top: BorderSide(
                                                        color: AppColors.border,
                                                        width: 1.0),
                                                    bottom: BorderSide(
                                                        color: AppColors.border,
                                                        width: 1.0),
                                                    left: BorderSide(
                                                        color: selectedIndex ==
                                                                0
                                                            ? AppColors
                                                                .primaryBlue
                                                            : AppColors.border,
                                                        width: 0.0),
                                                    right: BorderSide(
                                                        color: AppColors.border,
                                                        width: 1.0),
                                                  ),
                                                ),
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          width: 200.0.w,
                                                          child: Text(
                                                            widget.discountData
                                                                    ?.productSummary
                                                                    .trim() ??
                                                                '',
                                                            style: buildAppTextTheme()
                                                                .subtitle2!
                                                                .copyWith(
                                                                    color: AppColors
                                                                        .primaryBlue),
                                                            textAlign:
                                                                TextAlign.left,
                                                            maxLines: 1,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 8.0.h,
                                                        ),
                                                        Container(
                                                          width: 200.0.w,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Text(
                                                                '${widget.discountData?.validity} ${widget.discountData?.validityPeriod}',
                                                                style: buildAppTextTheme()
                                                                    .caption!
                                                                    .copyWith(
                                                                        color: AppColors
                                                                            .primaryBlue),
                                                                textAlign:
                                                                    TextAlign
                                                                        .left,
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              ),
                                                              SizedBox(
                                                                  width: 5.0.w),
                                                              if (widget.discountData
                                                                          ?.productType !=
                                                                      null &&
                                                                  widget.discountData
                                                                          ?.productType
                                                                          .trim() !=
                                                                      '')
                                                                Container(
                                                                    height:
                                                                        16.0.h,
                                                                    child:
                                                                        VerticalDivider(
                                                                      width:
                                                                          2.0.w,
                                                                      thickness:
                                                                          2.0.w,
                                                                      color: AppColors
                                                                          .border,
                                                                    )),
                                                              SizedBox(
                                                                  width: 7.0.w),
                                                              Text(
                                                                widget.discountData
                                                                        ?.productType ??
                                                                    '',
                                                                style: buildAppTextTheme()
                                                                    .caption!
                                                                    .copyWith(
                                                                        color: AppColors
                                                                            .primaryBlue),
                                                                textAlign:
                                                                    TextAlign
                                                                        .left,
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          height: 8.0.h,
                                                        ),
                                                        InkWell(
                                                          onTap: () =>
                                                              showModalBottomSheet(
                                                                  isScrollControlled:
                                                                      true,
                                                                  backgroundColor: AppColors
                                                                      .primaryBlue
                                                                      .withOpacity(
                                                                          0.9),
                                                                  barrierColor: AppColors
                                                                      .primaryBlue
                                                                      .withOpacity(
                                                                          0.9),
                                                                  context:
                                                                      context,
                                                                  builder:
                                                                      (BuildContext
                                                                          context) {
                                                                    return InsuranceDetailsBottomSheetWidget(
                                                                        index: widget.discountData?.productType ==
                                                                                'Tazur'
                                                                            ? 0
                                                                            : widget.discountData?.productType == 'Burgan'
                                                                                ? 1
                                                                                : widget.discountData?.productType == 'NLGIC'
                                                                                    ? 2
                                                                                    : widget.discountData?.productType == 'Warba'
                                                                                        ? 3
                                                                                        : 4);
                                                                  }),
                                                          child: Row(
                                                            children: [
                                                              Text(
                                                                'Information',
                                                                style: buildAppTextTheme()
                                                                    .caption!
                                                                    .copyWith(
                                                                        color: AppColors
                                                                            .orange),
                                                                textAlign:
                                                                    TextAlign
                                                                        .right,
                                                              ),
                                                              SizedBox(
                                                                width: 8.0.w,
                                                              ),
                                                              Image(
                                                                image: AssetImage(
                                                                    AppImages
                                                                        .INFO),
                                                                height: 18.0.h,
                                                                width: 18.0.w,
                                                                color: AppColors
                                                                    .orange,
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              if (widget.discountData
                                                          ?.promoDiscount !=
                                                      null &&
                                                  widget.discountData
                                                          ?.promoDiscount !=
                                                      '' &&
                                                  widget.discountData
                                                          ?.promoDiscount !=
                                                      '0')
                                                Positioned(
                                                  bottom: 0.0.s,
                                                  right: 0.0.s,
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .center,
                                                    children: [
                                                      Text(
                                                        'Promo Code',
                                                        style: buildAppTextTheme()
                                                            .bodyText1!
                                                            .copyWith(
                                                                color: AppColors
                                                                    .brownColor),
                                                        textAlign:
                                                            TextAlign.start,
                                                      ),
                                                      SizedBox(
                                                        width: 40.0.w,
                                                      ),
                                                      Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          color:
                                                              AppColors.skyBlue,
                                                          borderRadius:
                                                              BorderRadius.only(
                                                            bottomRight:
                                                                Radius.circular(
                                                                    6.0.s),
                                                            topLeft:
                                                                Radius.circular(
                                                                    8.0.s),
                                                          ),
                                                        ),
                                                        height: 40.0.h,
                                                        width: 50.0.w,
                                                        child: Center(
                                                          child: Text(
                                                            '${widget.discountData?.promoDiscount}%',
                                                            style:
                                                                buildAppTextTheme()
                                                                    .caption,
                                                            textAlign:
                                                                TextAlign.start,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                            ],
                                          ),
                                          Stack(
                                            children: [
                                              Container(
                                                //height: 92.0.h,
                                                width: 90.0.w,
                                                padding: EdgeInsets.all(2.0.s),
                                                decoration: BoxDecoration(
                                                  color: selectedIndex == 0
                                                      ? AppColors.primaryBlue
                                                      : AppColors.border,
                                                  border: Border.all(
                                                      color: selectedIndex == 0
                                                          ? AppColors
                                                              .primaryBlue
                                                          : AppColors.border,
                                                      width: 1.0),
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    bottomRight:
                                                        Radius.circular(10.0.s),
                                                    topRight:
                                                        Radius.circular(10.0.s),
                                                  ),
                                                ),
                                                child: Center(
                                                  child: Text(
                                                    '${num.parse((widget.discountData?.premiumAmount ?? '') != '' ? (widget.discountData?.premiumAmount ?? '') : '0').toStringAsFixed(3)}',
                                                    style: buildAppTextTheme()
                                                        .headline4!
                                                        .copyWith(
                                                            color: selectedIndex ==
                                                                    0
                                                                ? AppColors
                                                                    .white
                                                                : AppColors
                                                                    .primaryBlue),
                                                    textAlign: TextAlign.right,
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                top: 1.0.s,
                                                right: 1.5.s,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: selectedIndex == 0
                                                        ? AppColors.white
                                                        : AppColors.primaryBlue,
                                                    borderRadius:
                                                        BorderRadius.only(
                                                      bottomLeft:
                                                          Radius.circular(
                                                              10.0.s),
                                                      topRight: Radius.circular(
                                                          10.0.s),
                                                    ),
                                                  ),
                                                  height: 26.0.h,
                                                  width: 40.0.w,
                                                  child: Center(
                                                    child: Text(
                                                      'KWD',
                                                      style: buildAppTextTheme()
                                                          .bodyText1!
                                                          .copyWith(
                                                              color: selectedIndex ==
                                                                      0
                                                                  ? AppColors
                                                                      .primaryBlue
                                                                  : AppColors
                                                                      .white),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : searchList.isEmpty
                                    ? Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 17.0.s),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.9,
                                            child: Text(
                                              planType == 'TPL'
                                                  ? 'No data found on selected filter. Please check manufacture year or number of seats. For assistance call on 22217090.'
                                                  : 'No data found on selected filter. Please check manufacture year, make & model or value. For assistance call on 22217090.',
                                              style:
                                                  buildAppTextTheme().headline3,
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 30.0.h,
                                          ),
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.9,
                                            child: Text(
                                              planType == 'TPL'
                                                  ? 'لا توجد بيانات في الفلتر المحدد. يرجى التحقق من سنة الصنع أو عدد المقاعد. للمساعدة اتصل على 22217090.'
                                                  : 'لا توجد بيانات في الفلتر المحدد. يرجى التحقق من سنة الصنع ، والطراز أو القيمة. للمساعدة اتصل على 22217090.',
                                              style:
                                                  buildAppTextTheme().headline3,
                                              textAlign: TextAlign.center,
                                              textDirection: TextDirection.rtl,
                                            ),
                                          ),
                                        ],
                                      )
                                    : Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 2.0.w),
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.45,
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            child: ListView.builder(
                                                physics:
                                                    AlwaysScrollableScrollPhysics(),
                                                // shrinkWrap: true,
                                                itemCount: searchList.length,
                                                scrollDirection: Axis.vertical,
                                                itemBuilder: (context, index) {
                                                  return Container(
                                                    padding: EdgeInsets.only(
                                                        bottom: 14.0.s),
                                                    child: Container(
                                                      height: 115.0.h,
                                                      child: InkWell(
                                                        onTap: () async {
                                                          FocusScope.of(context)
                                                              .unfocus();
                                                          searchList[index]
                                                                  ?.planType =
                                                              planType!;
                                                          dashboardProvider
                                                                  .setAdditionalMotorInsuranceModel =
                                                              await MotorService()
                                                                  .getAdditionalMotorPlans(
                                                                      context:
                                                                          context,
                                                                      productType:
                                                                          searchList[index]?.productType ??
                                                                              '');
                                                          setState(() {
                                                            selectedIndex =
                                                                index;
                                                            customerProvider
                                                                .resetSelectedMotorInsuranceList();
                                                            customerProvider
                                                                .resetInsurerMotorDetails();
                                                            customerProvider
                                                                .selectedMotorInsuranceList
                                                                .add(searchList[
                                                                    index]);
                                                            if (planType ==
                                                                'TPL') {
                                                              customerProvider
                                                                      ?.insuranceMotorDetails
                                                                      ?.makeYear =
                                                                  yearController
                                                                      .text;
                                                            } else if (makeYear !=
                                                                null) {
                                                              customerProvider
                                                                      ?.insuranceMotorDetails
                                                                      ?.makeYear =
                                                                  makeYear!;
                                                            }
                                                            if (seats != null) {
                                                              customerProvider
                                                                  ?.insuranceMotorDetails
                                                                  ?.noOfSeats = seats!;
                                                            }
                                                            customerProvider
                                                                    ?.insuranceMotorDetails
                                                                    ?.carValue =
                                                                valueController
                                                                    .text!;
                                                            if (brand != null) {
                                                              customerProvider
                                                                  ?.insuranceMotorDetails
                                                                  ?.makeModel = brand!;
                                                            }
                                                          });
                                                        },
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Container(
                                                              //  height: 96.0.h,
                                                              width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width *
                                                                  0.2,
                                                              decoration:
                                                                  BoxDecoration(
                                                                color: selectedIndex ==
                                                                        index
                                                                    ? AppColors
                                                                        .border
                                                                        .withOpacity(
                                                                            0.5)
                                                                    : AppColors
                                                                        .white,
                                                                border: Border(
                                                                  top: BorderSide(
                                                                      color: AppColors
                                                                          .border,
                                                                      width:
                                                                          1.0),
                                                                  bottom: BorderSide(
                                                                      color: AppColors
                                                                          .border,
                                                                      width:
                                                                          1.0),
                                                                  left: BorderSide(
                                                                      color: AppColors
                                                                          .border,
                                                                      width:
                                                                          1.0),
                                                                  right: BorderSide(
                                                                      color: AppColors
                                                                          .border,
                                                                      width:
                                                                          1.0),
                                                                ),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .only(
                                                                  bottomLeft: Radius
                                                                      .circular(
                                                                          10.0.s),
                                                                  topLeft: Radius
                                                                      .circular(
                                                                          10.0.s),
                                                                ),
                                                              ),
                                                              child: Center(
                                                                child: Image(
                                                                  image: AssetImage(
                                                                      searchList[index]?.productType ==
                                                                              'Gulf Trust'
                                                                          ? AppImages
                                                                              .SPLASH_SCREEN
                                                                          : searchList[index]?.productType == 'Tazur'
                                                                              ? AppImages.TAZUR
                                                                              : searchList[index]?.productType == 'Burgan'
                                                                                  ? AppImages.BURGAN
                                                                                  : searchList[index]?.productType == 'NLGIC'
                                                                                      ? AppImages.NATIONAL
                                                                                      : AppImages.WARBA
                                                                      // widget.isSelected
                                                                      // ? AppImages.SELECTED_PLAN
                                                                      // : AppImages.PLAN
                                                                      ),
                                                                  height:
                                                                      63.0.h,
                                                                  width: 63.0.w,
                                                                  fit: BoxFit
                                                                      .fill,
                                                                ),
                                                              ),
                                                            ),
                                                            Container(
                                                              // height: 96.0.h,
                                                              width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width *
                                                                  0.55,
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      left:
                                                                          8.0.s,
                                                                      top: 10.0
                                                                          .s,
                                                                      bottom:
                                                                          6.0.s,
                                                                      right: 8.0
                                                                          .s),
                                                              decoration:
                                                                  BoxDecoration(
                                                                color: selectedIndex ==
                                                                        index
                                                                    ? AppColors
                                                                        .border
                                                                        .withOpacity(
                                                                            0.5)
                                                                    : AppColors
                                                                        .white,
                                                                border: Border(
                                                                  top: BorderSide(
                                                                      color: AppColors
                                                                          .border,
                                                                      width:
                                                                          1.0),
                                                                  bottom: BorderSide(
                                                                      color: AppColors
                                                                          .border,
                                                                      width:
                                                                          1.0),
                                                                  left: BorderSide(
                                                                      color: selectedIndex ==
                                                                              index
                                                                          ? AppColors
                                                                              .primaryBlue
                                                                          : AppColors
                                                                              .border,
                                                                      width:
                                                                          0.0),
                                                                  right: BorderSide(
                                                                      color: AppColors
                                                                          .border,
                                                                      width:
                                                                          1.0),
                                                                ),
                                                              ),
                                                              child: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  Column(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Container(
                                                                        width:
                                                                            200.0.w,
                                                                        child:
                                                                            Text(
                                                                          searchList[index]?.productSummary.trim() ??
                                                                              '',
                                                                          style: buildAppTextTheme()
                                                                              .subtitle2!
                                                                              .copyWith(color: AppColors.primaryBlue),
                                                                          textAlign:
                                                                              TextAlign.left,
                                                                          maxLines:
                                                                              1,
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                        ),
                                                                      ),
                                                                      SizedBox(
                                                                        height:
                                                                            10.0.h,
                                                                      ),
                                                                      Row(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.start,
                                                                        children: [
                                                                          Text(
                                                                            '${searchList[index]?.validity} ${searchList[index]?.validityPeriod}',
                                                                            style:
                                                                                buildAppTextTheme().caption!.copyWith(color: AppColors.primaryBlue),
                                                                            textAlign:
                                                                                TextAlign.left,
                                                                            maxLines:
                                                                                1,
                                                                            overflow:
                                                                                TextOverflow.ellipsis,
                                                                          ),
                                                                          SizedBox(
                                                                              width: 5.0.w),
                                                                          if (searchList[index]?.productType != null &&
                                                                              searchList[index]?.productType.trim() != '')
                                                                            Container(
                                                                              height: 16.0.h,
                                                                              child: VerticalDivider(
                                                                                width: 2.0.w,
                                                                                thickness: 2.0.w,
                                                                                color: AppColors.border,
                                                                              ),
                                                                            ),
                                                                          SizedBox(
                                                                              width: 7.0.w),
                                                                          Text(
                                                                            searchList[index]?.productType ??
                                                                                '',
                                                                            style:
                                                                                buildAppTextTheme().caption!.copyWith(color: AppColors.primaryBlue),
                                                                            textAlign:
                                                                                TextAlign.left,
                                                                            maxLines:
                                                                                1,
                                                                            overflow:
                                                                                TextOverflow.ellipsis,
                                                                          ),
                                                                        ],
                                                                      ),
                                                                      SizedBox(
                                                                        height:
                                                                            9.0.h,
                                                                      ),
                                                                      InkWell(
                                                                        onTap: () => showModalBottomSheet(
                                                                            isScrollControlled: true,
                                                                            backgroundColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                            barrierColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                            context: context,
                                                                            builder: (BuildContext context) {
                                                                              return InsuranceDetailsBottomSheetWidget(
                                                                                  index: searchList[index]?.productType == 'Tazur'
                                                                                      ? 0
                                                                                      : searchList[index]?.productType == 'Burgan'
                                                                                          ? 1
                                                                                          : searchList[index]?.productType == 'NLGIC'
                                                                                              ? 2
                                                                                              : searchList[index]?.productType == 'Warba'
                                                                                                  ? 3
                                                                                                  : 4);
                                                                            }),
                                                                        child:
                                                                            Row(
                                                                          children: [
                                                                            Text(
                                                                              'Information',
                                                                              style: buildAppTextTheme().caption!.copyWith(color: AppColors.orange),
                                                                              textAlign: TextAlign.right,
                                                                            ),
                                                                            SizedBox(
                                                                              width: 8.0.w,
                                                                            ),
                                                                            Image(
                                                                              image: AssetImage(AppImages.INFO),
                                                                              height: 18.0.h,
                                                                              width: 18.0.w,
                                                                              color: AppColors.orange,
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            Stack(
                                                              children: [
                                                                Container(
                                                                  //height: 92.0.h,
                                                                  //width: 94.0.w,
                                                                  width: MediaQuery.of(
                                                                              context)
                                                                          .size
                                                                          .width *
                                                                      0.2,
                                                                  padding:
                                                                      EdgeInsets
                                                                          .all(2.0
                                                                              .s),
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: selectedIndex ==
                                                                            index
                                                                        ? AppColors
                                                                            .primaryBlue
                                                                        : AppColors
                                                                            .border,
                                                                    border: Border.all(
                                                                        color: selectedIndex ==
                                                                                index
                                                                            ? AppColors
                                                                                .primaryBlue
                                                                            : AppColors
                                                                                .border,
                                                                        width:
                                                                            1.0),
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .only(
                                                                      bottomRight:
                                                                          Radius.circular(
                                                                              10.0.s),
                                                                      topRight:
                                                                          Radius.circular(
                                                                              10.0.s),
                                                                    ),
                                                                  ),
                                                                  child: Center(
                                                                    child: Text(
                                                                      '${num.parse(searchList[index]?.premiumAmount ?? '').toStringAsFixed(3)}',
                                                                      style: buildAppTextTheme()
                                                                          .headline4!
                                                                          .copyWith(
                                                                              color: selectedIndex == index ? AppColors.white : AppColors.primaryBlue),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .right,
                                                                    ),
                                                                  ),
                                                                ),
                                                                Positioned(
                                                                  top: 1.0.s,
                                                                  right: 1.5.s,
                                                                  child:
                                                                      Container(
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      color: selectedIndex ==
                                                                              index
                                                                          ? AppColors
                                                                              .white
                                                                          : AppColors
                                                                              .primaryBlue,
                                                                      borderRadius:
                                                                          BorderRadius
                                                                              .only(
                                                                        bottomLeft:
                                                                            Radius.circular(10.0.s),
                                                                        topRight:
                                                                            Radius.circular(10.0.s),
                                                                      ),
                                                                    ),
                                                                    height:
                                                                        26.0.h,
                                                                    width:
                                                                        40.0.w,
                                                                    child:
                                                                        Center(
                                                                      child:
                                                                          Text(
                                                                        'KWD',
                                                                        style: buildAppTextTheme()
                                                                            .bodyText1!
                                                                            .copyWith(color: selectedIndex == index ? AppColors.primaryBlue : AppColors.white),
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  );
                                                }),
                                          ),
                                          SizedBox(
                                            height: 300.0.h,
                                          ),
                                        ],
                                      ),
                          ],
                        ),
                      ),
                    )
                  : Center(
                      child: Text(
                        'Coming soon',
                        style: buildAppTextTheme().headline3,
                        textAlign: TextAlign.start,
                      ),
                    ),
        ),
      ),
    );
  }
}
