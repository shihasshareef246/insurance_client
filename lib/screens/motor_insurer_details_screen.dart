import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:gt_test/common/buttons/primary_button.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/screens/login_screen.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:page_transition/page_transition.dart';
import 'package:gt_test/screens/motor_payment_details_screen.dart';
import '../models/motor_insurance_group_model.dart';
import '../providers/customer_provider.dart';
import 'package:gt_test/models/insurance_details_model.dart';
import '../providers/profile_provider.dart';
import 'init_screen.dart';

class MotorInsurerDetailsScreen extends StatefulWidget {
  bool isEdit;

  MotorInsurerDetailsScreen({
    this.isEdit = false,
  });
  @override
  _MotorInsurerDetailsScreenState createState() => _MotorInsurerDetailsScreenState();
}

class _MotorInsurerDetailsScreenState extends State<MotorInsurerDetailsScreen> {
  CustomerProvider customerProvider = CustomerProvider();
  ProfileProvider profileProvider = ProfileProvider();
  late TextEditingController nameController;
  late TextEditingController mobileNoController;
  // late TextEditingController phoneNoController;
  late TextEditingController nationalityController;
  late TextEditingController dobController;
  late TextEditingController civilIdController;
  late TextEditingController civilIdExpiryController;
  late TextEditingController passportNoController;
  late TextEditingController passportExpiryController;
  late TextEditingController emailIdController;
  late TextEditingController areaController;
  late TextEditingController blockNoController;
  late TextEditingController streetController;
  late TextEditingController houseNoController;
  late TextEditingController floorNoController;

  late TextEditingController licenseNoController;
  late TextEditingController licenseExpiryController;
  late TextEditingController vinNoController;
  late TextEditingController plateNoController;
  late TextEditingController makeModelController;
  late TextEditingController carValueController;
  late TextEditingController noOfSeatsController;
  late TextEditingController makeYearController;

  FocusNode nameFocus = FocusNode();
  FocusNode mobileNoFocus = FocusNode();
  // FocusNode phoneNoFocus = FocusNode();
  FocusNode nationalityFocus = FocusNode();
  FocusNode dobFocus = FocusNode();
  FocusNode civilIdFocus = FocusNode();
  FocusNode civilIdExpiryFocus = FocusNode();
  FocusNode passportNoFocus = FocusNode();
  FocusNode passportExpiryFocus = FocusNode();
  FocusNode emailIdFocus = FocusNode();
  FocusNode areaFocus = FocusNode();
  FocusNode blockNoFocus = FocusNode();
  FocusNode streetFocus = FocusNode();
  FocusNode houseNoFocus = FocusNode();
  FocusNode floorNoFocus = FocusNode();

  FocusNode licenseNoFocus = FocusNode();
  FocusNode licenseExpiryFocus = FocusNode();
  FocusNode vinNoFocus = FocusNode();
  FocusNode plateNoFocus = FocusNode();
  FocusNode makeModelFocus = FocusNode();
  FocusNode carValueFocus = FocusNode();
  FocusNode noOfSeatsFocus = FocusNode();
  FocusNode makeYearFocus = FocusNode();

  bool nameValid = false;
  bool mobileNoValid = false;
  // bool phoneNoValid = false;
  bool nationalityValid = false;
  bool dobValid = false;
  bool civilIdValid = false;
  bool civilIdExpiryValid = false;
  bool passportNoValid = false;
  bool passportExpiryValid = false;
  bool emailIdValid = false;
  bool areaValid = false;
  bool blockNoValid = false;
  bool streetValid = false;
  bool houseNoValid = false;
  bool floorNoValid = false;

  bool licenseNoValid = false;
  bool licenseExpiryValid = false;
  bool vinNoValid = false;
  bool plateNoValid = false;
  bool makeModelValid = false;
  bool carValueValid = false;
  bool noOfSeatsValid = false;
  bool makeYearValid = false;

  bool nameChanged = false;
  bool mobileNoChanged = false;
  // bool phoneNoChanged = false;
  bool nationalityChanged = false;
  bool dobChanged = false;
  bool civilIdChanged = false;
  bool civilIdExpiryChanged = false;
  bool passportNoChanged = false;
  bool passportExpiryChanged = false;
  bool emailIdChanged = false;
  bool areaChanged = false;
  bool blockNoChanged = false;
  bool streetChanged = false;
  bool houseNoChanged = false;
  bool floorNoChanged = false;

  bool licenseNoChanged = false;
  bool licenseExpiryChanged = false;
  bool vinNoChanged = false;
  bool plateNoChanged = false;
  bool makeModelChanged = false;
  bool carValueChanged = false;
  bool noOfSeatsChanged = false;
  bool makeYearChanged = false;

  bool isLoading = false;
  String gender = 'Male';
  String unitType = 'H';
  final dateFormat = DateFormat("dd-MM-yyyy");

  @override
  void initState() {
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    nameController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.insurerName ?? ''
            : '');
    if (nameController.text.isNotEmpty) {
      nameValid = true;
    }
    mobileNoController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.mobileNo
            : '');
    if (mobileNoController.text.isNotEmpty) {
      mobileNoValid = true;
    }
    nationalityController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.nationality ?? ''
            : '');
    if (nationalityController.text.isNotEmpty) {
      nationalityValid = true;
    }
    gender = ((customerProvider?.insuranceDetails != null &&
            customerProvider.insuranceDetails.isNotEmpty &&
            customerProvider?.insuranceDetails[0]?.gender != '')
        ? customerProvider?.insuranceDetails[0]?.gender ?? 'Male'
        : 'Male')!;
    dobController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.dob
            : '');
    if (dobController.text.isNotEmpty) {
      dobValid = true;
    }
    civilIdController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.civilIdNo ?? ''
            : '');
    if (civilIdController.text.isNotEmpty) {
      civilIdValid = true;
    }
    civilIdExpiryController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.civilIDExpDate
            : '');
    if (civilIdExpiryController.text.isNotEmpty) {
      civilIdExpiryValid = true;
    }
    passportNoController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.passportNo ?? ''
            : '');
    if (passportNoController.text.isNotEmpty) {
      passportNoValid = true;
    }
    passportExpiryController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.passportExpDate
            : '');
    if (passportExpiryController.text.isNotEmpty) {
      passportExpiryValid = true;
    }
    emailIdController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.emailId ?? ''
            : '');
    if (emailIdController.text.isNotEmpty) {
      emailIdValid = true;
    }
    areaController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.area ?? ''
            : '');
    if (areaController.text.isNotEmpty) {
      areaValid = true;
    }
    blockNoController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.blockNo ?? ''
            : '');
    if (blockNoController.text.isNotEmpty) {
      blockNoValid = true;
    }
    streetController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.street ?? ''
            : '');
    if (streetController.text.isNotEmpty) {
      streetValid = true;
    }
    houseNoController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.bNoOrHouseNo ?? ''
            : '');
    if (houseNoController.text.isNotEmpty) {
      houseNoValid = true;
    }
    floorNoController = TextEditingController(
        text: (customerProvider?.insuranceDetails != null &&
                customerProvider.insuranceDetails.isNotEmpty)
            ? customerProvider?.insuranceDetails[0]?.floorNo ?? ''
            : '');

    if (floorNoController.text.isNotEmpty) {
      floorNoValid = true;
    }
    licenseNoController = TextEditingController(
        text: (customerProvider?.insuranceMotorDetails != null)
            ? customerProvider?.insuranceMotorDetails?.licenseNo ?? ''
            : '');
    if (licenseNoController.text.isNotEmpty) {
      licenseNoValid = true;
    }
    licenseExpiryController = TextEditingController(
        text: (customerProvider?.insuranceMotorDetails != null)
            ? customerProvider?.insuranceMotorDetails?.licenseExpiry
            : '');
    if (licenseExpiryController.text.isNotEmpty) {
      licenseExpiryValid = true;
    }
    vinNoController = TextEditingController(
        text: (customerProvider?.insuranceMotorDetails != null)
            ? customerProvider?.insuranceMotorDetails?.vinNo ?? ''
            : '');
    if (vinNoController.text.isNotEmpty) {
      vinNoValid = true;
    }
    plateNoController = TextEditingController(
        text: (customerProvider?.insuranceMotorDetails != null)
            ? customerProvider?.insuranceMotorDetails?.plateNo ?? ''
            : '');
    if (plateNoController.text.isNotEmpty) {
      plateNoValid = true;
    }
    makeModelController = TextEditingController(
        text: (customerProvider?.insuranceMotorDetails != null)
            ? customerProvider?.insuranceMotorDetails?.makeModel ?? ''
            : '');
    if (makeModelController.text.isNotEmpty) {
      makeModelValid = true;
    }
    carValueController = TextEditingController(
        text: (customerProvider?.insuranceMotorDetails != null)
            ? customerProvider?.insuranceMotorDetails?.carValue ?? ''
            : '');
    if (carValueController.text.isNotEmpty) {
      carValueValid = true;
    }
    noOfSeatsController = TextEditingController(
        text: (customerProvider?.insuranceMotorDetails != null)
            ? customerProvider?.insuranceMotorDetails?.noOfSeats ?? ''
            : '');
    if (noOfSeatsController.text.isNotEmpty) {
      noOfSeatsValid = true;
    }
    makeYearController = TextEditingController(
        text: (customerProvider?.insuranceMotorDetails != null)
            ? customerProvider?.insuranceMotorDetails?.makeYear
            : '');
    if (makeYearController.text.isNotEmpty) {
      makeYearValid = true;
    }
    super.initState();
  }

  @override
  void dispose() {
    nameController.dispose();
    mobileNoController.dispose();
    nationalityController.dispose();
    dobController.dispose();
    civilIdController.dispose();
    civilIdExpiryController.dispose();
    passportNoController.dispose();
    passportExpiryController.dispose();
    emailIdController.dispose();
    areaController.dispose();
    blockNoController.dispose();
    streetController.dispose();
    houseNoController.dispose();
    floorNoController.dispose();

    licenseNoController.dispose();
    licenseExpiryController.dispose();
    vinNoController.dispose();
    plateNoController.dispose();
    makeModelController.dispose();
    carValueController.dispose();
    noOfSeatsController.dispose();
    makeYearController.dispose();

    nameFocus.dispose();
    mobileNoFocus.dispose();
    nationalityFocus.dispose();
    dobFocus.dispose();
    civilIdFocus.dispose();
    civilIdExpiryFocus.dispose();
    passportNoFocus.dispose();
    passportExpiryFocus.dispose();
    emailIdFocus.dispose();
    areaFocus.dispose();
    blockNoFocus.dispose();
    streetFocus.dispose();
    houseNoFocus.dispose();
    floorNoFocus.dispose();

    licenseNoFocus.dispose();
    licenseExpiryFocus.dispose();
    vinNoFocus.dispose();
    plateNoFocus.dispose();
    makeModelFocus.dispose();
    carValueFocus.dispose();
    noOfSeatsFocus.dispose();
    makeYearFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.primaryBlue,
            leading: InkWell(
              onTap: () => Navigations.pop(context),
              child: Icon(
                Icons.arrow_back_ios,
                color: AppColors.white,
                size: 20.0.ics,
              ),
            ),
            title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Insurer Details',
                    style: buildAppTextTheme()
                        .headline3
                        ?.copyWith(color: AppColors.white),
                    textAlign: TextAlign.start,
                  ),
                  // SizedBox(
                  //   height: 5.0.h,
                  // ),
                  // Container(
                  //   width: 200.0.w,
                  //   child: Text(
                  //     desc!,
                  //     style: buildAppTextTheme().bodyText2,
                  //     textAlign: TextAlign.start,
                  //   ),
                  // ),
                ]),
          ),
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  left: 30.0.s, right: 30.0.s, top: 30.0.s, bottom: 60.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Please confirm your details before proceeding',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: nameFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),
                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp("[a-zA-Z ]")),
                      ],
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(nationalityFocus),
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: nameFocus,
                      style: nameFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: nameController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: nameFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text("Name",
                                style: nameFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: nameFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.nameChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.nameValid = false;
                          });
                        } else {
                          setState(() {
                            this.nameValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: nationalityFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),
                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(civilIdFocus),
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: nationalityFocus,
                      style: nationalityFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: nationalityController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: nationalityFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text("Nationality",
                                style: nationalityFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: nationalityFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.nationalityChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.nationalityValid = false;
                          });
                        } else {
                          setState(() {
                            this.nationalityValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      border: Border.all(color: AppColors.border),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 30.0.h,
                          width: 30.0.w,
                          child: Radio(
                            value: 'Male',
                            groupValue: this.gender,
                            onChanged: (val) {
                              setState(() {
                                this.gender = val.toString();
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 3.0.w,
                        ),
                        Text(
                          'Male',
                          style: buildAppTextTheme().headline6,
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(
                          width: 7.0.w,
                        ),
                        Container(
                          height: 30.0.h,
                          width: 30.0.w,
                          child: Radio(
                            value: 'Female',
                            groupValue: this.gender,
                            onChanged: (val) {
                              setState(() {
                                this.gender = val.toString();
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 3.0.w,
                        ),
                        Text(
                          'Female',
                          style: buildAppTextTheme().headline5,
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: dobFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(10.0.s),
                        bottom: Radius.zero,
                      ),
                    ),
                    child: DateTimeField(
                      focusNode: dobFocus,
                      style: dobFocus.hasFocus
                          ? buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.lightBlack)
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.focusText),
                      decoration: InputDecoration(
                        label: Row(
                          children: [
                            Text('*',
                                style: dobFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text("Date of Birth",
                                style: dobFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: dobFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        focusColor: AppColors.searchBlue,
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        border: InputBorder.none,
                      ),
                      controller: dobController,
                      onChanged: (value) {
                        setState(() {
                          this.dobChanged = true;
                        });
                        if (value != null &&
                            value.month <= 12 &&
                            value.month >= 1 &&
                            value.year <= 2100 &&
                            value.year >= 1900 &&
                            value.day <= 31 &&
                            value.day >= 1) {
                          setState(() {
                            this.dobValid = true;
                          });
                        } else {
                          setState(() {
                            this.dobValid = false;
                          });
                        }
                      },
                      format: dateFormat,
                      onShowPicker: (context, currentValue) {
                        return showDatePicker(
                            context: context,
                            firstDate: DateTime(1900),
                            initialDate: currentValue ?? DateTime.now(),
                            lastDate: DateTime(2100));
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: civilIdFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),
                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(emailIdFocus),
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: civilIdFocus,
                      style: civilIdFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.number,
                      controller: civilIdController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: civilIdFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text("Civil ID",
                                style: civilIdFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: civilIdFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.civilIdChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.civilIdValid = false;
                          });
                        } else {
                          setState(() {
                            this.civilIdValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  // Container(
                  //   height: 60.0.h,
                  //   width: 368.0.w,
                  //   decoration: BoxDecoration(
                  //     color: civilIdExpiryFocus.hasFocus
                  //         ? AppColors.paleBlue
                  //         : AppColors.white,
                  //     border: Border.all(color: AppColors.border),
                  //     borderRadius: BorderRadius.vertical(
                  //       top: Radius.circular(10.0.s),
                  //       bottom: Radius.zero,
                  //     ),
                  //   ),
                  //   child: DateTimeField(
                  //     focusNode: civilIdExpiryFocus,
                  //     style: civilIdExpiryFocus.hasFocus
                  //         ? buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.lightBlack)
                  //         : buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.focusText),
                  //     decoration: InputDecoration(
                  //       labelText: 'Civil Id expiry date',
                  //       labelStyle: civilIdExpiryFocus.hasFocus
                  //           ? buildAppTextTheme().subtitle2
                  //           : buildAppTextTheme().headline6,
                  //       focusColor: AppColors.searchBlue,
                  //       isDense: true,
                  //       contentPadding: EdgeInsets.only(
                  //         left: 10.0.s,
                  //         top: 8.0.s,
                  //         bottom: 8.0.s,
                  //         right: 10.0.s,
                  //       ),
                  //       border: InputBorder.none,
                  //     ),
                  //     controller: civilIdExpiryController,
                  //     onChanged: (value) {
                  //       setState(() {
                  //         this.civilIdExpiryChanged = true;
                  //       });
                  //       if (value != null &&
                  //           value.month <= 12 &&
                  //           value.month >= 1 &&
                  //           value.year <= 2100 &&
                  //           value.year >= 1900 &&
                  //           value.day <= 31 &&
                  //           value.day >= 1) {
                  //         setState(() {
                  //           this.civilIdExpiryValid = true;
                  //         });
                  //       } else {
                  //         setState(() {
                  //           this.civilIdExpiryValid = false;
                  //         });
                  //       }
                  //     },
                  //     format: dateFormat,
                  //     onShowPicker: (context, currentValue) {
                  //       return showDatePicker(
                  //           context: context,
                  //           firstDate: DateTime(1900),
                  //           initialDate: currentValue ?? DateTime.now(),
                  //           lastDate: DateTime(2100));
                  //     },
                  //   ),
                  // ),
                  // SizedBox(height: 12.0.h),
                  // Container(
                  //   height: 60.0.h,
                  //   width: 368.0.w,
                  //   decoration: BoxDecoration(
                  //     color: licenseNoFocus.hasFocus
                  //         ? AppColors.paleBlue
                  //         : AppColors.white,
                  //     border: Border.all(color: AppColors.border),
                  //
                  //     // border: Border(
                  //     //     top: BorderSide(color: AppColors.border, width: 1.0),
                  //     //     bottom:
                  //     //         BorderSide(color: AppColors.border, width: 1.0),
                  //     //     right:
                  //     //         BorderSide(color: AppColors.border, width: 1.0)),
                  //     borderRadius: BorderRadius.only(
                  //       bottomRight: Radius.circular(6.0.s),
                  //       topRight: Radius.circular(6.0.s),
                  //     ),
                  //   ),
                  //   child: TextFormField(
                  //     scrollPadding: EdgeInsets.only(bottom: 100),
                  //     focusNode: licenseNoFocus,
                  //     style: licenseNoFocus.hasFocus
                  //         ? buildAppTextTheme().headline5
                  //         : buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.black),
                  //     keyboardType: TextInputType.name,
                  //     controller: licenseNoController,
                  //     obscureText: false,
                  //     decoration: InputDecoration(
                  //       isDense: true,
                  //       contentPadding: EdgeInsets.only(
                  //         left: 10.0.s,
                  //         top: 8.0.s,
                  //         bottom: 8.0.s,
                  //         right: 10.0.s,
                  //       ),
                  //       labelText: 'License number',
                  //       labelStyle: licenseNoFocus.hasFocus
                  //           ? buildAppTextTheme().subtitle2
                  //           : buildAppTextTheme().headline6,
                  //       border: InputBorder.none,
                  //       focusedBorder: UnderlineInputBorder(
                  //         borderSide: BorderSide(color: AppColors.primaryBlue),
                  //         borderRadius: BorderRadius.vertical(
                  //           top: Radius.circular(10.0.s),
                  //           bottom: Radius.zero,
                  //         ),
                  //       ),
                  //     ),
                  //     autovalidateMode: AutovalidateMode.onUserInteraction,
                  //     onChanged: (value) {
                  //       setState(() {
                  //         this.licenseNoChanged = true;
                  //       });
                  //       if (value.isEmpty) {
                  //         setState(() {
                  //           this.licenseNoValid = false;
                  //         });
                  //       } else {
                  //         setState(() {
                  //           this.licenseNoValid = true;
                  //         });
                  //       }
                  //     },
                  //   ),
                  // ),
                  // SizedBox(height: 12.0.h),
                  // Container(
                  //   height: 60.0.h,
                  //   width: 368.0.w,
                  //   decoration: BoxDecoration(
                  //     color: licenseExpiryFocus.hasFocus
                  //         ? AppColors.paleBlue
                  //         : AppColors.white,
                  //     border: Border.all(color: AppColors.border),
                  //
                  //     // border: Border(
                  //     //     top: BorderSide(color: AppColors.border, width: 1.0),
                  //     //     bottom:
                  //     //         BorderSide(color: AppColors.border, width: 1.0),
                  //     //     right:
                  //     //         BorderSide(color: AppColors.border, width: 1.0)),
                  //     borderRadius: BorderRadius.only(
                  //       bottomRight: Radius.circular(6.0.s),
                  //       topRight: Radius.circular(6.0.s),
                  //     ),
                  //   ),
                  //   child: DateTimeField(
                  //     focusNode: licenseExpiryFocus,
                  //     style: licenseExpiryFocus.hasFocus
                  //         ? buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.lightBlack)
                  //         : buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.focusText),
                  //     decoration: InputDecoration(
                  //       labelText: 'License expiry date',
                  //       labelStyle: licenseExpiryFocus.hasFocus
                  //           ? buildAppTextTheme().subtitle2
                  //           : buildAppTextTheme().headline6,
                  //       focusColor: AppColors.searchBlue,
                  //       isDense: true,
                  //       contentPadding: EdgeInsets.only(
                  //         left: 10.0.s,
                  //         top: 8.0.s,
                  //         bottom: 8.0.s,
                  //         right: 10.0.s,
                  //       ),
                  //       border: InputBorder.none,
                  //     ),
                  //     controller: licenseExpiryController,
                  //     onChanged: (value) {
                  //       setState(() {
                  //         this.licenseExpiryChanged = true;
                  //       });
                  //       if (value != null &&
                  //           value.month <= 12 &&
                  //           value.month >= 1 &&
                  //           value.year <= 2100 &&
                  //           value.year >= 1900 &&
                  //           value.day <= 31 &&
                  //           value.day >= 1) {
                  //         setState(() {
                  //           this.licenseExpiryValid = true;
                  //         });
                  //       } else {
                  //         setState(() {
                  //           this.licenseExpiryValid = false;
                  //         });
                  //       }
                  //     },
                  //     format: dateFormat,
                  //     onShowPicker: (context, currentValue) {
                  //       return showDatePicker(
                  //           context: context,
                  //           firstDate: DateTime(1900),
                  //           initialDate: currentValue ?? DateTime.now(),
                  //           lastDate: DateTime(2100));
                  //     },
                  //   ),
                  // ),
                  // SizedBox(height: 12.0.h),
                  // Container(
                  //   height: 60.0.h,
                  //   width: 368.0.w,
                  //   decoration: BoxDecoration(
                  //     color: passportNoFocus.hasFocus
                  //         ? AppColors.paleBlue
                  //         : AppColors.white,
                  //     border: Border.all(color: AppColors.border),
                  //     // border: Border(
                  //     //     top: BorderSide(color: AppColors.border, width: 1.0),
                  //     //     bottom:
                  //     //         BorderSide(color: AppColors.border, width: 1.0),
                  //     //     right:
                  //     //         BorderSide(color: AppColors.border, width: 1.0)),
                  //     borderRadius: BorderRadius.only(
                  //       bottomRight: Radius.circular(6.0.s),
                  //       topRight: Radius.circular(6.0.s),
                  //     ),
                  //   ),
                  //   child: TextFormField(
                  //     scrollPadding: EdgeInsets.only(bottom: 100),
                  //     focusNode: passportNoFocus,
                  //     style: passportNoFocus.hasFocus
                  //         ? buildAppTextTheme().headline5
                  //         : buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.black),
                  //     keyboardType: TextInputType.name,
                  //     controller: passportNoController,
                  //     obscureText: false,
                  //     decoration: InputDecoration(
                  //       isDense: true,
                  //       contentPadding: EdgeInsets.only(
                  //         left: 10.0.s,
                  //         top: 8.0.s,
                  //         bottom: 8.0.s,
                  //         right: 10.0.s,
                  //       ),
                  //       labelText: 'Passport Number',
                  //       labelStyle: passportNoFocus.hasFocus
                  //           ? buildAppTextTheme().subtitle2
                  //           : buildAppTextTheme().headline6,
                  //       border: InputBorder.none,
                  //       focusedBorder: UnderlineInputBorder(
                  //         borderSide: BorderSide(color: AppColors.primaryBlue),
                  //         borderRadius: BorderRadius.vertical(
                  //           top: Radius.circular(10.0.s),
                  //           bottom: Radius.zero,
                  //         ),
                  //       ),
                  //     ),
                  //     autovalidateMode: AutovalidateMode.onUserInteraction,
                  //     onChanged: (value) {
                  //       setState(() {
                  //         this.passportNoChanged = true;
                  //       });
                  //       if (value.isEmpty) {
                  //         setState(() {
                  //           this.passportNoValid = false;
                  //         });
                  //       } else {
                  //         setState(() {
                  //           this.passportNoValid = true;
                  //         });
                  //       }
                  //     },
                  //   ),
                  // ),
                  // SizedBox(height: 12.0.h),
                  // Container(
                  //   height: 60.0.h,
                  //   width: 368.0.w,
                  //   decoration: BoxDecoration(
                  //     color: passportExpiryFocus.hasFocus
                  //         ? AppColors.paleBlue
                  //         : AppColors.white,
                  //     border: Border.all(color: AppColors.border),
                  //     borderRadius: BorderRadius.vertical(
                  //       top: Radius.circular(10.0.s),
                  //       bottom: Radius.zero,
                  //     ),
                  //   ),
                  //   child: DateTimeField(
                  //     focusNode: passportExpiryFocus,
                  //     style: passportExpiryFocus.hasFocus
                  //         ? buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.lightBlack)
                  //         : buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.focusText),
                  //     decoration: InputDecoration(
                  //       labelText: 'Passport expiry date',
                  //       labelStyle: passportExpiryFocus.hasFocus
                  //           ? buildAppTextTheme().subtitle2
                  //           : buildAppTextTheme().headline6,
                  //       focusColor: AppColors.searchBlue,
                  //       isDense: true,
                  //       contentPadding: EdgeInsets.only(
                  //         left: 10.0.s,
                  //         top: 8.0.s,
                  //         bottom: 8.0.s,
                  //         right: 10.0.s,
                  //       ),
                  //       border: InputBorder.none,
                  //     ),
                  //     controller: passportExpiryController,
                  //     onChanged: (value) {
                  //       setState(() {
                  //         this.passportExpiryChanged = true;
                  //       });
                  //       if (value != null &&
                  //           value.month <= 12 &&
                  //           value.month >= 1 &&
                  //           value.year <= 2100 &&
                  //           value.year >= 1900 &&
                  //           value.day <= 31 &&
                  //           value.day >= 1) {
                  //         setState(() {
                  //           this.passportExpiryValid = true;
                  //         });
                  //       } else {
                  //         setState(() {
                  //           this.passportExpiryValid = false;
                  //         });
                  //       }
                  //     },
                  //     format: dateFormat,
                  //     onShowPicker: (context, currentValue) {
                  //       return showDatePicker(
                  //           context: context,
                  //           firstDate: DateTime(1900),
                  //           initialDate: currentValue ?? DateTime.now(),
                  //           lastDate: DateTime(2100));
                  //     },
                  //   ),
                  // ),
                  // SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: emailIdFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),
                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(mobileNoFocus),
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: emailIdFocus,
                      style: emailIdFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.emailAddress,
                      controller: emailIdController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: emailIdFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text("Email Id",
                                style: emailIdFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: emailIdFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.emailIdChanged = true;
                        });
                        if (value.isEmpty ||
                            !value.contains('@') ||
                            !value.contains('.') ||
                            value.length <= 5) {
                          setState(() {
                            this.emailIdValid = false;
                          });
                        } else {
                          setState(() {
                            this.emailIdValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  // Container(
                  //   height: 60.0.h,
                  //   width: 368.0.w,
                  //   decoration: BoxDecoration(
                  //     color: phoneNoFocus.hasFocus
                  //         ? AppColors.paleBlue
                  //         : AppColors.white,
                  //     border: Border.all(color: AppColors.border),
                  //     // border: Border(
                  //     //     top: BorderSide(color: AppColors.border, width: 1.0),
                  //     //     bottom:
                  //     //         BorderSide(color: AppColors.border, width: 1.0),
                  //     //     right:
                  //     //         BorderSide(color: AppColors.border, width: 1.0)),
                  //     borderRadius: BorderRadius.only(
                  //       bottomRight: Radius.circular(6.0.s),
                  //       topRight: Radius.circular(6.0.s),
                  //     ),
                  //   ),
                  //   child: TextFormField(
                  //     scrollPadding: EdgeInsets.only(bottom: 100),
                  //     focusNode: phoneNoFocus,
                  //     style: phoneNoFocus.hasFocus
                  //         ? buildAppTextTheme().headline5
                  //         : buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.black),
                  //     keyboardType: TextInputType.number,
                  //     controller: phoneNoController,
                  //     obscureText: false,
                  //     decoration: InputDecoration(
                  //       isDense: true,
                  //       contentPadding: EdgeInsets.only(
                  //         left: 10.0.s,
                  //         top: 8.0.s,
                  //         bottom: 8.0.s,
                  //         right: 10.0.s,
                  //       ),
                  //       labelText: 'Phone No',
                  //       labelStyle: phoneNoFocus.hasFocus
                  //           ? buildAppTextTheme().subtitle2
                  //           : buildAppTextTheme().headline6,
                  //       border: InputBorder.none,
                  //       focusedBorder: UnderlineInputBorder(
                  //         borderSide: BorderSide(color: AppColors.primaryBlue),
                  //         borderRadius: BorderRadius.vertical(
                  //           top: Radius.circular(10.0.s),
                  //           bottom: Radius.zero,
                  //         ),
                  //       ),
                  //     ),
                  //     autovalidateMode: AutovalidateMode.onUserInteraction,
                  //     onChanged: (value) {
                  //       setState(() {
                  //         this.phoneNoChanged = true;
                  //       });
                  //       if (value.isEmpty) {
                  //         setState(() {
                  //           this.phoneNoValid = false;
                  //         });
                  //       } else {
                  //         setState(() {
                  //           this.phoneNoValid = true;
                  //         });
                  //       }
                  //     },
                  //   ),
                  // ),
                  // SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: mobileNoFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),
                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: mobileNoFocus,
                      style: mobileNoFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(houseNoFocus),
                      keyboardType: TextInputType.number,
                      controller: mobileNoController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: mobileNoFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text("Mobile No",
                                style: mobileNoFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: mobileNoFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.mobileNoChanged = true;
                        });
                        if (value.isEmpty || value.length != 8) {
                          setState(() {
                            this.mobileNoValid = false;
                          });
                        } else {
                          setState(() {
                            this.mobileNoValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      border: Border.all(color: AppColors.border),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 30.0.h,
                          width: 30.0.w,
                          child: Radio(
                            value: 'H',
                            groupValue: this.unitType,
                            onChanged: (val) {
                              setState(() {
                                this.unitType = val.toString();
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 3.0.w,
                        ),
                        Text(
                          'House',
                          style: buildAppTextTheme().headline6,
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(
                          width: 7.0.w,
                        ),
                        Container(
                          height: 30.0.h,
                          width: 30.0.w,
                          child: Radio(
                            value: 'F',
                            groupValue: this.unitType,
                            onChanged: (val) {
                              setState(() {
                                this.unitType = val.toString();
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          width: 3.0.w,
                        ),
                        Text(
                          'Flat',
                          style: buildAppTextTheme().headline5,
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: houseNoFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (_) => FocusScope.of(context)
                          .requestFocus(
                              unitType == 'F' ? floorNoFocus : blockNoFocus),
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: houseNoFocus,
                      style: houseNoFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: houseNoController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: houseNoFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text(
                                unitType == 'F'
                                    ? 'Flat number'
                                    : 'House number',
                                style: houseNoFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: houseNoFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.houseNoChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.houseNoValid = false;
                          });
                        } else {
                          setState(() {
                            this.houseNoValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  if (unitType == 'F')
                    Container(
                      height: 60.0.h,
                      width: 368.0.w,
                      decoration: BoxDecoration(
                        color: floorNoFocus.hasFocus
                            ? AppColors.paleBlue
                            : AppColors.white,
                        border: Border.all(color: AppColors.border),

                        // border: Border(
                        //     top: BorderSide(color: AppColors.border, width: 1.0),
                        //     bottom:
                        //         BorderSide(color: AppColors.border, width: 1.0),
                        //     right:
                        //         BorderSide(color: AppColors.border, width: 1.0)),
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(6.0.s),
                          topRight: Radius.circular(6.0.s),
                        ),
                      ),
                      child: TextFormField(
                        onFieldSubmitted: (_) =>
                            FocusScope.of(context).requestFocus(blockNoFocus),
                        scrollPadding: EdgeInsets.only(bottom: 100),
                        focusNode: floorNoFocus,
                        style: floorNoFocus.hasFocus
                            ? buildAppTextTheme().headline5
                            : buildAppTextTheme()
                                .subtitle1!
                                .copyWith(color: AppColors.black),
                        keyboardType: TextInputType.name,
                        controller: floorNoController,
                        obscureText: false,
                        decoration: InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.only(
                            left: 10.0.s,
                            top: 8.0.s,
                            bottom: 8.0.s,
                            right: 10.0.s,
                          ),
                          labelText: 'Floor number',
                          labelStyle: floorNoFocus.hasFocus
                              ? buildAppTextTheme().subtitle2
                              : buildAppTextTheme().headline6,
                          border: InputBorder.none,
                          focusedBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primaryBlue),
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(10.0.s),
                              bottom: Radius.zero,
                            ),
                          ),
                        ),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        onChanged: (value) {
                          setState(() {
                            this.floorNoChanged = true;
                          });
                          if (value.isEmpty) {
                            setState(() {
                              this.floorNoValid = false;
                            });
                          } else {
                            setState(() {
                              this.floorNoValid = true;
                            });
                          }
                        },
                      ),
                    ),
                  if (unitType == 'F') SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: blockNoFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(streetFocus),
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: blockNoFocus,
                      style: blockNoFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: blockNoController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: blockNoFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text('Block number',
                                style: blockNoFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: blockNoFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.blockNoChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.blockNoValid = false;
                          });
                        } else {
                          setState(() {
                            this.blockNoValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: streetFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(areaFocus),
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: streetFocus,
                      style: streetFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: streetController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: streetFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text('Street',
                                style: streetFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: streetFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.streetChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.streetValid = false;
                          });
                        } else {
                          setState(() {
                            this.streetValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: areaFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),
                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(vinNoFocus),
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: areaFocus,
                      style: areaFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: areaController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: areaFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text('Area',
                                style: areaFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: areaFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.areaChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.areaValid = false;
                          });
                        } else {
                          setState(() {
                            this.areaValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: vinNoFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(plateNoFocus),
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: vinNoFocus,
                      style: vinNoFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: vinNoController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: vinNoFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text('Chasis number',
                                style: vinNoFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: vinNoFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.vinNoChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.vinNoValid = false;
                          });
                        } else {
                          setState(() {
                            this.vinNoValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: plateNoFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(makeModelFocus),
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: plateNoFocus,
                      style: plateNoFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: plateNoController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: plateNoFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text('Plate number',
                                style: plateNoFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: plateNoFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.plateNoChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.plateNoValid = false;
                          });
                        } else {
                          setState(() {
                            this.plateNoValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  if (customerProvider
                          ?.selectedMotorInsuranceList[0]?.planType ==
                      'Comprehensive')
                    SizedBox(height: 12.0.h),
                  if (customerProvider
                          ?.selectedMotorInsuranceList[0]?.planType ==
                      'Comprehensive')
                    Container(
                      height: 60.0.h,
                      width: 368.0.w,
                      decoration: BoxDecoration(
                        color: makeModelFocus.hasFocus
                            ? AppColors.paleBlue
                            : AppColors.white,
                        border: Border.all(color: AppColors.border),

                        // border: Border(
                        //     top: BorderSide(color: AppColors.border, width: 1.0),
                        //     bottom:
                        //         BorderSide(color: AppColors.border, width: 1.0),
                        //     right:
                        //         BorderSide(color: AppColors.border, width: 1.0)),
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(6.0.s),
                          topRight: Radius.circular(6.0.s),
                        ),
                      ),
                      child: TextFormField(
                        onFieldSubmitted: (_) =>
                            FocusScope.of(context).requestFocus(carValueFocus),
                        enabled: false,
                        scrollPadding: EdgeInsets.only(bottom: 100),
                        focusNode: makeModelFocus,
                        style: makeModelFocus.hasFocus
                            ? buildAppTextTheme().headline5
                            : buildAppTextTheme()
                                .subtitle1!
                                .copyWith(color: AppColors.black),
                        keyboardType: TextInputType.name,
                        controller: makeModelController,
                        obscureText: false,
                        decoration: InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.only(
                            left: 10.0.s,
                            top: 8.0.s,
                            bottom: 8.0.s,
                            right: 10.0.s,
                          ),
                          label: Row(
                            children: [
                              Text('*',
                                  style: makeModelFocus.hasFocus
                                      ? buildAppTextTheme()
                                          .subtitle2!
                                          .copyWith(color: Colors.red)
                                      : buildAppTextTheme()
                                          .headline6!
                                          .copyWith(color: Colors.red)),
                              Padding(
                                padding: EdgeInsets.all(3.0),
                              ),
                              Text('Make model',
                                  style: makeModelFocus.hasFocus
                                      ? buildAppTextTheme().subtitle2
                                      : buildAppTextTheme().headline6)
                            ],
                          ),
                          labelStyle: makeModelFocus.hasFocus
                              ? buildAppTextTheme().subtitle2
                              : buildAppTextTheme().headline6,
                          border: InputBorder.none,
                          focusedBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primaryBlue),
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(10.0.s),
                              bottom: Radius.zero,
                            ),
                          ),
                        ),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        onChanged: (value) {
                          setState(() {
                            this.makeModelChanged = true;
                          });
                          if (value.isEmpty) {
                            setState(() {
                              this.makeModelValid = false;
                            });
                          } else {
                            setState(() {
                              this.makeModelValid = true;
                            });
                          }
                        },
                      ),
                    ),
                  if (customerProvider
                          ?.selectedMotorInsuranceList[0]?.planType ==
                      'Comprehensive')
                    SizedBox(height: 12.0.h),
                  if (customerProvider
                          ?.selectedMotorInsuranceList[0]?.planType ==
                      'Comprehensive')
                    Container(
                      height: 60.0.h,
                      width: 368.0.w,
                      decoration: BoxDecoration(
                        color: carValueFocus.hasFocus
                            ? AppColors.paleBlue
                            : AppColors.white,
                        border: Border.all(color: AppColors.border),

                        // border: Border(
                        //     top: BorderSide(color: AppColors.border, width: 1.0),
                        //     bottom:
                        //         BorderSide(color: AppColors.border, width: 1.0),
                        //     right:
                        //         BorderSide(color: AppColors.border, width: 1.0)),
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(6.0.s),
                          topRight: Radius.circular(6.0.s),
                        ),
                      ),
                      child: TextFormField(
                        onFieldSubmitted: (_) =>
                            FocusScope.of(context).requestFocus(noOfSeatsFocus),
                        enabled: false,
                        scrollPadding: EdgeInsets.only(bottom: 100),
                        focusNode: carValueFocus,
                        style: carValueFocus.hasFocus
                            ? buildAppTextTheme().headline5
                            : buildAppTextTheme()
                                .subtitle1!
                                .copyWith(color: AppColors.black),
                        keyboardType: TextInputType.number,
                        controller: carValueController,
                        obscureText: false,
                        decoration: InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.only(
                            left: 10.0.s,
                            top: 8.0.s,
                            bottom: 8.0.s,
                            right: 10.0.s,
                          ),
                          label: Row(
                            children: [
                              Text('*',
                                  style: carValueFocus.hasFocus
                                      ? buildAppTextTheme()
                                          .subtitle2!
                                          .copyWith(color: Colors.red)
                                      : buildAppTextTheme()
                                          .headline6!
                                          .copyWith(color: Colors.red)),
                              Padding(
                                padding: EdgeInsets.all(3.0),
                              ),
                              Text('Car value',
                                  style: carValueFocus.hasFocus
                                      ? buildAppTextTheme().subtitle2
                                      : buildAppTextTheme().headline6)
                            ],
                          ),
                          labelStyle: carValueFocus.hasFocus
                              ? buildAppTextTheme().subtitle2
                              : buildAppTextTheme().headline6,
                          border: InputBorder.none,
                          focusedBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primaryBlue),
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(10.0.s),
                              bottom: Radius.zero,
                            ),
                          ),
                        ),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        onChanged: (value) {
                          setState(() {
                            this.carValueChanged = true;
                          });
                          if (value.isEmpty) {
                            setState(() {
                              this.carValueValid = false;
                            });
                          } else {
                            setState(() {
                              this.carValueValid = true;
                            });
                          }
                        },
                      ),
                    ),
                  if (customerProvider
                          ?.selectedMotorInsuranceList[0]?.planType ==
                      'TPL')
                    SizedBox(height: 12.0.h),
                  if (customerProvider
                          ?.selectedMotorInsuranceList[0]?.planType ==
                      'TPL')
                    Container(
                      height: 60.0.h,
                      width: 368.0.w,
                      decoration: BoxDecoration(
                        color: noOfSeatsFocus.hasFocus
                            ? AppColors.paleBlue
                            : AppColors.white,
                        border: Border.all(color: AppColors.border),

                        // border: Border(
                        //     top: BorderSide(color: AppColors.border, width: 1.0),
                        //     bottom:
                        //         BorderSide(color: AppColors.border, width: 1.0),
                        //     right:
                        //         BorderSide(color: AppColors.border, width: 1.0)),
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(6.0.s),
                          topRight: Radius.circular(6.0.s),
                        ),
                      ),
                      child: TextFormField(
                        onFieldSubmitted: (_) =>
                            FocusScope.of(context).requestFocus(makeYearFocus),
                        enabled: false,
                        scrollPadding: EdgeInsets.only(bottom: 100),
                        focusNode: noOfSeatsFocus,
                        style: noOfSeatsFocus.hasFocus
                            ? buildAppTextTheme().headline5
                            : buildAppTextTheme()
                                .subtitle1!
                                .copyWith(color: AppColors.black),
                        keyboardType: TextInputType.number,
                        controller: noOfSeatsController,
                        obscureText: false,
                        decoration: InputDecoration(
                          isDense: true,
                          contentPadding: EdgeInsets.only(
                            left: 10.0.s,
                            top: 8.0.s,
                            bottom: 8.0.s,
                            right: 10.0.s,
                          ),
                          label: Row(
                            children: [
                              Text('*',
                                  style: noOfSeatsFocus.hasFocus
                                      ? buildAppTextTheme()
                                          .subtitle2!
                                          .copyWith(color: Colors.red)
                                      : buildAppTextTheme()
                                          .headline6!
                                          .copyWith(color: Colors.red)),
                              Padding(
                                padding: EdgeInsets.all(3.0),
                              ),
                              Text('No of seats',
                                  style: noOfSeatsFocus.hasFocus
                                      ? buildAppTextTheme().subtitle2
                                      : buildAppTextTheme().headline6)
                            ],
                          ),
                          labelStyle: noOfSeatsFocus.hasFocus
                              ? buildAppTextTheme().subtitle2
                              : buildAppTextTheme().headline6,
                          border: InputBorder.none,
                          focusedBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primaryBlue),
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(10.0.s),
                              bottom: Radius.zero,
                            ),
                          ),
                        ),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        onChanged: (value) {
                          setState(() {
                            this.noOfSeatsChanged = true;
                          });
                          if (value.isEmpty) {
                            setState(() {
                              this.noOfSeatsValid = false;
                            });
                          } else {
                            setState(() {
                              this.noOfSeatsValid = true;
                            });
                          }
                        },
                      ),
                    ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: makeYearFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).requestFocus(civilIdFocus),
                      enabled: false,
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: makeYearFocus,
                      style: makeYearFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.number,
                      controller: makeYearController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: makeYearFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text('Make year',
                                style: makeYearFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: makeYearFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.makeYearChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.makeYearValid = false;
                          });
                        } else {
                          setState(() {
                            this.makeYearValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 43.0.h),
                  Text(
                    'Please confirm your details before proceeding.',
                    style: buildAppTextTheme()
                        .headline6
                        ?.copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(height: 16.0.h),
                  PrimaryButton(
                      height: 65.0.h,
                      width: 368.0.w,
                      color: AppColors.primaryBlue,
                      disabled: !(widget.isEdit ||
                          (this.civilIdValid &&
                              this.nameValid &&
                              this.houseNoValid &&
                              this.emailIdValid &&
                              this.dobValid &&
                              this.mobileNoValid &&
                              // this.passportNoValid &&
                              // this.passportExpiryValid &&
                              this.streetValid &&
                              this.nationalityValid &&
                              this.areaValid &&
                              this.blockNoValid &&
                              // this.civilIdExpiryValid &&
                              //  this.licenseNoValid &&
                              //  this.licenseExpiryValid &&
                              this.vinNoValid &&
                              this.plateNoValid
                          //&&
                          // this.makeModelValid &&
                          // this.carValueValid &&
                          // this.noOfSeatsValid &&
                          // this.makeYearValid
                          )),
                      isLoading: isLoading,
                      onPressed: () async {
                        if (!isLoading) {
                          setState(() {
                            isLoading = true;
                          });
                          customerProvider.resetInsuranceDetails();
                          customerProvider.insuranceDetails
                              .add(PersonalDetailsModel(
                            //TODO
                            insuranceId: 1,
                            policyDocument: '123',
                            dob: dobController.text,
                            emailId: emailIdController.text,
                            mobileNo: mobileNoController.text,
                            // phoneNo: phoneNoController.text,
                            area: areaController.text,
                            blockNo: blockNoController.text,
                            bNoOrHouseNo: houseNoController.text,
                            civilIDExpDate: civilIdExpiryController.text,
                            civilIdNo: civilIdController.text,
                            floorNo: floorNoController.text,
                            gender: gender,
                            nationality: nationalityController.text,
                            street: streetController.text,
                            insurerName: nameController.text,
                            unitType: unitType,
                            passportExpDate: passportExpiryController.text,
                            passportNo: passportNoController.text,
                          ));
                          customerProvider.resetInsurerMotorDetails();
                          customerProvider.setInsurerMotorDetails =
                              InsurerMotorDetailsModel(
                                  noOfSeats: noOfSeatsController.text,
                                  makeYear: makeYearController.text,
                                  carValue: carValueController.text,
                                  licenseExpiry: licenseExpiryController.text,
                                  licenseNo: licenseNoController.text,
                                  makeModel: makeModelController.text,
                                  plateNo: plateNoController.text,
                                  vinNo: vinNoController.text);
                          if (widget.isEdit ||
                              (profileProvider.profileData?.customerId !=
                                      null &&
                                  profileProvider.profileData?.customerId !=
                                      '')) {
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.fade,
                                    child: MotorPaymentDetailsScreen()));
                          } else {
                            InitScreen.fromInsurerDetails = true;
                            Navigator.push(
                              context,
                              PageTransition(
                                type: PageTransitionType.fade,
                                child: LoginScreen(
                                  emailId: emailIdController.text,
                                  customerName: nameController.text,
                                  mobileNo: mobileNoController.text,
                                ),
                              ),
                            );
                          }
                          // Navigations.pushNamed(
                          //   context,
                          //   AppRouter.loginScreen,
                          // );
                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      text: 'Continue',
                      borderRadius: 80.0.s),
                  SizedBox(
                    height: 17.0.h,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
