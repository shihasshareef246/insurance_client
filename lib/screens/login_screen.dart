import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gt_test/screens/register_screen.dart';
import 'package:provider/provider.dart';
import 'package:page_transition/page_transition.dart';
import 'package:toast/toast.dart';

import 'package:gt_test/common/buttons/primary_button.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/routes/routes.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/screens/otp_screen.dart';
import '../common/images.dart';
import '../providers/profile_provider.dart';
import 'package:gt_test/models/profile_model.dart';
import 'package:gt_test/services/otp_service.dart';
import '../services/profile_service.dart';
import 'init_screen.dart';

class LoginScreen extends StatefulWidget {
  final String emailId;
  final String customerName;
  final String mobileNo;

  LoginScreen({
    this.emailId = '',
    this.customerName = '',
    this.mobileNo = '',
  });
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController mobileController;
  late FocusNode mobileFocus;
  bool mobileValid = false;
  bool mobileChanged = false;
  bool isLoading = false;
  late ProfileProvider profileProvider;
  bool isError = false;

  @override
  void initState() {
    ToastContext().init(context);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    mobileFocus = FocusNode();
    mobileController = TextEditingController(
        text: (widget.mobileNo != '') ? widget.mobileNo : '');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pushNamedAndRemoveUntil(
          context, AppRouter.preloginScreen),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: Container(
              padding:
                  EdgeInsets.only(left: 24.0.s, right: 20.0.s, bottom: 20.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 40.0.h,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () => Navigations.pushNamedAndRemoveUntil(
                          context, AppRouter.preloginScreen),
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: AppColors.primaryBlue,
                        size: 20.0.ics,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40.0.h,
                  ),
                  Image(
                    image: AssetImage(AppImages.LOGO),
                    height: 99.0.h,
                    width: 296.0.w,
                  ),
                  SizedBox(
                    height: 60.0.h,
                  ),
                  isError
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.all(6.0.s),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 6.0.s, vertical: 12.0.s),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: AppColors.red, width: 1.0),
                                borderRadius: BorderRadius.circular(
                                  6.0.s,
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.report_problem_outlined,
                                    color: AppColors.red,
                                    size: 36.0.ics,
                                  ),
                                  SizedBox(width: 10.0.w),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'There was a problem',
                                        style: buildAppTextTheme()
                                            .headline3!
                                            .copyWith(color: AppColors.red),
                                        textAlign: TextAlign.left,
                                      ),
                                      SizedBox(height: 10.0.h),
                                      Container(
                                        width: 300.0.w,
                                        child: Text(
                                          'The provided mobile number has not been registered. Please register to continue.',
                                          style: buildAppTextTheme()
                                              .headline5!
                                              .copyWith(color: AppColors.black),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 40.0.h),
                            Center(
                              child: InkWell(
                                onTap: () => Navigations.pushReplacementNamed(
                                    context, AppRouter.registerScreen),
                                child: Text(
                                  'Please click here to register.',
                                  style: buildAppTextTheme()
                                      .headline5!
                                      .copyWith(color: AppColors.primaryBlue),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                          ],
                        )
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Welcome to the Gulf Trust Co.',
                              style: buildAppTextTheme().headline1,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 37.0.h,
                            ),
                            Text(
                              'Verify Phone No',
                              style: buildAppTextTheme().headline2,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 13.0.h,
                            ),
                            Text(
                              'Please enter your mobile number to\nreceive a verification code',
                              style: buildAppTextTheme().headline6,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(height: 42.0.h),
                            Container(
                              height: 58.0.h,
                              decoration: BoxDecoration(
                                color: AppColors.white,
                                border: Border.all(color: AppColors.border),
                                borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(6.0.s),
                                  topRight: Radius.circular(6.0.s),
                                  bottomLeft: Radius.circular(6.0.s),
                                  topLeft: Radius.circular(6.0.s),
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    width: 63.0.w,
                                    color: AppColors.white,
                                    child: Center(
                                      child: Image(
                                        image: AssetImage(
                                          AppImages.FLAG,
                                        ),
                                        height: 30.0.h,
                                        width: 30.0.w,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 63.0.w,
                                    decoration: BoxDecoration(
                                      color: AppColors.white,
                                    ),
                                    child: Center(
                                      child: Text(
                                        '+965- ',
                                        style: buildAppTextTheme().headline5,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 241.0.w,
                                    decoration: BoxDecoration(
                                      color: mobileFocus.hasFocus
                                          ? AppColors.white
                                          : AppColors.white,
                                    ),
                                    child: Center(
                                      child: TextFormField(
                                        inputFormatters: [
                                          FilteringTextInputFormatter
                                              .digitsOnly,
                                          LengthLimitingTextInputFormatter(8),
                                        ],
                                        scrollPadding:
                                            EdgeInsets.only(bottom: 100),
                                        focusNode: mobileFocus,
                                        style: mobileFocus.hasFocus
                                            ? buildAppTextTheme().headline5
                                            : buildAppTextTheme().subtitle1,
                                        keyboardType: TextInputType.number,
                                        controller: mobileController,
                                        obscureText: false,
                                        decoration: InputDecoration(
                                          isDense: true,
                                          hintText: 'xxx xxx xx',
                                          // contentPadding: EdgeInsets.only(
                                          //   left: 10.0.s,
                                          //   top: 8.0.s,
                                          //   bottom: 8.0.s,
                                          //   right: 10.0.s,
                                          // ),
                                          //    labelText: '+965 - xxx xxx xx',
                                          labelStyle: mobileFocus.hasFocus
                                              ? buildAppTextTheme().subtitle2
                                              : buildAppTextTheme().subtitle2,
                                          border: InputBorder.none,
                                          // focusedBorder: UnderlineInputBorder(
                                          //   borderSide:
                                          //       BorderSide(color: AppColors.primaryBlue),
                                          //   borderRadius: BorderRadius.vertical(
                                          //     top: Radius.circular(10.0.s),
                                          //     bottom: Radius.zero,
                                          //   ),
                                          // ),
                                        ),
                                        autovalidateMode:
                                            AutovalidateMode.onUserInteraction,
                                        onChanged: (value) {
                                          setState(() {
                                            this.mobileChanged = true;
                                          });
                                          if (value.isEmpty ||
                                              value.length != 8) {
                                            setState(() {
                                              this.mobileValid = false;
                                            });
                                          } else {
                                            setState(() {
                                              this.mobileValid = true;
                                            });
                                          }
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            (this.mobileChanged &&
                                    !this.mobileValid &&
                                    !mobileFocus.hasFocus)
                                ? Text(
                                    'Enter Valid Phone number',
                                    style: buildAppTextTheme().subtitle1,
                                  )
                                : SizedBox(height: 0.0.h),
                            SizedBox(height: 48.0.h),
                            PrimaryButton(
                                height: 65.0.h,
                                width: 304.0.w,
                                color: AppColors.primaryBlue,
                                disabled: !(this.mobileValid),
                                isLoading: isLoading,
                                onPressed: () async {
                                  if (!isLoading) {
                                    setState(() {
                                      isLoading = true;
                                    });
                                    var response = await ProfileService()
                                        .getCustomerDetails(
                                            context: context,
                                            mobileNo: mobileController.text);
                                    // await CustomerService().validateCustomer(
                                    //     context: context, mobileNo: mobileController.text);
                                    if (response != null &&
                                        response.customerId != '') {
                                      profileProvider.setProfileData =
                                          ProfileModel(
                                              customerId: response.customerId,
                                              emailId: response.emailId,
                                              customerName:
                                                  response.customerName,
                                              civilidNo: response.civilidNo,
                                              dob: response.dob,
                                              mobileNo: response.mobileNo,
                                              phoneNo: response.phoneNo,
                                              photoUrl: response.photoUrl,
                                              otpVerified: false);
                                      // Toast.show('Valid user',
                                      //     backgroundColor: AppColors.green,
                                      //     textStyle:
                                      //         buildAppTextTheme().subtitle1!.copyWith(
                                      //               color: AppColors.white,
                                      //             ),
                                      //     duration: 3,
                                      //     gravity: 0);
                                      var otpResponse = await OTPService()
                                          .requestOTP(
                                              context: context,
                                              mobileNo: mobileController.text);
                                      if (otpResponse != null &&
                                          otpResponse.messageId != '') {
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: OtpScreen(
                                                messageId:
                                                    otpResponse.messageId,
                                                mobileNo: mobileController.text,
                                                from: 'Login',
                                              )),
                                        );
                                      } else {
                                        Toast.show(
                                            'Something went wrong. Please try again',
                                            backgroundColor: AppColors.red,
                                            textStyle: buildAppTextTheme()
                                                .subtitle1!
                                                .copyWith(
                                                  color: AppColors.white,
                                                ),
                                            duration: 3,
                                            gravity: 0);
                                      }
                                    } else if (InitScreen.fromInsurerDetails) {
                                      Navigator.push(
                                        context,
                                        PageTransition(
                                          type: PageTransitionType.fade,
                                          child: RegisterScreen(
                                              mobileNo: mobileController.text),
                                        ),
                                      );
                                    } else {
                                      setState(() {
                                        isError = true;
                                      });
                                      // Toast.show('User not found',
                                      //     backgroundColor: AppColors.red,
                                      //     textStyle: buildAppTextTheme()
                                      //         .subtitle1!
                                      //         .copyWith(
                                      //           color: AppColors.white,
                                      //         ),
                                      //     duration: 3,
                                      //     gravity: 0);
                                      // Navigator.push(
                                      //   context,
                                      //   PageTransition(
                                      //       type: PageTransitionType.fade,
                                      //       child: RegisterScreen()),
                                      // );
                                    }
                                    // Navigations.pushNamed(
                                    //   context,
                                    //   AppRouter.otpScreen,
                                    // );
                                    setState(() {
                                      isLoading = false;
                                    });
                                  }
                                },
                                text: 'Get OTP',
                                borderRadius: 80.0.s),
                          ],
                        ),
                  SizedBox(height: 60.0.h),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
