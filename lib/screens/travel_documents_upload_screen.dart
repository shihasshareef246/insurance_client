import 'dart:io';
// import 'package:amazon_s3_cognito/amazon_s3_cognito.dart';
// import 'package:amazon_s3_cognito/aws_region.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:gt_test/models/insurance_details_model.dart';
import 'package:gt_test/screens/travel_insurer_details_screen.dart';
import 'package:image_picker/image_picker.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import 'package:intl/intl.dart';

import 'package:gt_test/common/buttons/primary_button.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import '../common/images.dart';
import '../common/logger.dart';
import 'package:gt_test/models/file_model.dart';
import 'package:gt_test/providers/documents_provider.dart';
import 'package:gt_test/providers/profile_provider.dart';
import '../common/shared_prefs.dart';
import '../common/utils.dart';
import '../models/motor_insurance_group_model.dart';
import '../providers/customer_provider.dart';
import '../services/customer_service.dart';
import '../widgets/common/upload_bottom_sheet_widget.dart';
import '../widgets/common/post_upload_widget.dart';
import '../widgets/common/pre_upload_widget.dart';

class TravelDocumentsUploadScreen extends StatefulWidget {
  @override
  _TravelDocumentsUploadScreenState createState() =>
      _TravelDocumentsUploadScreenState();
}

class _TravelDocumentsUploadScreenState
    extends State<TravelDocumentsUploadScreen> {
  var uuid = Uuid();
  DocumentsProvider documentsProvider = DocumentsProvider();
  CustomerProvider customerProvider = CustomerProvider();
  ProfileProvider profileProvider = ProfileProvider();
  final picker = ImagePicker();
  File file = File("");
  String fileExtension = "";
  bool isLoading = false;
  bool isFrontCivilIdUploaded = false;
  bool isBackCivilIdUploaded = false;
  bool isPassportUploaded = false;
  late List<bool> isFrontCivilIdUploadedList;
  late List<bool> isBackCivilIdUploadedList;
  late List<bool> isPassportUploadedList;
  final dateFormat = DateFormat("dd-MM-yyyy");
  final timeFormat = DateFormat("h:mm a");

  @override
  void initState() {
    documentsProvider = Provider.of<DocumentsProvider>(context, listen: false);
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    //customerProvider?.insuranceDetails = [PersonalDetailsModel()];
    isFrontCivilIdUploaded = false;
    isBackCivilIdUploaded = false;
    isPassportUploaded = false;
    isFrontCivilIdUploadedList =
        List.filled(customerProvider.selectedTravelInsuranceList.length, false);
    isBackCivilIdUploadedList =
        List.filled(customerProvider.selectedTravelInsuranceList.length, false);
    isPassportUploadedList =
        List.filled(customerProvider.selectedTravelInsuranceList.length, false);
    super.initState();
  }

  // Future<String?> _uploadDocument(File file, String name) async {
  //   String? result;
  //   if (result == null) {
  //     try {
  //       setState(() {
  //         isLoading = true;
  //       });
  //       ImageData imageData = ImageData(name, file.path,
  //           uniqueId: uuid.v1(), imageUploadFolder: 'CustomerDocuments'
  //         //profileProvider.profileData?.customerId??''
  //       );
  //       String? uploadedImageUrl = await AmazonS3Cognito.upload(
  //           AppConstants.S3_BUCKET,
  //           AppConstants.S3_POOL,
  //           AwsRegion.US_EAST_1,
  //           AwsRegion.US_EAST_1
  //           imageData);
  //       documentsProvider.docsList.add(InsurerDocumentDetailsModel(
  //           documentName: name, documentUrl: uploadedImageUrl));
  //       setState(() {
  //         isLoading = false;
  //       });
  //     } catch (e) {
  //       print(e);
  //     }
  //   }
  //   return result;
  // }

  void getFileDialogue(String type, int index) {
    showModalBottomSheet(
        backgroundColor: AppColors.primaryBlue.withOpacity(0.9),
        barrierColor: AppColors.primaryBlue.withOpacity(0.9),
        context: context,
        builder: (BuildContext context) {
          return UploadBottomSheetWidget(
            title: 'Upload image via',
            onClick: () async {
              Navigator.pop(context);
              getFileFromCamera(type, index);
            },
            image: AppImages.CAMERA,
            listTitle: 'Camera',
            onClick1: () async {
              Navigator.pop(context);
              getFileFromGallery(type, index);
            },
            image1: AppImages.GALLERY,
            listTitle1: 'Gallery',
            onClick2: () async {
              Navigator.pop(context);
              getFileFromDocument(type, index);
            },
            image2: Icons.attach_file_sharp,
            listTitle2: 'Document',
          );
        });
  }

  void setFileStatus(String type, int index, {bool status = false}) {
    switch (type) {
      case 'Front_Of_Civil_ID':
        isFrontCivilIdUploadedList[index] = status;
        // _uploadDocument(file, 'Front_Of_Civil_ID.${fileExtension}');
        break;
      case 'Back_Of_Civil_ID':
        isBackCivilIdUploadedList[index] = status;
        //_uploadDocument(file, 'Back_Of_Civil_ID.${fileExtension}');
        break;
      case 'Passport':
        isPassportUploadedList[index] = status;
        //_uploadDocument(file, 'Driving License.${fileExtension}');
        break;
    }
  }

  Future getFileFromCamera(String type, int index) async {
    final pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);
    if (pickedFile != null) {
      setState(() {
        file = File(pickedFile.path);
        fileExtension =
            pickedFile.path.substring(pickedFile.path.lastIndexOf('.') + 1);
        documentsProvider.documentsList.add(FileModel(
            file: file,
            type: type,
            date: dateFormat.format(DateTime.now()).toString(),
            time: timeFormat.format(DateTime.now()).toString(),
            extension: fileExtension));
        setFileStatus(type, index, status: true);
      });
      String? url = await Utils.uploadDocument(file, type, fileExtension,
          profileProvider.profileData?.customerId ?? '');
      documentsProvider.docsList.add(
          InsurerDocumentDetailsModel(documentName: type, documentUrl: url));
      if (type == 'Front_Of_Civil_ID') {
        var res = await CustomerService().cidOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceDetails[index]?.civilIdNo =
              res.CivilId ?? '';
          customerProvider?.insuranceDetails[index]?.insurerName =
              res.Name ?? '';
          customerProvider?.insuranceDetails[index]?.dob = res.Dob ?? '';
          customerProvider?.insuranceDetails[index]?.passportNo =
              res.PassportNo ?? '';
        }
      } else {}
      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
  }

  Future getFileFromGallery(String type, int index) async {
    final pickedFile =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 50);
    if (pickedFile != null) {
      setState(() {
        file = File(pickedFile.path);
        fileExtension =
            pickedFile.path.substring(pickedFile.path.lastIndexOf('.') + 1);
        documentsProvider.documentsList.add(FileModel(
            file: file,
            type: type,
            date: dateFormat.format(DateTime.now()).toString(),
            time: timeFormat.format(DateTime.now()).toString(),
            extension: fileExtension));
        setFileStatus(type, index, status: true);
      });
      String? url = await Utils.uploadDocument(file, type, fileExtension,
          profileProvider.profileData?.customerId ?? '');
      documentsProvider.docsList.add(
          InsurerDocumentDetailsModel(documentName: type, documentUrl: url));
      if (type == 'Front_Of_Civil_ID') {
        var res = await CustomerService().cidOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceDetails[index]?.civilIdNo =
              res.CivilId ?? '';
          customerProvider?.insuranceDetails[index]?.insurerName =
              res.Name ?? '';
          customerProvider?.insuranceDetails[index]?.dob = res.Dob ?? '';
          customerProvider?.insuranceDetails[index]?.passportNo =
              res.PassportNo ?? '';
        }
      } else {}
      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
  }

  Future getFileFromDocument(String type, int index) async {
    FilePickerResult? pickedFile =
        await FilePicker.platform.pickFiles(type: FileType.any);
    if (pickedFile != null) {
      setState(() {
        print('file picked');
        file = File(pickedFile.files.single.path!);
        fileExtension = pickedFile.files.single.extension!;
        documentsProvider.documentsList.add(FileModel(
            file: file,
            type: type,
            date: dateFormat.format(DateTime.now()).toString(),
            time: timeFormat.format(DateTime.now()).toString(),
            extension: fileExtension));
        setFileStatus(type, index, status: true);
      });
      String? url = await Utils.uploadDocument(file, type, fileExtension,
          profileProvider.profileData?.customerId ?? '');
      documentsProvider.docsList.add(
          InsurerDocumentDetailsModel(documentName: type, documentUrl: url));
      if (type == 'Front_Of_Civil_ID') {
        var res = await CustomerService().cidOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceDetails[index]?.civilIdNo =
              res.CivilId ?? '';
          customerProvider?.insuranceDetails[index]?.insurerName =
              res.Name ?? '';
          customerProvider?.insuranceDetails[index]?.dob = res.Dob ?? '';
          customerProvider?.insuranceDetails[index]?.passportNo =
              res.PassportNo ?? '';
        }
      } else {}
      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
  }

  void imagePreview(String name, int index, String type) {
    File? doc;
    String? date;
    String? time;
    String? extension;
    documentsProvider.documentsList.forEach((element) {
      if (element?.type == type) {
        doc = element?.file;
        time = element?.time;
        date = element?.date;
        extension = element?.extension;
      }
    });
    showModalBottomSheet<dynamic>(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (doc != null)
                  Container(
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0.s),
                        topRight: Radius.circular(20.0.s),
                      ),
                    ),
                    child: Stack(
                      children: [
                        Container(
                          height: 226.0.h,
                          width: 368.0.w,
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0.s),
                              topRight: Radius.circular(20.0.s),
                            ),
                            child: Image(
                              image: FileImage(doc!),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Positioned(
                          top: 20.0.s,
                          right: 20.0.s,
                          child: InkWell(
                            child: CircleAvatar(
                              backgroundColor: AppColors.brownish,
                              child: Icon(Icons.close,
                                  size: 20.0.ics, color: AppColors.primaryBlue),
                            ),
                            onTap: () => Navigator.pop(context),
                          ),
                        ),
                      ],
                    ),
                  ),
                Container(
                  height: 226.0.h,
                  width: 368.0.w,
                  padding: EdgeInsets.all(20.0.s),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20.0.s),
                      bottomLeft: Radius.circular(20.0.s),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            name,
                            style: buildAppTextTheme().headline4,
                            textAlign: TextAlign.center,
                          ),
                          InkWell(
                            onTap: () {
                              documentsProvider.documentsList
                                  .removeWhere((item) => item?.type == type);
                              setFileStatus(type, index, status: false);
                              Navigator.pop(context);
                              setState(() {
                                doc = null;
                              });
                            },
                            child: Image(
                              image: AssetImage(AppImages.DELETE),
                              height: 33.0.h,
                              width: 33.0.w,
                              //color: AppColors.primaryBlue,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 11.0.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            date!,
                            style: buildAppTextTheme().caption,
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            time!,
                            style: buildAppTextTheme().caption,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 11.0.h,
                      ),
                      Text(
                        'Format : $extension',
                        style: buildAppTextTheme().subtitle2,
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ]);
        });
  }

  bool isValid() {
    bool isValid = false;
    int passportCount = 0;
    int frontCivilIdCount = 0;
    int backCivilIdCount = 0;
    isFrontCivilIdUploadedList.forEach((element) {
      if (element) {
        frontCivilIdCount++;
      }
    });
    isBackCivilIdUploadedList.forEach((element) {
      if (element) {
        backCivilIdCount++;
      }
    });
    isPassportUploadedList.forEach((element) {
      if (element) {
        passportCount++;
      }
    });
    isValid = ((frontCivilIdCount ==
            customerProvider.selectedTravelInsuranceList.length) &&
        (backCivilIdCount ==
            customerProvider.selectedTravelInsuranceList.length) &&
        (passportCount == customerProvider.selectedTravelInsuranceList.length));
    return isValid;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.primaryBlue,
          leading: InkWell(
            onTap: () => Navigations.pop(context),
            child: Icon(
              Icons.arrow_back_ios,
              color: AppColors.white,
              size: 20.0.ics,
            ),
          ),
          title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Insurer Details',
                  style: buildAppTextTheme()
                      .headline3
                      ?.copyWith(color: AppColors.white),
                  textAlign: TextAlign.start,
                ),
                // SizedBox(
                //   height: 5.0.h,
                // ),
                // Container(
                //   width: 200.0.w,
                //   child: Text(
                //     desc!,
                //     style: buildAppTextTheme().bodyText2,
                //     textAlign: TextAlign.start,
                //   ),
                // ),
              ]),
        ),
        backgroundColor: AppColors.pageColor,
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(
                left: 30.0.s, right: 30.0.s, top: 30.0.s, bottom: 60.0.s),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    'Upload documents',
                    style: buildAppTextTheme().headline3,
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  'Please Upload all of the following documents before going to the next step.',
                  style: buildAppTextTheme().headline5,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Text(
                  'Note: Enable access to App for using phone camera or gallery',
                  style: buildAppTextTheme().subtitle2,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                ListView.builder(
                    itemCount:
                        customerProvider.selectedTravelInsuranceList.length,
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Member ${index + 1}',
                            style: buildAppTextTheme()
                                .overline!
                                .copyWith(color: AppColors.primaryBlue),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 10.0.h,
                          ),

                          Container(
                            width: 360.0.w,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'View Sample',
                                  style: buildAppTextTheme()
                                      .overline!
                                      .copyWith(color: AppColors.primaryBlue),
                                  textAlign: TextAlign.center,
                                ),
                                SizedBox(
                                  width: 10.0.w,
                                ),
                                Text(
                                  'Upload Doc',
                                  style: buildAppTextTheme()
                                      .overline!
                                      .copyWith(color: AppColors.primaryBlue),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 4.0.h,
                          ),
                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //   crossAxisAlignment: CrossAxisAlignment.center,
                          //   children: [
                          Container(
                            width: 360.0.w,
                            child: !isFrontCivilIdUploadedList[index]
                                ? InkWell(
                                    onTap: () => getFileDialogue(
                                        'Front_Of_Civil_ID', index),
                                    child: PreUploadWidget(
                                        text: 'Front Of Civil ID',
                                        type: 'Front_Of_Civil_ID'),
                                  )
                                : InkWell(
                                    onTap: () => imagePreview(
                                        'Front Of Civil ID',
                                        index,
                                        'Front_Of_Civil_ID'),
                                    child: PostUploadWidget(
                                        text: 'Front Of Civil ID',
                                        type: 'Front_Of_Civil_ID'),
                                  ),
                          ),
                          // Column(
                          //   mainAxisAlignment: MainAxisAlignment.center,
                          //   crossAxisAlignment: CrossAxisAlignment.center,
                          //   children: [
                          //     Image(
                          //       image: AssetImage(AppImages.DOCUMENT),
                          //       height: 20.0.h,
                          //       width: 16.0.w,
                          //     ),
                          //     SizedBox(
                          //       height: 6.0.h,
                          //     ),
                          //     Text(
                          //       'View Sample',
                          //       style: buildAppTextTheme()
                          //           .overline
                          //           ?.copyWith(color: AppColors.primaryBlue),
                          //       textAlign: TextAlign.start,
                          //     ),
                          //   ],
                          // ),
                          //   ],
                          // ),
                          SizedBox(
                            height: 20.0.h,
                          ),
                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //   crossAxisAlignment: CrossAxisAlignment.center,
                          //   children: [
                          Container(
                            width: 360.0.w,
                            child: !isBackCivilIdUploadedList[index]
                                ? InkWell(
                                    onTap: () => getFileDialogue(
                                        'Back_Of_Civil_ID', index),
                                    child: PreUploadWidget(
                                        text: 'Back Of Civil ID',
                                        type: 'Back_Of_Civil_ID'),
                                  )
                                : InkWell(
                                    onTap: () => imagePreview(
                                        'Back Of Civil ID',
                                        index,
                                        'Back_Of_Civil_ID'),
                                    child: PostUploadWidget(
                                        text: 'Back Of Civil ID',
                                        type: 'Back_Of_Civil_ID'),
                                  ),
                          ),
                          //     Column(
                          //       mainAxisAlignment: MainAxisAlignment.center,
                          //       crossAxisAlignment: CrossAxisAlignment.center,
                          //       children: [
                          //         Image(
                          //           image: AssetImage(AppImages.DOCUMENT),
                          //           height: 20.0.h,
                          //           width: 16.0.w,
                          //         ),
                          //         SizedBox(
                          //           height: 6.0.h,
                          //         ),
                          //         Text(
                          //           'View Sample',
                          //           style: buildAppTextTheme()
                          //               .overline
                          //               ?.copyWith(color: AppColors.primaryBlue),
                          //           textAlign: TextAlign.start,
                          //         ),
                          //       ],
                          //     ),
                          //   ],
                          // ),
                          SizedBox(
                            height: 20.0.h,
                          ),
                          // Row(
                          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          //   crossAxisAlignment: CrossAxisAlignment.center,
                          //   children: [
                          Container(
                            width: 360.0.w,
                            child: !isPassportUploadedList[index]
                                ? InkWell(
                                    onTap: () =>
                                        getFileDialogue('Passport', index),
                                    child: PreUploadWidget(
                                        text: 'Passport', type: 'Passport'),
                                  )
                                : InkWell(
                                    onTap: () => imagePreview(
                                        'Passport', index, 'Passport'),
                                    child: PostUploadWidget(
                                        text: 'Passport', type: 'Passport'),
                                  ),
                          ),
                          SizedBox(height: 20.0.h),
                        ],
                      );
                    }),
                PrimaryButton(
                    height: 65.0.h,
                    width: 368.0.w,
                    color: AppColors.primaryBlue,
                    // disabled: !(isFrontCivilIdUploaded &&
                    //     isBackCivilIdUploaded &&
                    //     isPassportUploaded),
                    disabled: !isValid(),
                    isLoading: isLoading,
                    onPressed: () async {
                      if (!isLoading) {
                        setState(() {
                          isLoading = true;
                        });
                        // documentsProvider.documentsList.forEach((element) async {
                        //   String? url = await Utils.uploadDocument(element?.file, element?.type,
                        //       profileProvider.profileData?.customerId ?? '');
                        //   documentsProvider.docsList.add(InsurerDocumentDetailsModel(
                        //       documentName: element?.type, documentUrl: url));
                        // });
                        // await SharedPrefs().addStringPrefs(
                        //     'documents', documentsProvider.documentsList.toString());
                        Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.fade,
                              child: TravelInsurerDetailsScreen()),
                        );
                        await SharedPrefs().addStringPrefs('documents',
                            documentsProvider.documentsList.toString());

                        setState(() {
                          isLoading = false;
                        });
                      }
                    },
                    text: 'Continue',
                    borderRadius: 80.0.s),
                SizedBox(
                  height: 17.0.h,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
