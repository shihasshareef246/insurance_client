import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:gt_test/screens/register_claim_screen.dart';
import 'package:intl/intl.dart';
import 'package:image_picker/image_picker.dart';

import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import '../common/images.dart';
import '../common/logger.dart';
import '../models/customer_claims_model.dart';
import '../providers/customer_provider.dart';
import '../providers/profile_provider.dart';
import '../services/customer_service.dart';
import '../widgets/common/upload_bottom_sheet_widget.dart';
import '../widgets/common/post_upload_widget.dart';
import '../widgets/common/pre_upload_widget.dart';

class ClaimDetailsScreen extends StatefulWidget {
  final CustomerClaimsModel? claimDetails;
  const ClaimDetailsScreen({this.claimDetails});
  @override
  _ClaimDetailsScreenState createState() => _ClaimDetailsScreenState();
}

class _ClaimDetailsScreenState extends State<ClaimDetailsScreen> {
  ProfileProvider profileProvider = ProfileProvider();
  late CustomerProvider customerProvider;
  final picker = ImagePicker();
  File? file = File("");
  String fileExtension = "";
  bool isLoading = false;
  TextEditingController dobController = TextEditingController();
  FocusNode dobFocus = FocusNode();
  bool dobChanged = false;
  bool dobValid = false;

  final dateFormat = DateFormat("dd-MM-yyyy");
  final timeFormat = DateFormat("h:mm a");
  bool isPolicyDocsUploaded = false;
  bool isOtherDocsUploaded = false;

  void imagePreview(String? doc, String? url, String type) {
    showModalBottomSheet<dynamic>(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (url != null)
                  Container(
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0.s),
                        topRight: Radius.circular(20.0.s),
                      ),
                    ),
                    child: Stack(
                      children: [
                        Container(
                          height: 226.0.h,
                          width: 368.0.w,
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0.s),
                              topRight: Radius.circular(20.0.s),
                            ),
                            child: Image(
                              image: NetworkImage(url ?? ''),
                              height: 40.0.h,
                              width: 360.0.w,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Positioned(
                          top: 20.0.s,
                          right: 20.0.s,
                          child: InkWell(
                            child: CircleAvatar(
                              backgroundColor: AppColors.brownish,
                              child: Icon(Icons.close,
                                  size: 20.0.ics, color: AppColors.primaryBlue),
                            ),
                            onTap: () => Navigator.pop(context),
                          ),
                        ),
                      ],
                    ),
                  ),
                Container(
                  height: 226.0.h,
                  width: 368.0.w,
                  padding: EdgeInsets.all(20.0.s),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20.0.s),
                      bottomLeft: Radius.circular(20.0.s),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Claim document',
                            style: buildAppTextTheme().headline4,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 11.0.h,
                      ),
                    ],
                  ),
                ),
              ]);
        });
  }

  @override
  void initState() {
    isPolicyDocsUploaded = false;
    isOtherDocsUploaded = false;
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      customerProvider.setClaimDocuments = await CustomerService()
          .getClaimDocument(
              context: context,
              claimId: widget.claimDetails?.claimId.toString());
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.primaryBlue,
            leading: InkWell(
              onTap: () => Navigations.pop(context),
              child: Icon(
                Icons.arrow_back_ios,
                color: AppColors.white,
                size: 20.0.ics,
              ),
            ),
            title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'My Claims',
                    style: buildAppTextTheme()
                        .headline3
                        ?.copyWith(color: AppColors.white),
                    textAlign: TextAlign.start,
                  ),
                ]),
          ),
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  left: 30.0.s, right: 30.0.s, top: 23.0.s, bottom: 60.0.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'ID : #${widget.claimDetails?.claimId.toString() ?? ''}',
                        style: buildAppTextTheme().headline5,
                        textAlign: TextAlign.start,
                      ),
                      if (widget.claimDetails?.claimStatus == 'Pending')
                        InkWell(
                          onTap: () => Navigator.push(
                            context,
                            PageTransition(
                              type: PageTransitionType.fade,
                              child: RegisterClaimScreen(
                                planId: widget.claimDetails?.planId,
                                isEdit: true,
                                customerId:
                                    profileProvider.profileData?.customerId,
                                claimDate: widget.claimDetails?.claimDate,
                                claimId: widget.claimDetails?.claimId,
                                claimReason: widget.claimDetails?.claimReason,
                                insuranceId: widget.claimDetails?.insuranceId,
                                lossDate: widget.claimDetails?.lossDate,
                              ),
                            ),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image(
                                image: AssetImage(AppImages.EDIT),
                                height: 18.0.h,
                                width: 18.0.w,
                              ),
                              SizedBox(
                                width: 10.0.w,
                              ),
                              Text(
                                'Edit',
                                style: buildAppTextTheme().headline5,
                                textAlign: TextAlign.start,
                              ),
                            ],
                          ),
                        ),
                    ],
                  ),
                  SizedBox(
                    height: 30.0.h,
                  ),
                  Text(
                    'Claim Reason : ${widget.claimDetails?.claimReason.toString() ?? ''}',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 30.0.h,
                  ),
                  Text(
                    'Date of Incident',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 18.0.h,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: 14.0.s, vertical: 18.0.s),
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      border: Border.all(color: AppColors.border, width: 1.0),
                      borderRadius: BorderRadius.circular(
                        6.0.s,
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image(
                          image: AssetImage(AppImages.DATE),
                          height: 20.0.h,
                          width: 16.0.w,
                        ),
                        SizedBox(
                          width: 15.0.w,
                        ),
                        Text(
                          widget.claimDetails?.lossDate ?? '',
                          style: buildAppTextTheme().headline6,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                  // Container(
                  //   height: 56.0.h,
                  //   width: 360.0.w,
                  //   decoration: BoxDecoration(
                  //     color: dobFocus.hasFocus
                  //         ? AppColors.paleBlue
                  //         : AppColors.paleBrown,
                  //     border: Border.all(color: AppColors.border, width: 1.0),
                  //     borderRadius: BorderRadius.circular(
                  //       6.0.s,
                  //     ),
                  //   ),
                  //   child: DateTimeField(
                  //     focusNode: dobFocus,
                  //     style: dobFocus.hasFocus
                  //         ? buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.lightBlack)
                  //         : buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.focusText),
                  //     decoration: InputDecoration(
                  //       labelText: 'DOB',
                  //       labelStyle: dobFocus.hasFocus
                  //           ? buildAppTextTheme()
                  //               .subtitle1!
                  //               .copyWith(color: AppColors.lightGrey)
                  //           : buildAppTextTheme().subtitle1,
                  //       focusColor: AppColors.searchBlue,
                  //       isDense: true,
                  //       contentPadding: EdgeInsets.only(
                  //         left: 10.0.s,
                  //         top: 8.0.s,
                  //         bottom: 8.0.s,
                  //         right: 10.0.s,
                  //       ),
                  //       border: InputBorder.none,
                  //     ),
                  //     controller: dobController,
                  //     onChanged: (value) {
                  //       setState(() {
                  //         this.dobChanged = true;
                  //       });
                  //       if (value != null &&
                  //           value.month != null &&
                  //           value.year != null &&
                  //           value.day != null &&
                  //           value.month <= 12 &&
                  //           value.month >= 1 &&
                  //           value.year <= 2100 &&
                  //           value.year >= 1900 &&
                  //           value.day <= 31 &&
                  //           value.day >= 1) {
                  //         setState(() {
                  //           this.dobValid = true;
                  //         });
                  //       } else {
                  //         setState(() {
                  //           this.dobValid = false;
                  //         });
                  //       }
                  //     },
                  //     format: dateFormat,
                  //     onShowPicker: (context, currentValue) {
                  //       return showDatePicker(
                  //           context: context,
                  //           firstDate: DateTime(1900),
                  //           initialDate: currentValue ?? DateTime.now(),
                  //           lastDate: DateTime(2100));
                  //     },
                  //   ),
                  // ),
                  // (this.dobChanged && !this.dobValid && !dobFocus.hasFocus)
                  //     ? Text(
                  //         'Enter Valid DOB',
                  //         style: buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.darkRed),
                  //       )
                  //     : SizedBox(height: 20.0.h),
                  SizedBox(
                    height: 30.0.h,
                  ),
                  if (customerProvider.claimDocs.isNotEmpty) ...{
                    Text(
                      'Uploaded documents',
                      style: buildAppTextTheme().headline4,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                  },

                  ListView.builder(
                      padding: EdgeInsets.zero,
                      itemCount: customerProvider.claimDocs.length,
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return InkWell(
                            onTap: () => imagePreview(
                                customerProvider.claimDocs[index]?.DocumentName,
                                customerProvider.claimDocs[index]?.DocumentUrl,
                                'Claim_Document'),
                            child: PostUploadWidget(
                              text: 'Claim Document',
                              type: 'Claim_Document',
                            ));
                      }),
                  // !isPolicyDocsUploaded
                  //     ? InkWell(
                  //         onTap: () => getFileDialogue('Policy'),
                  //         child: PreUploadWidget(text: 'Policy documents'))
                  //     : PostUploadWidget(text: 'Policy documents'),
                  // SizedBox(
                  //   height: 20.0.h,
                  // ),
                  // !isOtherDocsUploaded
                  //     ? InkWell(
                  //         onTap: () => getFileDialogue('Other'),
                  //         child: PreUploadWidget(text: 'Other documents'))
                  //     : PostUploadWidget(text: 'Other documents'),
                  // SizedBox(
                  //   height: 20.0.h,
                  // ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
