import 'package:flutter/material.dart';

import '../widgets/common/bottom_navigation_widget.dart';

class BaseLayout extends StatefulWidget {
  final Widget page;
  final bool isAppBar;
  final String text;
  final bool isBottomBar;
  final int activePageIndex;
  //final Widget appBar;

  const BaseLayout({
    required this.page,
    this.isAppBar = false,
    this.text = '',
    this.isBottomBar = true,
    this.activePageIndex = 0,
    // required this.appBar,
  });

  @override
  _BaseLayoutState createState() => _BaseLayoutState();
}

class _BaseLayoutState extends State<BaseLayout> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //  appBar: widget.isAppBar ? widget.appBar : null,
        body: widget.page,
        bottomNavigationBar: BottomNavigation(
          activePageIndex: widget.activePageIndex,
        ));
  }
}
