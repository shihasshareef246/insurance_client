import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gt_test/screens/travel_payment_details_screen.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'package:gt_test/common/buttons/primary_button.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/screens/login_screen.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:page_transition/page_transition.dart';
import 'package:gt_test/screens/motor_payment_details_screen.dart';
import '../models/motor_insurance_group_model.dart';
import '../models/travel_insurance_group_model.dart';
import '../providers/customer_provider.dart';
import 'package:gt_test/models/insurance_details_model.dart';
import '../providers/profile_provider.dart';
import 'init_screen.dart';

class TravelInsurerDetailsScreen extends StatefulWidget {
  bool isEdit;

  TravelInsurerDetailsScreen({
    this.isEdit = false,
  });
  @override
  _TravelInsurerDetailsScreenState createState() =>
      _TravelInsurerDetailsScreenState();
}

class _TravelInsurerDetailsScreenState
    extends State<TravelInsurerDetailsScreen> {
  CustomerProvider customerProvider = CustomerProvider();
  ProfileProvider profileProvider = ProfileProvider();
  late TextEditingController nameController;
  late TextEditingController relationController;
  late TextEditingController dobController;
  late TextEditingController civilIdController;
  late TextEditingController passportNoController;
  late TextEditingController travelFromDateController;
  late TextEditingController travelToDateController;
  late TextEditingController totalDaysController;
  late List<TextEditingController> nameControllerList;
  late List<TextEditingController> relationControllerList;
  late List<TextEditingController> dobControllerList;
  late List<TextEditingController> civilIdControllerList;
  late List<TextEditingController> passportNoControllerList;
  late List<FocusNode> nameFocusList;
  late List<FocusNode> relationFocusList;
  late List<FocusNode> dobFocusList;
  late List<FocusNode> civilIdFocusList;
  late List<FocusNode> passportNoFocusList;
  FocusNode nameFocus = FocusNode();
  FocusNode relationFocus = FocusNode();
  FocusNode dobFocus = FocusNode();
  FocusNode civilIdFocus = FocusNode();
  FocusNode passportNoFocus = FocusNode();
  FocusNode travelFromDateFocus = FocusNode();
  FocusNode travelToDateFocus = FocusNode();
  FocusNode totalDaysFocus = FocusNode();

  bool nameValid = false;
  bool relationValid = false;
  bool dobValid = false;
  bool civilIdValid = false;
  bool passportNoValid = false;
  bool travelFromDateValid = false;
  bool travelToDateValid = false;
  bool totalDaysValid = false;

  bool nameChanged = false;
  bool relationChanged = false;
  bool dobChanged = false;
  bool civilIdChanged = false;
  bool passportNoChanged = false;
  bool travelFromDateChanged = false;
  bool travelToDateChanged = false;
  bool totalDaysChanged = false;

  bool isLoading = false;
  final dateFormat = DateFormat("dd-MM-yyyy");

  @override
  void initState() {
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    nameControllerList = [
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController()
    ];
    nameFocusList = [
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode()
    ];
    relationControllerList = [
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController()
    ];
    relationFocusList = [
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode()
    ];
    dobControllerList = [
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController()
    ];
    dobFocusList = [
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode()
    ];
    civilIdControllerList = [
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController()
    ];
    civilIdFocusList = [
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode()
    ];
    passportNoControllerList = [
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController(),
      TextEditingController()
    ];
    passportNoFocusList = [
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode()
    ];

    travelFromDateController = TextEditingController(
        text: (customerProvider.insuranceTravelDetails != null)
            ? customerProvider.insuranceTravelDetails?.travelFromDate
            : '');
    if (travelFromDateController.text.isNotEmpty) {
      travelFromDateValid = true;
    }
    travelToDateController = TextEditingController(
        text: (customerProvider?.insuranceTravelDetails != null)
            ? customerProvider?.insuranceTravelDetails?.travelToDate
            : '');
    if (travelToDateController.text.isNotEmpty) {
      travelToDateValid = true;
    }
    totalDaysController = TextEditingController(
        text: (customerProvider?.insuranceTravelDetails != null)
            ? customerProvider?.insuranceTravelDetails?.totalTravelDays ?? ''
            : '');
    if (totalDaysController.text.isNotEmpty) {
      totalDaysValid = true;
    }

    for (int i = 0;
        i <
            int.parse(customerProvider?.selectedTravelInsuranceList?.length
                    .toString() ??
                '');
        i++) {
      nameControllerList[i] = TextEditingController(
          text: (customerProvider?.insuranceDetails != null &&
                  customerProvider.insuranceDetails.isNotEmpty)
              ? customerProvider?.insuranceDetails[i]?.insurerName ?? ''
              : '');
      if (nameControllerList[i].text.isNotEmpty) {
        nameValid = true;
      }
      relationControllerList[i] = TextEditingController(
          text: (customerProvider?.insuranceDetails != null &&
                  customerProvider.insuranceDetails.isNotEmpty)
              ? customerProvider?.insuranceDetails[i]?.relation ?? ''
              : '');
      if (relationControllerList[i].text.isNotEmpty) {
        relationValid = true;
      }
      dobControllerList[i] = TextEditingController(
          text: (customerProvider?.insuranceDetails != null &&
                  customerProvider.insuranceDetails.isNotEmpty)
              ? customerProvider?.insuranceDetails[i]?.dob
              : '');
      if (dobControllerList[i].text.isNotEmpty) {
        dobValid = true;
      }
      civilIdControllerList[i] = TextEditingController(
          text: (customerProvider?.insuranceDetails != null &&
                  customerProvider.insuranceDetails.isNotEmpty)
              ? customerProvider?.insuranceDetails[i]?.civilIdNo ?? ''
              : '');
      if (civilIdControllerList[i].text.isNotEmpty) {
        civilIdValid = true;
      }
      passportNoControllerList[i] = TextEditingController(
          text: (customerProvider?.insuranceDetails != null &&
                  customerProvider.insuranceDetails.isNotEmpty)
              ? customerProvider?.insuranceDetails[i]?.passportNo ?? ''
              : '');
      if (passportNoControllerList[i].text.isNotEmpty) {
        passportNoValid = true;
      }
    }
    super.initState();
  }

  @override
  void dispose() {
    // nameController.dispose();
    // relationController.dispose();
    // dobController.dispose();
    // civilIdController.dispose();
    // passportNoController.dispose();
    // travelFromDateController.dispose();
    // travelToDateController.dispose();
    // totalDaysController.dispose();
    //
    // nameFocus.dispose();
    // relationFocus.dispose();
    // dobFocus.dispose();
    // civilIdFocus.dispose();
    // passportNoFocus.dispose();
    // travelFromDateFocus.dispose();
    // travelToDateFocus.dispose();
    // totalDaysFocus.dispose();
    super.dispose();
  }

  bool isValid() {
    bool isValid = false;
    int nameCount = 0;
    int relationCount = 0;
    int dobCount = 0;
    int civilIdCount = 0;
    int passportNoCount = 0;
    nameControllerList.forEach((element) {
      if (element.text.isNotEmpty) {
        nameCount++;
      }
    });
    relationControllerList.forEach((element) {
      if (element.text.isNotEmpty) {
        relationCount++;
      }
    });
    dobControllerList.forEach((element) {
      if (element.text.isNotEmpty) {
        dobCount++;
      }
    });
    civilIdControllerList.forEach((element) {
      if (element.text.isNotEmpty) {
        civilIdCount++;
      }
    });
    passportNoControllerList.forEach((element) {
      if (element.text.isNotEmpty) {
        passportNoCount++;
      }
    });
    isValid = (((customerProvider.selectedTravelInsuranceList.length < 2) &&
            (nameCount ==
                customerProvider.selectedTravelInsuranceList.length) &&
            (dobCount == customerProvider.selectedTravelInsuranceList.length) &&
            (civilIdCount ==
                customerProvider.selectedTravelInsuranceList.length) &&
            (passportNoCount ==
                customerProvider.selectedTravelInsuranceList.length)) ||
        ((customerProvider.selectedTravelInsuranceList.length >= 2) &&
            (nameCount ==
                customerProvider.selectedTravelInsuranceList.length) &&
            (relationCount ==
                customerProvider.selectedTravelInsuranceList.length) &&
            (dobCount == customerProvider.selectedTravelInsuranceList.length) &&
            (civilIdCount ==
                customerProvider.selectedTravelInsuranceList.length) &&
            (passportNoCount ==
                customerProvider.selectedTravelInsuranceList.length)));
    return isValid;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.primaryBlue,
            leading: InkWell(
              onTap: () => Navigations.pop(context),
              child: Icon(
                Icons.arrow_back_ios,
                color: AppColors.white,
                size: 20.0.ics,
              ),
            ),
            title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Insurer Details',
                    style: buildAppTextTheme()
                        .headline3
                        ?.copyWith(color: AppColors.white),
                    textAlign: TextAlign.start,
                  ),
                  // SizedBox(
                  //   height: 5.0.h,
                  // ),
                  // Container(
                  //   width: 200.0.w,
                  //   child: Text(
                  //     desc!,
                  //     style: buildAppTextTheme().bodyText2,
                  //     textAlign: TextAlign.start,
                  //   ),
                  // ),
                ]),
          ),
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  left: 30.0.s, right: 30.0.s, top: 30.0.s, bottom: 60.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Please confirm your details before proceeding',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: travelFromDateFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(10.0.s),
                        bottom: Radius.zero,
                      ),
                    ),
                    child: DateTimeField(
                      focusNode: travelFromDateFocus,
                      style: travelFromDateFocus.hasFocus
                          ? buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.lightBlack)
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.focusText),
                      decoration: InputDecoration(
                        label: Row(
                          children: [
                            Text('*',
                                style: travelFromDateFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text("Travel From Date",
                                style: travelFromDateFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: travelFromDateFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        focusColor: AppColors.searchBlue,
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        border: InputBorder.none,
                      ),
                      enabled: false,
                      resetIcon: Icon(
                        Icons.calendar_month_sharp,
                        size: 20.0.ics,
                      ),
                      controller: travelFromDateController,
                      onChanged: (value) {
                        setState(() {
                          this.travelFromDateChanged = true;
                        });
                        if (value != null &&
                            value.month <= 12 &&
                            value.month >= 1 &&
                            value.year <= 2100 &&
                            value.year >= 1900 &&
                            value.day <= 31 &&
                            value.day >= 1) {
                          setState(() {
                            this.travelFromDateValid = true;
                          });
                        } else {
                          setState(() {
                            this.travelFromDateValid = false;
                          });
                        }
                      },
                      format: dateFormat,
                      onShowPicker: (context, currentValue) {
                        return showDatePicker(
                            context: context,
                            firstDate: DateTime(1900),
                            initialDate: currentValue ?? DateTime.now(),
                            lastDate: DateTime(2100));
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: travelToDateFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(10.0.s),
                        bottom: Radius.zero,
                      ),
                    ),
                    child: DateTimeField(
                      focusNode: travelToDateFocus,
                      style: travelToDateFocus.hasFocus
                          ? buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.lightBlack)
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.focusText),
                      decoration: InputDecoration(
                        label: Row(
                          children: [
                            Text('*',
                                style: travelToDateFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text("Travel To Date",
                                style: travelToDateFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: travelToDateFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        focusColor: AppColors.searchBlue,
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        border: InputBorder.none,
                      ),
                      enabled: false,
                      resetIcon: Icon(
                        Icons.calendar_month_sharp,
                        size: 20.0.ics,
                      ),
                      controller: travelToDateController,
                      onChanged: (value) {
                        setState(() {
                          this.travelToDateChanged = true;
                        });
                        if (value != null &&
                            value.month <= 12 &&
                            value.month >= 1 &&
                            value.year <= 2100 &&
                            value.year >= 1900 &&
                            value.day <= 31 &&
                            value.day >= 1) {
                          setState(() {
                            this.travelToDateValid = true;
                          });
                        } else {
                          setState(() {
                            this.travelToDateValid = false;
                          });
                        }
                      },
                      format: dateFormat,
                      onShowPicker: (context, currentValue) {
                        return showDatePicker(
                            context: context,
                            firstDate: DateTime(1900),
                            initialDate: currentValue ?? DateTime.now(),
                            lastDate: DateTime(2100));
                      },
                    ),
                  ),
                  SizedBox(height: 12.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: totalDaysFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      // onFieldSubmitted: (_) =>
                      //     FocusScope.of(context).requestFocus(nameFocus),
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: totalDaysFocus,
                      style: totalDaysFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.number,
                      enabled: false,
                      controller: totalDaysController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        label: Row(
                          children: [
                            Text('*',
                                style: totalDaysFocus.hasFocus
                                    ? buildAppTextTheme()
                                        .subtitle2!
                                        .copyWith(color: Colors.red)
                                    : buildAppTextTheme()
                                        .headline6!
                                        .copyWith(color: Colors.red)),
                            Padding(
                              padding: EdgeInsets.all(3.0),
                            ),
                            Text("Total Days",
                                style: totalDaysFocus.hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6)
                          ],
                        ),
                        labelStyle: totalDaysFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.totalDaysChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.totalDaysValid = false;
                          });
                        } else {
                          setState(() {
                            this.totalDaysValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 12.0.h,
                  ),
                  ListView.builder(
                    itemCount:
                        customerProvider.selectedTravelInsuranceList.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    itemBuilder: (context, index) {
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Member ${index + 1}',
                            style: buildAppTextTheme()
                                .overline!
                                .copyWith(color: AppColors.primaryBlue),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 10.0.h,
                          ),
                          Container(
                            height: 60.0.h,
                            width: 368.0.w,
                            decoration: BoxDecoration(
                              color: nameFocusList[index].hasFocus
                                  ? AppColors.paleBlue
                                  : AppColors.white,
                              border: Border.all(color: AppColors.border),
                              // border: Border(
                              //     top: BorderSide(color: AppColors.border, width: 1.0),
                              //     bottom:
                              //         BorderSide(color: AppColors.border, width: 1.0),
                              //     right:
                              //         BorderSide(color: AppColors.border, width: 1.0)),
                              borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(6.0.s),
                                topRight: Radius.circular(6.0.s),
                              ),
                            ),
                            child: TextFormField(
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(
                                    RegExp("[a-zA-Z ]")),
                              ],
                              onFieldSubmitted: (_) => FocusScope.of(context)
                                  .requestFocus(relationFocusList[index]),
                              scrollPadding: EdgeInsets.only(bottom: 100),
                              focusNode: nameFocusList[index],
                              style: nameFocusList[index].hasFocus
                                  ? buildAppTextTheme().headline5
                                  : buildAppTextTheme()
                                      .subtitle1!
                                      .copyWith(color: AppColors.black),
                              keyboardType: TextInputType.name,
                              controller: nameControllerList[index],
                              obscureText: false,
                              decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.only(
                                  left: 10.0.s,
                                  top: 8.0.s,
                                  bottom: 8.0.s,
                                  right: 10.0.s,
                                ),
                                label: Row(
                                  children: [
                                    Text('*',
                                        style: nameFocusList[index].hasFocus
                                            ? buildAppTextTheme()
                                                .subtitle2!
                                                .copyWith(color: Colors.red)
                                            : buildAppTextTheme()
                                                .headline6!
                                                .copyWith(color: Colors.red)),
                                    Padding(
                                      padding: EdgeInsets.all(3.0),
                                    ),
                                    Text("Name",
                                        style: nameFocusList[index].hasFocus
                                            ? buildAppTextTheme().subtitle2
                                            : buildAppTextTheme().headline6)
                                  ],
                                ),
                                labelStyle: nameFocusList[index].hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6,
                                border: InputBorder.none,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: AppColors.primaryBlue),
                                  borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(10.0.s),
                                    bottom: Radius.zero,
                                  ),
                                ),
                              ),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              onChanged: (value) {
                                setState(() {
                                  this.nameChanged = true;
                                });
                                if (value.isEmpty) {
                                  setState(() {
                                    this.nameValid = false;
                                  });
                                } else {
                                  setState(() {
                                    this.nameValid = true;
                                  });
                                }
                              },
                            ),
                          ),
                          SizedBox(height: 12.0.h),
                          if (customerProvider
                                  .selectedTravelInsuranceList.length >=
                              2)
                            Container(
                              height: 60.0.h,
                              width: 368.0.w,
                              decoration: BoxDecoration(
                                color: relationFocusList[index].hasFocus
                                    ? AppColors.paleBlue
                                    : AppColors.white,
                                border: Border.all(color: AppColors.border),
                                // border: Border(
                                //     top: BorderSide(color: AppColors.border, width: 1.0),
                                //     bottom:
                                //         BorderSide(color: AppColors.border, width: 1.0),
                                //     right:
                                //         BorderSide(color: AppColors.border, width: 1.0)),
                                borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(6.0.s),
                                  topRight: Radius.circular(6.0.s),
                                ),
                              ),
                              child: TextFormField(
                                onFieldSubmitted: (_) => FocusScope.of(context)
                                    .requestFocus(civilIdFocusList[index]),
                                scrollPadding: EdgeInsets.only(bottom: 100),
                                focusNode: relationFocusList[index],
                                style: relationFocusList[index].hasFocus
                                    ? buildAppTextTheme().headline5
                                    : buildAppTextTheme()
                                        .subtitle1!
                                        .copyWith(color: AppColors.black),
                                keyboardType: TextInputType.name,
                                controller: relationControllerList[index],
                                obscureText: false,
                                decoration: InputDecoration(
                                  isDense: true,
                                  contentPadding: EdgeInsets.only(
                                    left: 10.0.s,
                                    top: 8.0.s,
                                    bottom: 8.0.s,
                                    right: 10.0.s,
                                  ),
                                  label: Row(
                                    children: [
                                      Text('*',
                                          style: relationFocusList[index]
                                                  .hasFocus
                                              ? buildAppTextTheme()
                                                  .subtitle2!
                                                  .copyWith(color: Colors.red)
                                              : buildAppTextTheme()
                                                  .headline6!
                                                  .copyWith(color: Colors.red)),
                                      Padding(
                                        padding: EdgeInsets.all(3.0),
                                      ),
                                      Text("Relation",
                                          style: relationFocusList[index]
                                                  .hasFocus
                                              ? buildAppTextTheme().subtitle2
                                              : buildAppTextTheme().headline6)
                                    ],
                                  ),
                                  labelStyle: relationFocusList[index].hasFocus
                                      ? buildAppTextTheme().subtitle2
                                      : buildAppTextTheme().headline6,
                                  border: InputBorder.none,
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                        color: AppColors.primaryBlue),
                                    borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(10.0.s),
                                      bottom: Radius.zero,
                                    ),
                                  ),
                                ),
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                onChanged: (value) {
                                  setState(() {
                                    this.relationChanged = true;
                                  });
                                  if (value.isEmpty) {
                                    setState(() {
                                      this.relationValid = false;
                                    });
                                  } else {
                                    setState(() {
                                      this.relationValid = true;
                                    });
                                  }
                                },
                              ),
                            ),
                          if (customerProvider
                                  .selectedTravelInsuranceList.length >=
                              2)
                            SizedBox(height: 12.0.h),
                          Container(
                            height: 60.0.h,
                            width: 368.0.w,
                            decoration: BoxDecoration(
                              color: dobFocusList[index].hasFocus
                                  ? AppColors.paleBlue
                                  : AppColors.white,
                              border: Border.all(color: AppColors.border),
                              borderRadius: BorderRadius.vertical(
                                top: Radius.circular(10.0.s),
                                bottom: Radius.zero,
                              ),
                            ),
                            child: DateTimeField(
                              focusNode: dobFocusList[index],
                              style: dobFocusList[index].hasFocus
                                  ? buildAppTextTheme()
                                      .subtitle1!
                                      .copyWith(color: AppColors.lightBlack)
                                  : buildAppTextTheme()
                                      .subtitle1!
                                      .copyWith(color: AppColors.focusText),
                              decoration: InputDecoration(
                                label: Row(
                                  children: [
                                    Text('*',
                                        style: dobFocusList[index].hasFocus
                                            ? buildAppTextTheme()
                                                .subtitle2!
                                                .copyWith(color: Colors.red)
                                            : buildAppTextTheme()
                                                .headline6!
                                                .copyWith(color: Colors.red)),
                                    Padding(
                                      padding: EdgeInsets.all(3.0),
                                    ),
                                    Text("Date of Birth",
                                        style: dobFocusList[index].hasFocus
                                            ? buildAppTextTheme().subtitle2
                                            : buildAppTextTheme().headline6)
                                  ],
                                ),
                                labelStyle: dobFocusList[index].hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6,
                                focusColor: AppColors.searchBlue,
                                isDense: true,
                                contentPadding: EdgeInsets.only(
                                  left: 10.0.s,
                                  top: 8.0.s,
                                  bottom: 8.0.s,
                                  right: 10.0.s,
                                ),
                                border: InputBorder.none,
                              ),
                              controller: dobControllerList[index],
                           //   enabled: false,
                              resetIcon: Icon(
                                Icons.calendar_month_sharp,
                                size: 20.0.ics,
                              ),
                              onChanged: (value) {
                                setState(() {
                                  this.dobChanged = true;
                                });
                                if (value != null &&
                                    value.month <= 12 &&
                                    value.month >= 1 &&
                                    value.year <= 2100 &&
                                    value.year >= 1900 &&
                                    value.day <= 31 &&
                                    value.day >= 1) {
                                  setState(() {
                                    this.dobValid = true;
                                  });
                                } else {
                                  setState(() {
                                    this.dobValid = false;
                                  });
                                }
                              },
                              format: dateFormat,
                              onShowPicker: (context, currentValue) {
                                return showDatePicker(
                                    context: context,
                                    firstDate: DateTime(1900),
                                    initialDate: currentValue ?? DateTime.now(),
                                    lastDate: DateTime(2100));
                              },
                            ),
                          ),
                          SizedBox(height: 12.0.h),
                          Container(
                            height: 60.0.h,
                            width: 368.0.w,
                            decoration: BoxDecoration(
                              color: civilIdFocusList[index].hasFocus
                                  ? AppColors.paleBlue
                                  : AppColors.white,
                              border: Border.all(color: AppColors.border),
                              // border: Border(
                              //     top: BorderSide(color: AppColors.border, width: 1.0),
                              //     bottom:
                              //         BorderSide(color: AppColors.border, width: 1.0),
                              //     right:
                              //         BorderSide(color: AppColors.border, width: 1.0)),
                              borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(6.0.s),
                                topRight: Radius.circular(6.0.s),
                              ),
                            ),
                            child: TextFormField(
                              onFieldSubmitted: (_) => FocusScope.of(context)
                                  .requestFocus(passportNoFocusList[index]),
                              scrollPadding: EdgeInsets.only(bottom: 100),
                              focusNode: civilIdFocusList[index],
                              style: civilIdFocusList[index].hasFocus
                                  ? buildAppTextTheme().headline5
                                  : buildAppTextTheme()
                                      .subtitle1!
                                      .copyWith(color: AppColors.black),
                              keyboardType: TextInputType.number,
                              controller: civilIdControllerList[index],
                              obscureText: false,
                              decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.only(
                                  left: 10.0.s,
                                  top: 8.0.s,
                                  bottom: 8.0.s,
                                  right: 10.0.s,
                                ),
                                label: Row(
                                  children: [
                                    Text('*',
                                        style: civilIdFocusList[index].hasFocus
                                            ? buildAppTextTheme()
                                                .subtitle2!
                                                .copyWith(color: Colors.red)
                                            : buildAppTextTheme()
                                                .headline6!
                                                .copyWith(color: Colors.red)),
                                    Padding(
                                      padding: EdgeInsets.all(3.0),
                                    ),
                                    Text("Civil ID",
                                        style: civilIdFocusList[index].hasFocus
                                            ? buildAppTextTheme().subtitle2
                                            : buildAppTextTheme().headline6)
                                  ],
                                ),
                                labelStyle: civilIdFocusList[index].hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6,
                                border: InputBorder.none,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: AppColors.primaryBlue),
                                  borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(10.0.s),
                                    bottom: Radius.zero,
                                  ),
                                ),
                              ),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              onChanged: (value) {
                                setState(() {
                                  this.civilIdChanged = true;
                                });
                                if (value.isEmpty) {
                                  setState(() {
                                    this.civilIdValid = false;
                                  });
                                } else {
                                  setState(() {
                                    this.civilIdValid = true;
                                  });
                                }
                              },
                            ),
                          ),
                          SizedBox(height: 12.0.h),
                          Container(
                            height: 60.0.h,
                            width: 368.0.w,
                            decoration: BoxDecoration(
                              color: passportNoFocusList[index].hasFocus
                                  ? AppColors.paleBlue
                                  : AppColors.white,
                              border: Border.all(color: AppColors.border),
                              // border: Border(
                              //     top: BorderSide(color: AppColors.border, width: 1.0),
                              //     bottom:
                              //         BorderSide(color: AppColors.border, width: 1.0),
                              //     right:
                              //         BorderSide(color: AppColors.border, width: 1.0)),
                              borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(6.0.s),
                                topRight: Radius.circular(6.0.s),
                              ),
                            ),
                            child: TextFormField(
                              // onFieldSubmitted: (_) => FocusScope.of(context)
                              //     .requestFocus(travelFromDateFocus),
                              scrollPadding: EdgeInsets.only(bottom: 100),
                              focusNode: passportNoFocusList[index],
                              style: passportNoFocusList[index].hasFocus
                                  ? buildAppTextTheme().headline5
                                  : buildAppTextTheme()
                                      .subtitle1!
                                      .copyWith(color: AppColors.black),
                              keyboardType: TextInputType.emailAddress,
                              controller: passportNoControllerList[index],
                              obscureText: false,
                              decoration: InputDecoration(
                                isDense: true,
                                contentPadding: EdgeInsets.only(
                                  left: 10.0.s,
                                  top: 8.0.s,
                                  bottom: 8.0.s,
                                  right: 10.0.s,
                                ),
                                label: Row(
                                  children: [
                                    Text('*',
                                        style: passportNoFocusList[index]
                                                .hasFocus
                                            ? buildAppTextTheme()
                                                .subtitle2!
                                                .copyWith(color: Colors.red)
                                            : buildAppTextTheme()
                                                .headline6!
                                                .copyWith(color: Colors.red)),
                                    Padding(
                                      padding: EdgeInsets.all(3.0),
                                    ),
                                    Text("Passport No",
                                        style:
                                            passportNoFocusList[index].hasFocus
                                                ? buildAppTextTheme().subtitle2
                                                : buildAppTextTheme().headline6)
                                  ],
                                ),
                                labelStyle: passportNoFocusList[index].hasFocus
                                    ? buildAppTextTheme().subtitle2
                                    : buildAppTextTheme().headline6,
                                border: InputBorder.none,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: AppColors.primaryBlue),
                                  borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(10.0.s),
                                    bottom: Radius.zero,
                                  ),
                                ),
                              ),
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              onChanged: (value) {
                                setState(() {
                                  this.passportNoChanged = true;
                                });
                                if (value.isEmpty) {
                                  setState(() {
                                    this.passportNoValid = false;
                                  });
                                } else {
                                  setState(() {
                                    this.passportNoValid = true;
                                  });
                                }
                              },
                            ),
                          ),
                          SizedBox(height: 12.0.h),
                        ],
                      );
                    },
                  ),
                  SizedBox(height: 43.0.h),
                  Text(
                    'Please confirm your details before proceeding.',
                    style: buildAppTextTheme()
                        .headline6
                        ?.copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(height: 16.0.h),
                  PrimaryButton(
                      height: 65.0.h,
                      width: 368.0.w,
                      color: AppColors.primaryBlue,
                      disabled: !(widget.isEdit || isValid()),
                      isLoading: isLoading,
                      onPressed: () async {
                        if (!isLoading) {
                          setState(() {
                            isLoading = true;
                          });
                          customerProvider.resetInsuranceDetails();

                          for (int i = 0;
                              i <
                                  int.parse(customerProvider
                                          ?.selectedTravelInsuranceList?.length
                                          .toString() ??
                                      '');
                              i++) {
                            customerProvider.insuranceDetails
                                .add(PersonalDetailsModel(
                              //TODO
                              insuranceId: 2,
                              dob: dobControllerList[i].text,
                              civilIdNo: civilIdControllerList[i].text,
                              relation: relationControllerList[i].text,
                              insurerName: nameControllerList[i].text,
                              passportNo: passportNoControllerList[i].text,
                            ));
                          }
                          if (widget.isEdit ||
                              (profileProvider.profileData?.customerId !=
                                      null &&
                                  profileProvider.profileData?.customerId !=
                                      '')) {
                            Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.fade,
                                    child: TravelPaymentDetailsScreen()));
                          } else {
                            InitScreen.fromInsurerDetails = true;
                            Navigator.push(
                              context,
                              PageTransition(
                                type: PageTransitionType.fade,
                                child: LoginScreen(
                                  customerName: nameControllerList[0].text,
                                ),
                              ),
                            );
                          }
                          // Navigations.pushNamed(
                          //   context,
                          //   AppRouter.loginScreen,
                          // );
                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      text: 'Continue',
                      borderRadius: 80.0.s),
                  SizedBox(
                    height: 17.0.h,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
