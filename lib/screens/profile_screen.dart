import 'dart:io';
import 'package:flutter/scheduler.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:image_picker/image_picker.dart';

import 'package:gt_test/common/logger.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/screens/prelogin_screen.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import '../common/buttons/raw_button.dart';
import '../common/images.dart';
import '../common/pref_status.dart';
import '../common/shared_prefs.dart';
import '../common/utils.dart';
import '../models/profile_model.dart';
import '../models/status_model.dart';
import '../providers/customer_provider.dart';
import '../providers/profile_provider.dart';
import '../services/profile_service.dart';
import '../widgets/common/upload_bottom_sheet_widget.dart';
import 'base.dart';
import 'home_screen.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  late ProfileProvider profileProvider;
  late CustomerProvider customerProvider;
  late TextEditingController nameController;
  late TextEditingController emailController;
  late TextEditingController phoneNoController;
  late TextEditingController mobileNoController;
  late TextEditingController dobController;
  late TextEditingController civilIdController;
  late FocusNode civilIdFocus;
  late FocusNode phoneNoFocus;
  late FocusNode nameFocus;
  late FocusNode emailFocus;
  late FocusNode dobFocus;
  late FocusNode mobileNoFocus;
  bool civilIdValid = false;
  bool phoneNoValid = false;
  bool nameValid = false;
  bool emailValid = false;
  bool dobValid = false;
  bool mobileNoValid = false;
  bool civilIdChanged = false;
  bool phoneNoChanged = false;
  bool nameChanged = false;
  bool emailChanged = false;
  bool dobChanged = false;
  bool mobileNoChanged = false;
  bool isLoading = false;
  bool isEdit = false;
  final picker = ImagePicker();
  File file = File("");
  String fileExtension = "";
  final dateFormat = DateFormat("dd-MM-yyyy");

  @override
  void initState() {
    super.initState();
    ToastContext().init(context);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    nameController =
        TextEditingController(text: profileProvider.profileData?.customerName);
    emailController =
        TextEditingController(text: profileProvider.profileData?.emailId);
    phoneNoController =
        TextEditingController(text: profileProvider.profileData?.phoneNo);
    mobileNoController =
        TextEditingController(text: profileProvider.profileData?.mobileNo);
    dobController =
        TextEditingController(text: profileProvider.profileData?.dob);
    civilIdController =
        TextEditingController(text: profileProvider.profileData?.civilidNo);
    civilIdFocus = FocusNode();
    phoneNoFocus = FocusNode();
    nameFocus = FocusNode();
    emailFocus = FocusNode();
    dobFocus = FocusNode();
    mobileNoFocus = FocusNode();
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      profileProvider.setProfileData = await ProfileService()
          .getCustomerDetails(
              context: context,
              customerId: profileProvider.profileData?.customerId ?? '');
      setState(() {});
    });
  }

  void getFileDialogue() {
    showModalBottomSheet(
        backgroundColor: AppColors.primaryBlue.withOpacity(0.9),
        barrierColor: AppColors.primaryBlue.withOpacity(0.9),
        context: context,
        builder: (BuildContext context) {
          return UploadBottomSheetWidget(
            title: 'Upload image via',
            onClick: () async {
              Navigator.pop(context);
              getFileFromCamera();
            },
            image: AppImages.CAMERA,
            listTitle: 'Camera',
            onClick1: () async {
              Navigator.pop(context);
              getFileFromGallery();
            },
            image1: AppImages.GALLERY,
            listTitle1: 'Gallery',
            onClick2: () async {
              Navigator.pop(context);
              getFileFromDocument();
            },
            image2: Icons.attach_file_sharp,
            listTitle2: 'Document',
          );
        });
  }

  Future getFileFromCamera() async {
    final pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);
    if (pickedFile != null) {
      setState(() {
        file = File(pickedFile.path);
        fileExtension =
            pickedFile.path.substring(pickedFile.path.lastIndexOf('.') + 1);
      });
      String? url = await Utils.uploadDocument(file, 'Profile_Picture',
          fileExtension, profileProvider.profileData?.customerId ?? '');
      StatusModel? response = await ProfileService().postCustomerPhoto(
          customerId: profileProvider.profileData?.customerId ?? '',
          photoUrl: url ?? '',
          context: context);
      if (response != null && response.status != null) {
        setState(() {
          profileProvider?.profileData?.photoUrl = url!;
        });
        await SharedPrefs().addMapPrefs(
            'userdetails', profileProvider.profileData?.toJson() ?? Map());
      }
      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
  }

  Future getFileFromGallery() async {
    final pickedFile =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 50);
    if (pickedFile != null) {
      setState(() {
        file = File(pickedFile.path);
        fileExtension =
            pickedFile.path.substring(pickedFile.path.lastIndexOf('.') + 1);
      });

      String? url = await Utils.uploadDocument(file, 'Photo', fileExtension,
          profileProvider.profileData?.customerId ?? '');
      StatusModel? response = await ProfileService().postCustomerPhoto(
          customerId: profileProvider.profileData?.customerId ?? '',
          photoUrl: url ?? '',
          context: context);
      if (response != null && response.status != null) {
        setState(() {
          profileProvider?.profileData?.photoUrl = url!;
        });
        await SharedPrefs().addMapPrefs(
            'userdetails', profileProvider.profileData?.toJson() ?? Map());
      }

      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
  }

  Future getFileFromDocument() async {
    FilePickerResult? pickedFile =
        await FilePicker.platform.pickFiles(type: FileType.any);
    if (pickedFile != null) {
      setState(() {
        print('file picked');
        file = File(pickedFile.files.single.path!);
        fileExtension = pickedFile.files.single.extension!;
      });

      String? url = await Utils.uploadDocument(file, 'Photo', fileExtension,
          profileProvider.profileData?.customerId ?? '');
      StatusModel? response = await ProfileService().postCustomerPhoto(
          customerId: profileProvider.profileData?.customerId ?? '',
          photoUrl: url ?? '',
          context: context);
      if (response != null && response.status != null) {
        setState(() {
          profileProvider?.profileData?.photoUrl = url!;
        });
        await SharedPrefs().addMapPrefs(
            'userdetails', profileProvider.profileData?.toJson() ?? Map());
      }

      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
  }

  @override
  Widget build(BuildContext context) {
    profileProvider = Provider.of<ProfileProvider>(context, listen: true);
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.primaryBlue,
            leading: InkWell(
              onTap: () => Navigations.pop(context),
              child: Icon(
                Icons.arrow_back_ios,
                color: AppColors.white,
                size: 20.0.ics,
              ),
            ),
            title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Profile',
                    style: buildAppTextTheme()
                        .headline3
                        ?.copyWith(color: AppColors.white),
                    textAlign: TextAlign.start,
                  ),
                ]),
          ),
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: false,
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  left: 30.0.s, right: 30.0.s, top: 65.0.s, bottom: 60.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Stack(
                      children: [
                        fileExtension != ''
                            ? CircleAvatar(
                                backgroundColor: AppColors.lightGrey,
                                radius: 80.0.s,
                                backgroundImage: FileImage(file),
                              )
                            : (profileProvider?.profileData?.photoUrl != null &&
                                    profileProvider?.profileData?.photoUrl !=
                                        '')
                                ? CircleAvatar(
                                    backgroundColor: AppColors.lightGrey,
                                    radius: 80.0.s,
                                    backgroundImage: NetworkImage(
                                        profileProvider
                                                ?.profileData?.photoUrl ??
                                            ''),
                                  )
                                : CircleAvatar(
                                    backgroundColor: AppColors.lightGrey,
                                    radius: 80.0.s,
                                    child: Image(
                                      image: AssetImage(AppImages.PROFILE),
                                      height: 99.0.h,
                                      width: 296.0.w,
                                    ),
                                  ),
                        Positioned(
                          bottom: 0.0.s,
                          right: 12.0.s,
                          child: InkWell(
                            onTap: () => getFileDialogue(),
                            child: CircleAvatar(
                              backgroundColor: AppColors.primaryBlue,
                              radius: 20.0.s,
                              child: Icon(
                                Icons.add,
                                color: AppColors.white,
                                size: 24.0.ics,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 45.0.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'General',
                        style: buildAppTextTheme().headline3,
                        textAlign: TextAlign.center,
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            isEdit = true;
                          });
                        },
                        child: isEdit
                            ? RawButton(
                                width: 73.0.w,
                                height: 48.0.h,
                                text: 'Save',
                                borderRadius: 4.0.s,
                                onPressed: () async {
                                  var response = await ProfileService()
                                      .postCustomerDetails(
                                    context: context,
                                    emailId: emailController.text,
                                    customerName: nameController.text,
                                    phoneNo: phoneNoController.text,
                                    dob: dobController.text,
                                    mobileNo: mobileNoController.text,
                                    civilIdNo: civilIdController.text,
                                    customerId: profileProvider
                                        ?.profileData?.customerId,
                                  );
                                  if (response != null &&
                                      response.customerId != '') {
                                    profileProvider.setProfileData =
                                        ProfileModel(
                                            customerId: response.customerId,
                                            emailId: emailController.text,
                                            customerName: nameController.text,
                                            civilidNo: civilIdController.text,
                                            dob: dobController.text,
                                            mobileNo: mobileNoController.text,
                                            phoneNo: phoneNoController.text,
                                            photoUrl: profileProvider
                                                    ?.profileData?.photoUrl ??
                                                '',
                                            otpVerified: true);
                                    await SharedPrefs().addMapPrefs(
                                        'userdetails',
                                        profileProvider.profileData?.toJson() ??
                                            Map());
                                    Toast.show('Profile updated successfully',
                                        backgroundColor: AppColors.green,
                                        textStyle: buildAppTextTheme()
                                            .subtitle1!
                                            .copyWith(
                                              color: AppColors.white,
                                            ),
                                        duration: 3,
                                        gravity: 0);
                                    Navigator.push(
                                      context,
                                      PageTransition(
                                        type: PageTransitionType.fade,
                                        child: BaseLayout(
                                          page: HomeScreen(),
                                          activePageIndex: 0,
                                        ),
                                      ),
                                    );
                                    // Navigations.pushNamed(
                                    //   context,
                                    //   AppRouter.homeScreen,
                                    // );
                                  }
                                },
                                bgColor: AppColors.primaryBlue,
                                textColor: AppColors.white,
                              )
                            : Container(
                                padding: EdgeInsets.all(12.0.s),
                                decoration: BoxDecoration(
                                  color: AppColors.white,
                                  border: Border.all(
                                      color: AppColors.border, width: 1.0),
                                  borderRadius: BorderRadius.circular(
                                    6.0.s,
                                  ),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Image(
                                      image: AssetImage(AppImages.EDIT),
                                      height: 18.0.h,
                                      width: 18.0.w,
                                    ),
                                    SizedBox(
                                      width: 10.0.w,
                                    ),
                                    Text(
                                      'Edit',
                                      style: buildAppTextTheme().headline6,
                                      textAlign: TextAlign.center,
                                    ),
                                  ],
                                ),
                              ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  Divider(
                    color: AppColors.border,
                    height: 1.0.h,
                  ),
                  SizedBox(
                    height: 23.0.h,
                  ),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: nameFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),
                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(30),
                      ],
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: nameFocus,
                      style: nameFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: nameController,
                      obscureText: false,
                      enabled: isEdit,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        labelText: 'Customer name',
                        labelStyle: nameFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.nameChanged = true;
                        });
                        if (value.isEmpty || value.length > 30) {
                          setState(() {
                            this.nameValid = false;
                          });
                        } else {
                          setState(() {
                            this.nameValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 30.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: emailFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: emailFocus,
                      style: emailFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: emailController,
                      obscureText: false,
                      enabled: isEdit,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        labelText: 'Email address',
                        labelStyle: emailFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.emailChanged = true;
                        });
                        if (value.isEmpty ||
                            !value.contains('@') ||
                            !value.contains('.')) {
                          setState(() {
                            this.emailValid = false;
                          });
                        } else {
                          setState(() {
                            this.emailValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 30.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: civilIdFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(10),
                      ],
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: civilIdFocus,
                      style: civilIdFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.number,
                      controller: civilIdController,
                      obscureText: false,
                      enabled: isEdit,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        labelText: 'Civil ID',
                        labelStyle: civilIdFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.civilIdChanged = true;
                        });
                        if (value.isEmpty || value.length != 10) {
                          setState(() {
                            this.civilIdValid = false;
                          });
                        } else {
                          setState(() {
                            this.civilIdValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 30.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: dobFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: DateTimeField(
                      focusNode: dobFocus,
                      style: dobFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      enabled: isEdit,
                      decoration: InputDecoration(
                        labelText: 'Date of Birth',
                        labelStyle: dobFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        focusColor: AppColors.searchBlue,
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        border: InputBorder.none,
                      ),
                      controller: dobController,
                      onChanged: (value) {
                        setState(() {
                          this.dobChanged = true;
                        });
                        if (value != null &&
                            value.month != null &&
                            value.year != null &&
                            value.day != null &&
                            value.month <= 12 &&
                            value.month >= 1 &&
                            value.year <= 2100 &&
                            value.year >= 1900 &&
                            value.day <= 31 &&
                            value.day >= 1) {
                          setState(() {
                            this.dobValid = true;
                          });
                        } else {
                          setState(() {
                            this.dobValid = false;
                          });
                        }
                      },
                      format: dateFormat,
                      onShowPicker: (context, currentValue) {
                        return showDatePicker(
                            context: context,
                            firstDate: DateTime(1900),
                            initialDate: currentValue ?? DateTime.now(),
                            lastDate: DateTime(2100));
                      },
                    ),
                  ),
                  SizedBox(height: 30.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: phoneNoFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(10),
                      ],
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: phoneNoFocus,
                      style: phoneNoFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.number,
                      controller: phoneNoController,
                      obscureText: false,
                      enabled: isEdit,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        labelText: 'Phone number',
                        labelStyle: phoneNoFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.phoneNoChanged = true;
                        });
                        if (value.isEmpty || value.length != 10) {
                          setState(() {
                            this.phoneNoValid = false;
                          });
                        } else {
                          setState(() {
                            this.phoneNoValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 30.0.h),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: mobileNoFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: mobileNoFocus,
                      style: mobileNoFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.number,
                      controller: mobileNoController,
                      obscureText: false,
                      enabled: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        labelText: 'Mobile number',
                        labelStyle: mobileNoFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().headline6,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.mobileNoChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.mobileNoValid = false;
                          });
                        } else {
                          setState(() {
                            this.mobileNoValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 30.0.h,
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: InkWell(
                      onTap: () async {
                        customerProvider.resetSelectedMotorInsuranceList();
                        customerProvider.resetSelectedAdditionalMotorInsuranceList();
                        customerProvider.resetInsuranceList();
                        customerProvider.resetClaimsList();
                        customerProvider.resetInsurerMotorDetails();
                        customerProvider.resetInsuranceDetails();
                        profileProvider.resetProfileData();
                        await SharedPrefs().removePrefs('userdetails');
                        Navigator.push(
                          context,
                          PageTransition(
                            type: PageTransitionType.fade,
                            child: PreLoginScreen(),
                          ),
                        );
                        // Navigations.pushNamed(
                        //   context,
                        //   AppRouter.preloginScreen,
                        // );
                      },
                      child: Text(
                        'Log Out',
                        style: buildAppTextTheme().headline3,
                        textAlign: TextAlign.right,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 69.0.h,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
