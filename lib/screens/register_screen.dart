import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

import 'package:gt_test/common/buttons/primary_button.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/routes/routes.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/screens/otp_screen.dart';
import 'package:page_transition/page_transition.dart';
import '../common/images.dart';
import '../models/profile_model.dart';
import '../providers/profile_provider.dart';
import '../services/customer_service.dart';
import '../services/otp_service.dart';
import '../services/profile_service.dart';

class RegisterScreen extends StatefulWidget {
  final String mobileNo;

  RegisterScreen({
    this.mobileNo = '',
  });
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  late ProfileProvider profileProvider;
  late TextEditingController mobileController;
  late FocusNode mobileFocus;
  bool mobileValid = false;
  bool mobileChanged = false;
  TextEditingController emailController = TextEditingController();
  FocusNode emailFocus = FocusNode();
  bool emailValid = false;
  bool emailChanged = false;
  TextEditingController nameController = TextEditingController();
  FocusNode nameFocus = FocusNode();
  bool nameValid = false;
  bool nameChanged = false;
  bool isLoading = false;
  bool isError = false;
  @override
  void initState() {
    ToastContext().init(context);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    mobileController = TextEditingController(text: widget.mobileNo ?? '');
    if (mobileController.text.isNotEmpty) {
      mobileValid = true;
    }
    mobileFocus = FocusNode();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pushNamedAndRemoveUntil(
          context, AppRouter.preloginScreen),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: Container(
              padding:
                  EdgeInsets.only(left: 24.0.s, right: 20.0.s, bottom: 20.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 40.0.h,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () => Navigations.pushNamedAndRemoveUntil(
                          context, AppRouter.preloginScreen),
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: AppColors.primaryBlue,
                        size: 20.0.ics,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 40.0.h,
                  ),
                  Image(
                    image: AssetImage(AppImages.LOGO),
                    height: 99.0.h,
                    width: 296.0.w,
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  isError
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.all(6.0.s),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 6.0.s, vertical: 12.0.s),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: AppColors.red, width: 1.0),
                                borderRadius: BorderRadius.circular(
                                  6.0.s,
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Icon(
                                    Icons.report_problem_outlined,
                                    color: AppColors.red,
                                    size: 36.0.ics,
                                  ),
                                  SizedBox(width: 10.0.w),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'There was a problem',
                                        style: buildAppTextTheme()
                                            .headline3!
                                            .copyWith(color: AppColors.red),
                                        textAlign: TextAlign.left,
                                      ),
                                      SizedBox(height: 10.0.h),
                                      Container(
                                        width: 300.0.w,
                                        child: Text(
                                          'The provided mobile number has already been used. Please use another mobile number.',
                                          style: buildAppTextTheme()
                                              .headline5!
                                              .copyWith(color: AppColors.black),
                                          textAlign: TextAlign.left,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 40.0.h),
                            Center(
                              child: InkWell(
                                onTap: () => Navigations.pushReplacementNamed(
                                    context, AppRouter.loginScreen),
                                child: Text(
                                  'Please click here to login.',
                                  style: buildAppTextTheme()
                                      .headline5!
                                      .copyWith(color: AppColors.primaryBlue),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                            ),
                          ],
                        )
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Welcome to the Gulf Trust Co.',
                              style: buildAppTextTheme().headline1,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 17.0.h,
                            ),
                            Text(
                              'Please enter your details to register',
                              style: buildAppTextTheme().headline6,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: 53.0.h,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  height: 60.0.h,
                                  width: 368.0.w,
                                  decoration: BoxDecoration(
                                    color: nameFocus.hasFocus
                                        ? AppColors.paleBlue
                                        : AppColors.white,
                                    border: Border.all(color: AppColors.border),

                                    // border: Border(
                                    //     top: BorderSide(color: AppColors.border, width: 1.0),
                                    //     bottom:
                                    //         BorderSide(color: AppColors.border, width: 1.0),
                                    //     right:
                                    //         BorderSide(color: AppColors.border, width: 1.0)),
                                    borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(6.0.s),
                                      topRight: Radius.circular(6.0.s),
                                    ),
                                  ),
                                  child: TextFormField(
                                    // inputFormatters: [
                                    //   FilteringTextInputFormatter.digitsOnly,
                                    //   LengthLimitingTextInputFormatter(10),
                                    // ],
                                    scrollPadding: EdgeInsets.only(bottom: 100),
                                    focusNode: nameFocus,
                                    style: nameFocus.hasFocus
                                        ? buildAppTextTheme().headline5
                                        : buildAppTextTheme()
                                            .subtitle1!
                                            .copyWith(color: AppColors.black),
                                    keyboardType: TextInputType.name,
                                    controller: nameController,
                                    obscureText: false,
                                    decoration: InputDecoration(
                                      isDense: true,
                                      contentPadding: EdgeInsets.only(
                                        left: 10.0.s,
                                        top: 8.0.s,
                                        bottom: 8.0.s,
                                        right: 10.0.s,
                                      ),
                                      labelText: 'Customer name',
                                      labelStyle: nameFocus.hasFocus
                                          ? buildAppTextTheme().subtitle1
                                          : buildAppTextTheme().subtitle1,
                                      border: InputBorder.none,
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: AppColors.primaryBlue),
                                        borderRadius: BorderRadius.vertical(
                                          top: Radius.circular(10.0.s),
                                          bottom: Radius.zero,
                                        ),
                                      ),
                                    ),
                                    autovalidateMode:
                                        AutovalidateMode.onUserInteraction,
                                    onChanged: (value) {
                                      setState(() {
                                        this.nameChanged = true;
                                      });
                                      if (value.isEmpty || value.length > 30) {
                                        setState(() {
                                          this.nameValid = false;
                                        });
                                      } else {
                                        setState(() {
                                          this.nameValid = true;
                                        });
                                      }
                                    },
                                  ),
                                ),
                                (this.nameChanged &&
                                        !this.nameValid &&
                                        !nameFocus.hasFocus)
                                    ? Text(
                                        'Enter valid name',
                                        style: buildAppTextTheme().subtitle1,
                                      )
                                    : SizedBox(height: 0.0.h),
                                SizedBox(height: 30.0.h),
                                Container(
                                  height: 60.0.h,
                                  width: 368.0.w,
                                  decoration: BoxDecoration(
                                    color: emailFocus.hasFocus
                                        ? AppColors.paleBlue
                                        : AppColors.white,
                                    border: Border.all(color: AppColors.border),

                                    // border: Border(
                                    //     top: BorderSide(color: AppColors.border, width: 1.0),
                                    //     bottom:
                                    //         BorderSide(color: AppColors.border, width: 1.0),
                                    //     right:
                                    //         BorderSide(color: AppColors.border, width: 1.0)),
                                    borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(6.0.s),
                                      topRight: Radius.circular(6.0.s),
                                    ),
                                  ),
                                  child: TextFormField(
                                    scrollPadding: EdgeInsets.only(bottom: 100),
                                    focusNode: emailFocus,
                                    style: emailFocus.hasFocus
                                        ? buildAppTextTheme().headline5
                                        : buildAppTextTheme()
                                            .subtitle1!
                                            .copyWith(color: AppColors.black),
                                    keyboardType: TextInputType.emailAddress,
                                    controller: emailController,
                                    obscureText: false,
                                    decoration: InputDecoration(
                                      isDense: true,
                                      contentPadding: EdgeInsets.only(
                                        left: 10.0.s,
                                        top: 8.0.s,
                                        bottom: 8.0.s,
                                        right: 10.0.s,
                                      ),
                                      labelText: 'Email address',
                                      labelStyle: emailFocus.hasFocus
                                          ? buildAppTextTheme().subtitle1
                                          : buildAppTextTheme().subtitle1,
                                      border: InputBorder.none,
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            color: AppColors.primaryBlue),
                                        borderRadius: BorderRadius.vertical(
                                          top: Radius.circular(10.0.s),
                                          bottom: Radius.zero,
                                        ),
                                      ),
                                    ),
                                    autovalidateMode:
                                        AutovalidateMode.onUserInteraction,
                                    onChanged: (value) {
                                      setState(() {
                                        this.emailChanged = true;
                                      });
                                      if (value.isEmpty) {
                                        setState(() {
                                          this.emailValid = false;
                                        });
                                      } else {
                                        setState(() {
                                          this.emailValid = true;
                                        });
                                      }
                                    },
                                  ),
                                ),
                                (this.emailChanged &&
                                        !this.emailValid &&
                                        !emailFocus.hasFocus)
                                    ? Text(
                                        'Enter valid email address',
                                        style: buildAppTextTheme().subtitle1,
                                      )
                                    : SizedBox(height: 0.0.h),
                                SizedBox(height: 30.0.h),
                                Container(
                                  height: 58.0.h,
                                  width: 368.0.w,
                                  decoration: BoxDecoration(
                                    color: AppColors.white,
                                    border: Border.all(color: AppColors.border),
                                    borderRadius: BorderRadius.only(
                                      bottomRight: Radius.circular(6.0.s),
                                      topRight: Radius.circular(6.0.s),
                                      bottomLeft: Radius.circular(6.0.s),
                                      topLeft: Radius.circular(6.0.s),
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: 63.0.w,
                                        color: AppColors.white,
                                        child: Center(
                                          child: Image(
                                            image: AssetImage(
                                              AppImages.FLAG,
                                            ),
                                            height: 30.0.h,
                                            width: 30.0.w,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 63.0.w,
                                        decoration: BoxDecoration(
                                          color: AppColors.white,
                                        ),
                                        child: Center(
                                          child: Text(
                                            '+965- ',
                                            style:
                                                buildAppTextTheme().headline5,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        width: 220.0.w,
                                        decoration: BoxDecoration(
                                          color: mobileFocus.hasFocus
                                              ? AppColors.white
                                              : AppColors.white,
                                        ),
                                        child: Center(
                                          child: TextFormField(
                                            inputFormatters: [
                                              FilteringTextInputFormatter
                                                  .digitsOnly,
                                              LengthLimitingTextInputFormatter(
                                                  8),
                                            ],
                                            scrollPadding:
                                                EdgeInsets.only(bottom: 100),
                                            focusNode: mobileFocus,
                                            style: mobileFocus.hasFocus
                                                ? buildAppTextTheme().headline5
                                                : buildAppTextTheme()
                                                    .subtitle1!
                                                    .copyWith(
                                                        color: AppColors.black),
                                            keyboardType: TextInputType.number,
                                            controller: mobileController,
                                            obscureText: false,
                                            decoration: InputDecoration(
                                              isDense: true,
                                              hintText: 'xxx xxx xx',
                                              // contentPadding: EdgeInsets.only(
                                              //   left: 10.0.s,
                                              //   top: 8.0.s,
                                              //   bottom: 8.0.s,
                                              //   right: 10.0.s,
                                              // ),
                                              //    labelText: '+965 - xxx xxx xx',
                                              labelStyle: mobileFocus.hasFocus
                                                  ? buildAppTextTheme()
                                                      .subtitle2
                                                  : buildAppTextTheme()
                                                      .subtitle2,
                                              border: InputBorder.none,
                                              // focusedBorder: UnderlineInputBorder(
                                              //   borderSide:
                                              //       BorderSide(color: AppColors.primaryBlue),
                                              //   borderRadius: BorderRadius.vertical(
                                              //     top: Radius.circular(10.0.s),
                                              //     bottom: Radius.zero,
                                              //   ),
                                              // ),
                                            ),
                                            autovalidateMode: AutovalidateMode
                                                .onUserInteraction,
                                            onChanged: (value) {
                                              setState(() {
                                                this.mobileChanged = true;
                                              });
                                              if (value.isEmpty ||
                                                  value.length != 8) {
                                                setState(() {
                                                  this.mobileValid = false;
                                                });
                                              } else {
                                                setState(() {
                                                  this.mobileValid = true;
                                                });
                                              }
                                            },
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                (this.mobileChanged &&
                                        !this.mobileValid &&
                                        !mobileFocus.hasFocus)
                                    ? Text(
                                        'Enter Valid Phone number',
                                        style: buildAppTextTheme().subtitle1,
                                      )
                                    : SizedBox(height: 0.0.h),
                              ],
                            ),
                            SizedBox(height: 100.0.h),
                            PrimaryButton(
                                height: 65.0.h,
                                width: 304.0.w,
                                color: AppColors.primaryBlue,
                                disabled: !(this.mobileValid &&
                                    emailValid &&
                                    nameValid),
                                isLoading: isLoading,
                                onPressed: () async {
                                  if (!isLoading) {
                                    setState(() {
                                      isLoading = true;
                                    });
                                    var response = await ProfileService()
                                        .getCustomerDetails(
                                            context: context,
                                            mobileNo: mobileController.text);
                                    if (response != null &&
                                        response.customerId != '') {
                                      // profileProvider.setProfileData = ProfileModel(
                                      //     customerId: response.customerId,
                                      //     emailId: emailController.text,
                                      //     customerName: nameController.text,
                                      //     civilidNo: response.civilidNo,
                                      //     dob: response.dob,
                                      //     mobileNo: mobileController.text,
                                      //     phoneNo: response.phoneNo);
                                      setState(() {
                                        isError = true;
                                      });
                                      // Toast.show('User already exists',
                                      //     backgroundColor: AppColors.red,
                                      //     textStyle: buildAppTextTheme()
                                      //         .subtitle1!
                                      //         .copyWith(
                                      //           color: AppColors.white,
                                      //         ),
                                      //     duration: 3,
                                      //     gravity: 0);
                                      // Navigator.push(
                                      //   context,
                                      //   PageTransition(
                                      //       type: PageTransitionType.fade,
                                      //       child: LoginScreen()),
                                      // );
                                    } else {
                                      profileProvider.setProfileData =
                                          ProfileModel(
                                              emailId: emailController.text,
                                              customerName: nameController.text,
                                              mobileNo: mobileController.text,
                                              otpVerified: false);
                                      var otpResponse = await OTPService()
                                          .requestOTP(
                                              context: context,
                                              mobileNo: mobileController.text);
                                      if (otpResponse != null &&
                                          otpResponse.messageId != '') {
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: OtpScreen(
                                                messageId:
                                                    otpResponse.messageId,
                                                mobileNo: mobileController.text,
                                                from: 'Register',
                                              )),
                                        );
                                      } else {
                                        Toast.show(
                                            'Something went wrong. Please try again',
                                            backgroundColor: AppColors.red,
                                            textStyle: buildAppTextTheme()
                                                .subtitle1!
                                                .copyWith(
                                                  color: AppColors.white,
                                                ),
                                            duration: 3,
                                            gravity: 0);
                                      }
                                    }
                                    // var response1 = await CustomerService()
                                    //     .validateAndRegisterCustomer(
                                    //         context: context,
                                    //         mobileNo: mobileController.text,
                                    //         customerName: nameController.text,
                                    //         emailId: emailController.text);
                                    // if (response1 != null && response1.CustomerId != '') {
                                    //   Toast.show('Registered successfully',
                                    //       backgroundColor: AppColors.green,
                                    //       textStyle:
                                    //           buildAppTextTheme().subtitle1!.copyWith(
                                    //                 color: AppColors.white,
                                    //               ),
                                    //       duration: 3,
                                    //       gravity: 0);
                                    //   Navigator.push(
                                    //     context,
                                    //     PageTransition(
                                    //         type: PageTransitionType.fade,
                                    //         child: OtpScreen()),
                                    //   );
                                    // }
                                    // Navigations.pushNamed(
                                    //   context,
                                    //   AppRouter.otpScreen,
                                    // );
                                    setState(() {
                                      isLoading = false;
                                    });
                                  }
                                },
                                text: 'Get OTP',
                                borderRadius: 80.0.s),
                            SizedBox(height: 60.0.h),
                          ],
                        ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
