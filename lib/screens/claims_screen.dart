import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/widgets/common/claims_widget.dart';
import 'package:provider/provider.dart';
import '../common/images.dart';
import '../providers/customer_provider.dart';
import '../providers/profile_provider.dart';
import '../services/customer_service.dart';

class ClaimsScreen extends StatefulWidget {
  @override
  _ClaimsScreenState createState() => _ClaimsScreenState();
}

class _ClaimsScreenState extends State<ClaimsScreen> {
  TextEditingController searchController = TextEditingController();
  FocusNode searchFocus = FocusNode();
  bool searchValid = false;
  bool searchChanged = false;
  bool isLoading = false;
  CustomerProvider customerProvider = CustomerProvider();
  ProfileProvider profileProvider = ProfileProvider();

  @override
  void initState() {
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    customerProvider.resetClaimDocuments();
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      customerProvider.setClaimsList = await CustomerService()
          .getInsurerClaimDetails(
              context: context,
              customerId: profileProvider.profileData?.customerId);
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.primaryBlue,
            leading: InkWell(
              onTap: () => Navigations.pop(context),
              child: Icon(
                Icons.arrow_back_ios,
                color: AppColors.white,
                size: 20.0.ics,
              ),
            ),
            title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'My Claims',
                    style: buildAppTextTheme()
                        .headline3
                        ?.copyWith(color: AppColors.white),
                    textAlign: TextAlign.start,
                  ),
                ]),
          ),
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  left: 24.0.s, right: 20.0.s, top: 23.0.s, bottom: 40.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // Row(
                  //   children: [
                  //     Container(
                  //       height: 56.0.h,
                  //       width: 52.0.w,
                  //       decoration: BoxDecoration(
                  //         color: searchFocus.hasFocus
                  //             ? AppColors.paleBlue
                  //             : AppColors.paleBrown,
                  //         border: Border(
                  //           left: BorderSide(
                  //             color: AppColors.border,
                  //             width: 1.0,
                  //           ),
                  //           top: BorderSide(
                  //             color: AppColors.border,
                  //             width: 1.0,
                  //           ),
                  //           bottom: BorderSide(
                  //             color: AppColors.border,
                  //             width: 1.0,
                  //           ),
                  //         ),
                  //         // borderRadius: BorderRadius.only(
                  //         //   bottomRight: Radius.circular(6.0.s),
                  //         //   topRight: Radius.circular(6.0.s),
                  //         // ),
                  //       ),
                  //       child: Icon(
                  //         Icons.search_outlined,
                  //         color: AppColors.primaryBlue,
                  //         size: 24.0.ics,
                  //       ),
                  //     ),
                  //     Container(
                  //       height: 56.0.h,
                  //       width: 316.0.w,
                  //       decoration: BoxDecoration(
                  //         color: searchFocus.hasFocus
                  //             ? AppColors.paleBlue
                  //             : AppColors.paleBrown,
                  //         border: Border(
                  //           right: BorderSide(
                  //             color: AppColors.border,
                  //             width: 1.0,
                  //           ),
                  //           top: BorderSide(
                  //             color: AppColors.border,
                  //             width: 1.0,
                  //           ),
                  //           bottom: BorderSide(
                  //             color: AppColors.border,
                  //             width: 1.0,
                  //           ),
                  //         ),
                  //         // borderRadius: BorderRadius.only(
                  //         //   bottomRight: Radius.circular(6.0.s),
                  //         //   topRight: Radius.circular(6.0.s),
                  //         // ),
                  //       ),
                  //       child: TextFormField(
                  //         scrollPadding: EdgeInsets.only(bottom: 100),
                  //         focusNode: searchFocus,
                  //         style: searchFocus.hasFocus
                  //             ? buildAppTextTheme().headline5
                  //             : buildAppTextTheme()
                  //                 .subtitle1!
                  //                 .copyWith(color: AppColors.black),
                  //         keyboardType: TextInputType.name,
                  //         controller: searchController,
                  //         obscureText: false,
                  //         decoration: InputDecoration(
                  //           isDense: true,
                  //           contentPadding: EdgeInsets.only(
                  //             left: 10.0.s,
                  //             top: 8.0.s,
                  //             bottom: 8.0.s,
                  //             right: 10.0.s,
                  //           ),
                  //           labelText: 'Search here',
                  //           labelStyle: searchFocus.hasFocus
                  //               ? buildAppTextTheme().subtitle1
                  //               : buildAppTextTheme().subtitle1,
                  //           border: InputBorder.none,
                  //           focusedBorder: UnderlineInputBorder(
                  //             borderSide:
                  //                 BorderSide(color: AppColors.primaryBlue),
                  //             borderRadius: BorderRadius.vertical(
                  //               top: Radius.circular(10.0.s),
                  //               bottom: Radius.zero,
                  //             ),
                  //           ),
                  //         ),
                  //         onChanged: (value) {},
                  //       ),
                  //     ),
                  //   ],
                  // ),
                  // SizedBox(
                  //   height: 30.0.h,
                  // ),
                  Container(
                    padding: EdgeInsets.only(bottom: 100.0.s),
                    height: MediaQuery.of(context).size.height * 0.8,
                    child: customerProvider.claimsList.isNotEmpty
                        ? ListView.builder(
                            itemCount: customerProvider.claimsList.length,
                            scrollDirection: Axis.vertical,
                            itemBuilder: (context, index) {
                              return Container(
                                padding: EdgeInsets.only(bottom: 20.0.s),
                                child: ClaimsWidget(
                                  claimDetails:
                                      customerProvider?.claimsList[index],
                                  title: 'Motor Insurance',
                                  subTitle:
                                      'Claim Date ${customerProvider!.claimsList[index]!.claimDate ?? ''}',
                                  desc: '',
                                  claimAmount:
                                      '${customerProvider!.claimsList[index]!.claimAmount} KWD',
                                  productType: customerProvider!
                                      .claimsList[index]!.productType,
                                  status: customerProvider!
                                          .claimsList[index]!.claimStatus ??
                                      '',
                                  image: AppImages.MOTOR,
                                  id: customerProvider!
                                          .claimsList[index]!.claimId
                                          .toString() ??
                                      '',
                                ),
                              );
                            })
                        : Center(
                            child: Text(
                              'No claims registered',
                              style: buildAppTextTheme().headline1,
                              textAlign: TextAlign.center,
                            ),
                          ),
                  ),
                  // SizedBox(height: 45.0.h),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
