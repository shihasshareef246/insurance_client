import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gt_test/screens/travel_payment_details_screen.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';

import 'package:gt_test/common/buttons/primary_button.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/routes/routes.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/screens/motor_payment_details_screen.dart';
import '../common/images.dart';
import '../common/shared_prefs.dart';
import '../models/profile_model.dart';
import '../providers/customer_provider.dart';
import '../providers/profile_provider.dart';
import '../services/otp_service.dart';
import '../services/profile_service.dart';
import 'base.dart';
import 'home_screen.dart';

class OtpScreen extends StatefulWidget {
  String messageId;
  String mobileNo;
  String from;
  OtpScreen({
    this.messageId = '',
    this.mobileNo = '',
    this.from = 'Login',
  });
  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  TextEditingController otpController = TextEditingController();
  FocusNode otpFocus = FocusNode();
  late CustomerProvider customerProvider;
  late ProfileProvider profileProvider;
  bool otpValid = false;
  bool otpChanged = false;
  bool isLoading = false;
  bool isResendEnabled = false;
  late Timer timer;
  int remainingTime = 60;
  String currentText = "";

  @override
  void initState() {
    super.initState();
    startTimer();
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
  }

  @override
  void dispose() async {
    super.dispose();
    if (timer != null) {
      timer.cancel();
    }
    otpController.clear();
  }

  void startTimer() {
    remainingTime = 60;
    timer = Timer.periodic(
      Duration(seconds: 1),
      (Timer timer) {
        if (remainingTime == 0) {
          setState(
            () {
              timer.cancel();
              isResendEnabled = true;
            },
          );
        } else {
          setState(
            () {
              remainingTime--;
            },
          );
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: Container(
              padding:
                  EdgeInsets.only(left: 24.0.s, right: 20.0.s, bottom: 20.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 40.0.h,
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: InkWell(
                      onTap: () => Navigations.pop(context),
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: AppColors.primaryBlue,
                        size: 20.0.ics,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  Image(
                    image: AssetImage(AppImages.PASSWORD),
                    height: 278.0.h,
                    width: 296.0.w,
                  ),
                  SizedBox(
                    height: 40.0.h,
                  ),
                  Text(
                    'OTP',
                    style: buildAppTextTheme().headline3,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 15.0.h,
                  ),
                  Text(
                    'We’ve sent you an OTP on your given mobile number. Please enter it below to verify.',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 71.0.h,
                  ),
                  Container(
                    height: 58.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: otpFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border),

                      // border: Border(
                      //     top: BorderSide(color: AppColors.border, width: 1.0),
                      //     bottom:
                      //         BorderSide(color: AppColors.border, width: 1.0),
                      //     right:
                      //         BorderSide(color: AppColors.border, width: 1.0)),
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                        LengthLimitingTextInputFormatter(6),
                      ],
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: otpFocus,
                      style: otpFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.number,
                      controller: otpController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        labelText: 'Enter OTP',
                        labelStyle: otpFocus.hasFocus
                            ? buildAppTextTheme().subtitle1
                            : buildAppTextTheme().subtitle1,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.otpChanged = true;
                        });
                        if (value.isEmpty || value.length != 6) {
                          setState(() {
                            this.otpValid = false;
                          });
                        } else {
                          setState(() {
                            this.otpValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  (this.otpChanged && !this.otpValid && !otpFocus.hasFocus)
                      ? Align(
                          alignment: Alignment.centerLeft,
                          child: Container(
                            padding: EdgeInsets.only(left: 5.0.s),
                            child: Text(
                              'OTP incorrect',
                              style: buildAppTextTheme()
                                  .subtitle1!
                                  .copyWith(color: AppColors.red),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        )
                      : SizedBox(height: 0.0.h),
                  SizedBox(height: 40.0.h),
                  PrimaryButton(
                      height: 65.0.h,
                      width: 304.0.w,
                      color: AppColors.primaryBlue,
                      disabled: !(this.otpValid),
                      isLoading: isLoading,
                      onPressed: () async {
                        if (!isLoading) {
                          setState(() {
                            isLoading = true;
                          });

                          var response = await OTPService().validateOTP(
                              context: context,
                              otp: otpController.text,
                              otpRefId: widget.messageId);
                          if (response != null && response.status == 'Valid') {
                            ProfileModel? postResponse;
                            if (widget.from == 'Register') {
                              postResponse =
                                  await ProfileService().postCustomerDetails(
                                context: context,
                                emailId:
                                    profileProvider?.profileData?.emailId ?? '',
                                customerName: profileProvider
                                        ?.profileData?.customerName ??
                                    '',
                                mobileNo:
                                    profileProvider?.profileData?.mobileNo ??
                                        '',
                              );
                              profileProvider.profileData?.customerId =
                                  postResponse?.customerId ?? '';
                              profileProvider.profileData?.otpVerified = true;
                              Toast.show('Registered successfully',
                                  backgroundColor: AppColors.green,
                                  textStyle:
                                      buildAppTextTheme().subtitle1!.copyWith(
                                            color: AppColors.white,
                                          ),
                                  duration: 3,
                                  gravity: 0);
                            } else {
                              profileProvider.profileData?.otpVerified = true;
                              // Toast.show('Logged in successfully',
                              //     backgroundColor: AppColors.green,
                              //     textStyle:
                              //         buildAppTextTheme().subtitle1!.copyWith(
                              //               color: AppColors.white,
                              //             ),
                              //     duration: 3,
                              //     gravity: 0);
                            }
                            await SharedPrefs().addMapPrefs('userdetails',
                                profileProvider.profileData?.toJson() ?? Map());

                            if (customerProvider
                                    ?.selectedMotorInsuranceList?.isNotEmpty ??
                                false) {
                              Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.fade,
                                    child: MotorPaymentDetailsScreen()),
                              );
                            } else if (customerProvider
                                    ?.selectedTravelInsuranceList?.isNotEmpty ??
                                false) {
                              Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.fade,
                                    child: TravelPaymentDetailsScreen()),
                              );
                            } else {
                              Navigator.push(
                                context,
                                PageTransition(
                                  type: PageTransitionType.fade,
                                  child: BaseLayout(
                                    page: HomeScreen(),
                                    activePageIndex: 0,
                                  ),
                                ),
                              );
                            }
                          } else {
                            Toast.show('Incorrect OTP',
                                backgroundColor: AppColors.red,
                                textStyle:
                                    buildAppTextTheme().subtitle1!.copyWith(
                                          color: AppColors.white,
                                        ),
                                duration: 3,
                                gravity: 0);
                          }
                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      text: 'Verify',
                      borderRadius: 80.0.s),
                  SizedBox(height: 20.0.h),
                  isResendEnabled
                      ? InkWell(
                          onTap: () async {
                            var otpResponse = await OTPService().requestOTP(
                                context: context, mobileNo: widget.mobileNo);
                            if (otpResponse != null &&
                                otpResponse.messageId != '') {
                              Navigator.push(
                                context,
                                PageTransition(
                                  type: PageTransitionType.fade,
                                  child: OtpScreen(
                                    messageId: otpResponse.messageId,
                                    mobileNo: widget.mobileNo,
                                    from: widget.from,
                                  ),
                                ),
                              );
                            } else {
                              Toast.show(
                                  'Something went wrong. Please try again',
                                  backgroundColor: AppColors.red,
                                  textStyle:
                                      buildAppTextTheme().subtitle1!.copyWith(
                                            color: AppColors.white,
                                          ),
                                  duration: 3,
                                  gravity: 0);
                            }
                          },
                          child: Text('Resend code',
                              style: buildAppTextTheme().headline5),
                        )
                      : Text('Resend code in ${remainingTime.toString()}s',
                          style: buildAppTextTheme().headline5),
                  SizedBox(
                    height: 40.0.h,
                  ),
                  if (isResendEnabled) ...{
                    Text(
                      'Not ${widget.mobileNo}?',
                      style: buildAppTextTheme()
                          .headline5!
                          .copyWith(color: AppColors.lightGrey),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10.0.h,
                    ),
                    InkWell(
                      onTap: () {
                        Navigations.pushReplacementNamed(
                            context, AppRouter.loginScreen);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.arrow_back,
                            color: AppColors.primaryBlue,
                            size: 24.0.ics,
                          ),
                          SizedBox(
                            width: 10.0.w,
                          ),
                          Text(
                            'Edit Mobile Number',
                            style: buildAppTextTheme().headline5!.copyWith(
                                  decoration: TextDecoration.underline,
                                ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  },
                  SizedBox(
                    height: 20.0.h,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
