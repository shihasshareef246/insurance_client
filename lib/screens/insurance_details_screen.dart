import 'package:flutter/material.dart';

import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/containers/insurance_details_container.dart';

class InsuranceDetailsScreen extends StatefulWidget {
  @override
  _InsuranceDetailsScreenState createState() => _InsuranceDetailsScreenState();
}

class _InsuranceDetailsScreenState extends State<InsuranceDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)?.settings.arguments as Map;
    int transactionId =
        args['transactionId'] != null ? args['transactionId'] : 0;
    String status = args['status'] != null ? args['status'] : '';

    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.primaryBlue,
          leading: InkWell(
            onTap: () => Navigations.pop(context),
            child: Icon(
              Icons.arrow_back_ios,
              color: AppColors.white,
              size: 20.0.ics,
            ),
          ),
          title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'My Insurance',
                  style: buildAppTextTheme()
                      .headline3
                      ?.copyWith(color: AppColors.white),
                  textAlign: TextAlign.start,
                ),
              ]),
        ),
        backgroundColor: AppColors.pageColor,
        resizeToAvoidBottomInset: true,
        body: InsuranceDetailsContainer(
            transactionId: transactionId, status: status),
      ),
    );
  }
}
