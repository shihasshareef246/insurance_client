import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:gt_test/models/profile_model.dart';
import 'package:provider/provider.dart';

import 'package:gt_test/common/images.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/pref_status.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/routes/routes.dart';
import 'package:gt_test/common/shared_prefs.dart';
import '../providers/documents_provider.dart';
import '../providers/profile_provider.dart';

class InitScreen extends StatefulWidget {

  static bool fromInsurerDetails = false;
  @override
  _InitScreenState createState() => _InitScreenState();
}

class _InitScreenState extends State<InitScreen> {
  late DocumentsProvider documentsProvider;
  late ProfileProvider profileProvider;
  @override
  void initState() {
    documentsProvider = Provider.of<DocumentsProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    super.initState();

    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      PrefsStatus userdetails = await SharedPrefs().getMapPrefs('userdetails');
      PrefsStatus introShown = await SharedPrefs().getBoolPrefs('introShown');
      // PrefsStatus documents = await SharedPrefs().getStringPrefs('documents');
      // if (documents.status && documents.value != '[]')
      //   documentsProvider.setDocuments = documents.value;
      if (userdetails.status && userdetails.value['otpVerified']) {
        profileProvider.setProfileData = ProfileModel(
          customerId: userdetails.value['customerId'] ?? '',
          emailId: userdetails.value['emailId'] ?? '',
          customerName: userdetails.value['customerName'] ?? '',
          civilidNo: userdetails.value['civilidNo'] ?? '',
          dob: userdetails.value['dob'] ?? '',
          mobileNo: userdetails.value['mobileNo'] ?? '',
          phoneNo: userdetails.value['phoneNo'] ?? '',
          otpVerified: userdetails.value['otpVerified'] ?? false,
          photoUrl: userdetails.value['photoUrl'] ?? '',
        );
        Future.delayed(
          const Duration(seconds: 1),
          () => Navigations.pushReplacementNamed(context, AppRouter.homeScreen),
        );
      } else if (introShown.status) {
        Future.delayed(
          const Duration(seconds: 1),
          () => Navigations.pushReplacementNamed(
              context, AppRouter.preloginScreen),
        );
      } else {
        Future.delayed(
          const Duration(seconds: 1),
          () => Navigations.pushReplacementNamed(
              context, AppRouter.preliminaryScreen),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: EdgeInsets.only(left: 81.0.s, right: 51.0.s),
          child: Image(
            image: AssetImage(AppImages.LOGO),
            // fit: BoxFit.fill,
            height: 99.0.h,
            width: 296.0.w,
          ),
        ),
      ),
    );
  }
}
