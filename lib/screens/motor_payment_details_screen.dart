import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:flutter_upayments/flutter_upayments.dart';
import 'package:toast/toast.dart';
import 'package:uuid/uuid.dart';

import 'package:gt_test/common/buttons/raw_button_with_icon.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/routes/routes.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/services/payment_service.dart';
import 'package:gt_test/widgets/insurer_details_widget.dart';
import 'package:gt_test/widgets/payment_plan_widget.dart';
import 'package:gt_test/common/popup/cod_popup.dart';
import 'package:gt_test/models/motor_insurance_group_model.dart';
import 'package:gt_test/models/payment_body_model.dart';
import 'package:gt_test/common/popup/confirm_popup.dart';
import '../common/popup/modal_popup.dart';
import '../common/popup/payment_success_popup.dart';
import '../models/payment_model.dart';
import '../models/payment_status_model.dart';
import '../models/status_model.dart';
import '../models/transaction_model.dart';
import '../providers/customer_provider.dart';
import '../providers/dashboard_provider.dart';
import '../providers/documents_provider.dart';
import '../providers/payments_provider.dart';
import '../providers/profile_provider.dart';
import '../services/customer_service.dart';
import 'init_screen.dart';

class MotorPaymentDetailsScreen extends StatefulWidget {
  @override
  _MotorPaymentDetailsScreenState createState() => _MotorPaymentDetailsScreenState();
}

class _MotorPaymentDetailsScreenState extends State<MotorPaymentDetailsScreen> {
  DashboardProvider dashboardProvider = DashboardProvider();
  CustomerProvider customerProvider = CustomerProvider();
  PaymentsProvider paymentsProvider = PaymentsProvider();
  ProfileProvider profileProvider = ProfileProvider();
  DocumentsProvider documentsProvider = DocumentsProvider();
  TextEditingController promoCodeController = TextEditingController();
  FocusNode promoCodeFocus = FocusNode();
  bool promoCodeValid = false;
  bool promoCodeChanged = false;
  bool isLoading = false;
  bool isAccepted = false;
  String paymentMode = 'Online payment';
  bool isPromoSuccess = false;
  bool isPromoFailure = false;
  var uuid = Uuid();
  final dateFormat = DateFormat("dd-MM-yyyy");
  final timeFormat = DateFormat("dd-MM-yyyy HH:mm:ss");
  int promoDiscount = 0;

  double getAmount() {
    double additionalAmount = 0;
    customerProvider?.selectedAdditionalMotorInsuranceList.forEach((element) {
      additionalAmount += element?.premiumAmount ?? 0;
    });
    return (isPromoSuccess && !isPromoFailure)
        ? (additionalAmount +
            double.parse(
                customerProvider?.selectedMotorInsuranceList[0]?.otherFee ??
                    '0.0') +
            double.parse(
                customerProvider?.selectedMotorInsuranceList[0]?.premiumAmount ??
                    '0.0') -
            (additionalAmount +
                    double.parse(customerProvider
                            ?.selectedMotorInsuranceList[0]?.premiumAmount ??
                        '0.0')) *
                double.parse(promoDiscount.toString()) /
                100)
        : (additionalAmount +
            double.parse(
                customerProvider?.selectedMotorInsuranceList[0]?.otherFee ??
                    '0.0') +
            double.parse(
                customerProvider?.selectedMotorInsuranceList[0]?.premiumAmount ??
                    '0.0'));
  }

  @override
  void initState() {
    super.initState();
    InitScreen.fromInsurerDetails = false;
    dashboardProvider = Provider.of<DashboardProvider>(context, listen: false);
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    paymentsProvider = Provider.of<PaymentsProvider>(context, listen: false);
    documentsProvider = Provider.of<DocumentsProvider>(context, listen: false);
    ToastContext().init(context);
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      // documentsProvider.docsList.clear();
      // documentsProvider.documentsList.forEach((element) async {
      //   String? url = await Utils.uploadDocument(element?.file, element?.type,
      //       element?.extension, profileProvider.profileData?.customerId ?? '');
      //   documentsProvider.docsList.add(InsurerDocumentDetailsModel(
      //       documentName: element?.type, documentUrl: url));
      //   if (element?.type == 'Driving_License') {
      //     var res = await CustomerService()
      //         .dlOcr(context: context, url: url);
      //     if (res != null) {}
      //   }
      // });
      // await SharedPrefs().addStringPrefs(
      //     'documents', documentsProvider.documentsList.toString());
    });
  }

  OnSuccess(isSuccess, data, message) async {
    print(isSuccess);
    print(data);
    print(message);
    List<InsurerMotorAdditionalDetailsModel?> additionalPlans = [];
    customerProvider.selectedAdditionalMotorInsuranceList.forEach((element) {
      additionalPlans.add(InsurerMotorAdditionalDetailsModel(
          additionalCoverID: element?.additionalCoverID?.toString()));
    });
    TransactionModel? response =
        await CustomerService().postInsurerInsuranceDetails(
      context: context,
    );
    if (response != null && response.transactionId != 0) {
      // PaymentStatusModel? response =
      //     await PaymentService().paymentStatus(context: context);
      StatusModel? paymentResponse = await PaymentService().postPaymentToDb(
          context: context,
          status: 'Payment Success',
          paymentdate: dateFormat.format(DateTime.now()).toString(),
          transactiondate: timeFormat.format(DateTime.now()).toString(),
          PaymentMode: paymentMode == 'Online payment' ? 'Knet' : 'Cash',
          paymentreferenceno: data['Ref'][0],
          premiumAmount: getAmount().toDouble().toString(),
          transactionid: response?.transactionId.toString());
    }
    ModalPopUp(
        crossIconRequired: false,
        crossIconSize: 16.0.s,
        iconColor: AppColors.greyishBlack,
        backgroundColor: AppColors.white,
        childcontainer: Center(
          child: PaymentSuccessPopUp(
            onClick: () => {},
            message: '',
            amount: getAmount().toStringAsFixed(3),
          ),
        ),
        barrierDismissable: false,
        barrierColor: Colors.transparent,
        closeClick: () => {}).showPopup(context);
    // Navigations.pushNamedAndRemoveUntil(context, AppRouter.homeScreen);
  }

  OnFailure(isSuccess, data, message) async {
    print(isSuccess);
    print(data);
    print(message);
    List<InsurerMotorAdditionalDetailsModel?> additionalPlans = [];
    customerProvider.selectedAdditionalMotorInsuranceList.forEach((element) {
      additionalPlans.add(InsurerMotorAdditionalDetailsModel(
          additionalCoverID: element?.additionalCoverID?.toString()));
    });
    PaymentStatusModel? paymentResponse = await PaymentService()
        .paymentStatus(context: context, txnRefNo: data['Ref'][0]);
    TransactionModel? response =
        await CustomerService().postInsurerInsuranceDetails(
      context: context,
    );
    if (response != null && response.transactionId != 0) {
      StatusModel? failureResponse = await PaymentService().postPaymentToDb(
          context: context,
          status: 'Payment Failure',
          paymentdate: dateFormat.format(DateTime.now()).toString(),
          PaymentMode: paymentMode == 'Online payment' ? 'Knet' : 'Cash',
          paymentreferenceno: data['Ref'][0],
          premiumAmount: getAmount().toDouble().toString(),
          transactiondate: timeFormat.format(DateTime.now()).toString(),
          transactionid: response?.transactionId.toString());
    }
    Navigations.pushNamedAndRemoveUntil(
        context, AppRouter.motorPaymentDetailsScreen);
  }

  showGateway(BuildContext context, PaymentModel paymentData) {
    paymentDetails userData = paymentDetails(
      merchantId: '1201',
      username: 'test',
      password: 'test',
      apiKey: 'jtest123',
      orderId: uuid.v1(),
      totalPrice: getAmount().toDouble().toString(),
      currencyCode: 'NA',
      successUrl: 'https://example.com/success.html',
      errorUrl: 'https://example.com/error.html',
      testMode: '1',
      customerFName: profileProvider.profileData?.customerName,
      customerEmail: profileProvider.profileData?.emailId,
      customerMobile: profileProvider.profileData?.mobileNo,
      paymentGateway: paymentData.PaymentGateway,
      whitelabled: 'true',
      productTitle:
          customerProvider.selectedMotorInsuranceList[0]?.productDescription,
      productName:
          customerProvider.selectedMotorInsuranceList[0]?.productSummary,
      productPrice:
          customerProvider.selectedMotorInsuranceList[0]?.premiumAmount,
      productQty: '1',
      reference: uuid.v1(),
      notifyURL: 'https://example.com/success.html',
    );
    paymentsProvider.setPaymentBody = PaymentBodyModel();
    RequestPayment(context, userData, OnSuccess, OnFailure);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () =>
          Navigations.pushNamedAndRemoveUntil(context, AppRouter.loginScreen),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.primaryBlue,
            leading: SizedBox.shrink(),
            // leading: InkWell(
            //   onTap: () => Navigations.pushNamedAndRemoveUntil(
            //       context, AppRouter.loginScreen),
            //   child: Icon(
            //     Icons.arrow_back_ios,
            //     color: AppColors.white,
            //     size: 20.0.ics,
            //   ),
            // ),
            title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Payment Details',
                    style: buildAppTextTheme()
                        .headline3
                        ?.copyWith(color: AppColors.white),
                    textAlign: TextAlign.start,
                  ),
                ]),
          ),
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  left: 30.0.s, right: 30.0.s, top: 23.0.s, bottom: 60.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InsurerDetailsWidget(
                      data:
                          customerProvider.insuranceDetails,
                      ),
                  SizedBox(
                    height: 11.0.h,
                  ),
                  Container(
                    width: 200.0.w,
                    child: Text(
                      'Plan details',
                      style: buildAppTextTheme().headline6,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  SizedBox(
                    height: 9.0.h,
                  ),
                  PaymentPlanWidget(
                      name: customerProvider
                              .selectedMotorInsuranceList[0]?.productSummary
                              .trim() ??
                          '',
                      productType: customerProvider
                              .selectedMotorInsuranceList[0]?.productType ??
                          '',
                      promoDiscount: customerProvider
                              .selectedMotorInsuranceList[0]?.promoDiscount
                              .trim() ??
                          '',
                      year:
                          '${customerProvider.selectedMotorInsuranceList[0]?.validity ?? ''} ${customerProvider.selectedMotorInsuranceList[0]?.validityPeriod ?? ''}',
                      amount:
                          '${num.parse(customerProvider.selectedMotorInsuranceList[0]?.premiumAmount ?? '0').toStringAsFixed(3)}'),
                  SizedBox(
                    height: 15.0.h,
                  ),
                  if (customerProvider
                      .selectedAdditionalMotorInsuranceList.isNotEmpty) ...[
                    Container(
                      width: 200.0.w,
                      child: Text(
                        'Additional plan details',
                        style: buildAppTextTheme().headline6,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(
                      height: 9.0.h,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          bottom: 10.0.s, left: 18.0.s, right: 18.0.s),
                      decoration: BoxDecoration(
                        color: AppColors.white,
                        border: Border.all(color: AppColors.border, width: 1.0),
                        borderRadius: BorderRadius.circular(
                          6.0.s,
                        ),
                      ),
                      // height: customerProvider
                      //         .selectedAdditionalInsuranceList.length *
                      //     36,
                      child: ListView.builder(
                          itemCount: customerProvider
                              .selectedAdditionalMotorInsuranceList.length,
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, index) {
                            return Container(
                              padding: EdgeInsets.only(top: 10.0.s),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${index + 1})',
                                    style: buildAppTextTheme().headline6,
                                    textAlign: TextAlign.start,
                                  ),
                                  SizedBox(
                                    width: 8.0.w,
                                  ),
                                  Container(
                                    width: 164.0.w,
                                    child: Text(
                                      '${customerProvider.selectedAdditionalMotorInsuranceList[index]?.coverDetails}',
                                      style: buildAppTextTheme().headline6,
                                      textAlign: TextAlign.start,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8.0.w,
                                  ),
                                  Container(
                                    width: 96.0.w,
                                    child: Text(
                                      '${customerProvider.selectedAdditionalMotorInsuranceList[index]?.premiumAmount?.toStringAsFixed(3) ?? 0}KWD',
                                      style: buildAppTextTheme().headline6,
                                      textAlign: TextAlign.start,
                                    ),
                                  ),
                                ],
                              ),
                            );
                          }),
                    ),
                    // Container(
                    //   height: customerProvider
                    //           .selectedAdditionalInsuranceList.length *
                    //       130,
                    //   child: ListView.builder(
                    //       itemCount: customerProvider
                    //           .selectedAdditionalInsuranceList.length,
                    //       scrollDirection: Axis.vertical,
                    //       physics: AlwaysScrollableScrollPhysics(),
                    //       itemBuilder: (context, index) {
                    //         return Container(
                    //           padding: EdgeInsets.only(bottom: 20.0.s),
                    //           child: PaymentPlanWidget(
                    //               name: customerProvider
                    //                       .selectedAdditionalInsuranceList[
                    //                           index]
                    //                       ?.coverDetails
                    //                       .trim() ??
                    //                   '',
                    //               productType: customerProvider
                    //                       .selectedMotorInsuranceList[0]
                    //                       ?.productType ??
                    //                   '',
                    //               year:
                    //                   '${customerProvider.selectedAdditionalInsuranceList[0]?.productType ?? ''} ${customerProvider.selectedMotorInsuranceList[0]?.validityPeriod ?? ''}',
                    //               amount: customerProvider
                    //                       .selectedAdditionalInsuranceList[
                    //                           index]
                    //                       ?.premiumAmount
                    //                       .toString() ??
                    //                   ''),
                    //         );
                    //       }),
                    // ),
                    SizedBox(
                      height: 15.0.h,
                    ),
                  ],
                  Text(
                    'Promo Code',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 15.0.h,
                  ),
                  Container(
                    height: 60.0.h,
                    width: 368.0.w,
                    decoration: BoxDecoration(
                      color: promoCodeFocus.hasFocus
                          ? AppColors.paleBlue
                          : AppColors.white,
                      border: Border.all(color: AppColors.border, width: 1.0),
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(6.0.s),
                        topLeft: Radius.circular(6.0.s),
                        bottomRight: Radius.circular(6.0.s),
                        topRight: Radius.circular(6.0.s),
                      ),
                    ),
                    child: TextFormField(
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: promoCodeFocus,
                      style: promoCodeFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: promoCodeController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        labelText: 'Enter Promo Code to get Discount',
                        labelStyle: promoCodeFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().subtitle2,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.promoCodeChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.promoCodeValid = false;
                          });
                        } else {
                          setState(() {
                            this.promoCodeValid = true;
                          });
                        }
                      },
                      onFieldSubmitted: (value) async {
                        bool exists = false;
                        dashboardProvider.discountPlanList.forEach((element) {
                          if (element?.promoCode == value) {
                            setState(() {
                              promoDiscount =
                                  int.parse(element?.promoDiscount ?? '0');
                              isPromoSuccess = true;
                              isPromoFailure = false;
                              exists = true;
                              getAmount();
                            });
                          }
                        });
                        if (!exists) {
                          setState(() {
                            promoDiscount = 0;
                            isPromoFailure = true;
                            isPromoSuccess = false;
                            getAmount();
                          });
                        }
                      },
                    ),
                  ),
                  isPromoSuccess
                      ? Text(
                          'PromoCode Applied',
                          style: buildAppTextTheme()
                              .subtitle2!
                              .copyWith(color: AppColors.primaryGreen),
                        )
                      : isPromoFailure
                          ? Text(
                              'PromoCode Invalid',
                              style: buildAppTextTheme()
                                  .subtitle2!
                                  .copyWith(color: AppColors.primaryRed),
                            )
                          : SizedBox(height: 20.0.h),
                  SizedBox(
                    height: 18.0.h,
                  ),
                  Text(
                    'Payment type',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  Container(
                    padding: EdgeInsets.all(22.0.s),
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      border: Border.all(color: AppColors.border, width: 1.0),
                      borderRadius: BorderRadius.circular(
                        6.0.s,
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'Please select what type of payment do you like!',
                          style: buildAppTextTheme().subtitle2,
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(
                          height: 20.0.h,
                        ),
                        Container(
                          height: 30.0.h,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                height: 30.0.h,
                                width: 30.0.w,
                                child: Radio(
                                  value: 'Online payment',
                                  groupValue: this.paymentMode,
                                  onChanged: (val) {
                                    setState(() {
                                      this.paymentMode = val.toString();
                                    });
                                  },
                                ),
                              ),
                              SizedBox(
                                width: 3.0.w,
                              ),
                              Text(
                                'Online payment',
                                style: buildAppTextTheme().headline6,
                                textAlign: TextAlign.start,
                              ),
                              SizedBox(
                                width: 7.0.w,
                              ),
                              Container(
                                height: 30.0.h,
                                width: 30.0.w,
                                child: Radio(
                                  value: 'Cash on delivery',
                                  groupValue: this.paymentMode,
                                  onChanged: (val) {
                                    setState(() {
                                      this.paymentMode = val.toString();
                                    });
                                  },
                                ),
                              ),
                              SizedBox(
                                width: 3.0.w,
                              ),
                              Text(
                                'Cash on delivery',
                                style: buildAppTextTheme().headline5,
                                textAlign: TextAlign.start,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 18.0.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Discount',
                        style: buildAppTextTheme().headline6,
                        textAlign: TextAlign.start,
                      ),
                      Text(
                        (isPromoSuccess && !isPromoFailure)
                            ? '${promoDiscount?.toString() ?? 0}%'
                            : '0%',
                        //promoDiscount.toString(),
                        style: buildAppTextTheme().headline6,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Service Fee',
                        style: buildAppTextTheme().headline6,
                        textAlign: TextAlign.start,
                      ),
                      Text(
                        '${double.parse(customerProvider?.selectedMotorInsuranceList[0]?.otherFee ?? '0')?.toStringAsFixed(3) ?? 0} KWD',
                        style: buildAppTextTheme().headline6,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  SizedBox(height: 18.0.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Checkbox(
                        splashRadius: 10.0.s,
                        activeColor: AppColors.white,
                        value: isAccepted,
                        onChanged: (value) {
                          setState(() {
                            isAccepted = !isAccepted;
                          });
                        },
                        checkColor: AppColors.primaryBlue,
                      ),
                      Text(
                        'I Accept Terms & Conditions',
                        style: buildAppTextTheme().headline6,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  SizedBox(height: 18.0.h),
                  Container(
                    height: 90.0.h,
                    width: 368.0.w,
                    padding: EdgeInsets.only(
                      left: 30.0.s,
                      right: 15.0.s,
                    ),
                    decoration: BoxDecoration(
                      color: AppColors.primaryBlue,
                      borderRadius: BorderRadius.circular(130.0.s),
                      //border: Border.all(color: AppColors.white),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'KWD ${getAmount().toStringAsFixed(3)}',
                          style: buildAppTextTheme()
                              .headline6!
                              .copyWith(color: AppColors.white),
                          textAlign: TextAlign.start,
                        ),
                        RawButtonWithIcon(
                          width: 121.0.w,
                          height: 48.0.h,
                          text: 'Pay Now',
                          disabled: !isAccepted,
                          borderRadius: 60.0.s,
                          isLoading: isLoading,
                          onPressed: () async {
                            if (!isLoading) {
                              setState(() {
                                isLoading = true;
                              });

                              if (paymentMode == 'Online payment') {
                                // StatusModel? response = await CustomerService().postInsurerInsuranceDetails(
                                //   context: context,
                                // );
                                //  if (response != null && response.status != '') {}
                                PaymentModel? response =
                                    await PaymentService().postPaymentToGateway(
                                  context: context,
                                  amount: getAmount().toDouble().toString(),
                                  name:
                                      profileProvider.profileData?.customerName,
                                  mobile: profileProvider.profileData?.mobileNo,
                                );
                                if (response != null &&
                                    response.PaymentGateway != '') {
                                  showGateway(context, response);
                                }
                                print(response);
                                // if (!isLoading) {
                                //   setState(() {
                                //     isLoading = true;
                                //   });
                                //   PaymentModel? response = await PaymentService()
                                //       .postPaymentToGateway(context: context);
                                //   if (response != null &&
                                //       response.PaymentGateway != '') {
                                //     showGateway(context, response);
                                //     // UrlLauncher.redirectToBrowser(
                                //     //     context, response.PayUrl);
                                //     setState(() {
                                //       isLoading = false;
                                //     });
                                //   } else {
                                //     Toast.show('Something went wrong',
                                //         backgroundColor: AppColors.red,
                                //         textStyle:
                                //             buildAppTextTheme().subtitle1!.copyWith(
                                //                   color: AppColors.white,
                                //                 ),
                                //         duration: 3,
                                //         gravity: 0);
                                //     setState(() {
                                //       isLoading = false;
                                //     });
                                //   }
                                // }
                              } else {
                                TransactionModel? response =
                                    await CustomerService()
                                        .postInsurerInsuranceDetails(
                                  context: context,
                                );
                                if (response != null &&
                                    response.transactionId != 0) {
                                  StatusModel? paymentResponse =
                                      await PaymentService().postPaymentToDb(
                                          context: context,
                                          status: 'Payment Success',
                                          paymentdate: dateFormat
                                              .format(DateTime.now())
                                              .toString(),
                                          transactiondate: timeFormat
                                              .format(DateTime.now())
                                              .toString(),
                                          PaymentMode: 'Cash',
                                          premiumAmount:
                                              getAmount().toDouble().toString(),
                                          transactionid: response?.transactionId
                                              .toString());
                                  if (paymentResponse != null &&
                                      paymentResponse.status != '') {
                                    ModalPopUp(
                                            crossIconRequired: false,
                                            crossIconSize: 16.0.s,
                                            iconColor: AppColors.greyishBlack,
                                            backgroundColor: AppColors.white,
                                            childcontainer: Center(
                                              child: CodPopUp(
                                                onClick: () => {},
                                                message: '',
                                                amount: getAmount()
                                                    .toStringAsFixed(3),
                                              ),
                                            ),
                                            barrierDismissable: false,
                                            barrierColor: Colors.transparent,
                                            closeClick: () => {})
                                        .showPopup(context);
                                  } else {
                                    Toast.show(
                                        'Something went wrong. Please try again',
                                        backgroundColor: AppColors.red,
                                        textStyle: buildAppTextTheme()
                                            .subtitle1!
                                            .copyWith(
                                              color: AppColors.white,
                                            ),
                                        duration: 3,
                                        gravity: 0);
                                  }
                                } else {
                                  Toast.show(
                                      'Something went wrong. Please try again',
                                      backgroundColor: AppColors.red,
                                      textStyle: buildAppTextTheme()
                                          .subtitle1!
                                          .copyWith(
                                            color: AppColors.white,
                                          ),
                                      duration: 3,
                                      gravity: 0);
                                }
                              }
                              setState(() {
                                isLoading = false;
                              });
                            }
                          },
                          bgColor: AppColors.white,
                          textColor: AppColors.primaryBlue,
                          icon: Icons.arrow_forward_outlined,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10.0.h),
                  Center(
                    child: InkWell(
                      onTap: () {
                        ModalPopUp(
                            crossIconRequired: false,
                            crossIconSize: 16.0.s,
                            iconColor: AppColors.greyishBlack,
                            backgroundColor: AppColors.white,
                            childcontainer: Center(
                              child: ConfirmPopUp(
                                onClick: () => Navigations.pushReplacementNamed(
                                    context, AppRouter.homeScreen),
                                message:
                                    'All your details will be lost, do you wish to proceed?',
                              ),
                            ),
                            barrierDismissable: false,
                            barrierColor: Colors.transparent,
                            closeClick: () => {}).showPopup(context);
                      },
                      child: Text(
                        'Do not wish to proceed now, May be later!',
                        style: buildAppTextTheme().headline5!.copyWith(
                            color: AppColors.primaryBlue,
                            decoration: TextDecoration.underline,
                            fontStyle: FontStyle.italic),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                  // RichText(
                  //   textAlign: TextAlign.center,
                  //   text: TextSpan(
                  //     children: [
                  //       TextSpan(
                  //         text: 'Do not wish to proceed now, May be later!',
                  //         style: buildAppTextTheme()
                  //             .headline5!
                  //             .copyWith(color: AppColors.primaryBlue),
                  //         recognizer: TapGestureRecognizer()
                  //           ..onTap = () => ModalPopUp(
                  //               crossIconRequired: false,
                  //               crossIconSize: 16.0.s,
                  //               iconColor: AppColors.greyishBlack,
                  //               backgroundColor: AppColors.white,
                  //               childcontainer: Center(
                  //                 child: ConfirmPopUp(
                  //                   onClick: () =>
                  //                       Navigations.pushReplacementNamed(
                  //                           context, AppRouter.homeScreen),
                  //                   message:
                  //                   'All your details will be lost, do you wish to proceed?',
                  //                 ),
                  //               ),
                  //               barrierDismissable: false,
                  //               barrierColor: Colors.transparent,
                  //               closeClick: () => {}).showPopup(context),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  SizedBox(
                    height: 30.0.h,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
