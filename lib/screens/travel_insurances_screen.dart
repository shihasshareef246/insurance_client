import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:gt_test/models/travel_insurance_model.dart';
import 'package:gt_test/screens/travel_documents_upload_screen.dart';
import 'package:intl/intl.dart';
import 'dart:ui' as ui;

import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/models/motor_insurance_model.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import '../common/buttons/primary_button.dart';
import '../common/images.dart';
import '../models/discount_plan_model.dart';
import '../models/insurance_details_model.dart';
import '../models/travel_insurance_group_model.dart';
import '../providers/customer_provider.dart';
import '../providers/dashboard_provider.dart';
import '../services/motor_service.dart';
import '../services/travel_service.dart';
import '../widgets/common/custom_accordian.dart';
import '../widgets/insurance_details_bottom_sheet_widget.dart';
import '../widgets/travel_additional_cover_bottom_sheet_widget.dart';

class TravelInsurancesScreen extends StatefulWidget {
  final DiscountPlanModel? discountData;
  final String type;
  const TravelInsurancesScreen({
    this.discountData,
    required this.type,
  });
  @override
  _TravelInsurancesScreenState createState() => _TravelInsurancesScreenState();
}

class _TravelInsurancesScreenState extends State<TravelInsurancesScreen> {
  final dateFormat = DateFormat("dd-MM-yyyy");
  DashboardProvider dashboardProvider = DashboardProvider();
  CustomerProvider customerProvider = CustomerProvider();
  TextEditingController searchController = TextEditingController();
  FocusNode searchFocus = FocusNode();
  FocusNode planTypeFocus = FocusNode();
  FocusNode membersFocus = FocusNode();
  bool searchValid = false;
  bool searchChanged = false;
  bool isLoading = false;
  bool isSorted = true;
  bool isFiltered = false;
  // bool isSearched = false;
  List<TravelInsuranceModel?> searchList = [];
  List<TravelInsuranceModel?> selectedList = List.filled(6, null);
  //List<MotorInsuranceModel?> filterList = [];
  String? planType = 'Individual';
  String? noOfMembers = '2';
  TextEditingController fromDateController = TextEditingController();
  TextEditingController toDateController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  FocusNode fromDateFocus = FocusNode();
  FocusNode toDateFocus = FocusNode();
  FocusNode dobFocus = FocusNode();
  late List<TextEditingController> dobControllerList;
  late List<FocusNode> dobFocusList;
  String? seats = '5';
  String? selectedValue;
  int selectedIndex = -1;
  int selectedAccordianIndex = -1;
  bool startDateValid = false;
  bool endDateValid = false;
  bool dobValid = false;
  bool startDateChanged = false;
  bool endDateChanged = false;
  bool dobChanged = false;
  @override
  void initState() {
    dashboardProvider = Provider.of<DashboardProvider>(context, listen: false);
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    customerProvider.resetInsuranceDetails();
    // dobControllerList = List.filled(
    //     9,
    //     TextEditingController(
    //         text: dateFormat.format(DateTime.now()).toString()));
    // dobFocusList = List.filled(9, FocusNode());
    dobControllerList = [
      TextEditingController(text: dateFormat.format(DateTime.now()).toString()),
      TextEditingController(text: dateFormat.format(DateTime.now()).toString()),
      TextEditingController(text: dateFormat.format(DateTime.now()).toString()),
      TextEditingController(text: dateFormat.format(DateTime.now()).toString()),
      TextEditingController(text: dateFormat.format(DateTime.now()).toString()),
      TextEditingController(text: dateFormat.format(DateTime.now()).toString())
    ];
    dobFocusList = [
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode(),
      FocusNode()
    ];
    searchList = dashboardProvider.travelInsuranceList;
    fromDateController.text = dateFormat.format(DateTime.now()).toString();
    toDateController.text = dateFormat.format(DateTime.now()).toString();
    dobController.text = dateFormat.format(DateTime.now()).toString();
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      dashboardProvider.setTravelInsuranceList = await TravelService()
          .getTravelPlans(
              context: context,
              dob: dateFormat.format(DateTime.now()).toString(),
              planType: 'Individual',
              travelFromDate: dateFormat.format(DateTime.now()).toString(),
              travelToDate: dateFormat.format(DateTime.now()).toString());
      setState(() {});
    });
    super.initState();
  }

  bool isValid() {
    bool isValid = false;
    int count = 0;
    selectedList.forEach((element) {
      if (element != null) {
        count++;
      }
    });
    isValid = (count == int.parse(noOfMembers!));
    return isValid;
  }

  List<MotorInsuranceModel> searchProduct({
    required String qry,
    required BuildContext context,
  }) {
    List<MotorInsuranceModel> queryList = [];
    for (var insurance in dashboardProvider.motorInsuranceList) {
      if ((insurance?.productDescription.contains(qry) ?? false) ||
          (insurance?.productDescription.contains(qry.toUpperCase()) ??
              false) ||
          (insurance?.productDescription.contains(qry.toLowerCase()) ??
              false) ||
          (insurance?.productType.contains(qry) ?? false) ||
          (insurance?.productType.contains(qry.toUpperCase()) ?? false) ||
          (insurance?.productType.contains(qry.toLowerCase()) ?? false) ||
          (insurance?.validity.toString().contains(qry) ?? false) ||
          (insurance?.validity.toString().contains(qry.toUpperCase()) ??
              false) ||
          (insurance?.validity.toString().contains(qry.toLowerCase()) ??
              false) ||
          (insurance?.premiumAmount.contains(qry) ?? false) ||
          (insurance?.premiumAmount.contains(qry.toUpperCase()) ?? false) ||
          (insurance?.premiumAmount.contains(qry.toLowerCase()) ?? false) ||
          (insurance?.productSummary.contains(qry) ?? false) ||
          (insurance?.productSummary.contains(qry.toUpperCase()) ?? false) ||
          (insurance?.productSummary.contains(qry.toLowerCase()) ?? false) ||
          (insurance?.promoDiscountText.contains(qry) ?? false) ||
          (insurance?.promoDiscountText.contains(qry.toUpperCase()) ?? false) ||
          (insurance?.promoDiscountText.contains(qry.toLowerCase()) ?? false) ||
          (insurance?.promoDiscount.contains(qry) ?? false) ||
          (insurance?.promoDiscount.contains(qry.toUpperCase()) ?? false) ||
          (insurance?.promoDiscount.contains(qry.toLowerCase()) ?? false)) {
        queryList.add(insurance!);
      }
    }
    return queryList;
  }

  List<String?> getDropdownValues(String type) {
    List<String?> values = [];
    switch (type) {
      case 'planType':
        values = ['Individual', 'Family'];
        break;
      case 'noOfMembers':
        values = ['2', '3', '4', '5', '6'];
        break;
    }
    return values;
  }

  void resetFilters() async {
    setState(() {
      widget.discountData?.premiumAmount = '';
      fromDateController.text = dateFormat.format(DateTime.now()).toString();
      toDateController.text = dateFormat.format(DateTime.now()).toString();
      dobController.text = dateFormat.format(DateTime.now()).toString();
      planType = 'Individual';
      selectedList = List.filled(6, null);
      selectedIndex = -1;
    });
  }

  void sortByPrices() async {
    setState(() {
      if (isSorted) {
        searchList
            .sort((a, b) => (b!.premiumAmount).compareTo(a!.premiumAmount));
      } else {
        searchList
            .sort((a, b) => (a!.premiumAmount).compareTo(b!.premiumAmount));
      }
      isSorted = !isSorted;
      selectedIndex = -1;
    });
  }

  void onFilter(String dob, String planType) async {
    var result;
    customerProvider.resetSelectedMotorInsuranceList();
    result = await TravelService().getTravelPlans(
        context: context,
        dob: dob,
        planType: planType,
        travelFromDate: fromDateController.text.toString(),
        travelToDate: toDateController.text.toString());
    setState(() {
      isFiltered = true;
      selectedIndex = -1;
      searchList = result;
    });
  }

  Widget accordianWidget(
      {required int index,
      required Widget childWidget,
      required Widget parentWidget,
      required EdgeInsets accordianPadding,
      required bool isExpanded}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: accordianPadding,
          child: InkWell(
            onTap: () {
              setState(() {
                if (isExpanded) {
                  selectedAccordianIndex = -1;
                } else {
                  selectedAccordianIndex = index;
                }
                isExpanded = !isExpanded;
              });
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(child: parentWidget),
                !isExpanded
                    ? Icon(Icons.expand_more,
                        size: 25.0, color: AppColors.darkBlue)
                    : Icon(Icons.expand_less,
                        size: 25.0, color: AppColors.darkBlue)
              ],
            ),
          ),
        ),
        Visibility(
          visible: isExpanded,
          child: Container(
            padding: accordianPadding,
            child: childWidget,
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // filterList.clear();
    //  searchList.forEach((product) {
    //    if (product?.productType == ' ' || product?.productType == planType) {
    //      filterList.add(product);
    //    }
    //  });
    List<Widget> searchResult = List.generate(
      int.parse(noOfMembers!),
      (int memberIndex) => accordianWidget(
          index: memberIndex,
          isExpanded: selectedAccordianIndex == memberIndex,
          accordianPadding: EdgeInsets.only(
            top: 12.s,
            //left: 12.s, right: 12.s
          ),
          parentWidget: Container(
            padding: EdgeInsets.symmetric(vertical: 8.0.s,horizontal: 12.0.s),
            child: Text(
              'Add Member Details ${memberIndex + 1}',
              style: Theme.of(context).textTheme.subtitle1,
            ),
          ),
          childWidget: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'DOB of Member ${memberIndex + 1}',
                style: buildAppTextTheme()
                    .bodyText1!
                    .copyWith(color: AppColors.primaryBlue),
              ),
              SizedBox(height: 8.0.h),
              Container(
                height: 56.0.h,
                width: 180.0.w,
                // padding: EdgeInsets.symmetric(
                //     vertical: 4.0.s,
                //     horizontal: 10.0.s),
                decoration: BoxDecoration(
                  // color: (yearFocus.hasFocus ||
                  //         yearController.text.isNotEmpty)
                  //     ? AppColors.white
                  //     : AppColors.primaryBlue,
                  color: AppColors.primaryBlue,
                  borderRadius: BorderRadius.circular(67.0.s),
                  border:
                      Border.all(width: 1.0.s, color: AppColors.primaryBlue),
                ),
                child: Center(
                  child: DateTimeField(
                    focusNode: dobFocusList[memberIndex],
                    style: dobFocusList[memberIndex].hasFocus
                        ? buildAppTextTheme()
                            .headline5!
                            .copyWith(color: AppColors.white)
                        : buildAppTextTheme()
                            .subtitle1!
                            .copyWith(color: AppColors.white),
                    decoration: InputDecoration(
                      labelText: 'Date of Birth',
                      labelStyle: dobFocusList[memberIndex].hasFocus
                          ? buildAppTextTheme().subtitle2
                          : buildAppTextTheme().headline6,
                      focusColor: AppColors.searchBlue,
                      isDense: true,
                      contentPadding: EdgeInsets.only(
                        left: 10.0.s,
                        bottom: 8.0.s,
                      ),
                      border: InputBorder.none,
                    ),
                    controller: dobControllerList[memberIndex],
                    onChanged: (value) {
                      onFilter(dobControllerList[memberIndex].text.toString(),
                          planType!);
                      setState(() {
                        this.dobChanged = true;
                      });
                      if (value != null &&
                          value.month <= 12 &&
                          value.month >= 1 &&
                          value.year <= 2100 &&
                          value.year >= 1900 &&
                          value.day <= 31 &&
                          value.day >= 1) {
                        setState(() {
                          this.dobValid = true;
                        });
                      } else {
                        setState(() {
                          this.dobValid = false;
                        });
                      }
                    },
                    resetIcon: Icon(Icons.close,
                        size: 20.0.ics, color: AppColors.white),
                    format: dateFormat,
                    onShowPicker: (context, currentValue) {
                      return showDatePicker(
                          context: context,
                          firstDate: DateTime(1900),
                          initialDate: currentValue ?? DateTime.now(),
                          lastDate: DateTime(2100));
                    },
                  ),
                ),
              ),
              SizedBox(height: 10.0.h),
              searchList.isEmpty
                  ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 17.0.s),
                          width: MediaQuery.of(context).size.width * 0.9,
                          child: Text(
                            'No data found on selected filter. Please check From date, To date, Plan type and DOB. For assistance call on 22217090.',
                            style: buildAppTextTheme().headline3,
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(
                          height: 30.0.h,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.9,
                          child: Text(
                            'لا توجد بيانات في الفلتر المحدد. يرجى التحقق من سنة الصنع ، والطراز أو القيمة. للمساعدة اتصل على 22217090.',
                            style: buildAppTextTheme().headline3,
                            textAlign: TextAlign.center,
                            textDirection: ui.TextDirection.rtl,
                          ),
                        ),
                      ],
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 2.0.w),
                          height: MediaQuery.of(context).size.height * 0.45,
                          width: MediaQuery.of(context).size.width,
                          child: ListView.builder(
                              physics: AlwaysScrollableScrollPhysics(),
                              // shrinkWrap: true,
                              itemCount: searchList.length,
                              scrollDirection: Axis.vertical,
                              itemBuilder: (context, index) {
                                return Container(
                                  padding: EdgeInsets.only(bottom: 14.0.s),
                                  child: Container(
                                    height: 115.0.h,
                                    child: InkWell(
                                      onTap: () async {
                                        FocusScope.of(context).unfocus();
                                        dashboardProvider
                                                .setAdditionalTravelInsuranceList =
                                            await TravelService()
                                                .getAdditionalTravelDetails(
                                                    context: context,
                                                    premiumAmount: searchList[
                                                                index]
                                                            ?.premiumAmount ??
                                                        '');
                                        setState(() {
                                          selectedList[memberIndex] =
                                              searchList[index];
                                        });
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            //  height: 96.0.h,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.2,
                                            decoration: BoxDecoration(
                                              color: selectedList[memberIndex]
                                                          ?.planId ==
                                                      searchList[index]?.planId
                                                  ? AppColors.border
                                                      .withOpacity(0.5)
                                                  : AppColors.white,
                                              border: Border(
                                                top: BorderSide(
                                                    color: AppColors.border,
                                                    width: 1.0),
                                                bottom: BorderSide(
                                                    color: AppColors.border,
                                                    width: 1.0),
                                                left: BorderSide(
                                                    color: AppColors.border,
                                                    width: 1.0),
                                                right: BorderSide(
                                                    color: AppColors.border,
                                                    width: 1.0),
                                              ),
                                              borderRadius: BorderRadius.only(
                                                bottomLeft:
                                                    Radius.circular(10.0.s),
                                                topLeft:
                                                    Radius.circular(10.0.s),
                                              ),
                                            ),
                                            child: Center(
                                              child: Image(
                                                image: AssetImage(searchList[
                                                                    index]
                                                                ?.productType ==
                                                            'Gulf Trust'
                                                        ? AppImages
                                                            .SPLASH_SCREEN
                                                        : searchList[index]
                                                                    ?.productType ==
                                                                'Tazur'
                                                            ? AppImages.TAZUR
                                                            : searchList[index]
                                                                        ?.productType ==
                                                                    'Burgan'
                                                                ? AppImages
                                                                    .BURGAN
                                                                : searchList[index]
                                                                            ?.productType ==
                                                                        'NLGIC'
                                                                    ? AppImages
                                                                        .NATIONAL
                                                                    : AppImages
                                                                        .WARBA
                                                    // widget.isSelected
                                                    // ? AppImages.SELECTED_PLAN
                                                    // : AppImages.PLAN
                                                    ),
                                                height: 63.0.h,
                                                width: 63.0.w,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // height: 96.0.h,
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.55,
                                            padding: EdgeInsets.only(
                                                left: 8.0.s,
                                                top: 10.0.s,
                                                bottom: 6.0.s,
                                                right: 8.0.s),
                                            decoration: BoxDecoration(
                                              color: selectedList[memberIndex]
                                                          ?.planId ==
                                                      searchList[index]?.planId
                                                  ? AppColors.border
                                                      .withOpacity(0.5)
                                                  : AppColors.white,
                                              border: Border(
                                                top: BorderSide(
                                                    color: AppColors.border,
                                                    width: 1.0),
                                                bottom: BorderSide(
                                                    color: AppColors.border,
                                                    width: 1.0),
                                                left: BorderSide(
                                                    color: selectedList[
                                                                    memberIndex]
                                                                ?.planId ==
                                                            searchList[index]
                                                                ?.planId
                                                        ? AppColors.primaryBlue
                                                        : AppColors.border,
                                                    width: 0.0),
                                                right: BorderSide(
                                                    color: AppColors.border,
                                                    width: 1.0),
                                              ),
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      width: 200.0.w,
                                                      child: Text(
                                                        searchList[index]
                                                                ?.productSummary
                                                                .trim() ??
                                                            '',
                                                        style: buildAppTextTheme()
                                                            .subtitle2!
                                                            .copyWith(
                                                                color: AppColors
                                                                    .primaryBlue),
                                                        textAlign:
                                                            TextAlign.left,
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 10.0.h,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        // Text(
                                                        //   '${searchList[index]?.validity} ${searchList[index]?.validityPeriod}',
                                                        //   style:
                                                        //       buildAppTextTheme().caption!.copyWith(color: AppColors.primaryBlue),
                                                        //   textAlign:
                                                        //       TextAlign.left,
                                                        //   maxLines:
                                                        //       1,
                                                        //   overflow:
                                                        //       TextOverflow.ellipsis,
                                                        // ),
                                                        // SizedBox(
                                                        //     width: 5.0.w),
                                                        // if (searchList[index]?.productType != null &&
                                                        //     searchList[index]?.productType.trim() != '')
                                                        //   Container(
                                                        //     height: 16.0.h,
                                                        //     child: VerticalDivider(
                                                        //       width: 2.0.w,
                                                        //       thickness: 2.0.w,
                                                        //       color: AppColors.border,
                                                        //     ),
                                                        //   ),
                                                        // SizedBox(
                                                        //     width: 7.0.w),
                                                        Text(
                                                          searchList[index]
                                                                  ?.productType ??
                                                              '',
                                                          style: buildAppTextTheme()
                                                              .caption!
                                                              .copyWith(
                                                                  color: AppColors
                                                                      .primaryBlue),
                                                          textAlign:
                                                              TextAlign.left,
                                                          maxLines: 1,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 9.0.h,
                                                    ),
                                                    InkWell(
                                                      onTap: () =>
                                                          showModalBottomSheet(
                                                              isScrollControlled:
                                                                  true,
                                                              backgroundColor:
                                                                  AppColors
                                                                      .primaryBlue
                                                                      .withOpacity(
                                                                          0.9),
                                                              barrierColor: AppColors
                                                                  .primaryBlue
                                                                  .withOpacity(
                                                                      0.9),
                                                              context: context,
                                                              builder:
                                                                  (BuildContext
                                                                      context) {
                                                                return InsuranceDetailsBottomSheetWidget(
                                                                    index: searchList[index]?.productType ==
                                                                            'Tazur'
                                                                        ? 0
                                                                        : searchList[index]?.productType ==
                                                                                'Burgan'
                                                                            ? 1
                                                                            : searchList[index]?.productType == 'NLGIC'
                                                                                ? 2
                                                                                : searchList[index]?.productType == 'Warba'
                                                                                    ? 3
                                                                                    : 4);
                                                              }),
                                                      child: Row(
                                                        children: [
                                                          Text(
                                                            'Information',
                                                            style: buildAppTextTheme()
                                                                .caption!
                                                                .copyWith(
                                                                    color: AppColors
                                                                        .orange),
                                                            textAlign:
                                                                TextAlign.right,
                                                          ),
                                                          SizedBox(
                                                            width: 8.0.w,
                                                          ),
                                                          Image(
                                                            image: AssetImage(
                                                                AppImages.INFO),
                                                            height: 18.0.h,
                                                            width: 18.0.w,
                                                            color: AppColors
                                                                .orange,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                          Stack(
                                            children: [
                                              Container(
                                                //height: 92.0.h,
                                                //width: 94.0.w,
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.2,
                                                padding: EdgeInsets.all(2.0.s),
                                                decoration: BoxDecoration(
                                                  color:
                                                      selectedList[memberIndex]
                                                                  ?.planId ==
                                                              searchList[index]
                                                                  ?.planId
                                                          ? AppColors
                                                              .primaryBlue
                                                          : AppColors.border,
                                                  border: Border.all(
                                                      color: selectedList[
                                                                      memberIndex]
                                                                  ?.planId ==
                                                              searchList[index]
                                                                  ?.planId
                                                          ? AppColors
                                                              .primaryBlue
                                                          : AppColors.border,
                                                      width: 1.0),
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    bottomRight:
                                                        Radius.circular(10.0.s),
                                                    topRight:
                                                        Radius.circular(10.0.s),
                                                  ),
                                                ),
                                                child: Center(
                                                  child: Text(
                                                    '${num.parse(searchList[index]?.premiumAmount ?? '').toStringAsFixed(3)}',
                                                    style: buildAppTextTheme()
                                                        .headline4!
                                                        .copyWith(
                                                            color: selectedList[
                                                                            memberIndex]
                                                                        ?.planId ==
                                                                    searchList[
                                                                            index]
                                                                        ?.planId
                                                                ? AppColors
                                                                    .white
                                                                : AppColors
                                                                    .primaryBlue),
                                                    textAlign: TextAlign.right,
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                top: 1.0.s,
                                                right: 1.5.s,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    color: selectedList[
                                                                    memberIndex]
                                                                ?.planId ==
                                                            searchList[index]
                                                                ?.planId
                                                        ? AppColors.white
                                                        : AppColors.primaryBlue,
                                                    borderRadius:
                                                        BorderRadius.only(
                                                      bottomLeft:
                                                          Radius.circular(
                                                              10.0.s),
                                                      topRight: Radius.circular(
                                                          10.0.s),
                                                    ),
                                                  ),
                                                  height: 26.0.h,
                                                  width: 40.0.w,
                                                  child: Center(
                                                    child: Text(
                                                      'KWD',
                                                      style: buildAppTextTheme()
                                                          .bodyText1!
                                                          .copyWith(
                                                              color: selectedList[
                                                                              memberIndex]
                                                                          ?.planId ==
                                                                      searchList[
                                                                              index]
                                                                          ?.planId
                                                                  ? AppColors
                                                                      .primaryBlue
                                                                  : AppColors
                                                                      .white),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              }),
                        ),
                        // SizedBox(
                        //   height: 300.0.h,
                        // ),
                      ],
                    ),
            ],
          )),
    );
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          bottomSheet: Container(
            color: AppColors.white,
            padding: EdgeInsets.only(
                left: 20.0.s, right: 20.0.s, top: 20.0.s, bottom: 30.0.s),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                if (customerProvider.selectedTravelInsuranceList.isNotEmpty)
                  Container(
                    padding: EdgeInsets.only(bottom: 30.0.s),
                    child: InkWell(
                      onTap: () => showModalBottomSheet(
                          backgroundColor:
                              AppColors.primaryBlue.withOpacity(0.9),
                          barrierColor: AppColors.primaryBlue.withOpacity(0.9),
                          context: context,
                          builder: (BuildContext context) {
                            return TravelAdditionalCoverBottomSheetWidget();
                          }),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            'Additional Covers for you',
                            style: buildAppTextTheme().headline4,
                            textAlign: TextAlign.center,
                          ),
                          Image(
                            image: AssetImage(AppImages.ADD_NEW),
                            height: 32.0.h,
                            width: 32.0.w,
                          ),
                        ],
                      ),
                    ),
                  ),
                Center(
                  child: PrimaryButton(
                      height: 67.0.h,
                      width: 380.0.w,
                      color: AppColors.primaryBlue,
                      isLoading: isLoading,
                      onPressed: () async {
                        if (!isLoading) {
                          setState(() {
                            isLoading = true;
                          });
                          if (planType != 'Individual') {
                            customerProvider.resetSelectedTravelInsuranceList();
                            selectedList.forEach((element) {
                              if (element != null) {
                                customerProvider.selectedTravelInsuranceList
                                    .add(element);
                              }
                            });
                            for (int i = 0;
                                i < num.parse(noOfMembers ?? "0");
                                i++) {
                              customerProvider.insuranceDetails.add(
                                  PersonalDetailsModel(
                                      dob: dobControllerList[i].text));
                            }
                          } else {
                            customerProvider.insuranceDetails.add(
                                PersonalDetailsModel(dob: dobController.text));
                          }
                          customerProvider.setInsurerTravelDetails =
                              InsurerTravelDetailsModel(
                                  travelToDate: toDateController.text,
                                  travelFromDate: fromDateController.text,
                                  totalTravelDays: (DateFormat("dd-MM-yyyy")
                                              .parse(toDateController.text
                                                  .toString())
                                              .difference(
                                                  DateFormat("dd-MM-yyyy")
                                                      .parse(fromDateController
                                                          .text
                                                          .toString()))
                                              .inDays +
                                          1)
                                      .toString());

                          Navigator.push(
                            context,
                            PageTransition(
                              type: PageTransitionType.fade,
                              child: TravelDocumentsUploadScreen(),
                            ),
                          );
                          // Navigations.pushNamed(
                          //   context,
                          //   AppRouter.documentsUploadScreen,
                          // );
                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      text: 'Continue',
                      disabled: planType == 'Individual'
                          ? selectedIndex == -1
                          : !isValid(),
                      borderRadius: 80.0.s),
                ),
              ],
            ),
          ),
          appBar: AppBar(
            backgroundColor: AppColors.primaryBlue,
            leading: InkWell(
              onTap: () => Navigations.pop(context),
              child: Icon(
                Icons.arrow_back_ios,
                color: AppColors.white,
                size: 20.0.ics,
              ),
            ),
            title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    widget.type == 'MOTOR'
                        ? 'Motor Insurance'
                        : widget.type == 'TRAVEL'
                            ? 'Travel Insurance'
                            : widget.type == 'MEDICAL'
                                ? 'Medical Insurance'
                                : 'Motor Insurance', //TODO
                    style: buildAppTextTheme()
                        .headline3
                        ?.copyWith(color: AppColors.white),
                    textAlign: TextAlign.start,
                  ),
                ]),
          ),
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: false,
          body: widget.type == 'OFFER'
              ? SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(
                        left: 8.0.s, right: 8.0.s, top: 20.0.s, bottom: 10.0.s),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 10.0.h,
                        ),
                        Container(
                          height: 90.0.h,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: [
                              SizedBox(
                                width: 12.0.w,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Enter Car Value KWD',
                                        style: buildAppTextTheme()
                                            .bodyText1!
                                            .copyWith(
                                                color: AppColors.primaryBlue),
                                      ),
                                      SizedBox(height: 8.0.h),
                                      Container(
                                        height: 56.0.h,
                                        width: 120.0.w,
                                        padding: EdgeInsets.symmetric(
                                            vertical: 4.0.s,
                                            horizontal: 10.0.s),
                                        decoration: BoxDecoration(
                                          // color: (valueFocus.hasFocus ||
                                          //         valueController.text.isNotEmpty)
                                          //     ? AppColors.white
                                          //     : AppColors.primaryBlue,
                                          color: AppColors.primaryBlue,
                                          borderRadius:
                                              BorderRadius.circular(67.0.s),
                                          border: Border.all(
                                              width: 1.0.s,
                                              color: AppColors.primaryBlue),
                                        ),
                                        child: Center(
                                          child: TextFormField(
                                            textAlign: TextAlign.center,
                                            inputFormatters: [
                                              FilteringTextInputFormatter
                                                  .digitsOnly,
                                              LengthLimitingTextInputFormatter(
                                                  7),
                                            ], // focus to next
                                            onFieldSubmitted: (_) =>
                                                FocusScope.of(context)
                                                    .requestFocus(
                                                        planTypeFocus),

                                            // scrollPadding:
                                            //     EdgeInsets.only(bottom: 100),
                                            focusNode: fromDateFocus,
                                            // style: valueFocus.hasFocus
                                            //     ? buildAppTextTheme().headline5
                                            //     : buildAppTextTheme()
                                            //         .subtitle1!
                                            //         .copyWith(
                                            //             color: AppColors.black),
                                            style: buildAppTextTheme()
                                                .subtitle1!
                                                .copyWith(
                                                    color: AppColors.white),
                                            keyboardType: TextInputType.number,
                                            controller: fromDateController,
                                            obscureText: false,
                                            decoration: InputDecoration(
                                              isDense: true,
                                              // contentPadding: EdgeInsets.only(
                                              //   left: 2.0.s,
                                              //   top: 2.0.s,
                                              //   bottom: 2.0.s,
                                              //   right: 2.0.s,
                                              // ),
                                              // labelText: 'Value',
                                              // labelStyle: (valueFocus.hasFocus ||
                                              //         valueController
                                              //             .text.isNotEmpty)
                                              //     ? buildAppTextTheme().subtitle2
                                              //     : buildAppTextTheme()
                                              //         .subtitle2!
                                              //         .copyWith(
                                              //             color: AppColors.white),
                                              border: InputBorder.none,
                                            ),
                                            onChanged: (value) async {
                                              onFilter(
                                                  dobController.text.toString(),
                                                  planType!);
                                              // var result;
                                              // if (makeYear == null ||
                                              //     makeYear == 'Year') {
                                              //   if (seats == null ||
                                              //       seats == 'Seats') {
                                              //     result = await CategoryService()
                                              //         .getMotorInsurancePlans(
                                              //             context: context,
                                              //             value: valueController
                                              //                     .text.isEmpty
                                              //                 ? 0
                                              //                 : int.parse(
                                              //                     valueController
                                              //                         .text),
                                              //             makeModel: brand ?? '',
                                              //             planType: planType!);
                                              //   } else {
                                              //     result = await CategoryService()
                                              //         .getMotorInsurancePlans(
                                              //             context: context,
                                              //             value: valueController
                                              //                     .text.isEmpty
                                              //                 ? 0
                                              //                 : int.parse(
                                              //                     valueController
                                              //                         .text),
                                              //             makeModel: brand!,
                                              //             noOfSeats:
                                              //                 int.parse(seats!),
                                              //             planType: planType!);
                                              //   }
                                              // } else {
                                              //   if (seats == null ||
                                              //       seats == 'Seats') {
                                              //     result = await CategoryService()
                                              //         .getMotorInsurancePlans(
                                              //             context: context,
                                              //             value: valueController
                                              //                     .text.isEmpty
                                              //                 ? 0
                                              //                 : int.parse(
                                              //                     valueController
                                              //                         .text),
                                              //             makeModel: brand!,
                                              //             makeYear:
                                              //                 int.parse(makeYear!),
                                              //             planType: planType!);
                                              //   } else {
                                              //     result = await CategoryService()
                                              //         .getMotorInsurancePlans(
                                              //             context: context,
                                              //             value: valueController
                                              //                     .text.isEmpty
                                              //                 ? 0
                                              //                 : int.parse(
                                              //                     valueController
                                              //                         .text),
                                              //             makeModel: brand!,
                                              //             makeYear:
                                              //                 int.parse(makeYear!),
                                              //             noOfSeats:
                                              //                 int.parse(seats!),
                                              //             planType: planType!);
                                              //   }
                                              // }
                                              // setState(() {
                                              //   searchList = result;
                                              // });
                                            },
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 8.0.w,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Select Manufacture Year',
                                        style: buildAppTextTheme()
                                            .bodyText1!
                                            .copyWith(
                                                color: AppColors.primaryBlue),
                                      ),
                                      SizedBox(height: 8.0.h),
                                      Container(
                                        height: 56.0.h,
                                        width: 120.0.w,
                                        decoration: BoxDecoration(
                                          color: AppColors.primaryBlue,
                                          borderRadius:
                                              BorderRadius.circular(67.0.s),
                                          border: Border.all(
                                              width: 1.0.s,
                                              color: AppColors.primaryBlue),
                                        ),
                                        child: Center(
                                          child: DropdownButtonHideUnderline(
                                            child: DropdownButton2(
                                              focusNode: planTypeFocus,
                                              isExpanded: true,
                                              // hint: Row(
                                              //   children: [
                                              //     Expanded(
                                              //       child: Text(
                                              //         'Year',
                                              //         style: buildAppTextTheme()
                                              //             .headline6!
                                              //             .copyWith(
                                              //                 color: AppColors.white),
                                              //         overflow: TextOverflow.ellipsis,
                                              //       ),
                                              //     ),
                                              //   ],
                                              // ),
                                              items: getDropdownValues(
                                                      'planType')
                                                  .map((item) =>
                                                      DropdownMenuItem<String>(
                                                        value: item,
                                                        child: Text(
                                                          item!,
                                                          style: buildAppTextTheme()
                                                              .headline6!
                                                              .copyWith(
                                                                  color: AppColors
                                                                      .white),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                      ))
                                                  .toList(),
                                              value: this.planType,
                                              onChanged: (value) {
                                                planType = value.toString();
                                                onFilter(
                                                    dobController.text
                                                        .toString(),
                                                    planType!);
                                                // onSelectedItem(
                                                //     'planType', value.toString());
                                                FocusScope.of(context)
                                                    .requestFocus(searchFocus);
                                              },
                                              icon: Icon(
                                                Icons
                                                    .keyboard_arrow_down_outlined,
                                                color: AppColors.white,
                                                size: 24.0.ics,
                                              ),
                                              // iconSize: 14,
                                              // iconEnabledColor: Colors.yellow,
                                              // iconDisabledColor: Colors.grey,
                                              buttonHeight: 50,
                                              buttonWidth: 160,
                                              buttonPadding:
                                                  const EdgeInsets.only(
                                                      left: 14, right: 14),
                                              buttonDecoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        67.0.s),
                                                border: Border.all(
                                                    width: 1.0.s,
                                                    color:
                                                        AppColors.primaryBlue),
                                                color: AppColors.primaryBlue,
                                              ),
                                              buttonElevation: 2,
                                              itemHeight: 40,
                                              itemPadding:
                                                  const EdgeInsets.only(
                                                      left: 14, right: 14),
                                              dropdownMaxHeight: 200,
                                              dropdownWidth: 100,
                                              dropdownPadding: null,
                                              dropdownDecoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        9.0.s),
                                                color: AppColors.primaryBlue,
                                              ),
                                              dropdownElevation: 8,
                                              scrollbarRadius:
                                                  const Radius.circular(40),
                                              scrollbarThickness: 6,
                                              scrollbarAlwaysShow: true,
                                              offset: const Offset(0, 0),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 8.0.w,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 20.0.h,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 10.0.h),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      resetFilters();
                                      onFilter(dobController.text.toString(),
                                          planType!);
                                    },
                                    child: Text(
                                      'Reset Filter',
                                      style: buildAppTextTheme()
                                          .headline6!
                                          .copyWith(
                                              color: AppColors.primaryBlue),
                                      overflow: TextOverflow.fade,
                                    ),
                                  ),
                                  SizedBox(width: 4.0.w),
                                  InkWell(
                                    onTap: () {
                                      resetFilters();
                                      onFilter(dobController.text.toString(),
                                          planType!);
                                    },
                                    child: Icon(
                                      Icons.replay,
                                      color: AppColors.primaryBlue,
                                      size: 18.0.ics,
                                    ),
                                  ),
                                ],
                              ),
                              InkWell(
                                onTap: () => sortByPrices(),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Sort on price',
                                      style: buildAppTextTheme()
                                          .headline6!
                                          .copyWith(
                                              color: AppColors.primaryBlue),
                                      overflow: TextOverflow.fade,
                                    ),
                                    SizedBox(width: 4.0.w),
                                    Image(
                                      image: AssetImage(AppImages.SORT),
                                      color: AppColors.primaryBlue,
                                      height: 24.0.h,
                                      width: 24.0.w,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10.0.h,
                        ),
                        // isSearched &&
                        (!isFiltered)
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 2.0.w),
                                    height: MediaQuery.of(context).size.height *
                                        0.45,
                                    width: MediaQuery.of(context).size.width,
                                    child: ListView.builder(
                                        physics:
                                            AlwaysScrollableScrollPhysics(),
                                        // shrinkWrap: true,
                                        itemCount: dashboardProvider
                                            .discountPlanList.length,
                                        scrollDirection: Axis.vertical,
                                        itemBuilder: (context, index) {
                                          return Container(
                                            padding:
                                                EdgeInsets.only(bottom: 14.0.s),
                                            child: Container(
                                              height: 115.0.h,
                                              child: InkWell(
                                                onTap: () async {
                                                  FocusScope.of(context)
                                                      .unfocus();
                                                  // searchList[index]
                                                  //     ?.planType =
                                                  // planType!;
                                                  dashboardProvider
                                                          .setAdditionalMotorInsuranceModel =
                                                      await MotorService()
                                                          .getAdditionalMotorPlans(
                                                              context: context,
                                                              productType: dashboardProvider
                                                                      .discountPlanList[
                                                                          index]
                                                                      ?.productType ??
                                                                  '');
                                                  setState(() {
                                                    selectedIndex = index;
                                                    customerProvider
                                                        .resetSelectedMotorInsuranceList();
                                                    customerProvider
                                                        .resetInsurerMotorDetails();
                                                    customerProvider
                                                        .selectedMotorInsuranceList
                                                        .add(
                                                            MotorInsuranceModel(
                                                      productType: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.productType ??
                                                          '',
                                                      productSummary: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.productSummary ??
                                                          '',
                                                      productDescription:
                                                          dashboardProvider
                                                                  .discountPlanList[
                                                                      index]
                                                                  ?.productDescription ??
                                                              '',
                                                      premiumAmount: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.premiumAmount ??
                                                          '0',
                                                      promoCode: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.promoCode ??
                                                          '',
                                                      promoDiscount: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.promoDiscount ??
                                                          '0',
                                                      promoDiscountText:
                                                          dashboardProvider
                                                                  .discountPlanList[
                                                                      index]
                                                                  ?.promoDiscountText ??
                                                              '',
                                                      totalPremium: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.totalPremium ??
                                                          '0',
                                                      otherFee: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.otherFee ??
                                                          '0',
                                                      validity: int.parse(
                                                          dashboardProvider
                                                                  .discountPlanList[
                                                                      index]
                                                                  ?.validity ??
                                                              '0'),
                                                      validityPeriod: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.validityPeriod ??
                                                          '',
                                                      planid: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.planId ??
                                                          0,
                                                      planType: dashboardProvider
                                                              .discountPlanList[
                                                                  index]
                                                              ?.planType ??
                                                          '',
                                                    ));

                                                    customerProvider
                                                        ?.insuranceMotorDetails
                                                        ?.makeYear = planType!;
                                                    if (seats != null) {
                                                      customerProvider
                                                          ?.insuranceMotorDetails
                                                          ?.noOfSeats = seats!;
                                                    }
                                                    customerProvider
                                                            ?.insuranceMotorDetails
                                                            ?.carValue =
                                                        fromDateController
                                                            .text!;
                                                  });
                                                },
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.start,
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.center,
                                                  children: [
                                                    Container(
                                                      //  height: 96.0.h,
                                                      width:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width *
                                                              0.2,
                                                      decoration: BoxDecoration(
                                                        color: selectedIndex ==
                                                                index
                                                            ? AppColors.border
                                                                .withOpacity(
                                                                    0.5)
                                                            : AppColors.white,
                                                        border: Border(
                                                          top: BorderSide(
                                                              color: AppColors
                                                                  .border,
                                                              width: 1.0),
                                                          bottom: BorderSide(
                                                              color: AppColors
                                                                  .border,
                                                              width: 1.0),
                                                          left: BorderSide(
                                                              color: AppColors
                                                                  .border,
                                                              width: 1.0),
                                                          right: BorderSide(
                                                              color: AppColors
                                                                  .border,
                                                              width: 1.0),
                                                        ),
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  10.0.s),
                                                          topLeft:
                                                              Radius.circular(
                                                                  10.0.s),
                                                        ),
                                                      ),
                                                      child: Center(
                                                        child: Image(
                                                          image: AssetImage(
                                                              dashboardProvider
                                                                          .discountPlanList[
                                                                              index]
                                                                          ?.productType ==
                                                                      'Gulf Trust'
                                                                  ? AppImages
                                                                      .SPLASH_SCREEN
                                                                  : dashboardProvider
                                                                              .discountPlanList[
                                                                                  index]
                                                                              ?.productType ==
                                                                          'Tazur'
                                                                      ? AppImages
                                                                          .TAZUR
                                                                      : dashboardProvider.discountPlanList[index]?.productType ==
                                                                              'Burgan'
                                                                          ? AppImages
                                                                              .BURGAN
                                                                          : dashboardProvider.discountPlanList[index]?.productType == 'NLGIC'
                                                                              ? AppImages.NATIONAL
                                                                              : AppImages.WARBA
                                                              // widget.isSelected
                                                              // ? AppImages.SELECTED_PLAN
                                                              // : AppImages.PLAN
                                                              ),
                                                          height: 63.0.h,
                                                          width: 63.0.w,
                                                          fit: BoxFit.fill,
                                                        ),
                                                      ),
                                                    ),
                                                    Stack(
                                                      children: [
                                                        Container(
                                                          // height: 96.0.h,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.55,
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 8.0.s,
                                                                  top: 10.0.s,
                                                                  bottom: 6.0.s,
                                                                  right: 8.0.s),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: selectedIndex ==
                                                                    index
                                                                ? AppColors
                                                                    .border
                                                                    .withOpacity(
                                                                        0.5)
                                                                : AppColors
                                                                    .white,
                                                            border: Border(
                                                              top: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              bottom: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              left: BorderSide(
                                                                  color: selectedIndex == index
                                                                      ? AppColors
                                                                          .primaryBlue
                                                                      : AppColors
                                                                          .border,
                                                                  width: 0.0),
                                                              right: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                            ),
                                                          ),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Column(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Container(
                                                                    width:
                                                                        200.0.w,
                                                                    child: Text(
                                                                      dashboardProvider
                                                                              .discountPlanList[index]
                                                                              ?.productSummary
                                                                              .trim() ??
                                                                          '',
                                                                      style: buildAppTextTheme()
                                                                          .subtitle2!
                                                                          .copyWith(
                                                                              color: AppColors.primaryBlue),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .left,
                                                                      maxLines:
                                                                          1,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                        10.0.h,
                                                                  ),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        '${dashboardProvider.discountPlanList[index]?.validity} ${dashboardProvider.discountPlanList[index]?.validityPeriod}',
                                                                        style: buildAppTextTheme()
                                                                            .caption!
                                                                            .copyWith(color: AppColors.primaryBlue),
                                                                        textAlign:
                                                                            TextAlign.left,
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                      ),
                                                                      SizedBox(
                                                                          width:
                                                                              5.0.w),
                                                                      if (dashboardProvider.discountPlanList[index]?.productType !=
                                                                              null &&
                                                                          dashboardProvider.discountPlanList[index]?.productType.trim() !=
                                                                              '')
                                                                        Container(
                                                                          height:
                                                                              16.0.h,
                                                                          child:
                                                                              VerticalDivider(
                                                                            width:
                                                                                2.0.w,
                                                                            thickness:
                                                                                2.0.w,
                                                                            color:
                                                                                AppColors.border,
                                                                          ),
                                                                        ),
                                                                      SizedBox(
                                                                          width:
                                                                              7.0.w),
                                                                      Text(
                                                                        dashboardProvider.discountPlanList[index]?.productType ??
                                                                            '',
                                                                        style: buildAppTextTheme()
                                                                            .caption!
                                                                            .copyWith(color: AppColors.primaryBlue),
                                                                        textAlign:
                                                                            TextAlign.left,
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                        9.0.h,
                                                                  ),
                                                                  InkWell(
                                                                    onTap: () => showModalBottomSheet(
                                                                        isScrollControlled: true,
                                                                        backgroundColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                        barrierColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                        context: context,
                                                                        builder: (BuildContext context) {
                                                                          return InsuranceDetailsBottomSheetWidget(
                                                                              index: dashboardProvider.discountPlanList[index]?.productType == 'Tazur'
                                                                                  ? 0
                                                                                  : dashboardProvider.discountPlanList[index]?.productType == 'Burgan'
                                                                                      ? 1
                                                                                      : dashboardProvider.discountPlanList[index]?.productType == 'NLGIC'
                                                                                          ? 2
                                                                                          : dashboardProvider.discountPlanList[index]?.productType == 'Warba'
                                                                                              ? 3
                                                                                              : 4);
                                                                        }),
                                                                    child: Row(
                                                                      children: [
                                                                        Text(
                                                                          'Information',
                                                                          style: buildAppTextTheme()
                                                                              .caption!
                                                                              .copyWith(color: AppColors.orange),
                                                                          textAlign:
                                                                              TextAlign.right,
                                                                        ),
                                                                        SizedBox(
                                                                          width:
                                                                              8.0.w,
                                                                        ),
                                                                        Image(
                                                                          image:
                                                                              AssetImage(AppImages.INFO),
                                                                          height:
                                                                              18.0.h,
                                                                          width:
                                                                              18.0.w,
                                                                          color:
                                                                              AppColors.orange,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Positioned(
                                                          bottom: 0.0.s,
                                                          right: 0.0.s,
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Text(
                                                                'Promo Code',
                                                                style: buildAppTextTheme()
                                                                    .bodyText1!
                                                                    .copyWith(
                                                                        color: AppColors
                                                                            .brownColor),
                                                                textAlign:
                                                                    TextAlign
                                                                        .start,
                                                              ),
                                                              SizedBox(
                                                                width: 40.0.w,
                                                              ),
                                                              Container(
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color: AppColors
                                                                      .skyBlue,
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    bottomRight:
                                                                        Radius.circular(
                                                                            6.0.s),
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            8.0.s),
                                                                  ),
                                                                ),
                                                                height: 40.0.h,
                                                                width: 50.0.w,
                                                                child: Center(
                                                                  child: Text(
                                                                    '${dashboardProvider.discountPlanList[index]?.promoDiscount}%',
                                                                    style: buildAppTextTheme()
                                                                        .caption,
                                                                    textAlign:
                                                                        TextAlign
                                                                            .start,
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Stack(
                                                      children: [
                                                        Container(
                                                          //height: 92.0.h,
                                                          //width: 94.0.w,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.2,
                                                          padding:
                                                              EdgeInsets.all(
                                                                  2.0.s),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: selectedIndex ==
                                                                    index
                                                                ? AppColors
                                                                    .primaryBlue
                                                                : AppColors
                                                                    .border,
                                                            border: Border.all(
                                                                color: selectedIndex ==
                                                                        index
                                                                    ? AppColors
                                                                        .primaryBlue
                                                                    : AppColors
                                                                        .border,
                                                                width: 1.0),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              bottomRight: Radius
                                                                  .circular(
                                                                      10.0.s),
                                                              topRight: Radius
                                                                  .circular(
                                                                      10.0.s),
                                                            ),
                                                          ),
                                                          child: Center(
                                                            child: Text(
                                                              dashboardProvider
                                                                          .discountPlanList[
                                                                              index]
                                                                          ?.premiumAmount !=
                                                                      ''
                                                                  ? '${num.parse(dashboardProvider.discountPlanList[index]?.premiumAmount ?? '0').toStringAsFixed(3)}'
                                                                  : '',
                                                              style: buildAppTextTheme()
                                                                  .headline4!
                                                                  .copyWith(
                                                                      color: selectedIndex ==
                                                                              index
                                                                          ? AppColors
                                                                              .white
                                                                          : AppColors
                                                                              .primaryBlue),
                                                              textAlign:
                                                                  TextAlign
                                                                      .right,
                                                            ),
                                                          ),
                                                        ),
                                                        Positioned(
                                                          top: 1.0.s,
                                                          right: 1.5.s,
                                                          child: Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              color: selectedIndex ==
                                                                      index
                                                                  ? AppColors
                                                                      .white
                                                                  : AppColors
                                                                      .primaryBlue,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .only(
                                                                bottomLeft: Radius
                                                                    .circular(
                                                                        10.0.s),
                                                                topRight: Radius
                                                                    .circular(
                                                                        10.0.s),
                                                              ),
                                                            ),
                                                            height: 26.0.h,
                                                            width: 40.0.w,
                                                            child: Center(
                                                              child: Text(
                                                                'KWD',
                                                                style: buildAppTextTheme().bodyText1!.copyWith(
                                                                    color: selectedIndex ==
                                                                            index
                                                                        ? AppColors
                                                                            .primaryBlue
                                                                        : AppColors
                                                                            .white),
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          );
                                        }),
                                  ),
                                  SizedBox(
                                    height: 300.0.h,
                                  ),
                                ],
                              )
                            : searchList.isEmpty
                                ? Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 17.0.s),
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.9,
                                        child: Text(
                                          'No data found on selected filter. Please check From date, To date, Plan type and DOB. For assistance call on 22217090.',
                                          style: buildAppTextTheme().headline3,
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 30.0.h,
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.9,
                                        child: Text(
                                          'لا توجد بيانات في الفلتر المحدد. يرجى التحقق من سنة الصنع ، والطراز أو القيمة. للمساعدة اتصل على 22217090.',
                                          style: buildAppTextTheme().headline3,
                                          textAlign: TextAlign.center,
                                          textDirection: ui.TextDirection.rtl,
                                        ),
                                      ),
                                    ],
                                  )
                                : Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 2.0.w),
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.45,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: ListView.builder(
                                            physics:
                                                AlwaysScrollableScrollPhysics(),
                                            // shrinkWrap: true,
                                            itemCount: searchList.length,
                                            scrollDirection: Axis.vertical,
                                            itemBuilder: (context, index) {
                                              return Container(
                                                padding: EdgeInsets.only(
                                                    bottom: 14.0.s),
                                                child: Container(
                                                  height: 115.0.h,
                                                  child: InkWell(
                                                    onTap: () async {
                                                      FocusScope.of(context)
                                                          .unfocus();
                                                      dashboardProvider
                                                              .setAdditionalTravelInsuranceList =
                                                          await TravelService()
                                                              .getAdditionalTravelDetails(
                                                                  context:
                                                                      context,
                                                                  premiumAmount:
                                                                      searchList[index]
                                                                              ?.premiumAmount ??
                                                                          '');
                                                      setState(() {
                                                        selectedIndex = index;
                                                        customerProvider
                                                            .resetSelectedMotorInsuranceList();
                                                        customerProvider
                                                            .resetInsurerMotorDetails();
                                                        customerProvider
                                                            .selectedTravelInsuranceList
                                                            .add(searchList[
                                                                index]);
                                                        customerProvider
                                                            ?.insuranceMotorDetails
                                                            ?.makeYear = planType!;
                                                        if (seats != null) {
                                                          customerProvider
                                                              ?.insuranceMotorDetails
                                                              ?.noOfSeats = seats!;
                                                        }
                                                        customerProvider
                                                                ?.insuranceMotorDetails
                                                                ?.carValue =
                                                            fromDateController
                                                                .text!;
                                                      });
                                                    },
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Container(
                                                          //  height: 96.0.h,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.2,
                                                          decoration:
                                                              BoxDecoration(
                                                            color: selectedIndex ==
                                                                    index
                                                                ? AppColors
                                                                    .border
                                                                    .withOpacity(
                                                                        0.5)
                                                                : AppColors
                                                                    .white,
                                                            border: Border(
                                                              top: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              bottom: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              left: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              right: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                            ),
                                                            borderRadius:
                                                                BorderRadius
                                                                    .only(
                                                              bottomLeft: Radius
                                                                  .circular(
                                                                      10.0.s),
                                                              topLeft: Radius
                                                                  .circular(
                                                                      10.0.s),
                                                            ),
                                                          ),
                                                          child: Center(
                                                            child: Image(
                                                              image: AssetImage(
                                                                  searchList[index]
                                                                              ?.productType ==
                                                                          'Gulf Trust'
                                                                      ? AppImages
                                                                          .SPLASH_SCREEN
                                                                      : searchList[index]?.productType ==
                                                                              'Tazur'
                                                                          ? AppImages
                                                                              .TAZUR
                                                                          : searchList[index]?.productType == 'Burgan'
                                                                              ? AppImages.BURGAN
                                                                              : searchList[index]?.productType == 'NLGIC'
                                                                                  ? AppImages.NATIONAL
                                                                                  : AppImages.WARBA
                                                                  // widget.isSelected
                                                                  // ? AppImages.SELECTED_PLAN
                                                                  // : AppImages.PLAN
                                                                  ),
                                                              height: 63.0.h,
                                                              width: 63.0.w,
                                                              fit: BoxFit.fill,
                                                            ),
                                                          ),
                                                        ),
                                                        Container(
                                                          // height: 96.0.h,
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.55,
                                                          padding:
                                                              EdgeInsets.only(
                                                                  left: 8.0.s,
                                                                  top: 10.0.s,
                                                                  bottom: 6.0.s,
                                                                  right: 8.0.s),
                                                          decoration:
                                                              BoxDecoration(
                                                            color: selectedIndex ==
                                                                    index
                                                                ? AppColors
                                                                    .border
                                                                    .withOpacity(
                                                                        0.5)
                                                                : AppColors
                                                                    .white,
                                                            border: Border(
                                                              top: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              bottom: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                              left: BorderSide(
                                                                  color: selectedIndex == index
                                                                      ? AppColors
                                                                          .primaryBlue
                                                                      : AppColors
                                                                          .border,
                                                                  width: 0.0),
                                                              right: BorderSide(
                                                                  color: AppColors
                                                                      .border,
                                                                  width: 1.0),
                                                            ),
                                                          ),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              Column(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Container(
                                                                    width:
                                                                        200.0.w,
                                                                    child: Text(
                                                                      searchList[index]
                                                                              ?.productSummary
                                                                              .trim() ??
                                                                          '',
                                                                      style: buildAppTextTheme()
                                                                          .subtitle2!
                                                                          .copyWith(
                                                                              color: AppColors.primaryBlue),
                                                                      textAlign:
                                                                          TextAlign
                                                                              .left,
                                                                      maxLines:
                                                                          1,
                                                                      overflow:
                                                                          TextOverflow
                                                                              .ellipsis,
                                                                    ),
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                        10.0.h,
                                                                  ),
                                                                  Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        '${searchList[index]?.validity} ${searchList[index]?.validityPeriod}',
                                                                        style: buildAppTextTheme()
                                                                            .caption!
                                                                            .copyWith(color: AppColors.primaryBlue),
                                                                        textAlign:
                                                                            TextAlign.left,
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                      ),
                                                                      SizedBox(
                                                                          width:
                                                                              5.0.w),
                                                                      if (searchList[index]?.productType !=
                                                                              null &&
                                                                          searchList[index]?.productType.trim() !=
                                                                              '')
                                                                        Container(
                                                                          height:
                                                                              16.0.h,
                                                                          child:
                                                                              VerticalDivider(
                                                                            width:
                                                                                2.0.w,
                                                                            thickness:
                                                                                2.0.w,
                                                                            color:
                                                                                AppColors.border,
                                                                          ),
                                                                        ),
                                                                      SizedBox(
                                                                          width:
                                                                              7.0.w),
                                                                      Text(
                                                                        searchList[index]?.productType ??
                                                                            '',
                                                                        style: buildAppTextTheme()
                                                                            .caption!
                                                                            .copyWith(color: AppColors.primaryBlue),
                                                                        textAlign:
                                                                            TextAlign.left,
                                                                        maxLines:
                                                                            1,
                                                                        overflow:
                                                                            TextOverflow.ellipsis,
                                                                      ),
                                                                    ],
                                                                  ),
                                                                  SizedBox(
                                                                    height:
                                                                        9.0.h,
                                                                  ),
                                                                  InkWell(
                                                                    onTap: () => showModalBottomSheet(
                                                                        isScrollControlled: true,
                                                                        backgroundColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                        barrierColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                        context: context,
                                                                        builder: (BuildContext context) {
                                                                          return InsuranceDetailsBottomSheetWidget(
                                                                              index: searchList[index]?.productType == 'Tazur'
                                                                                  ? 0
                                                                                  : searchList[index]?.productType == 'Burgan'
                                                                                      ? 1
                                                                                      : searchList[index]?.productType == 'NLGIC'
                                                                                          ? 2
                                                                                          : searchList[index]?.productType == 'Warba'
                                                                                              ? 3
                                                                                              : 4);
                                                                        }),
                                                                    child: Row(
                                                                      children: [
                                                                        Text(
                                                                          'Information',
                                                                          style: buildAppTextTheme()
                                                                              .caption!
                                                                              .copyWith(color: AppColors.orange),
                                                                          textAlign:
                                                                              TextAlign.right,
                                                                        ),
                                                                        SizedBox(
                                                                          width:
                                                                              8.0.w,
                                                                        ),
                                                                        Image(
                                                                          image:
                                                                              AssetImage(AppImages.INFO),
                                                                          height:
                                                                              18.0.h,
                                                                          width:
                                                                              18.0.w,
                                                                          color:
                                                                              AppColors.orange,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        Stack(
                                                          children: [
                                                            Container(
                                                              //height: 92.0.h,
                                                              //width: 94.0.w,
                                                              width: MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width *
                                                                  0.2,
                                                              padding:
                                                                  EdgeInsets
                                                                      .all(2.0
                                                                          .s),
                                                              decoration:
                                                                  BoxDecoration(
                                                                color: selectedIndex ==
                                                                        index
                                                                    ? AppColors
                                                                        .primaryBlue
                                                                    : AppColors
                                                                        .border,
                                                                border: Border.all(
                                                                    color: selectedIndex ==
                                                                            index
                                                                        ? AppColors
                                                                            .primaryBlue
                                                                        : AppColors
                                                                            .border,
                                                                    width: 1.0),
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .only(
                                                                  bottomRight: Radius
                                                                      .circular(
                                                                          10.0.s),
                                                                  topRight: Radius
                                                                      .circular(
                                                                          10.0.s),
                                                                ),
                                                              ),
                                                              child: Center(
                                                                child: Text(
                                                                  '${num.parse(searchList[index]?.premiumAmount ?? '').toStringAsFixed(3)}',
                                                                  style: buildAppTextTheme()
                                                                      .headline4!
                                                                      .copyWith(
                                                                          color: selectedIndex == index
                                                                              ? AppColors.white
                                                                              : AppColors.primaryBlue),
                                                                  textAlign:
                                                                      TextAlign
                                                                          .right,
                                                                ),
                                                              ),
                                                            ),
                                                            Positioned(
                                                              top: 1.0.s,
                                                              right: 1.5.s,
                                                              child: Container(
                                                                decoration:
                                                                    BoxDecoration(
                                                                  color: selectedIndex == index
                                                                      ? AppColors
                                                                          .white
                                                                      : AppColors
                                                                          .primaryBlue,
                                                                  borderRadius:
                                                                      BorderRadius
                                                                          .only(
                                                                    bottomLeft:
                                                                        Radius.circular(
                                                                            10.0.s),
                                                                    topRight: Radius
                                                                        .circular(
                                                                            10.0.s),
                                                                  ),
                                                                ),
                                                                height: 26.0.h,
                                                                width: 40.0.w,
                                                                child: Center(
                                                                  child: Text(
                                                                    'KWD',
                                                                    style: buildAppTextTheme()
                                                                        .bodyText1!
                                                                        .copyWith(
                                                                            color: selectedIndex == index
                                                                                ? AppColors.primaryBlue
                                                                                : AppColors.white),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .center,
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }),
                                      ),
                                      SizedBox(
                                        height: 300.0.h,
                                      ),
                                    ],
                                  ),
                      ],
                    ),
                  ),
                )
              : widget.type == 'TRAVEL'
                  ? SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 8.0.s,
                            right: 8.0.s,
                            top: 20.0.s,
                            bottom: 10.0.s),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 10.0.h,
                            ),
                            Container(
                              height: 90.0.h,
                              child: ListView(
                                scrollDirection: Axis.horizontal,
                                children: [
                                  SizedBox(
                                    width: 12.0.w,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text(
                                            'Travel Start Date',
                                            style: buildAppTextTheme()
                                                .bodyText1!
                                                .copyWith(
                                                    color:
                                                        AppColors.primaryBlue),
                                          ),
                                          SizedBox(height: 8.0.h),
                                          Container(
                                            height: 56.0.h,
                                            width: 180.0.w,
                                            // padding: EdgeInsets.symmetric(
                                            //     vertical: 4.0.s,
                                            //     horizontal: 10.0.s),
                                            decoration: BoxDecoration(
                                              // color: (yearFocus.hasFocus ||
                                              //         yearController.text.isNotEmpty)
                                              //     ? AppColors.white
                                              //     : AppColors.primaryBlue,
                                              color: AppColors.primaryBlue,
                                              borderRadius:
                                                  BorderRadius.circular(67.0.s),
                                              border: Border.all(
                                                  width: 1.0.s,
                                                  color: AppColors.primaryBlue),
                                            ),
                                            child: Center(
                                              child: DateTimeField(
                                                focusNode: fromDateFocus,
                                                style: fromDateFocus.hasFocus
                                                    ? buildAppTextTheme()
                                                        .headline5!
                                                        .copyWith(
                                                            color:
                                                                AppColors.white)
                                                    : buildAppTextTheme()
                                                        .subtitle1!
                                                        .copyWith(
                                                            color: AppColors
                                                                .white),
                                                decoration: InputDecoration(
                                                  labelText:
                                                      'Travel Start Date',
                                                  labelStyle:
                                                      fromDateFocus.hasFocus
                                                          ? buildAppTextTheme()
                                                              .subtitle2
                                                          : buildAppTextTheme()
                                                              .headline6,
                                                  focusColor:
                                                      AppColors.searchBlue,
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.only(
                                                    left: 10.0.s,
                                                    bottom: 8.0.s,
                                                  ),
                                                  border: InputBorder.none,
                                                ),
                                                controller: fromDateController,
                                                onChanged: (value) {
                                                  onFilter(
                                                      dobController.text
                                                          .toString(),
                                                      planType!);
                                                  setState(() {
                                                    this.startDateChanged =
                                                        true;
                                                  });
                                                  if (value != null &&
                                                      value.month != null &&
                                                      value.year != null &&
                                                      value.day != null &&
                                                      value.month <= 12 &&
                                                      value.month >= 1 &&
                                                      value.year <= 2100 &&
                                                      value.year >= 1900 &&
                                                      value.day <= 31 &&
                                                      value.day >= 1) {
                                                    setState(() {
                                                      this.startDateValid =
                                                          true;
                                                    });
                                                  } else {
                                                    setState(() {
                                                      this.startDateValid =
                                                          false;
                                                    });
                                                  }
                                                },
                                                resetIcon: Icon(Icons.close,
                                                    size: 20.0.ics,
                                                    color: AppColors.white),
                                                format: dateFormat,
                                                onShowPicker:
                                                    (context, currentValue) {
                                                  return showDatePicker(
                                                      context: context,
                                                      firstDate: DateTime(1900),
                                                      initialDate:
                                                          currentValue ??
                                                              DateTime.now(),
                                                      lastDate: DateTime(2100));
                                                },
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        width: 8.0.w,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text(
                                            'Travel End Date',
                                            style: buildAppTextTheme()
                                                .bodyText1!
                                                .copyWith(
                                                    color:
                                                        AppColors.primaryBlue),
                                          ),
                                          SizedBox(height: 8.0.h),
                                          Container(
                                            height: 56.0.h,
                                            width: 180.0.w,
                                            // padding: EdgeInsets.symmetric(
                                            //     vertical: 4.0.s,
                                            //     horizontal: 10.0.s),
                                            decoration: BoxDecoration(
                                              // color: (yearFocus.hasFocus ||
                                              //         yearController.text.isNotEmpty)
                                              //     ? AppColors.white
                                              //     : AppColors.primaryBlue,
                                              color: AppColors.primaryBlue,
                                              borderRadius:
                                                  BorderRadius.circular(67.0.s),
                                              border: Border.all(
                                                  width: 1.0.s,
                                                  color: AppColors.primaryBlue),
                                            ),
                                            child: Center(
                                              child: DateTimeField(
                                                focusNode: toDateFocus,
                                                style: toDateFocus.hasFocus
                                                    ? buildAppTextTheme()
                                                        .headline5!
                                                        .copyWith(
                                                            color:
                                                                AppColors.white)
                                                    : buildAppTextTheme()
                                                        .subtitle1!
                                                        .copyWith(
                                                            color: AppColors
                                                                .white),
                                                decoration: InputDecoration(
                                                  labelText: 'Travel End Date',
                                                  labelStyle:
                                                      toDateFocus.hasFocus
                                                          ? buildAppTextTheme()
                                                              .subtitle2
                                                          : buildAppTextTheme()
                                                              .headline6,
                                                  focusColor:
                                                      AppColors.searchBlue,
                                                  isDense: true,
                                                  contentPadding:
                                                      EdgeInsets.only(
                                                    left: 10.0.s,
                                                    bottom: 8.0.s,
                                                  ),
                                                  border: InputBorder.none,
                                                ),
                                                controller: toDateController,
                                                onChanged: (value) {
                                                  onFilter(
                                                      dobController.text
                                                          .toString(),
                                                      planType!);
                                                  setState(() {
                                                    this.endDateChanged = true;
                                                  });
                                                  if (value != null &&
                                                      value.month != null &&
                                                      value.year != null &&
                                                      value.day != null &&
                                                      value.month <= 12 &&
                                                      value.month >= 1 &&
                                                      value.year <= 2100 &&
                                                      value.year >= 1900 &&
                                                      value.day <= 31 &&
                                                      value.day >= 1) {
                                                    setState(() {
                                                      this.endDateValid = true;
                                                    });
                                                  } else {
                                                    setState(() {
                                                      this.endDateValid = false;
                                                    });
                                                  }
                                                },
                                                resetIcon: Icon(Icons.close,
                                                    size: 20.0.ics,
                                                    color: AppColors.white),
                                                format: dateFormat,
                                                onShowPicker:
                                                    (context, currentValue) {
                                                  return showDatePicker(
                                                      context: context,
                                                      firstDate: DateTime(1900),
                                                      initialDate:
                                                          currentValue ??
                                                              DateTime.now(),
                                                      lastDate: DateTime(2100));
                                                },
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        width: 8.0.w,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text(
                                            'Select Plan Type',
                                            style: buildAppTextTheme()
                                                .bodyText1!
                                                .copyWith(
                                                    color:
                                                        AppColors.primaryBlue),
                                          ),
                                          SizedBox(height: 8.0.h),
                                          Container(
                                            height: 56.0.h,
                                            width: 180.0.w,
                                            decoration: BoxDecoration(
                                              color: AppColors.primaryBlue,
                                              borderRadius:
                                                  BorderRadius.circular(67.0.s),
                                              border: Border.all(
                                                  width: 1.0.s,
                                                  color: AppColors.primaryBlue),
                                            ),
                                            child: Center(
                                              child:
                                                  DropdownButtonHideUnderline(
                                                child: DropdownButton2(
                                                  focusNode: planTypeFocus,
                                                  isExpanded: true,
                                                  // hint: Row(
                                                  //   children: [
                                                  //     Expanded(
                                                  //       child: Text(
                                                  //         'Year',
                                                  //         style: buildAppTextTheme()
                                                  //             .headline6!
                                                  //             .copyWith(
                                                  //                 color: AppColors.white),
                                                  //         overflow: TextOverflow.ellipsis,
                                                  //       ),
                                                  //     ),
                                                  //   ],
                                                  // ),
                                                  items: getDropdownValues(
                                                          'planType')
                                                      .map((item) =>
                                                          DropdownMenuItem<
                                                              String>(
                                                            value: item,
                                                            child: Text(
                                                              item!,
                                                              style: buildAppTextTheme()
                                                                  .headline6!
                                                                  .copyWith(
                                                                      color: AppColors
                                                                          .white),
                                                              overflow:
                                                                  TextOverflow
                                                                      .ellipsis,
                                                            ),
                                                          ))
                                                      .toList(),
                                                  value: this.planType,
                                                  onChanged: (value) {
                                                    planType = value.toString();
                                                    onFilter(
                                                        dobController.text
                                                            .toString(),
                                                        planType!);
                                                    // onSelectedItem(
                                                    //     'makeYear', value.toString());
                                                    if (value == 'Individual') {
                                                      FocusScope.of(context)
                                                          .requestFocus(
                                                              dobFocus);
                                                    } else {
                                                      FocusScope.of(context)
                                                          .requestFocus(
                                                              membersFocus);
                                                    }
                                                  },
                                                  icon: Icon(
                                                    Icons
                                                        .keyboard_arrow_down_outlined,
                                                    color: AppColors.white,
                                                    size: 24.0.ics,
                                                  ),
                                                  // iconSize: 14,
                                                  // iconEnabledColor: Colors.yellow,
                                                  // iconDisabledColor: Colors.grey,
                                                  buttonHeight: 50,
                                                  buttonWidth: 160,
                                                  buttonPadding:
                                                      const EdgeInsets.only(
                                                          left: 14, right: 14),
                                                  buttonDecoration:
                                                      BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            67.0.s),
                                                    border: Border.all(
                                                        width: 1.0.s,
                                                        color: AppColors
                                                            .primaryBlue),
                                                    color:
                                                        AppColors.primaryBlue,
                                                  ),
                                                  buttonElevation: 2,
                                                  itemHeight: 40,
                                                  itemPadding:
                                                      const EdgeInsets.only(
                                                          left: 14, right: 14),
                                                  dropdownMaxHeight: 200,
                                                  dropdownWidth: 100,
                                                  dropdownPadding: null,
                                                  dropdownDecoration:
                                                      BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            9.0.s),
                                                    color:
                                                        AppColors.primaryBlue,
                                                  ),
                                                  dropdownElevation: 8,
                                                  scrollbarRadius:
                                                      const Radius.circular(40),
                                                  scrollbarThickness: 6,
                                                  scrollbarAlwaysShow: true,
                                                  offset: const Offset(0, 0),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      if (planType == 'Family')
                                        SizedBox(
                                          width: 8.0.w,
                                        ),
                                      if (planType == 'Family')
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              'No of members',
                                              style: buildAppTextTheme()
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: AppColors
                                                          .primaryBlue),
                                            ),
                                            SizedBox(height: 8.0.h),
                                            Container(
                                              height: 56.0.h,
                                              width: 180.0.w,
                                              decoration: BoxDecoration(
                                                color: AppColors.primaryBlue,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        67.0.s),
                                                border: Border.all(
                                                    width: 1.0.s,
                                                    color:
                                                        AppColors.primaryBlue),
                                              ),
                                              child: Center(
                                                child:
                                                    DropdownButtonHideUnderline(
                                                  child: DropdownButton2(
                                                    focusNode: membersFocus,
                                                    isExpanded: true,
                                                    // hint: Row(
                                                    //   children: [
                                                    //     Expanded(
                                                    //       child: Text(
                                                    //         'Year',
                                                    //         style: buildAppTextTheme()
                                                    //             .headline6!
                                                    //             .copyWith(
                                                    //                 color: AppColors.white),
                                                    //         overflow: TextOverflow.ellipsis,
                                                    //       ),
                                                    //     ),
                                                    //   ],
                                                    // ),
                                                    items: getDropdownValues(
                                                            'noOfMembers')
                                                        .map((item) =>
                                                            DropdownMenuItem<
                                                                String>(
                                                              value: item,
                                                              child: Text(
                                                                item!,
                                                                style: buildAppTextTheme()
                                                                    .headline6!
                                                                    .copyWith(
                                                                        color: AppColors
                                                                            .white),
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              ),
                                                            ))
                                                        .toList(),
                                                    value: this.noOfMembers,
                                                    onChanged: (value) {
                                                      noOfMembers =
                                                          value.toString();
                                                      onFilter(
                                                          dobController.text
                                                              .toString(),
                                                          planType!);
                                                      // onSelectedItem(
                                                      //     'makeYear', value.toString());
                                                      FocusScope.of(context)
                                                          .requestFocus(
                                                              dobFocus);
                                                    },
                                                    icon: Icon(
                                                      Icons
                                                          .keyboard_arrow_down_outlined,
                                                      color: AppColors.white,
                                                      size: 24.0.ics,
                                                    ),
                                                    // iconSize: 14,
                                                    // iconEnabledColor: Colors.yellow,
                                                    // iconDisabledColor: Colors.grey,
                                                    buttonHeight: 50,
                                                    buttonWidth: 160,
                                                    buttonPadding:
                                                        const EdgeInsets.only(
                                                            left: 14,
                                                            right: 14),
                                                    buttonDecoration:
                                                        BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              67.0.s),
                                                      border: Border.all(
                                                          width: 1.0.s,
                                                          color: AppColors
                                                              .primaryBlue),
                                                      color:
                                                          AppColors.primaryBlue,
                                                    ),
                                                    buttonElevation: 2,
                                                    itemHeight: 40,
                                                    itemPadding:
                                                        const EdgeInsets.only(
                                                            left: 14,
                                                            right: 14),
                                                    dropdownMaxHeight: 200,
                                                    dropdownWidth: 100,
                                                    dropdownPadding: null,
                                                    dropdownDecoration:
                                                        BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              9.0.s),
                                                      color:
                                                          AppColors.primaryBlue,
                                                    ),
                                                    dropdownElevation: 8,
                                                    scrollbarRadius:
                                                        const Radius.circular(
                                                            40),
                                                    scrollbarThickness: 6,
                                                    scrollbarAlwaysShow: true,
                                                    offset: const Offset(0, 0),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      if (planType != 'Family')
                                        SizedBox(
                                          width: 8.0.w,
                                        ),
                                      if (planType != 'Family')
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              'DOB',
                                              style: buildAppTextTheme()
                                                  .bodyText1!
                                                  .copyWith(
                                                      color: AppColors
                                                          .primaryBlue),
                                            ),
                                            SizedBox(height: 8.0.h),
                                            Container(
                                              height: 56.0.h,
                                              width: 180.0.w,
                                              // padding: EdgeInsets.symmetric(
                                              //     vertical: 4.0.s,
                                              //     horizontal: 10.0.s),
                                              decoration: BoxDecoration(
                                                // color: (yearFocus.hasFocus ||
                                                //         yearController.text.isNotEmpty)
                                                //     ? AppColors.white
                                                //     : AppColors.primaryBlue,
                                                color: AppColors.primaryBlue,
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        67.0.s),
                                                border: Border.all(
                                                    width: 1.0.s,
                                                    color:
                                                        AppColors.primaryBlue),
                                              ),
                                              child: Center(
                                                child: DateTimeField(
                                                  focusNode: dobFocus,
                                                  style: dobFocus.hasFocus
                                                      ? buildAppTextTheme()
                                                          .headline5!
                                                          .copyWith(
                                                              color: AppColors
                                                                  .white)
                                                      : buildAppTextTheme()
                                                          .subtitle1!
                                                          .copyWith(
                                                              color: AppColors
                                                                  .white),
                                                  decoration: InputDecoration(
                                                    labelText: 'Date of Birth',
                                                    labelStyle: toDateFocus
                                                            .hasFocus
                                                        ? buildAppTextTheme()
                                                            .subtitle2
                                                        : buildAppTextTheme()
                                                            .headline6,
                                                    focusColor:
                                                        AppColors.searchBlue,
                                                    isDense: true,
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                      left: 10.0.s,
                                                      bottom: 8.0.s,
                                                    ),
                                                    border: InputBorder.none,
                                                  ),
                                                  controller: dobController,
                                                  onChanged: (value) {
                                                    onFilter(
                                                        dobController.text
                                                            .toString(),
                                                        planType!);
                                                    setState(() {
                                                      this.dobChanged = true;
                                                    });
                                                    if (value != null &&
                                                        value.month != null &&
                                                        value.year != null &&
                                                        value.day != null &&
                                                        value.month <= 12 &&
                                                        value.month >= 1 &&
                                                        value.year <= 2100 &&
                                                        value.year >= 1900 &&
                                                        value.day <= 31 &&
                                                        value.day >= 1) {
                                                      setState(() {
                                                        this.dobValid = true;
                                                      });
                                                    } else {
                                                      setState(() {
                                                        this.dobValid = false;
                                                      });
                                                    }
                                                  },
                                                  resetIcon: Icon(Icons.close,
                                                      size: 20.0.ics,
                                                      color: AppColors.white),
                                                  format: dateFormat,
                                                  onShowPicker:
                                                      (context, currentValue) {
                                                    return showDatePicker(
                                                        context: context,
                                                        firstDate:
                                                            DateTime(1900),
                                                        initialDate:
                                                            currentValue ??
                                                                DateTime.now(),
                                                        lastDate:
                                                            DateTime(2100));
                                                  },
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 10.0.h),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          resetFilters();
                                          onFilter(
                                              dobController.text.toString(),
                                              planType!);
                                        },
                                        child: Text(
                                          'Reset Filter',
                                          style: buildAppTextTheme()
                                              .headline6!
                                              .copyWith(
                                                  color: AppColors.primaryBlue),
                                          overflow: TextOverflow.fade,
                                        ),
                                      ),
                                      SizedBox(width: 4.0.w),
                                      InkWell(
                                        onTap: () {
                                          resetFilters();
                                          onFilter(
                                              dobController.text.toString(),
                                              planType!);
                                        },
                                        child: Icon(
                                          Icons.replay,
                                          color: AppColors.primaryBlue,
                                          size: 18.0.ics,
                                        ),
                                      ),
                                    ],
                                  ),
                                  InkWell(
                                    onTap: () => sortByPrices(),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Sort on price',
                                          style: buildAppTextTheme()
                                              .headline6!
                                              .copyWith(
                                                  color: AppColors.primaryBlue),
                                          overflow: TextOverflow.fade,
                                        ),
                                        SizedBox(width: 4.0.w),
                                        Image(
                                          image: AssetImage(AppImages.SORT),
                                          color: AppColors.primaryBlue,
                                          height: 24.0.h,
                                          width: 24.0.w,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 10.0.h,
                            ),
                            // isSearched &&
                            planType == 'Family'
                                ? (widget.discountData != null &&
                                        widget.discountData?.premiumAmount !=
                                            null &&
                                        !isFiltered)
                                    ? Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 2.0.w),
                                        height: 130.0.h,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: InkWell(
                                          onTap: () {
                                            FocusScope.of(context).unfocus();
                                            setState(() {
                                              selectedIndex = 0;
                                              customerProvider
                                                  .resetSelectedMotorInsuranceList();
                                              customerProvider
                                                  .resetInsurerMotorDetails();
                                              customerProvider
                                                  .selectedMotorInsuranceList
                                                  .add(MotorInsuranceModel(
                                                productType: widget.discountData
                                                        ?.productType ??
                                                    '',
                                                productSummary: widget
                                                        .discountData
                                                        ?.productSummary ??
                                                    '',
                                                productDescription: widget
                                                        .discountData
                                                        ?.productDescription ??
                                                    '',
                                                premiumAmount: widget
                                                        .discountData
                                                        ?.premiumAmount ??
                                                    '0',
                                                promoCode: widget.discountData
                                                        ?.promoCode ??
                                                    '',
                                                promoDiscount: widget
                                                        .discountData
                                                        ?.promoDiscount ??
                                                    '0',
                                                promoDiscountText: widget
                                                        .discountData
                                                        ?.promoDiscountText ??
                                                    '',
                                                totalPremium: widget
                                                        .discountData
                                                        ?.totalPremium ??
                                                    '0',
                                                otherFee: widget.discountData
                                                        ?.otherFee ??
                                                    '0',
                                                validity: int.parse(widget
                                                        .discountData
                                                        ?.validity ??
                                                    '0'),
                                                validityPeriod: widget
                                                        .discountData
                                                        ?.validityPeriod ??
                                                    '',
                                                planid: widget
                                                        .discountData?.planId ??
                                                    0,
                                                planType: widget.discountData
                                                        ?.planType ??
                                                    '',
                                              ));

                                              customerProvider
                                                  ?.insuranceMotorDetails
                                                  ?.makeYear = planType!;
                                              if (seats != null) {
                                                customerProvider
                                                    ?.insuranceMotorDetails
                                                    ?.noOfSeats = seats!;
                                              }
                                              customerProvider
                                                      ?.insuranceMotorDetails
                                                      ?.carValue =
                                                  fromDateController.text!;
                                            });
                                          },
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                //  height: 96.0.h,
                                                width: 85.0.w,
                                                decoration: BoxDecoration(
                                                  color: selectedIndex == 0
                                                      ? AppColors.border
                                                          .withOpacity(0.5)
                                                      : AppColors.white,
                                                  border: Border(
                                                    top: BorderSide(
                                                        color: AppColors.border,
                                                        width: 1.0),
                                                    bottom: BorderSide(
                                                        color: AppColors.border,
                                                        width: 1.0),
                                                    left: BorderSide(
                                                        color: AppColors.border,
                                                        width: 1.0),
                                                    right: BorderSide(
                                                        color: AppColors.border,
                                                        width: 1.0),
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(10.0.s),
                                                    topLeft:
                                                        Radius.circular(10.0.s),
                                                  ),
                                                ),
                                                child: Center(
                                                  child: Image(
                                                    image: AssetImage(
                                                        widget.discountData
                                                                    ?.productType ==
                                                                'Gulf Trust'
                                                            ? AppImages
                                                                .SPLASH_SCREEN
                                                            : widget.discountData
                                                                        ?.productType ==
                                                                    'Tazur'
                                                                ? AppImages
                                                                    .TAZUR
                                                                : widget.discountData
                                                                            ?.productType ==
                                                                        'Burgan'
                                                                    ? AppImages
                                                                        .BURGAN
                                                                    : widget.discountData?.productType ==
                                                                            'NLGIC'
                                                                        ? AppImages
                                                                            .NATIONAL
                                                                        : AppImages
                                                                            .WARBA
                                                        // widget.isSelected
                                                        // ? AppImages.SELECTED_PLAN
                                                        // : AppImages.PLAN
                                                        ),
                                                    height: 63.0.h,
                                                    width: 63.0.w,
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                              ),
                                              Stack(
                                                children: [
                                                  Container(
                                                    // height: 96.0.h,
                                                    padding: EdgeInsets.only(
                                                        left: 8.0.s,
                                                        top: 10.0.s,
                                                        bottom: 6.0.s,
                                                        right: 8.0.s),
                                                    decoration: BoxDecoration(
                                                      color: selectedIndex == 0
                                                          ? AppColors.border
                                                              .withOpacity(0.5)
                                                          : AppColors.white,
                                                      border: Border(
                                                        top: BorderSide(
                                                            color: AppColors
                                                                .border,
                                                            width: 1.0),
                                                        bottom: BorderSide(
                                                            color: AppColors
                                                                .border,
                                                            width: 1.0),
                                                        left: BorderSide(
                                                            color: selectedIndex ==
                                                                    0
                                                                ? AppColors
                                                                    .primaryBlue
                                                                : AppColors
                                                                    .border,
                                                            width: 0.0),
                                                        right: BorderSide(
                                                            color: AppColors
                                                                .border,
                                                            width: 1.0),
                                                      ),
                                                    ),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                              width: 200.0.w,
                                                              child: Text(
                                                                widget.discountData
                                                                        ?.productSummary
                                                                        .trim() ??
                                                                    '',
                                                                style: buildAppTextTheme()
                                                                    .subtitle2!
                                                                    .copyWith(
                                                                        color: AppColors
                                                                            .primaryBlue),
                                                                textAlign:
                                                                    TextAlign
                                                                        .left,
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              height: 8.0.h,
                                                            ),
                                                            Container(
                                                              width: 200.0.w,
                                                              child: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  Text(
                                                                    '${widget.discountData?.validity} ${widget.discountData?.validityPeriod}',
                                                                    style: buildAppTextTheme()
                                                                        .caption!
                                                                        .copyWith(
                                                                            color:
                                                                                AppColors.primaryBlue),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .left,
                                                                    maxLines: 1,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                  ),
                                                                  SizedBox(
                                                                      width: 5.0
                                                                          .w),
                                                                  if (widget.discountData
                                                                              ?.productType !=
                                                                          null &&
                                                                      widget.discountData
                                                                              ?.productType
                                                                              .trim() !=
                                                                          '')
                                                                    Container(
                                                                        height:
                                                                            16.0
                                                                                .h,
                                                                        child:
                                                                            VerticalDivider(
                                                                          width:
                                                                              2.0.w,
                                                                          thickness:
                                                                              2.0.w,
                                                                          color:
                                                                              AppColors.border,
                                                                        )),
                                                                  SizedBox(
                                                                      width: 7.0
                                                                          .w),
                                                                  Text(
                                                                    widget.discountData
                                                                            ?.productType ??
                                                                        '',
                                                                    style: buildAppTextTheme()
                                                                        .caption!
                                                                        .copyWith(
                                                                            color:
                                                                                AppColors.primaryBlue),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .left,
                                                                    maxLines: 1,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              height: 8.0.h,
                                                            ),
                                                            InkWell(
                                                              onTap: () =>
                                                                  showModalBottomSheet(
                                                                      isScrollControlled:
                                                                          true,
                                                                      backgroundColor: AppColors
                                                                          .primaryBlue
                                                                          .withOpacity(
                                                                              0.9),
                                                                      barrierColor: AppColors
                                                                          .primaryBlue
                                                                          .withOpacity(
                                                                              0.9),
                                                                      context:
                                                                          context,
                                                                      builder:
                                                                          (BuildContext
                                                                              context) {
                                                                        return InsuranceDetailsBottomSheetWidget(
                                                                            index: widget.discountData?.productType == 'Tazur'
                                                                                ? 0
                                                                                : widget.discountData?.productType == 'Burgan'
                                                                                    ? 1
                                                                                    : widget.discountData?.productType == 'NLGIC'
                                                                                        ? 2
                                                                                        : widget.discountData?.productType == 'Warba'
                                                                                            ? 3
                                                                                            : 4);
                                                                      }),
                                                              child: Row(
                                                                children: [
                                                                  Text(
                                                                    'Information',
                                                                    style: buildAppTextTheme()
                                                                        .caption!
                                                                        .copyWith(
                                                                            color:
                                                                                AppColors.orange),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .right,
                                                                  ),
                                                                  SizedBox(
                                                                    width:
                                                                        8.0.w,
                                                                  ),
                                                                  Image(
                                                                    image: AssetImage(
                                                                        AppImages
                                                                            .INFO),
                                                                    height:
                                                                        18.0.h,
                                                                    width:
                                                                        18.0.w,
                                                                    color: AppColors
                                                                        .orange,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  if (widget.discountData
                                                              ?.promoDiscount !=
                                                          null &&
                                                      widget.discountData
                                                              ?.promoDiscount !=
                                                          '' &&
                                                      widget.discountData
                                                              ?.promoDiscount !=
                                                          '0')
                                                    Positioned(
                                                      bottom: 0.0.s,
                                                      right: 0.0.s,
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            'Promo Code',
                                                            style: buildAppTextTheme()
                                                                .bodyText1!
                                                                .copyWith(
                                                                    color: AppColors
                                                                        .brownColor),
                                                            textAlign:
                                                                TextAlign.start,
                                                          ),
                                                          SizedBox(
                                                            width: 40.0.w,
                                                          ),
                                                          Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              color: AppColors
                                                                  .skyBlue,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .only(
                                                                bottomRight: Radius
                                                                    .circular(
                                                                        6.0.s),
                                                                topLeft: Radius
                                                                    .circular(
                                                                        8.0.s),
                                                              ),
                                                            ),
                                                            height: 40.0.h,
                                                            width: 50.0.w,
                                                            child: Center(
                                                              child: Text(
                                                                '${widget.discountData?.promoDiscount}%',
                                                                style:
                                                                    buildAppTextTheme()
                                                                        .caption,
                                                                textAlign:
                                                                    TextAlign
                                                                        .start,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                ],
                                              ),
                                              Stack(
                                                children: [
                                                  Container(
                                                    //height: 92.0.h,
                                                    width: 90.0.w,
                                                    padding:
                                                        EdgeInsets.all(2.0.s),
                                                    decoration: BoxDecoration(
                                                      color: selectedIndex == 0
                                                          ? AppColors
                                                              .primaryBlue
                                                          : AppColors.border,
                                                      border: Border.all(
                                                          color: selectedIndex ==
                                                                  0
                                                              ? AppColors
                                                                  .primaryBlue
                                                              : AppColors
                                                                  .border,
                                                          width: 1.0),
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        bottomRight:
                                                            Radius.circular(
                                                                10.0.s),
                                                        topRight:
                                                            Radius.circular(
                                                                10.0.s),
                                                      ),
                                                    ),
                                                    child: Center(
                                                      child: Text(
                                                        '${num.parse((widget.discountData?.premiumAmount ?? '') != '' ? (widget.discountData?.premiumAmount ?? '') : '0').toStringAsFixed(3)}',
                                                        style: buildAppTextTheme()
                                                            .headline4!
                                                            .copyWith(
                                                                color: selectedIndex ==
                                                                        0
                                                                    ? AppColors
                                                                        .white
                                                                    : AppColors
                                                                        .primaryBlue),
                                                        textAlign:
                                                            TextAlign.right,
                                                      ),
                                                    ),
                                                  ),
                                                  Positioned(
                                                    top: 1.0.s,
                                                    right: 1.5.s,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        color: selectedIndex ==
                                                                0
                                                            ? AppColors.white
                                                            : AppColors
                                                                .primaryBlue,
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  10.0.s),
                                                          topRight:
                                                              Radius.circular(
                                                                  10.0.s),
                                                        ),
                                                      ),
                                                      height: 26.0.h,
                                                      width: 40.0.w,
                                                      child: Center(
                                                        child: Text(
                                                          'KWD',
                                                          style: buildAppTextTheme()
                                                              .bodyText1!
                                                              .copyWith(
                                                                  color: selectedIndex ==
                                                                          0
                                                                      ? AppColors
                                                                          .primaryBlue
                                                                      : AppColors
                                                                          .white),
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    : Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          ListView(
                                            padding: EdgeInsets.zero,
                                            shrinkWrap: true,
                                            physics:
                                                const NeverScrollableScrollPhysics(),
                                            children: searchResult,
                                          ),
                                          SizedBox(height: 200.0.h),
                                        ],
                                      )
                                : (widget.discountData != null &&
                                        widget.discountData?.premiumAmount !=
                                            null &&
                                        !isFiltered)
                                    ? Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 2.0.w),
                                        height: 130.0.h,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: InkWell(
                                          onTap: () {
                                            FocusScope.of(context).unfocus();
                                            setState(() {
                                              selectedIndex = 0;
                                              customerProvider
                                                  .resetSelectedMotorInsuranceList();
                                              customerProvider
                                                  .resetInsurerMotorDetails();
                                              customerProvider
                                                  .selectedMotorInsuranceList
                                                  .add(MotorInsuranceModel(
                                                productType: widget.discountData
                                                        ?.productType ??
                                                    '',
                                                productSummary: widget
                                                        .discountData
                                                        ?.productSummary ??
                                                    '',
                                                productDescription: widget
                                                        .discountData
                                                        ?.productDescription ??
                                                    '',
                                                premiumAmount: widget
                                                        .discountData
                                                        ?.premiumAmount ??
                                                    '0',
                                                promoCode: widget.discountData
                                                        ?.promoCode ??
                                                    '',
                                                promoDiscount: widget
                                                        .discountData
                                                        ?.promoDiscount ??
                                                    '0',
                                                promoDiscountText: widget
                                                        .discountData
                                                        ?.promoDiscountText ??
                                                    '',
                                                totalPremium: widget
                                                        .discountData
                                                        ?.totalPremium ??
                                                    '0',
                                                otherFee: widget.discountData
                                                        ?.otherFee ??
                                                    '0',
                                                validity: int.parse(widget
                                                        .discountData
                                                        ?.validity ??
                                                    '0'),
                                                validityPeriod: widget
                                                        .discountData
                                                        ?.validityPeriod ??
                                                    '',
                                                planid: widget
                                                        .discountData?.planId ??
                                                    0,
                                                planType: widget.discountData
                                                        ?.planType ??
                                                    '',
                                              ));

                                              customerProvider
                                                  ?.insuranceMotorDetails
                                                  ?.makeYear = planType!;
                                              if (seats != null) {
                                                customerProvider
                                                    ?.insuranceMotorDetails
                                                    ?.noOfSeats = seats!;
                                              }
                                              customerProvider
                                                      ?.insuranceMotorDetails
                                                      ?.carValue =
                                                  fromDateController.text!;
                                            });
                                          },
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                //  height: 96.0.h,
                                                width: 85.0.w,
                                                decoration: BoxDecoration(
                                                  color: selectedIndex == 0
                                                      ? AppColors.border
                                                          .withOpacity(0.5)
                                                      : AppColors.white,
                                                  border: Border(
                                                    top: BorderSide(
                                                        color: AppColors.border,
                                                        width: 1.0),
                                                    bottom: BorderSide(
                                                        color: AppColors.border,
                                                        width: 1.0),
                                                    left: BorderSide(
                                                        color: AppColors.border,
                                                        width: 1.0),
                                                    right: BorderSide(
                                                        color: AppColors.border,
                                                        width: 1.0),
                                                  ),
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    bottomLeft:
                                                        Radius.circular(10.0.s),
                                                    topLeft:
                                                        Radius.circular(10.0.s),
                                                  ),
                                                ),
                                                child: Center(
                                                  child: Image(
                                                    image: AssetImage(
                                                        widget.discountData
                                                                    ?.productType ==
                                                                'Gulf Trust'
                                                            ? AppImages
                                                                .SPLASH_SCREEN
                                                            : widget.discountData
                                                                        ?.productType ==
                                                                    'Tazur'
                                                                ? AppImages
                                                                    .TAZUR
                                                                : widget.discountData
                                                                            ?.productType ==
                                                                        'Burgan'
                                                                    ? AppImages
                                                                        .BURGAN
                                                                    : widget.discountData?.productType ==
                                                                            'NLGIC'
                                                                        ? AppImages
                                                                            .NATIONAL
                                                                        : AppImages
                                                                            .WARBA
                                                        // widget.isSelected
                                                        // ? AppImages.SELECTED_PLAN
                                                        // : AppImages.PLAN
                                                        ),
                                                    height: 63.0.h,
                                                    width: 63.0.w,
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                              ),
                                              Stack(
                                                children: [
                                                  Container(
                                                    // height: 96.0.h,
                                                    padding: EdgeInsets.only(
                                                        left: 8.0.s,
                                                        top: 10.0.s,
                                                        bottom: 6.0.s,
                                                        right: 8.0.s),
                                                    decoration: BoxDecoration(
                                                      color: selectedIndex == 0
                                                          ? AppColors.border
                                                              .withOpacity(0.5)
                                                          : AppColors.white,
                                                      border: Border(
                                                        top: BorderSide(
                                                            color: AppColors
                                                                .border,
                                                            width: 1.0),
                                                        bottom: BorderSide(
                                                            color: AppColors
                                                                .border,
                                                            width: 1.0),
                                                        left: BorderSide(
                                                            color: selectedIndex ==
                                                                    0
                                                                ? AppColors
                                                                    .primaryBlue
                                                                : AppColors
                                                                    .border,
                                                            width: 0.0),
                                                        right: BorderSide(
                                                            color: AppColors
                                                                .border,
                                                            width: 1.0),
                                                      ),
                                                    ),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: [
                                                        Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Container(
                                                              width: 200.0.w,
                                                              child: Text(
                                                                widget.discountData
                                                                        ?.productSummary
                                                                        .trim() ??
                                                                    '',
                                                                style: buildAppTextTheme()
                                                                    .subtitle2!
                                                                    .copyWith(
                                                                        color: AppColors
                                                                            .primaryBlue),
                                                                textAlign:
                                                                    TextAlign
                                                                        .left,
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              height: 8.0.h,
                                                            ),
                                                            Container(
                                                              width: 200.0.w,
                                                              child: Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .center,
                                                                children: [
                                                                  Text(
                                                                    '${widget.discountData?.validity} ${widget.discountData?.validityPeriod}',
                                                                    style: buildAppTextTheme()
                                                                        .caption!
                                                                        .copyWith(
                                                                            color:
                                                                                AppColors.primaryBlue),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .left,
                                                                    maxLines: 1,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                  ),
                                                                  SizedBox(
                                                                      width: 5.0
                                                                          .w),
                                                                  if (widget.discountData
                                                                              ?.productType !=
                                                                          null &&
                                                                      widget.discountData
                                                                              ?.productType
                                                                              .trim() !=
                                                                          '')
                                                                    Container(
                                                                        height:
                                                                            16.0
                                                                                .h,
                                                                        child:
                                                                            VerticalDivider(
                                                                          width:
                                                                              2.0.w,
                                                                          thickness:
                                                                              2.0.w,
                                                                          color:
                                                                              AppColors.border,
                                                                        )),
                                                                  SizedBox(
                                                                      width: 7.0
                                                                          .w),
                                                                  Text(
                                                                    widget.discountData
                                                                            ?.productType ??
                                                                        '',
                                                                    style: buildAppTextTheme()
                                                                        .caption!
                                                                        .copyWith(
                                                                            color:
                                                                                AppColors.primaryBlue),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .left,
                                                                    maxLines: 1,
                                                                    overflow:
                                                                        TextOverflow
                                                                            .ellipsis,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                            SizedBox(
                                                              height: 8.0.h,
                                                            ),
                                                            InkWell(
                                                              onTap: () =>
                                                                  showModalBottomSheet(
                                                                      isScrollControlled:
                                                                          true,
                                                                      backgroundColor: AppColors
                                                                          .primaryBlue
                                                                          .withOpacity(
                                                                              0.9),
                                                                      barrierColor: AppColors
                                                                          .primaryBlue
                                                                          .withOpacity(
                                                                              0.9),
                                                                      context:
                                                                          context,
                                                                      builder:
                                                                          (BuildContext
                                                                              context) {
                                                                        return InsuranceDetailsBottomSheetWidget(
                                                                            index: widget.discountData?.productType == 'Tazur'
                                                                                ? 0
                                                                                : widget.discountData?.productType == 'Burgan'
                                                                                    ? 1
                                                                                    : widget.discountData?.productType == 'NLGIC'
                                                                                        ? 2
                                                                                        : widget.discountData?.productType == 'Warba'
                                                                                            ? 3
                                                                                            : 4);
                                                                      }),
                                                              child: Row(
                                                                children: [
                                                                  Text(
                                                                    'Information',
                                                                    style: buildAppTextTheme()
                                                                        .caption!
                                                                        .copyWith(
                                                                            color:
                                                                                AppColors.orange),
                                                                    textAlign:
                                                                        TextAlign
                                                                            .right,
                                                                  ),
                                                                  SizedBox(
                                                                    width:
                                                                        8.0.w,
                                                                  ),
                                                                  Image(
                                                                    image: AssetImage(
                                                                        AppImages
                                                                            .INFO),
                                                                    height:
                                                                        18.0.h,
                                                                    width:
                                                                        18.0.w,
                                                                    color: AppColors
                                                                        .orange,
                                                                  ),
                                                                ],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  if (widget.discountData
                                                              ?.promoDiscount !=
                                                          null &&
                                                      widget.discountData
                                                              ?.promoDiscount !=
                                                          '' &&
                                                      widget.discountData
                                                              ?.promoDiscount !=
                                                          '0')
                                                    Positioned(
                                                      bottom: 0.0.s,
                                                      right: 0.0.s,
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text(
                                                            'Promo Code',
                                                            style: buildAppTextTheme()
                                                                .bodyText1!
                                                                .copyWith(
                                                                    color: AppColors
                                                                        .brownColor),
                                                            textAlign:
                                                                TextAlign.start,
                                                          ),
                                                          SizedBox(
                                                            width: 40.0.w,
                                                          ),
                                                          Container(
                                                            decoration:
                                                                BoxDecoration(
                                                              color: AppColors
                                                                  .skyBlue,
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .only(
                                                                bottomRight: Radius
                                                                    .circular(
                                                                        6.0.s),
                                                                topLeft: Radius
                                                                    .circular(
                                                                        8.0.s),
                                                              ),
                                                            ),
                                                            height: 40.0.h,
                                                            width: 50.0.w,
                                                            child: Center(
                                                              child: Text(
                                                                '${widget.discountData?.promoDiscount}%',
                                                                style:
                                                                    buildAppTextTheme()
                                                                        .caption,
                                                                textAlign:
                                                                    TextAlign
                                                                        .start,
                                                              ),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                ],
                                              ),
                                              Stack(
                                                children: [
                                                  Container(
                                                    //height: 92.0.h,
                                                    width: 90.0.w,
                                                    padding:
                                                        EdgeInsets.all(2.0.s),
                                                    decoration: BoxDecoration(
                                                      color: selectedIndex == 0
                                                          ? AppColors
                                                              .primaryBlue
                                                          : AppColors.border,
                                                      border: Border.all(
                                                          color: selectedIndex ==
                                                                  0
                                                              ? AppColors
                                                                  .primaryBlue
                                                              : AppColors
                                                                  .border,
                                                          width: 1.0),
                                                      borderRadius:
                                                          BorderRadius.only(
                                                        bottomRight:
                                                            Radius.circular(
                                                                10.0.s),
                                                        topRight:
                                                            Radius.circular(
                                                                10.0.s),
                                                      ),
                                                    ),
                                                    child: Center(
                                                      child: Text(
                                                        '${num.parse((widget.discountData?.premiumAmount ?? '') != '' ? (widget.discountData?.premiumAmount ?? '') : '0').toStringAsFixed(3)}',
                                                        style: buildAppTextTheme()
                                                            .headline4!
                                                            .copyWith(
                                                                color: selectedIndex ==
                                                                        0
                                                                    ? AppColors
                                                                        .white
                                                                    : AppColors
                                                                        .primaryBlue),
                                                        textAlign:
                                                            TextAlign.right,
                                                      ),
                                                    ),
                                                  ),
                                                  Positioned(
                                                    top: 1.0.s,
                                                    right: 1.5.s,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        color: selectedIndex ==
                                                                0
                                                            ? AppColors.white
                                                            : AppColors
                                                                .primaryBlue,
                                                        borderRadius:
                                                            BorderRadius.only(
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  10.0.s),
                                                          topRight:
                                                              Radius.circular(
                                                                  10.0.s),
                                                        ),
                                                      ),
                                                      height: 26.0.h,
                                                      width: 40.0.w,
                                                      child: Center(
                                                        child: Text(
                                                          'KWD',
                                                          style: buildAppTextTheme()
                                                              .bodyText1!
                                                              .copyWith(
                                                                  color: selectedIndex ==
                                                                          0
                                                                      ? AppColors
                                                                          .primaryBlue
                                                                      : AppColors
                                                                          .white),
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    : searchList.isEmpty
                                        ? Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 17.0.s),
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.9,
                                                child: Text(
                                                  'No data found on selected filter. Please check From date, To date, Plan type and DOB. For assistance call on 22217090.',
                                                  style: buildAppTextTheme()
                                                      .headline3,
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                              SizedBox(
                                                height: 30.0.h,
                                              ),
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.9,
                                                child: Text(
                                                  'لا توجد بيانات في الفلتر المحدد. يرجى التحقق من سنة الصنع ، والطراز أو القيمة. للمساعدة اتصل على 22217090.',
                                                  style: buildAppTextTheme()
                                                      .headline3,
                                                  textAlign: TextAlign.center,
                                                  textDirection:
                                                      ui.TextDirection.rtl,
                                                ),
                                              ),
                                            ],
                                          )
                                        : Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 2.0.w),
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.45,
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                child: ListView.builder(
                                                    physics:
                                                        AlwaysScrollableScrollPhysics(),
                                                    // shrinkWrap: true,
                                                    itemCount:
                                                        searchList.length,
                                                    scrollDirection:
                                                        Axis.vertical,
                                                    itemBuilder:
                                                        (context, index) {
                                                      return Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                                bottom: 14.0.s),
                                                        child: Container(
                                                          height: 115.0.h,
                                                          child: InkWell(
                                                            onTap: () async {
                                                              FocusScope.of(
                                                                      context)
                                                                  .unfocus();
                                                              dashboardProvider
                                                                      .setAdditionalTravelInsuranceList =
                                                                  await TravelService().getAdditionalTravelDetails(
                                                                      context:
                                                                          context,
                                                                      premiumAmount:
                                                                          searchList[index]?.premiumAmount ??
                                                                              '');
                                                              setState(() {
                                                                selectedIndex =
                                                                    index;
                                                                customerProvider
                                                                    .resetSelectedTravelInsuranceList();
                                                                customerProvider
                                                                    .selectedTravelInsuranceList
                                                                    .add(searchList[
                                                                        index]);
                                                              });
                                                            },
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .center,
                                                              children: [
                                                                Container(
                                                                  //  height: 96.0.h,
                                                                  width: MediaQuery.of(
                                                                              context)
                                                                          .size
                                                                          .width *
                                                                      0.2,
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: selectedIndex ==
                                                                            index
                                                                        ? AppColors
                                                                            .border
                                                                            .withOpacity(
                                                                                0.5)
                                                                        : AppColors
                                                                            .white,
                                                                    border:
                                                                        Border(
                                                                      top: BorderSide(
                                                                          color: AppColors
                                                                              .border,
                                                                          width:
                                                                              1.0),
                                                                      bottom: BorderSide(
                                                                          color: AppColors
                                                                              .border,
                                                                          width:
                                                                              1.0),
                                                                      left: BorderSide(
                                                                          color: AppColors
                                                                              .border,
                                                                          width:
                                                                              1.0),
                                                                      right: BorderSide(
                                                                          color: AppColors
                                                                              .border,
                                                                          width:
                                                                              1.0),
                                                                    ),
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .only(
                                                                      bottomLeft:
                                                                          Radius.circular(
                                                                              10.0.s),
                                                                      topLeft: Radius
                                                                          .circular(
                                                                              10.0.s),
                                                                    ),
                                                                  ),
                                                                  child: Center(
                                                                    child:
                                                                        Image(
                                                                      image: AssetImage(
                                                                          searchList[index]?.productType == 'Gulf Trust'
                                                                              ? AppImages.SPLASH_SCREEN
                                                                              : searchList[index]?.productType == 'Tazur'
                                                                                  ? AppImages.TAZUR
                                                                                  : searchList[index]?.productType == 'Burgan'
                                                                                      ? AppImages.BURGAN
                                                                                      : searchList[index]?.productType == 'NLGIC'
                                                                                          ? AppImages.NATIONAL
                                                                                          : AppImages.WARBA
                                                                          // widget.isSelected
                                                                          // ? AppImages.SELECTED_PLAN
                                                                          // : AppImages.PLAN
                                                                          ),
                                                                      height:
                                                                          63.0.h,
                                                                      width:
                                                                          63.0.w,
                                                                      fit: BoxFit
                                                                          .fill,
                                                                    ),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  // height: 96.0.h,
                                                                  width: MediaQuery.of(
                                                                              context)
                                                                          .size
                                                                          .width *
                                                                      0.55,
                                                                  padding: EdgeInsets.only(
                                                                      left:
                                                                          8.0.s,
                                                                      top: 10.0
                                                                          .s,
                                                                      bottom:
                                                                          6.0.s,
                                                                      right: 8.0
                                                                          .s),
                                                                  decoration:
                                                                      BoxDecoration(
                                                                    color: selectedIndex ==
                                                                            index
                                                                        ? AppColors
                                                                            .border
                                                                            .withOpacity(
                                                                                0.5)
                                                                        : AppColors
                                                                            .white,
                                                                    border:
                                                                        Border(
                                                                      top: BorderSide(
                                                                          color: AppColors
                                                                              .border,
                                                                          width:
                                                                              1.0),
                                                                      bottom: BorderSide(
                                                                          color: AppColors
                                                                              .border,
                                                                          width:
                                                                              1.0),
                                                                      left: BorderSide(
                                                                          color: selectedIndex == index
                                                                              ? AppColors.primaryBlue
                                                                              : AppColors.border,
                                                                          width: 0.0),
                                                                      right: BorderSide(
                                                                          color: AppColors
                                                                              .border,
                                                                          width:
                                                                              1.0),
                                                                    ),
                                                                  ),
                                                                  child: Row(
                                                                    mainAxisAlignment:
                                                                        MainAxisAlignment
                                                                            .start,
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .center,
                                                                    children: [
                                                                      Column(
                                                                        mainAxisAlignment:
                                                                            MainAxisAlignment.start,
                                                                        crossAxisAlignment:
                                                                            CrossAxisAlignment.start,
                                                                        children: [
                                                                          Container(
                                                                            width:
                                                                                200.0.w,
                                                                            child:
                                                                                Text(
                                                                              searchList[index]?.productSummary.trim() ?? '',
                                                                              style: buildAppTextTheme().subtitle2!.copyWith(color: AppColors.primaryBlue),
                                                                              textAlign: TextAlign.left,
                                                                              maxLines: 1,
                                                                              overflow: TextOverflow.ellipsis,
                                                                            ),
                                                                          ),
                                                                          SizedBox(
                                                                            height:
                                                                                10.0.h,
                                                                          ),
                                                                          Row(
                                                                            mainAxisAlignment:
                                                                                MainAxisAlignment.start,
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.start,
                                                                            children: [
                                                                              // Text(
                                                                              //   '${searchList[index]?.validity} ${searchList[index]?.validityPeriod}',
                                                                              //   style:
                                                                              //       buildAppTextTheme().caption!.copyWith(color: AppColors.primaryBlue),
                                                                              //   textAlign:
                                                                              //       TextAlign.left,
                                                                              //   maxLines:
                                                                              //       1,
                                                                              //   overflow:
                                                                              //       TextOverflow.ellipsis,
                                                                              // ),
                                                                              // SizedBox(
                                                                              //     width: 5.0.w),
                                                                              // if (searchList[index]?.productType != null &&
                                                                              //     searchList[index]?.productType.trim() != '')
                                                                              //   Container(
                                                                              //     height: 16.0.h,
                                                                              //     child: VerticalDivider(
                                                                              //       width: 2.0.w,
                                                                              //       thickness: 2.0.w,
                                                                              //       color: AppColors.border,
                                                                              //     ),
                                                                              //   ),
                                                                              // SizedBox(
                                                                              //     width: 7.0.w),
                                                                              Text(
                                                                                searchList[index]?.productType ?? '',
                                                                                style: buildAppTextTheme().caption!.copyWith(color: AppColors.primaryBlue),
                                                                                textAlign: TextAlign.left,
                                                                                maxLines: 1,
                                                                                overflow: TextOverflow.ellipsis,
                                                                              ),
                                                                            ],
                                                                          ),
                                                                          SizedBox(
                                                                            height:
                                                                                9.0.h,
                                                                          ),
                                                                          InkWell(
                                                                            onTap: () => showModalBottomSheet(
                                                                                isScrollControlled: true,
                                                                                backgroundColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                                barrierColor: AppColors.primaryBlue.withOpacity(0.9),
                                                                                context: context,
                                                                                builder: (BuildContext context) {
                                                                                  return InsuranceDetailsBottomSheetWidget(
                                                                                      index: searchList[index]?.productType == 'Tazur'
                                                                                          ? 0
                                                                                          : searchList[index]?.productType == 'Burgan'
                                                                                              ? 1
                                                                                              : searchList[index]?.productType == 'NLGIC'
                                                                                                  ? 2
                                                                                                  : searchList[index]?.productType == 'Warba'
                                                                                                      ? 3
                                                                                                      : 4);
                                                                                }),
                                                                            child:
                                                                                Row(
                                                                              children: [
                                                                                Text(
                                                                                  'Information',
                                                                                  style: buildAppTextTheme().caption!.copyWith(color: AppColors.orange),
                                                                                  textAlign: TextAlign.right,
                                                                                ),
                                                                                SizedBox(
                                                                                  width: 8.0.w,
                                                                                ),
                                                                                Image(
                                                                                  image: AssetImage(AppImages.INFO),
                                                                                  height: 18.0.h,
                                                                                  width: 18.0.w,
                                                                                  color: AppColors.orange,
                                                                                ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                                Stack(
                                                                  children: [
                                                                    Container(
                                                                      //height: 92.0.h,
                                                                      //width: 94.0.w,
                                                                      width: MediaQuery.of(context)
                                                                              .size
                                                                              .width *
                                                                          0.2,
                                                                      padding: EdgeInsets
                                                                          .all(2.0
                                                                              .s),
                                                                      decoration:
                                                                          BoxDecoration(
                                                                        color: selectedIndex ==
                                                                                index
                                                                            ? AppColors.primaryBlue
                                                                            : AppColors.border,
                                                                        border: Border.all(
                                                                            color: selectedIndex == index
                                                                                ? AppColors.primaryBlue
                                                                                : AppColors.border,
                                                                            width: 1.0),
                                                                        borderRadius:
                                                                            BorderRadius.only(
                                                                          bottomRight:
                                                                              Radius.circular(10.0.s),
                                                                          topRight:
                                                                              Radius.circular(10.0.s),
                                                                        ),
                                                                      ),
                                                                      child:
                                                                          Center(
                                                                        child:
                                                                            Text(
                                                                          '${num.parse(searchList[index]?.premiumAmount ?? '').toStringAsFixed(3)}',
                                                                          style: buildAppTextTheme()
                                                                              .headline4!
                                                                              .copyWith(color: selectedIndex == index ? AppColors.white : AppColors.primaryBlue),
                                                                          textAlign:
                                                                              TextAlign.right,
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    Positioned(
                                                                      top:
                                                                          1.0.s,
                                                                      right:
                                                                          1.5.s,
                                                                      child:
                                                                          Container(
                                                                        decoration:
                                                                            BoxDecoration(
                                                                          color: selectedIndex == index
                                                                              ? AppColors.white
                                                                              : AppColors.primaryBlue,
                                                                          borderRadius:
                                                                              BorderRadius.only(
                                                                            bottomLeft:
                                                                                Radius.circular(10.0.s),
                                                                            topRight:
                                                                                Radius.circular(10.0.s),
                                                                          ),
                                                                        ),
                                                                        height:
                                                                            26.0.h,
                                                                        width:
                                                                            40.0.w,
                                                                        child:
                                                                            Center(
                                                                          child:
                                                                              Text(
                                                                            'KWD',
                                                                            style:
                                                                                buildAppTextTheme().bodyText1!.copyWith(color: selectedIndex == index ? AppColors.primaryBlue : AppColors.white),
                                                                            textAlign:
                                                                                TextAlign.center,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ),
                                                      );
                                                    }),
                                              ),
                                              SizedBox(
                                                height: 300.0.h,
                                              ),
                                            ],
                                          ),
                          ],
                        ),
                      ),
                    )
                  : Center(
                      child: Text(
                        'Coming soon',
                        style: buildAppTextTheme().headline3,
                        textAlign: TextAlign.start,
                      ),
                    ),
        ),
      ),
    );
  }
}
