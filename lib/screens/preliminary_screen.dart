import 'package:flutter/material.dart';

import 'package:gt_test/common/buttons/raw_button.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/screens/prelogin_screen.dart';
import 'package:page_transition/page_transition.dart';
import '../common/images.dart';

class PreliminaryScreen extends StatefulWidget {
  @override
  _PreliminaryScreenState createState() => _PreliminaryScreenState();
}

class _PreliminaryScreenState extends State<PreliminaryScreen> {
  int index = 0;
  var projectData = [
    {
      'title': 'Kuwait Insurance Broker Company',
      'desc':
          'Gulf Trust Insurance Broker Company is one of the leading professional consultancy companies operating in Kuwait. We provide risk and insurance management and advisory services with a personal touch. We differentiate ourselves from others by committing dedicated personal attention by our Principals to each client.',
      'image': AppImages.PRELIMINARY1,
    },
    {
      'title': 'Our Vision',
      'desc':
          'Our vision is to provide quality service to our clients, in a professional & competitive manner to exceed their expectations. We envisage long term relationships with our clients based on this vision as we will only provide quality products via our network of Insurance suppliers and continued industry development.',
      'image': AppImages.PRELIMINARY2,
    }
  ];
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: Scaffold(
        backgroundColor: AppColors.pageColor,
        body: Container(
          decoration: BoxDecoration(
            color: AppColors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image(
                image: AssetImage(AppImages.PRELIMINARY_HEADER),
                // height: 278.0.h,
                width: 428.0.w,
              ),
              Image(
                image: AssetImage(
                  projectData[index]['image']!,
                ),
                height: 278.0.h,
                width: 296.0.w,
              ),
              SizedBox(
                height: 40.0.h,
              ),
              Container(
                width: 248.0.w,
                child: Text(
                  projectData[index]['title']!,
                  style: buildAppTextTheme().headline5,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 20.0.h,
              ),
              Container(
                padding: EdgeInsets.only(
                  left: 30.0.s,
                  right: 30.0.s,
                ),
                child: Text(
                  projectData[index]['desc']!,
                  style: buildAppTextTheme().headline6,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: 35.0.h,
              ),
              Center(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 7.0.h),
                        height: 8.0.h,
                        width: index == 0 ? 32.0.w : 8.0.w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6.0.h),
                            color: AppColors.primaryBlue),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 7.0.h),
                        height: 8.0.h,
                        width: index == 0 ? 8.0.w : 32.0.w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6.0.h),
                            color: AppColors.primaryBlue),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 7.0.h),
                        height: 8.0.h,
                        width: 8.0.w,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6.0.h),
                            color: AppColors.primaryBlue),
                      ),
                    ]),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Container(
                padding: EdgeInsets.only(
                  left: 30.0.s,
                  right: 30.0.s,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    RawButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.fade,
                              child: PreLoginScreen()),
                        );
                        // Navigations.pushNamed(
                        //   context,
                        //   AppRouter.preloginScreen,
                        // );
                      },
                      text: 'Skip',
                      bgColor: AppColors.white,
                      textColor: AppColors.primaryBlue,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        if (index == 1)
                          InkWell(
                            onTap: () {
                              setState(() {
                                index--;
                              });
                            },
                            child: CircleAvatar(
                              backgroundColor: AppColors.primaryBlue,
                              child: Container(
                                padding: EdgeInsets.only(left: 10),
                                child: Icon(
                                  Icons.arrow_back_ios,
                                  color: AppColors.white,
                                  size: 24.0.ics,
                                ),
                              ),
                            ),
                          ),
                        if (index == 1)
                          SizedBox(
                            width: 10.0.w,
                          ),
                        InkWell(
                          onTap: () {
                            if (index == 0) {
                              setState(() {
                                index++;
                              });
                            } else {
                              Navigator.push(
                                context,
                                PageTransition(
                                    type: PageTransitionType.fade,
                                    child: PreLoginScreen()),
                              );
                              // Navigations.pushNamed(
                              //   context,
                              //   AppRouter.preloginScreen,
                              // );
                            }
                          },
                          child: CircleAvatar(
                            backgroundColor: AppColors.primaryBlue,
                            child: Icon(
                              Icons.arrow_forward_ios,
                              color: AppColors.white,
                              size: 24.0.ics,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
