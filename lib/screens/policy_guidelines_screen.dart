import 'package:flutter/material.dart';

import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/theme/app_theme.dart';

class PolicyGuidelinesScreen extends StatefulWidget {
  @override
  _PolicyGuidelinesScreenState createState() => _PolicyGuidelinesScreenState();
}

class _PolicyGuidelinesScreenState extends State<PolicyGuidelinesScreen> {
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.primaryBlue,
          leading: InkWell(
            onTap: ()=> Navigations.pop(context),
            child: Icon(
              Icons.arrow_back_ios,
              color: AppColors.white,
              size: 20.0.ics,
            ),
          ),
          title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Policy Guideline',
                  style: buildAppTextTheme()
                      .headline3
                      ?.copyWith(color: AppColors.white),
                  textAlign: TextAlign.start,
                ),
              ]),
        ),
        backgroundColor: AppColors.pageColor,
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(
                left: 30.0.s, right: 30.0.s, top: 23.0.s, bottom: 60.0.h),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Insurance details :',
                  style: buildAppTextTheme()
                      .headline2!
                      .copyWith(color: AppColors.black),
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  'Step-1',
                  style: buildAppTextTheme().headline5,
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate of subrogation (+200KWD)',
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Text(
                  'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate Waiver of subrogation (+200KWD)',
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  'Step-2',
                  style: buildAppTextTheme().headline5,
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate of subrogation (+200KWD)',
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate Waiver of subrogation (+200KWD) In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate is a placeholder text commonly used to demonstrat',
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  'Step-3',
                  style: buildAppTextTheme().headline5,
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate of subrogation (+200KWD)',
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate Waiver of subrogation (+200KWD) In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate is a placeholder text commonly used to demonstrat',
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 30.0.h,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
