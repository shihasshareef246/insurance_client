import 'dart:io';
// import 'package:amazon_s3_cognito/amazon_s3_cognito.dart';
// import 'package:amazon_s3_cognito/aws_region.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:gt_test/models/insurance_details_model.dart';
import 'package:image_picker/image_picker.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import 'package:intl/intl.dart';

import 'package:gt_test/common/buttons/primary_button.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import '../common/images.dart';
import '../common/logger.dart';
import 'package:gt_test/models/file_model.dart';
import 'package:gt_test/providers/documents_provider.dart';
import 'package:gt_test/providers/profile_provider.dart';
import 'package:gt_test/screens/motor_insurer_details_screen.dart';
import '../common/shared_prefs.dart';
import '../common/utils.dart';
import '../models/motor_insurance_group_model.dart';
import '../providers/customer_provider.dart';
import '../services/customer_service.dart';
import '../widgets/common/upload_bottom_sheet_widget.dart';
import '../widgets/common/post_upload_widget.dart';
import '../widgets/common/pre_upload_widget.dart';

class MedicalDocumentsUploadScreen extends StatefulWidget {
  @override
  _MedicalDocumentsUploadScreenState createState() => _MedicalDocumentsUploadScreenState();
}

class _MedicalDocumentsUploadScreenState extends State<MedicalDocumentsUploadScreen> {
  var uuid = Uuid();
  DocumentsProvider documentsProvider = DocumentsProvider();
  CustomerProvider customerProvider = CustomerProvider();
  ProfileProvider profileProvider = ProfileProvider();
  final picker = ImagePicker();
  File file = File("");
  String fileExtension = "";
  TextEditingController otpController = TextEditingController();
  FocusNode otpFocus = FocusNode();
  bool otpValid = false;
  bool otpChanged = false;
  bool isLoading = false;
  bool isFrontCivilIdUploaded = false;
  bool isBackCivilIdUploaded = false;
  bool isDrivingLicenseUploaded = false;
  bool isCarRegistrationUploaded = false;
  bool isFrontPhotoUploaded = false;
  bool isBackPhotoUploaded = false;
  bool isChasisPhotoUploaded = false;
  bool isRightPhotoUploaded = false;
  bool isLeftPhotoUploaded = false;
  final dateFormat = DateFormat("dd-MM-yyyy");
  final timeFormat = DateFormat("h:mm a");

  @override
  void initState() {
    documentsProvider = Provider.of<DocumentsProvider>(context, listen: false);
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    customerProvider?.insuranceDetails = [PersonalDetailsModel()];
    isFrontCivilIdUploaded = false;
    isBackCivilIdUploaded = false;
    isDrivingLicenseUploaded = false;
    isCarRegistrationUploaded = false;
    isFrontPhotoUploaded = false;
    isBackPhotoUploaded = false;
    isChasisPhotoUploaded = false;
    isRightPhotoUploaded = false;
    isLeftPhotoUploaded = false;
    super.initState();
  }

  // Future<String?> _uploadDocument(File file, String name) async {
  //   String? result;
  //   if (result == null) {
  //     try {
  //       setState(() {
  //         isLoading = true;
  //       });
  //       ImageData imageData = ImageData(name, file.path,
  //           uniqueId: uuid.v1(), imageUploadFolder: 'CustomerDocuments'
  //         //profileProvider.profileData?.customerId??''
  //       );
  //       String? uploadedImageUrl = await AmazonS3Cognito.upload(
  //           AppConstants.S3_BUCKET,
  //           AppConstants.S3_POOL,
  //           AwsRegion.US_EAST_1,
  //           AwsRegion.US_EAST_1
  //           imageData);
  //       documentsProvider.docsList.add(InsurerDocumentDetailsModel(
  //           documentName: name, documentUrl: uploadedImageUrl));
  //       setState(() {
  //         isLoading = false;
  //       });
  //     } catch (e) {
  //       print(e);
  //     }
  //   }
  //   return result;
  // }

  void getFileDialogue(String type) {
    showModalBottomSheet(
        backgroundColor: AppColors.primaryBlue.withOpacity(0.9),
        barrierColor: AppColors.primaryBlue.withOpacity(0.9),
        context: context,
        builder: (BuildContext context) {
          return UploadBottomSheetWidget(
            title: 'Upload image via',
            onClick: () async {
              Navigator.pop(context);
              getFileFromCamera(type);
            },
            image: AppImages.CAMERA,
            listTitle: 'Camera',
            onClick1: () async {
              Navigator.pop(context);
              getFileFromGallery(type);
            },
            image1: AppImages.GALLERY,
            listTitle1: 'Gallery',
            onClick2: () async {
              Navigator.pop(context);
              getFileFromDocument(type);
            },
            image2: Icons.attach_file_sharp,
            listTitle2: 'Document',
          );
        });
  }

  void setFileStatus(String type, {bool status = false}) {
    switch (type) {
      case 'Front_Of_Civil_ID':
        isFrontCivilIdUploaded = status;
        // _uploadDocument(file, 'Front_Of_Civil_ID.${fileExtension}');
        break;
      case 'Back_Of_Civil_ID':
        isBackCivilIdUploaded = status;
        //_uploadDocument(file, 'Back_Of_Civil_ID.${fileExtension}');
        break;
      case 'Driving_License':
        isDrivingLicenseUploaded = status;
        //_uploadDocument(file, 'Driving License.${fileExtension}');
        break;
      case 'Car_Registration':
        isCarRegistrationUploaded = status;
        //_uploadDocument(file, 'Car Registration.${fileExtension}');
        break;
      case 'Front_Photo':
        isFrontPhotoUploaded = status;
        //_uploadDocument(file, 'Front Photo.${fileExtension}');
        break;
      case 'Back_Photo':
        isBackPhotoUploaded = status;
        //_uploadDocument(file, 'Back Photo.${fileExtension}');
        break;
      case 'Car_Photo_with_Chasis_No':
        isChasisPhotoUploaded = status;
        //_uploadDocument(file, 'Car Photo with Chasis No.${fileExtension}');
        break;
      case 'Right_Side_Photo':
        isRightPhotoUploaded = status;
        //_uploadDocument(file, 'Right Side Photo.${fileExtension}');
        break;
      case 'Left_Side_Photo':
        isLeftPhotoUploaded = status;
        //_uploadDocument(file, 'Left Side Photo.${fileExtension}');
        break;
    }
  }

  Future getFileFromCamera(String type) async {
    final pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);
    if (pickedFile != null) {
      setState(() {
        file = File(pickedFile.path);
        fileExtension =
            pickedFile.path.substring(pickedFile.path.lastIndexOf('.') + 1);
        documentsProvider.documentsList.add(FileModel(
            file: file,
            type: type,
            date: dateFormat.format(DateTime.now()).toString(),
            time: timeFormat.format(DateTime.now()).toString(),
            extension: fileExtension));
        setFileStatus(type, status: true);
      });
      String? url = await Utils.uploadDocument(file, type, fileExtension,
          profileProvider.profileData?.customerId ?? '');
      documentsProvider.docsList.add(
          InsurerDocumentDetailsModel(documentName: type, documentUrl: url));
      if (type == 'Driving_License') {
        var res = await CustomerService().dlOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceMotorDetails?.licenseNo =
              res.LicenseNo ?? '';
          customerProvider?.insuranceMotorDetails?.licenseExpiry =
              dateFormat.format(DateTime.parse(res.ExpiryDate ?? ''));
          customerProvider?.insuranceDetails[0]?.dob =
              dateFormat.format(DateTime.parse(res.Dob ?? ''));
          customerProvider?.insuranceDetails[0]?.gender =
              res.Gender != '' ? res.Gender : 'Male';
          customerProvider?.insuranceDetails[0]?.insurerName = res.Name ?? '';
          customerProvider?.insuranceDetails[0]?.nationality =
              res.Nationality ?? '';
        }
      } else if (type == 'Car_Registration') {
        var res = await CustomerService().crOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceDetails[0]?.civilIdNo = res.CivilId ?? '';
          customerProvider?.insuranceMotorDetails?.plateNo =
              res.PlateNumber ?? '';
          customerProvider?.insuranceMotorDetails?.vinNo = res.VinNumber ?? '';
        }
      } else if (type == 'Front_Of_Civil_ID') {
        var res = await CustomerService().cidOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceDetails[0]?.civilIdNo = res.CivilId ?? '';
          customerProvider?.insuranceDetails[0]?.blockNo = res.Block ?? '';
          customerProvider?.insuranceDetails[0]?.bNoOrHouseNo =
              res.BuildingNo ?? '';
          customerProvider?.insuranceDetails[0]?.floorNo = res.FloorNo ?? '';
        }
      } else {}
      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
  }

  Future getFileFromGallery(String type) async {
    final pickedFile =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 50);
    if (pickedFile != null) {
      setState(() {
        file = File(pickedFile.path);
        fileExtension =
            pickedFile.path.substring(pickedFile.path.lastIndexOf('.') + 1);
        documentsProvider.documentsList.add(FileModel(
            file: file,
            type: type,
            date: dateFormat.format(DateTime.now()).toString(),
            time: timeFormat.format(DateTime.now()).toString(),
            extension: fileExtension));
        setFileStatus(type, status: true);
      });
      String? url = await Utils.uploadDocument(file, type, fileExtension,
          profileProvider.profileData?.customerId ?? '');
      documentsProvider.docsList.add(
          InsurerDocumentDetailsModel(documentName: type, documentUrl: url));
      if (type == 'Driving_License') {
        var res = await CustomerService().dlOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceMotorDetails?.licenseNo =
              res.LicenseNo ?? '';
          customerProvider?.insuranceMotorDetails?.licenseExpiry =
              dateFormat.format(DateTime.parse(res.ExpiryDate ?? ''));
          customerProvider?.insuranceDetails[0]?.dob =
              dateFormat.format(DateTime.parse(res.Dob ?? ''));
          customerProvider?.insuranceDetails[0]?.gender =
              res.Gender != '' ? res.Gender : 'Male';
          customerProvider?.insuranceDetails[0]?.insurerName = res.Name ?? '';
          customerProvider?.insuranceDetails[0]?.nationality =
              res.Nationality ?? '';
        }
      } else if (type == 'Car_Registration') {
        var res = await CustomerService().crOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceDetails[0]?.civilIdNo = res.CivilId ?? '';
          customerProvider?.insuranceMotorDetails?.plateNo =
              res.PlateNumber ?? '';
          customerProvider?.insuranceMotorDetails?.vinNo = res.VinNumber ?? '';
        }
      } else if (type == 'Front_Of_Civil_ID') {
        var res = await CustomerService().cidOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceDetails[0]?.civilIdNo = res.CivilId ?? '';
          customerProvider?.insuranceDetails[0]?.blockNo = res.Block ?? '';
          customerProvider?.insuranceDetails[0]?.bNoOrHouseNo =
              res.BuildingNo ?? '';
          customerProvider?.insuranceDetails[0]?.floorNo = res.FloorNo ?? '';
        }
      } else {}
      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
  }

  Future getFileFromDocument(String type) async {
    FilePickerResult? pickedFile =
        await FilePicker.platform.pickFiles(type: FileType.any);
    if (pickedFile != null) {
      setState(() {
        print('file picked');
        file = File(pickedFile.files.single.path!);
        fileExtension = pickedFile.files.single.extension!;
        documentsProvider.documentsList.add(FileModel(
            file: file,
            type: type,
            date: dateFormat.format(DateTime.now()).toString(),
            time: timeFormat.format(DateTime.now()).toString(),
            extension: fileExtension));
        setFileStatus(type, status: true);
      });
      String? url = await Utils.uploadDocument(file, type, fileExtension,
          profileProvider.profileData?.customerId ?? '');
      documentsProvider.docsList.add(
          InsurerDocumentDetailsModel(documentName: type, documentUrl: url));
      if (type == 'Driving_License') {
        var res = await CustomerService().dlOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceMotorDetails?.licenseNo =
              res.LicenseNo ?? '';
          customerProvider?.insuranceMotorDetails?.licenseExpiry =
              dateFormat.format(DateTime.parse(res.ExpiryDate ?? ''));
          customerProvider?.insuranceDetails[0]?.dob =
              dateFormat.format(DateTime.parse(res.Dob ?? ''));
          customerProvider?.insuranceDetails[0]?.gender =
              res.Gender != '' ? res.Gender : 'Male';
          customerProvider?.insuranceDetails[0]?.insurerName = res.Name ?? '';
          customerProvider?.insuranceDetails[0]?.nationality =
              res.Nationality ?? '';
        }
      } else if (type == 'Car_Registration') {
        var res = await CustomerService().crOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceDetails[0]?.civilIdNo = res.CivilId ?? '';
          customerProvider?.insuranceMotorDetails?.plateNo =
              res.PlateNumber ?? '';
          customerProvider?.insuranceMotorDetails?.vinNo = res.VinNumber ?? '';
        }
      } else if (type == 'Front_Of_Civil_ID') {
        var res = await CustomerService().cidOcr(context: context, url: url);
        if (res != null) {
          customerProvider?.insuranceDetails[0]?.civilIdNo = res.CivilId ?? '';
          customerProvider?.insuranceDetails[0]?.blockNo = res.Block ?? '';
          customerProvider?.insuranceDetails[0]?.bNoOrHouseNo =
              res.BuildingNo ?? '';
          customerProvider?.insuranceDetails[0]?.floorNo = res.FloorNo ?? '';
        }
      } else {}
      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
  }

  void imagePreview(String name, String type) {
    File? doc;
    String? date;
    String? time;
    String? extension;
    documentsProvider.documentsList.forEach((element) {
      if (element?.type == type) {
        doc = element?.file;
        time = element?.time;
        date = element?.date;
        extension = element?.extension;
      }
    });
    showModalBottomSheet<dynamic>(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (doc != null)
                  Container(
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0.s),
                        topRight: Radius.circular(20.0.s),
                      ),
                    ),
                    child: Stack(
                      children: [
                        Container(
                          height: 226.0.h,
                          width: 368.0.w,
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0.s),
                              topRight: Radius.circular(20.0.s),
                            ),
                            child: Image(
                              image: FileImage(doc!),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Positioned(
                          top: 20.0.s,
                          right: 20.0.s,
                          child: InkWell(
                            child: CircleAvatar(
                              backgroundColor: AppColors.brownish,
                              child: Icon(Icons.close,
                                  size: 20.0.ics, color: AppColors.primaryBlue),
                            ),
                            onTap: () => Navigator.pop(context),
                          ),
                        ),
                      ],
                    ),
                  ),
                Container(
                  height: 226.0.h,
                  width: 368.0.w,
                  padding: EdgeInsets.all(20.0.s),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20.0.s),
                      bottomLeft: Radius.circular(20.0.s),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            name,
                            style: buildAppTextTheme().headline4,
                            textAlign: TextAlign.center,
                          ),
                          InkWell(
                            onTap: () {
                              documentsProvider.documentsList
                                  .removeWhere((item) => item?.type == type);
                              setFileStatus(type, status: false);
                              Navigator.pop(context);
                              setState(() {
                                doc = null;
                              });
                            },
                            child: Image(
                              image: AssetImage(AppImages.DELETE),
                              height: 33.0.h,
                              width: 33.0.w,
                              //color: AppColors.primaryBlue,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 11.0.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            date!,
                            style: buildAppTextTheme().caption,
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            time!,
                            style: buildAppTextTheme().caption,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 11.0.h,
                      ),
                      Text(
                        'Format : $extension',
                        style: buildAppTextTheme().subtitle2,
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ]);
        });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: AppColors.primaryBlue,
          leading: InkWell(
            onTap: () => Navigations.pop(context),
            child: Icon(
              Icons.arrow_back_ios,
              color: AppColors.white,
              size: 20.0.ics,
            ),
          ),
          title: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Insurer Details',
                  style: buildAppTextTheme()
                      .headline3
                      ?.copyWith(color: AppColors.white),
                  textAlign: TextAlign.start,
                ),
                // SizedBox(
                //   height: 5.0.h,
                // ),
                // Container(
                //   width: 200.0.w,
                //   child: Text(
                //     desc!,
                //     style: buildAppTextTheme().bodyText2,
                //     textAlign: TextAlign.start,
                //   ),
                // ),
              ]),
        ),
        backgroundColor: AppColors.pageColor,
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(
                left: 30.0.s, right: 30.0.s, top: 30.0.s, bottom: 60.0.s),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Text(
                    'Upload documents',
                    style: buildAppTextTheme().headline3,
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Text(
                  'Please Upload all of the following documents before going to the next step.',
                  style: buildAppTextTheme().headline5,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 10.0.h,
                ),
                Text(
                  'Note: Enable access to App for using phone camera or gallery',
                  style: buildAppTextTheme().subtitle2,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                Container(
                  width: 360.0.w,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'View Sample',
                        style: buildAppTextTheme()
                            .overline!
                            .copyWith(color: AppColors.primaryBlue),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        width: 10.0.w,
                      ),
                      Text(
                        'Upload Doc',
                        style: buildAppTextTheme()
                            .overline!
                            .copyWith(color: AppColors.primaryBlue),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 4.0.h,
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                Container(
                  width: 360.0.w,
                  child: !isFrontCivilIdUploaded
                      ? InkWell(
                          onTap: () => getFileDialogue('Front_Of_Civil_ID'),
                          child: PreUploadWidget(
                              text: 'Front Of Civil ID',
                              type: 'Front_Of_Civil_ID'),
                        )
                      : InkWell(
                          onTap: () => imagePreview(
                              'Front Of Civil ID', 'Front_Of_Civil_ID'),
                          child: PostUploadWidget(
                              text: 'Front Of Civil ID',
                              type: 'Front_Of_Civil_ID'),
                        ),
                ),
                // Column(
                //   mainAxisAlignment: MainAxisAlignment.center,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                //     Image(
                //       image: AssetImage(AppImages.DOCUMENT),
                //       height: 20.0.h,
                //       width: 16.0.w,
                //     ),
                //     SizedBox(
                //       height: 6.0.h,
                //     ),
                //     Text(
                //       'View Sample',
                //       style: buildAppTextTheme()
                //           .overline
                //           ?.copyWith(color: AppColors.primaryBlue),
                //       textAlign: TextAlign.start,
                //     ),
                //   ],
                // ),
                //   ],
                // ),
                SizedBox(
                  height: 20.0.h,
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                Container(
                  width: 360.0.w,
                  child: !isBackCivilIdUploaded
                      ? InkWell(
                          onTap: () => getFileDialogue('Back_Of_Civil_ID'),
                          child: PreUploadWidget(
                              text: 'Back Of Civil ID',
                              type: 'Back_Of_Civil_ID'),
                        )
                      : InkWell(
                          onTap: () => imagePreview(
                              'Back Of Civil ID', 'Back_Of_Civil_ID'),
                          child: PostUploadWidget(
                              text: 'Back Of Civil ID',
                              type: 'Back_Of_Civil_ID'),
                        ),
                ),
                //     Column(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       crossAxisAlignment: CrossAxisAlignment.center,
                //       children: [
                //         Image(
                //           image: AssetImage(AppImages.DOCUMENT),
                //           height: 20.0.h,
                //           width: 16.0.w,
                //         ),
                //         SizedBox(
                //           height: 6.0.h,
                //         ),
                //         Text(
                //           'View Sample',
                //           style: buildAppTextTheme()
                //               .overline
                //               ?.copyWith(color: AppColors.primaryBlue),
                //           textAlign: TextAlign.start,
                //         ),
                //       ],
                //     ),
                //   ],
                // ),
                SizedBox(
                  height: 20.0.h,
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                Container(
                  width: 360.0.w,
                  child: !isDrivingLicenseUploaded
                      ? InkWell(
                          onTap: () => getFileDialogue('Driving_License'),
                          child: PreUploadWidget(
                              text: 'Driving License', type: 'Driving_License'),
                        )
                      : InkWell(
                          onTap: () => imagePreview(
                              'Driving License', 'Driving_License'),
                          child: PostUploadWidget(
                              text: 'Driving License', type: 'Driving_License'),
                        ),
                ),
                //     Column(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       crossAxisAlignment: CrossAxisAlignment.center,
                //       children: [
                //         Image(
                //           image: AssetImage(AppImages.DOCUMENT),
                //           height: 20.0.h,
                //           width: 16.0.w,
                //         ),
                //         SizedBox(
                //           height: 6.0.h,
                //         ),
                //         Text(
                //           'View Sample',
                //           style: buildAppTextTheme()
                //               .overline
                //               ?.copyWith(color: AppColors.primaryBlue),
                //           textAlign: TextAlign.start,
                //         ),
                //       ],
                //     ),
                //   ],
                // ),
                SizedBox(
                  height: 20.0.h,
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                Container(
                  width: 360.0.w,
                  child: !isCarRegistrationUploaded
                      ? InkWell(
                          onTap: () => getFileDialogue('Car_Registration'),
                          child: PreUploadWidget(
                              text: 'Car Registration',
                              type: 'Car_Registration'),
                        )
                      : InkWell(
                          onTap: () => imagePreview(
                              'Car Registration', 'Car_Registration'),
                          child: PostUploadWidget(
                              text: 'Car Registration',
                              type: 'Car_Registration'),
                        ),
                ),
                //     Column(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       crossAxisAlignment: CrossAxisAlignment.center,
                //       children: [
                //         Image(
                //           image: AssetImage(AppImages.DOCUMENT),
                //           height: 20.0.h,
                //           width: 16.0.w,
                //         ),
                //         SizedBox(
                //           height: 6.0.h,
                //         ),
                //         Text(
                //           'View Sample',
                //           style: buildAppTextTheme()
                //               .overline
                //               ?.copyWith(color: AppColors.primaryBlue),
                //           textAlign: TextAlign.start,
                //         ),
                //       ],
                //     ),
                //   ],
                // ),
                SizedBox(
                  height: 30.0.h,
                ),
                Text(
                  'Please Upload Car Photos',
                  style: buildAppTextTheme().headline5,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 20.0.h,
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                Container(
                  width: 360.0.w,
                  child: !isFrontPhotoUploaded
                      ? InkWell(
                          onTap: () => getFileDialogue('Front_Photo'),
                          child: PreUploadWidget(
                              text: 'Front Photo', type: 'Front_Photo'),
                        )
                      : InkWell(
                          onTap: () =>
                              imagePreview('Front Photo', 'Front_Photo'),
                          child: PostUploadWidget(
                              text: 'Front Photo', type: 'Front_Photo'),
                        ),
                ),
                //     Column(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       crossAxisAlignment: CrossAxisAlignment.center,
                //       children: [
                //         Image(
                //           image: AssetImage(AppImages.DOCUMENT),
                //           height: 20.0.h,
                //           width: 16.0.w,
                //         ),
                //         SizedBox(
                //           height: 6.0.h,
                //         ),
                //         Text(
                //           'View Sample',
                //           style: buildAppTextTheme()
                //               .overline
                //               ?.copyWith(color: AppColors.primaryBlue),
                //           textAlign: TextAlign.start,
                //         ),
                //       ],
                //     ),
                //   ],
                // ),
                SizedBox(
                  height: 20.0.h,
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                Container(
                  width: 360.0.w,
                  child: !isBackPhotoUploaded
                      ? InkWell(
                          onTap: () => getFileDialogue('Back_Photo'),
                          child: PreUploadWidget(
                              text: 'Back Photo', type: 'Back_Photo'),
                        )
                      : InkWell(
                          onTap: () => imagePreview('Back Photo', 'Back_Photo'),
                          child: PostUploadWidget(
                              text: 'Back Photo', type: 'Back_Photo'),
                        ),
                ),
                //     Column(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       crossAxisAlignment: CrossAxisAlignment.center,
                //       children: [
                //         Image(
                //           image: AssetImage(AppImages.DOCUMENT),
                //           height: 20.0.h,
                //           width: 16.0.w,
                //         ),
                //         SizedBox(
                //           height: 6.0.h,
                //         ),
                //         Text(
                //           'View Sample',
                //           style: buildAppTextTheme()
                //               .overline
                //               ?.copyWith(color: AppColors.primaryBlue),
                //           textAlign: TextAlign.start,
                //         ),
                //       ],
                //     ),
                //   ],
                // ),
                SizedBox(
                  height: 20.0.h,
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                Container(
                  width: 360.0.w,
                  child: !isRightPhotoUploaded
                      ? InkWell(
                          onTap: () => getFileDialogue('Right_Side_Photo'),
                          child: PreUploadWidget(
                              text: 'Right Side Photo',
                              type: 'Right_Side_Photo'),
                        )
                      : InkWell(
                          onTap: () => imagePreview(
                              'Right Side Photo', 'Right_Side_Photo'),
                          child: PostUploadWidget(
                              text: 'Right Side Photo',
                              type: 'Right_Side_Photo'),
                        ),
                ),
                //     Column(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       crossAxisAlignment: CrossAxisAlignment.center,
                //       children: [
                //         Image(
                //           image: AssetImage(AppImages.DOCUMENT),
                //           height: 20.0.h,
                //           width: 16.0.w,
                //         ),
                //         SizedBox(
                //           height: 6.0.h,
                //         ),
                //         Text(
                //           'View Sample',
                //           style: buildAppTextTheme()
                //               .overline
                //               ?.copyWith(color: AppColors.primaryBlue),
                //           textAlign: TextAlign.start,
                //         ),
                //       ],
                //     ),
                //   ],
                // ),
                SizedBox(
                  height: 20.0.h,
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                Container(
                  width: 360.0.w,
                  child: !isLeftPhotoUploaded
                      ? InkWell(
                          onTap: () => getFileDialogue('Left_Side_Photo'),
                          child: PreUploadWidget(
                              text: 'Left Side Photo', type: 'Left_Side_Photo'),
                        )
                      : InkWell(
                          onTap: () => imagePreview(
                              'Left Side Photo', 'Left_Side_Photo'),
                          child: PostUploadWidget(
                              text: 'Left Side Photo', type: 'Left_Side_Photo'),
                        ),
                ),
                //     Column(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       crossAxisAlignment: CrossAxisAlignment.center,
                //       children: [
                //         Image(
                //           image: AssetImage(AppImages.DOCUMENT),
                //           height: 20.0.h,
                //           width: 16.0.w,
                //         ),
                //         SizedBox(
                //           height: 6.0.h,
                //         ),
                //         Text(
                //           'View Sample',
                //           style: buildAppTextTheme()
                //               .overline
                //               ?.copyWith(color: AppColors.primaryBlue),
                //           textAlign: TextAlign.start,
                //         ),
                //       ],
                //     ),
                //   ],
                // ),
                SizedBox(
                  height: 20.0.h,
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //   crossAxisAlignment: CrossAxisAlignment.center,
                //   children: [
                Container(
                  width: 360.0.w,
                  child: !isChasisPhotoUploaded
                      ? InkWell(
                          onTap: () =>
                              getFileDialogue('Car_Photo_with_Chasis_No'),
                          child: PreUploadWidget(
                              text: 'Car Photo with Chasis No',
                              type: 'Car_Photo_with_Chasis_No'),
                        )
                      : InkWell(
                          onTap: () => imagePreview('Car Photo with Chasis No',
                              'Car_Photo_with_Chasis_No'),
                          child: PostUploadWidget(
                              text: 'Car Photo with Chasis No',
                              type: 'Car_Photo_with_Chasis_No'),
                        ),
                ),
                //     Column(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       crossAxisAlignment: CrossAxisAlignment.center,
                //       children: [
                //         Image(
                //           image: AssetImage(AppImages.DOCUMENT),
                //           height: 20.0.h,
                //           width: 16.0.w,
                //         ),
                //         SizedBox(
                //           height: 6.0.h,
                //         ),
                //         Text(
                //           'View Sample',
                //           style: buildAppTextTheme()
                //               .overline
                //               ?.copyWith(color: AppColors.primaryBlue),
                //           textAlign: TextAlign.start,
                //         ),
                //       ],
                //     ),
                //   ],
                // ),
                SizedBox(height: 22.0.h),
                PrimaryButton(
                    height: 65.0.h,
                    width: 368.0.w,
                    color: AppColors.primaryBlue,
                    disabled: !(isFrontCivilIdUploaded &&
                        isBackCivilIdUploaded &&
                        isDrivingLicenseUploaded &&
                        isCarRegistrationUploaded &&
                        isFrontPhotoUploaded &&
                        isBackPhotoUploaded &&
                        isChasisPhotoUploaded &&
                        isRightPhotoUploaded &&
                        isLeftPhotoUploaded),
                    isLoading: isLoading,
                    onPressed: () async {
                      if (!isLoading) {
                        setState(() {
                          isLoading = true;
                        });
                        // documentsProvider.documentsList.forEach((element) async {
                        //   String? url = await Utils.uploadDocument(element?.file, element?.type,
                        //       profileProvider.profileData?.customerId ?? '');
                        //   documentsProvider.docsList.add(InsurerDocumentDetailsModel(
                        //       documentName: element?.type, documentUrl: url));
                        // });
                        // await SharedPrefs().addStringPrefs(
                        //     'documents', documentsProvider.documentsList.toString());
                        Navigator.push(
                          context,
                          PageTransition(
                              type: PageTransitionType.fade,
                              child: MotorInsurerDetailsScreen()),
                        );
                        await SharedPrefs().addStringPrefs('documents',
                            documentsProvider.documentsList.toString());

                        setState(() {
                          isLoading = false;
                        });
                      }
                    },
                    text: 'Continue',
                    borderRadius: 80.0.s),
                SizedBox(
                  height: 17.0.h,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
