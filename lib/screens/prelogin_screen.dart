import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:page_transition/page_transition.dart';

import 'package:gt_test/common/buttons/primary_button.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/screens/login_screen.dart';
import 'package:gt_test/screens/register_screen.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import '../common/images.dart';
import '../models/profile_model.dart';
import '../providers/customer_provider.dart';
import '../services/travel_service.dart';
import '../widgets/contact_us_bottom_sheet_widget.dart';
import '../common/shared_prefs.dart';
import '../providers/dashboard_provider.dart';
import '../services/motor_service.dart';
import '../widgets/common/product_offers_widget.dart';

class PreLoginScreen extends StatefulWidget {
  @override
  _PreLoginScreenState createState() => _PreLoginScreenState();
}

class _PreLoginScreenState extends State<PreLoginScreen> {
  DashboardProvider dashboardProvider = DashboardProvider();
  CustomerProvider customerProvider = CustomerProvider();
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    dashboardProvider = Provider.of<DashboardProvider>(context, listen: false);
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    customerProvider.resetSelectedMotorInsuranceList();
    customerProvider.resetSelectedAdditionalMotorInsuranceList();
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      dashboardProvider.setDiscountPlanList =
          await MotorService().getDiscountPlans(context: context);
      dashboardProvider.setMotorInsuranceList = await MotorService()
          .getMotorInsurancePlans(
              context: context, makeYear: DateTime.now().year.toInt());
      dashboardProvider.setTravelInsuranceList = await TravelService()
          .getTravelPlans(
              context: context,
              dob: dateFormat.format(DateTime.now()).toString(),
              planType: 'Individual',
              travelFromDate: dateFormat.format(DateTime.now()).toString(),
              travelToDate: dateFormat.format(DateTime.now()).toString());
      await SharedPrefs().addBoolPrefs('introShown', true);
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => exit(0),
      child: Scaffold(
        backgroundColor: AppColors.pageColor,
        resizeToAvoidBottomInset: false,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage(
                  AppImages.CHORD_BG,
                ),
                fit: BoxFit.fill),
          ),
          child: Container(
            padding: EdgeInsets.only(
                left: 24.0.s, right: 20.0.s, top: 50.0.s, bottom: 16.0.s),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Image(
                      image: AssetImage(AppImages.LOGO1),
                      height: 54.0.h,
                      width: 200.0.w,
                      alignment: Alignment.centerLeft,
                    ),
                    InkWell(
                      onTap: () => showModalBottomSheet(
                          backgroundColor:
                              AppColors.primaryBlue.withOpacity(0.9),
                          barrierColor: AppColors.primaryBlue.withOpacity(0.9),
                          context: context,
                          builder: (BuildContext context) {
                            return ContactUsBottomSheetWidget();
                          }),
                      child: Image(
                        image: AssetImage(AppImages.CALL),
                        height: 40.0.h,
                        width: 40.0.w,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16.0.h,
                ),
                ProductOffersWidget(),
                SizedBox(height: 20.0.h),
                Center(
                  child: PrimaryButton(
                      height: 60.0.h,
                      width: 338.0.w,
                      color: AppColors.primaryBlue,
                      //  disabled: !(this.otpValid),
                      isLoading: isLoading,
                      onPressed: () async {
                        if (!isLoading) {
                          setState(() {
                            isLoading = true;
                          });
                          Navigator.push(
                            context,
                            PageTransition(
                              type: PageTransitionType.fade,
                              child: LoginScreen(),
                            ),
                          );
                          // Navigations.pushNamed(
                          //   context,
                          //   AppRouter.loginScreen,
                          // );
                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      text: 'Login',
                      borderRadius: 80.0.s),
                ),
                SizedBox(height: 10.0.h),
                Center(
                  child: InkWell(
                    onTap: () {
                      //   Navigations.pushNamed(
                      // context,
                      // AppRouter.registerScreen,
                      Navigator.push(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: RegisterScreen(),
                        ),
                      );
                    },
                    child: Text(
                      'New User',
                      style: buildAppTextTheme().headline5,
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                // SizedBox(
                //   height: 12.0.h,
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
