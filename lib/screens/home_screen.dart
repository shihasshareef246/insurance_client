import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:gt_test/services/travel_service.dart';
import 'package:provider/provider.dart';

import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/services/customer_service.dart';
import 'package:gt_test/services/profile_service.dart';
import '../common/images.dart';
import '../models/profile_model.dart';
import '../providers/customer_provider.dart';
import '../providers/dashboard_provider.dart';
import '../providers/profile_provider.dart';
import '../services/motor_service.dart';
import '../widgets/common/product_offers_widget.dart';
import '../widgets/contact_us_bottom_sheet_widget.dart';
import 'init_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TextEditingController otpController = TextEditingController();
  FocusNode otpFocus = FocusNode();
  bool otpValid = false;
  bool otpChanged = false;
  bool isLoading = false;
  late CustomerProvider customerProvider;
  late ProfileProvider profileProvider;
  late DashboardProvider dashboardProvider;
  @override
  void initState() {
    InitScreen.fromInsurerDetails = false;
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    dashboardProvider = Provider.of<DashboardProvider>(context, listen: false);
    customerProvider.resetSelectedAdditionalMotorInsuranceList();
    customerProvider.resetSelectedMotorInsuranceList();
    customerProvider.resetInsuranceDetails();
    customerProvider.resetInsurerMotorDetails();
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      dashboardProvider.setDiscountPlanList =
          await MotorService().getDiscountPlans(context: context);
      dashboardProvider.setMotorInsuranceList = await MotorService()
          .getMotorInsurancePlans(
              context: context, makeYear: DateTime.now().year.toInt());
      dashboardProvider.setTravelInsuranceList = await TravelService()
          .getTravelPlans(
              context: context,
              dob: dateFormat.format(DateTime.now()).toString(),
              planType: 'Individual',
              travelFromDate: dateFormat.format(DateTime.now()).toString(),
              travelToDate: dateFormat.format(DateTime.now()).toString());
      customerProvider.setInsuranceList = await CustomerService()
          .getInsurerInsurancePlans(
              context: context,
              customerId: profileProvider.profileData?.customerId);
      customerProvider.setClaimsList = await CustomerService()
          .getInsurerClaimDetails(
              context: context,
              customerId: profileProvider.profileData?.customerId);
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: false,
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(
                    AppImages.CHORD_BG,
                  ),
                  fit: BoxFit.fill),
            ),
            child: Container(
              padding: EdgeInsets.only(
                left: 24.0.s,
                right: 20.0.s,
                top: 36.0.s,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Image(
                        image: AssetImage(AppImages.FEATURES),
                        height: 52.0.h,
                        width: 82.0.w,
                        alignment: Alignment.centerLeft,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Image(
                            image: AssetImage(AppImages.NOTIFICATIONS),
                            height: 40.0.h,
                            width: 40.0.w,
                          ),
                          SizedBox(
                            width: 16.0.w,
                          ),
                          InkWell(
                            onTap: () => showModalBottomSheet(
                                backgroundColor:
                                    AppColors.primaryBlue.withOpacity(0.9),
                                barrierColor:
                                    AppColors.primaryBlue.withOpacity(0.9),
                                context: context,
                                builder: (BuildContext context) {
                                  return ContactUsBottomSheetWidget();
                                }),
                            child: Image(
                              image: AssetImage(AppImages.CALL),
                              height: 40.0.h,
                              width: 40.0.w,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16.0.h,
                  ),
                  Text(
                    (profileProvider?.profileData?.customerName != null &&
                            profileProvider?.profileData?.customerName != '')
                        ? 'Hello, ${profileProvider?.profileData?.customerName ?? ''}'
                        : 'Hello, ${profileProvider?.profileData?.phoneNo ?? ''}',
                    style: buildAppTextTheme().headline2,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 12.0.h,
                  ),
                  ProductOffersWidget(),
                  //  SizedBox(height: 24.0.h),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
