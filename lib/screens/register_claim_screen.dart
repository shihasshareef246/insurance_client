import 'dart:io';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:gt_test/models/claim_docs_model.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:toast/toast.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

import 'package:gt_test/common/buttons/primary_button.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/navigator/navigations.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/screens/claims_screen.dart';
import 'package:gt_test/screens/policy_guidelines_screen.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/widgets/common/post_upload_widget.dart';
import 'package:uuid/uuid.dart';
import '../common/images.dart';
import '../common/logger.dart';
import '../common/routes/routes.dart';
import '../common/utils.dart';
import '../models/file_model.dart';
import '../providers/customer_provider.dart';
import '../providers/profile_provider.dart';
import '../services/customer_service.dart';
import '../widgets/common/upload_bottom_sheet_widget.dart';

class RegisterClaimScreen extends StatefulWidget {
  final int? insuranceId;
  final int? planId;
  final int? transactionId;
  final String? claimDate;
  final String? claimReason;
  final String? lossDate;
  final String? customerId;
  final int? claimId;
  final bool isEdit;

  RegisterClaimScreen({
    this.insuranceId,
    this.planId,
    this.transactionId,
    this.claimDate,
    this.claimReason,
    this.lossDate,
    this.customerId,
    this.claimId,
    this.isEdit = false,
  });
  @override
  _RegisterClaimScreenState createState() => _RegisterClaimScreenState();
}

class _RegisterClaimScreenState extends State<RegisterClaimScreen> {
  var uuid = Uuid();
  final picker = ImagePicker();
  File? file = File("");
  String fileExtension = "";
  List<FileModel> documents = [];
  List<ClaimDocsModel> claimDocs = [];
  TextEditingController reportController = TextEditingController();
  FocusNode reportFocus = FocusNode();
  TextEditingController dateController = TextEditingController();
  FocusNode dobFocus = FocusNode();
  bool dobValid = false;
  bool dobChanged = false;
  final dateFormat = DateFormat("dd-MM-yyyy");
  final timeFormat = DateFormat("h:mm a");
  ProfileProvider profileProvider = ProfileProvider();
  late CustomerProvider customerProvider;

  bool reportValid = false;
  bool reportChanged = false;
  bool isLoading = false;
  bool isUploading = false;
  bool isPolicyDocsUploaded = false;
  bool isOtherDocsUploaded = false;
  bool isDocUploaded = false;

  void editImagePreview(String? doc, String? url, String type) {
    showModalBottomSheet<dynamic>(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (url != null)
                  Container(
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0.s),
                        topRight: Radius.circular(20.0.s),
                      ),
                    ),
                    child: Stack(
                      children: [
                        Container(
                          height: 226.0.h,
                          width: 368.0.w,
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0.s),
                              topRight: Radius.circular(20.0.s),
                            ),
                            child: Image(
                              image: NetworkImage(url ?? ''),
                              height: 40.0.h,
                              width: 360.0.w,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Positioned(
                          top: 20.0.s,
                          right: 20.0.s,
                          child: InkWell(
                            child: CircleAvatar(
                              backgroundColor: AppColors.brownish,
                              child: Icon(Icons.close,
                                  size: 20.0.ics, color: AppColors.primaryBlue),
                            ),
                            onTap: () => Navigator.pop(context),
                          ),
                        ),
                      ],
                    ),
                  ),
                Container(
                  height: 226.0.h,
                  width: 368.0.w,
                  padding: EdgeInsets.all(20.0.s),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20.0.s),
                      bottomLeft: Radius.circular(20.0.s),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Claim document',
                            style: buildAppTextTheme().headline4,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 11.0.h,
                      ),
                    ],
                  ),
                ),
              ]);
        });
  }

  void imagePreview(
    File? doc,
    String ext,
    String name,
    String type,
    String date,
    String time,
  ) {
    showModalBottomSheet<dynamic>(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (doc != null)
                  Container(
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0.s),
                        topRight: Radius.circular(20.0.s),
                      ),
                    ),
                    child: Stack(
                      children: [
                        Container(
                          height: 226.0.h,
                          width: 368.0.w,
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20.0.s),
                              topRight: Radius.circular(20.0.s),
                            ),
                            child: Image(
                              image: FileImage(doc!),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Positioned(
                          top: 20.0.s,
                          right: 20.0.s,
                          child: InkWell(
                            child: CircleAvatar(
                              backgroundColor: AppColors.brownish,
                              child: Icon(Icons.close,
                                  size: 20.0.ics, color: AppColors.primaryBlue),
                            ),
                            onTap: () => Navigator.pop(context),
                          ),
                        ),
                      ],
                    ),
                  ),
                Container(
                  height: 226.0.h,
                  width: 368.0.w,
                  padding: EdgeInsets.all(20.0.s),
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20.0.s),
                      bottomLeft: Radius.circular(20.0.s),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Claim document',
                            style: buildAppTextTheme().headline4,
                            textAlign: TextAlign.center,
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                              setState(() {
                                documents.removeWhere(
                                    (element) => element.file == doc);
                                claimDocs.removeWhere(
                                    (element) => element.DocumentName == name);
                              });
                            },
                            child: Image(
                              image: AssetImage(AppImages.DELETE),
                              height: 33.0.h,
                              width: 33.0.w,
                              //color: AppColors.primaryBlue,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 11.0.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            date!,
                            style: buildAppTextTheme().caption,
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            time!,
                            style: buildAppTextTheme().caption,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 11.0.h,
                      ),
                      Text(
                        'Format : $ext',
                        style: buildAppTextTheme().subtitle2,
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ]);
        });
  }

  void getFileDialogue(String type) {
    showModalBottomSheet(
        backgroundColor: AppColors.primaryBlue.withOpacity(0.9),
        barrierColor: AppColors.primaryBlue.withOpacity(0.9),
        context: context,
        builder: (BuildContext context) {
          return UploadBottomSheetWidget(
            title: 'Upload image via',
            onClick: () async {
              Navigator.pop(context);
              getFileFromCamera(type);
            },
            image: AppImages.CAMERA,
            listTitle: 'Camera',
            onClick1: () async {
              Navigator.pop(context);
              getFileFromGallery(type);
            },
            image1: AppImages.GALLERY,
            listTitle1: 'Gallery',
            onClick2: () async {
              Navigator.pop(context);
              getFileFromDocument(type);
            },
            image2: Icons.attach_file_sharp,
            listTitle2: 'Document',
          );
        });
  }

  Future getFileFromCamera(String type) async {
    setState(() {
      isUploading = true;
    });
    final pickedFile =
        await picker.pickImage(source: ImageSource.camera, imageQuality: 50);
    var name = uuid.v1().substring(0, 4);
    if (pickedFile != null) {
      setState(() {
        file = File(pickedFile.path);
        fileExtension =
            pickedFile.path.substring(pickedFile.path.lastIndexOf('.') + 1);
        if (type == 'Policy') {
          isPolicyDocsUploaded = true;
        } else if (type == 'Other') {
          isOtherDocsUploaded = true;
        } else {
          documents.add(FileModel(
              file: file!,
              type: 'Claim_Document_$name',
              extension: fileExtension,
              date: dateFormat.format(DateTime.now()),
              time: timeFormat.format(DateTime.now())));
          isDocUploaded = true;
        }
      });
      String? url = await Utils.uploadDocument(file, 'Claim_Document_$name',
          fileExtension, profileProvider.profileData?.customerId ?? '');
      claimDocs.add(ClaimDocsModel(
          DocumentName: 'Claim_Document_$name', DocumentUrl: url ?? ''));
      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
    setState(() {
      isUploading = false;
    });
  }

  Future getFileFromGallery(String type) async {
    setState(() {
      isUploading = true;
    });
    final pickedFile =
        await picker.pickImage(source: ImageSource.gallery, imageQuality: 50);
    var name = uuid.v1().substring(0, 4);
    if (pickedFile != null) {
      setState(() {
        file = File(pickedFile.path);
        fileExtension =
            pickedFile.path.substring(pickedFile.path.lastIndexOf('.') + 1);
        if (type == 'Policy') {
          isPolicyDocsUploaded = true;
        } else if (type == 'Other') {
          isOtherDocsUploaded = true;
        } else {
          documents.add(FileModel(
              file: file!,
              type: 'Claim_Document_$name',
              extension: fileExtension,
              date: dateFormat.format(DateTime.now()),
              time: timeFormat.format(DateTime.now())));
          isDocUploaded = true;
        }
      });
      String? url = await Utils.uploadDocument(file, 'Claim_Document_$name',
          fileExtension, profileProvider.profileData?.customerId ?? '');
      claimDocs.add(ClaimDocsModel(
          DocumentName: 'Claim_Document_$name', DocumentUrl: url ?? ''));
      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
    setState(() {
      isUploading = false;
    });
  }

  Future getFileFromDocument(String type) async {
    setState(() {
      isUploading = true;
    });
    FilePickerResult? pickedFile =
        await FilePicker.platform.pickFiles(type: FileType.any);
    var name = uuid.v1().substring(0, 4);
    if (pickedFile != null) {
      setState(() {
        print('file picked');
        file = File(pickedFile.files.single.path!);
        fileExtension = pickedFile.files.single.extension!;
        if (type == 'Policy') {
          isPolicyDocsUploaded = true;
        } else if (type == 'Other') {
          isOtherDocsUploaded = true;
        } else {
          documents.add(FileModel(
              file: file!,
              type: 'Claim_Document_$name',
              extension: fileExtension,
              date: dateFormat.format(DateTime.now()),
              time: timeFormat.format(DateTime.now())));
          isDocUploaded = true;
        }
      });
      String? url = await Utils.uploadDocument(file, 'Claim_Document_$name',
          fileExtension, profileProvider.profileData?.customerId ?? '');
      claimDocs.add(ClaimDocsModel(
          DocumentName: 'Claim_Document_$name', DocumentUrl: url ?? ''));
      Logger.d('Upload Success', '');
    } else {
      Logger.d('No file selected', '');
    }
    setState(() {
      isUploading = false;
    });
  }

  @override
  void initState() {
    isPolicyDocsUploaded = false;
    isOtherDocsUploaded = false;
    isDocUploaded = false;
    ToastContext().init(context);
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    if (!widget.isEdit) {
      customerProvider.resetClaimDocuments();
    }
    reportController.text = widget.claimReason ?? '';
    dateController.text = widget.lossDate ?? '';
    if (widget.isEdit) {
      reportValid = true;
      dobValid = true;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Navigations.pop(context),
      child: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.primaryBlue,
            leading: InkWell(
              onTap: () => Navigations.pop(context),
              child: Icon(
                Icons.arrow_back_ios,
                color: AppColors.white,
                size: 20.0.ics,
              ),
            ),
            title: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Register Claim',
                    style: buildAppTextTheme()
                        .headline3
                        ?.copyWith(color: AppColors.white),
                    textAlign: TextAlign.start,
                  ),
                ]),
          ),
          backgroundColor: AppColors.pageColor,
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.only(
                  left: 30.0.s, right: 30.0.s, top: 23.0.s, bottom: 60.0.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (widget.isEdit)
                    Text(
                      'ID : #${widget.claimId ?? ''}',
                      style: buildAppTextTheme().headline3,
                      textAlign: TextAlign.start,
                    ),
                  if (widget.isEdit)
                    SizedBox(
                      height: 20.0.h,
                    ),
                  Text(
                    'Incident report',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 15.0.h,
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: 20.0.s, right: 20.0.s, top: 20.0.s),
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      border: Border.all(color: AppColors.border, width: 1.0),
                      borderRadius: BorderRadius.circular(
                        6.0.s,
                      ),
                    ),
                    child: TextFormField(
                      scrollPadding: EdgeInsets.only(bottom: 100),
                      focusNode: reportFocus,
                      style: reportFocus.hasFocus
                          ? buildAppTextTheme().headline5
                          : buildAppTextTheme()
                              .subtitle1!
                              .copyWith(color: AppColors.black),
                      keyboardType: TextInputType.name,
                      controller: reportController,
                      obscureText: false,
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.only(
                          left: 10.0.s,
                          top: 8.0.s,
                          bottom: 8.0.s,
                          right: 10.0.s,
                        ),
                        labelText: 'Short Incident report description',
                        labelStyle: reportFocus.hasFocus
                            ? buildAppTextTheme().subtitle2
                            : buildAppTextTheme().subtitle2,
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: AppColors.primaryBlue),
                          borderRadius: BorderRadius.vertical(
                            top: Radius.circular(10.0.s),
                            bottom: Radius.zero,
                          ),
                        ),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      onChanged: (value) {
                        setState(() {
                          this.reportChanged = true;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            this.reportValid = false;
                          });
                        } else {
                          setState(() {
                            this.reportValid = true;
                          });
                        }
                      },
                    ),
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  Text(
                    'Date of Incident',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 10.0.h,
                  ),
                  Container(
                    padding: EdgeInsets.all(20.0.s),
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      border: Border.all(color: AppColors.border, width: 1.0),
                      borderRadius: BorderRadius.circular(
                        6.0.s,
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Please enter the date of accident for furthur claims and receive money from claims!',
                          style: buildAppTextTheme().subtitle2,
                          textAlign: TextAlign.start,
                        ),
                        SizedBox(
                          height: 20.0.h,
                        ),
                        Container(
                          height: 60.0.h,
                          width: 368.0.w,
                          decoration: BoxDecoration(
                            color: dobFocus.hasFocus
                                ? AppColors.paleBlue
                                : AppColors.white,
                            border: Border.all(color: AppColors.border),

                            // border: Border(
                            //     top: BorderSide(color: AppColors.border, width: 1.0),
                            //     bottom:
                            //         BorderSide(color: AppColors.border, width: 1.0),
                            //     right:
                            //         BorderSide(color: AppColors.border, width: 1.0)),
                            borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(6.0.s),
                              topRight: Radius.circular(6.0.s),
                            ),
                          ),
                          child: DateTimeField(
                            focusNode: dobFocus,
                            style: dobFocus.hasFocus
                                ? buildAppTextTheme().headline5
                                : buildAppTextTheme()
                                    .subtitle1!
                                    .copyWith(color: AppColors.black),
                            decoration: InputDecoration(
                              labelText: 'dd/mm/yyyy',
                              labelStyle: dobFocus.hasFocus
                                  ? buildAppTextTheme().subtitle2
                                  : buildAppTextTheme().subtitle2,
                              focusColor: AppColors.searchBlue,
                              isDense: true,
                              contentPadding: EdgeInsets.only(
                                left: 10.0.s,
                                top: 8.0.s,
                                bottom: 8.0.s,
                                right: 10.0.s,
                              ),
                              border: InputBorder.none,
                            ),
                            controller: dateController,
                            onChanged: (value) {
                              setState(() {
                                this.dobChanged = true;
                              });
                              if (value != null &&
                                  value.month <= 12 &&
                                  value.month >= 1 &&
                                  value.year <= 2100 &&
                                  value.year >= 1900 &&
                                  value.day <= 31 &&
                                  value.day >= 1) {
                                setState(() {
                                  this.dobValid = true;
                                });
                              } else {
                                setState(() {
                                  this.dobValid = false;
                                });
                              }
                            },
                            format: dateFormat,
                            onShowPicker: (context, currentValue) {
                              return showDatePicker(
                                  context: context,
                                  firstDate: DateTime(1900),
                                  initialDate: currentValue ?? DateTime.now(),
                                  lastDate: DateTime(2100));
                            },
                          ),
                        ),
                        // Container(
                        //   padding: EdgeInsets.symmetric(
                        //       horizontal: 14.0.s, vertical: 18.0.s),
                        //   decoration: BoxDecoration(
                        //     color: AppColors.white,
                        //     border:
                        //         Border.all(color: AppColors.border, width: 1.0),
                        //     borderRadius: BorderRadius.circular(
                        //       6.0.s,
                        //     ),
                        //   ),
                        //   child: Row(
                        //     mainAxisAlignment: MainAxisAlignment.start,
                        //     crossAxisAlignment: CrossAxisAlignment.center,
                        //     children: [
                        //       Image(
                        //         image: AssetImage(AppImages.DATE),
                        //         height: 20.0.h,
                        //         width: 16.0.w,
                        //       ),
                        //       SizedBox(
                        //         width: 15.0.w,
                        //       ),
                        //       Text(
                        //         'dd/mm/yyyy',
                        //         style: buildAppTextTheme().headline6,
                        //         textAlign: TextAlign.center,
                        //       ),
                        //     ],
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                  // Container(
                  //   height: 56.0.h,
                  //   width: 360.0.w,
                  //   decoration: BoxDecoration(
                  //     color: dobFocus.hasFocus
                  //         ? AppColors.paleBlue
                  //         : AppColors.paleBrown,
                  //     border: Border.all(color: AppColors.border, width: 1.0),
                  //     borderRadius: BorderRadius.circular(
                  //       6.0.s,
                  //     ),
                  //   ),
                  //   child: DateTimeField(
                  //     focusNode: dobFocus,
                  //     style: dobFocus.hasFocus
                  //         ? buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.lightBlack)
                  //         : buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.focusText),
                  //     decoration: InputDecoration(
                  //       labelText: 'DOB',
                  //       labelStyle: dobFocus.hasFocus
                  //           ? buildAppTextTheme()
                  //               .subtitle1!
                  //               .copyWith(color: AppColors.lightGrey)
                  //           : buildAppTextTheme().subtitle1,
                  //       focusColor: AppColors.searchBlue,
                  //       isDense: true,
                  //       contentPadding: EdgeInsets.only(
                  //         left: 10.0.s,
                  //         top: 8.0.s,
                  //         bottom: 8.0.s,
                  //         right: 10.0.s,
                  //       ),
                  //       border: InputBorder.none,
                  //     ),
                  //     controller: dobController,
                  //     onChanged: (value) {
                  //       setState(() {
                  //         this.dobChanged = true;
                  //       });
                  //       if (value != null &&
                  //           value.month != null &&
                  //           value.year != null &&
                  //           value.day != null &&
                  //           value.month <= 12 &&
                  //           value.month >= 1 &&
                  //           value.year <= 2100 &&
                  //           value.year >= 1900 &&
                  //           value.day <= 31 &&
                  //           value.day >= 1) {
                  //         setState(() {
                  //           this.dobValid = true;
                  //         });
                  //       } else {
                  //         setState(() {
                  //           this.dobValid = false;
                  //         });
                  //       }
                  //     },
                  //     format: dateFormat,
                  //     onShowPicker: (context, currentValue) {
                  //       return showDatePicker(
                  //           context: context,
                  //           firstDate: DateTime(1900),
                  //           initialDate: currentValue ?? DateTime.now(),
                  //           lastDate: DateTime(2100));
                  //     },
                  //   ),
                  // ),
                  // (this.dobChanged && !this.dobValid && !dobFocus.hasFocus)
                  //     ? Text(
                  //         'Enter Valid DOB',
                  //         style: buildAppTextTheme()
                  //             .subtitle1!
                  //             .copyWith(color: AppColors.darkRed),
                  //       )
                  //     : SizedBox(height: 20.0.h),
                  SizedBox(
                    height: 30.0.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Policy guidelines',
                        style: buildAppTextTheme().headline4,
                        textAlign: TextAlign.center,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            PageTransition(
                              type: PageTransitionType.fade,
                              child: PolicyGuidelinesScreen(),
                            ),
                          );
                          // Navigations.pushNamed(
                          // context,
                          // AppRouter.policyGuidelineScreen,
                          //    );
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'View Now',
                              style: buildAppTextTheme().headline6,
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              width: 5.0.w,
                            ),
                            Icon(
                              Icons.arrow_forward_ios,
                              color: AppColors.primaryBlue,
                              size: 20.0.ics,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  customerProvider.claimDocs.isNotEmpty
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                              Text(
                                'Uploaded documents',
                                style: buildAppTextTheme().headline4,
                                textAlign: TextAlign.center,
                              ),
                              SizedBox(
                                height: 20.0.h,
                              ),
                              ListView.builder(
                                  padding: EdgeInsets.zero,
                                  itemCount: customerProvider.claimDocs.length,
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                        onTap: () => editImagePreview(
                                            customerProvider
                                                .claimDocs[index]?.DocumentName,
                                            customerProvider
                                                .claimDocs[index]?.DocumentUrl,
                                            'Claim_Document'),
                                        child: PostUploadWidget(
                                          text: 'Claim Document',
                                          type: 'Claim_Document',
                                        ));
                                  })
                            ])
                      : Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'Please upload documents',
                                  style: buildAppTextTheme().headline4,
                                  textAlign: TextAlign.center,
                                ),
                                InkWell(
                                  onTap: () => getFileDialogue(''),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Add',
                                        style: buildAppTextTheme().headline6,
                                        textAlign: TextAlign.center,
                                      ),
                                      SizedBox(
                                        width: 5.0.w,
                                      ),
                                      Icon(
                                        Icons.add,
                                        color: AppColors.primaryBlue,
                                        size: 20.0.ics,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20.0.h,
                            ),
                            if (documents.isNotEmpty)
                              ListView.builder(
                                itemCount: documents.length,
                                shrinkWrap: true,
                                scrollDirection: Axis.vertical,
                                itemBuilder: (context, index) {
                                  return Container(
                                    padding: EdgeInsets.only(bottom: 10.0.s),
                                    child: InkWell(
                                      onTap: () => imagePreview(
                                        documents[index].file,
                                        documents[index].extension,
                                        documents[index].type,
                                        'Claim_Document',
                                        documents[index].date,
                                        documents[index].time,
                                      ),
                                      child: PostUploadWidget(
                                        text: 'Claim Document ${index + 1}',
                                        type: 'Claim_Document',
                                      ),
                                    ),
                                  );
                                },
                              ),
                          ],
                        ),

                  // !isPolicyDocsUploaded
                  //     ? InkWell(
                  //         onTap: () => getFileDialogue('Policy'),
                  //         child: PreUploadWidget(text: 'Policy documents'))
                  //     : PostUploadWidget(text: 'Policy documents'),
                  // SizedBox(
                  //   height: 20.0.h,
                  // ),
                  // !isOtherDocsUploaded
                  //     ? InkWell(
                  //         onTap: () => getFileDialogue('Other'),
                  //         child: PreUploadWidget(text: 'Other documents'))
                  //     : PostUploadWidget(text: 'Other documents'),
                  SizedBox(height: 54.0.h),
                ],
              ),
            ),
          ),
          bottomSheet: Container(
            padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
            child: PrimaryButton(
                height: 65.0.h,
                width: 400.0.w,
                color: AppColors.primaryBlue,
                isLoading: isLoading,
                disabled: !(this.dobValid && this.reportValid && !isUploading),
                onPressed: () async {
                  if (!isLoading) {
                    setState(() {
                      isLoading = true;
                    });

                    if (widget.isEdit) {
                      var response = await CustomerService().updateClaim(
                          context: context,
                          insuranceId: 1,
                          planId: widget.planId,
                          claimDate:
                              dateFormat.format(DateTime.now()).toString(),
                          claimReason: reportController.text,
                          lossDate: dateController.text,
                          claimId: widget.claimId,
                          customerId:
                              profileProvider.profileData?.customerId ?? '');
                      if (response != null && response.status != '') {
                        Toast.show('Claim updated successfully',
                            backgroundColor: AppColors.green,
                            textStyle: buildAppTextTheme().subtitle1!.copyWith(
                                  color: AppColors.white,
                                ),
                            duration: 3,
                            gravity: 0);
                        Navigations.pushReplacementNamed(
                            context, AppRouter.claimsScreen);
                        // Navigator.push(
                        //   context,
                        //   PageTransition(
                        //     type: PageTransitionType.fade,
                        //     child: BaseLayout(
                        //       page: ClaimsScreen(),
                        //       activePageIndex: 2,
                        //     ),
                        //   ),
                        // );
                      }
                    } else {
                      //  await uploadDoc().then((docs) async {
                      var response = await CustomerService().registerClaims(
                          context: context,
                          insuranceId: 1,
                          planId: widget.planId,
                          claimDate:
                              dateFormat.format(DateTime.now()).toString(),
                          claimReason: reportController.text,
                          lossDate: dateController.text,
                          customerId:
                              profileProvider.profileData?.customerId ?? '',
                          claimDocs: claimDocs);
                      if (response != null && response == 200) {
                        Toast.show('Claim saved successfully',
                            backgroundColor: AppColors.green,
                            textStyle: buildAppTextTheme().subtitle1!.copyWith(
                                  color: AppColors.white,
                                ),
                            duration: 3,
                            gravity: 0);
                        Navigations.pushReplacementNamed(
                            context, AppRouter.claimsScreen);
                        // Navigator.push(
                        //   context,
                        //   PageTransition(
                        //     type: PageTransitionType.fade,
                        //     child: BaseLayout(
                        //       page: ClaimsScreen(),
                        //       activePageIndex: 2,
                        //     ),
                        //   ),
                        // );
                        // Navigations.pushNamed(
                        //   context,
                        //   AppRouter.claimsScreen,
                        // );
                      }
                      //      });
                    }
                    setState(() {
                      isLoading = false;
                    });
                  }
                },
                text: 'Claim',
                borderRadius: 80.0.s),
          ),
        ),
      ),
    );
  }
}
