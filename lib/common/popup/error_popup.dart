import 'package:flutter/material.dart';

import '../buttons/primary_button.dart';
import '../colors.dart';
import '../responsiveness.dart';
import '../theme/app_theme.dart';

class ErrorPopUp extends StatefulWidget {
  final String message;
  final Function onClick;

  ErrorPopUp({this.message = '', required this.onClick});
  @override
  _ErrorPopUpState createState() => _ErrorPopUpState();
}

class _ErrorPopUpState extends State<ErrorPopUp> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15.0.s, horizontal: 15.0.s),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(
          50.0.s,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'Error',
            style: buildAppTextTheme().headline6!.copyWith(
                color: AppColors.darkRed, fontWeight: FontWeight.w700),
          ),
          SizedBox(height: 30.0.h),
          Center(
            child: Text(
              widget.message,
              style: buildAppTextTheme().headline6!.copyWith(
                  color: AppColors.lightBlack, fontWeight: FontWeight.w700),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(height: 20.0.h),
          PrimaryButton(
            onPressed: () {
              widget.onClick != null
                  ? widget.onClick()
                  : Navigator.pop(
                      context,
                    );
            },
            text: 'OK',
            color: AppColors.primaryBlue,
            disabled: false,
          ),
        ],
      ),
    );
  }
}
