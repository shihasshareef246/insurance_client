import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../buttons/primary_button.dart';
import '../colors.dart';
import '../images.dart';
import '../navigator/navigations.dart';
import '../responsiveness.dart';
import '../routes/routes.dart';
import '../theme/app_theme.dart';

class CodPopUp extends StatefulWidget {
  final String message;
  final String amount;
  final Function onClick;

  CodPopUp({this.message = '', this.amount = '', required this.onClick});
  @override
  _CodPopUpState createState() => _CodPopUpState();
}

class _CodPopUpState extends State<CodPopUp> {
  final dateFormat = DateFormat("dd-MM-yyyy");
  final timeFormat = DateFormat("dd-MM-yyyy HH:mm:ss");

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15.0.s, horizontal: 30.0.s),
      child: Column(
        children: [
          Image(
            image: AssetImage(AppImages.PAYMENT_SUCCESS),
            height: 150.0.h,
            width: 150.0.w,
          ),
          SizedBox(height: 20.0.h),
          Text(
            'Success',
            style: buildAppTextTheme().headline6!.copyWith(fontSize: 24.0.f),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20.0.h),
          Text(
            'You have successfully applied for motor insurance policy. Our representative will contact you soon for making the payment.',
            style: buildAppTextTheme().subtitle2,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 30.0.h),
          Text(
            '${widget.amount} / KWD',
            style: buildAppTextTheme().headline1,
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 40.0.h),
          PrimaryButton(
            width: 216.0.w,
            height: 65.0.h,
            onPressed: () async {
              Navigator.pop(
                context,
              );
              Navigations.pushNamed(
                context,
                AppRouter.homeScreen,
              );
            },
            text: 'Done',
            disabled: false,
            color: AppColors.primaryBlue,
          ),
        ],
      ),
    );
  }
}
