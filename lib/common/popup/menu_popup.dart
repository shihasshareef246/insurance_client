import 'package:flutter/material.dart';
import 'dart:ui';

import '../navigator/navigations.dart';
import '../responsiveness.dart';

class MenuPopUp {
  final Color backgroundColor;
  final Widget childcontainer;
  final Function closeClick;
  final double crossIconSize;
  final Color iconColor;
  final double modalWidth;
  final double actualWidth;
  final double leftPadding;
  final double rightPadding;
  final double topPadding;
  final double bottomPadding;
  final bool crossIconRequired;
  final bool barrierDismissable;
  final Color barrierColor;
  MenuPopUp({
    required this.backgroundColor,
    required this.childcontainer,
    required this.closeClick,
    this.crossIconSize = 30.0,
    required this.iconColor,
    this.modalWidth = 0.8,
    this.actualWidth = 0,
    this.leftPadding = 0,
    this.rightPadding = 0,
    this.topPadding = 0,
    this.bottomPadding = 0,
    this.crossIconRequired = false,
    this.barrierDismissable = true,
    required this.barrierColor,
  });

  void showPopup(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          backgroundColor: Colors.white,
          contentPadding: EdgeInsets.all(0.0.s),
          insetPadding: EdgeInsets.only(
            right: rightPadding,
            left: leftPadding,
            bottom: bottomPadding,
            top: topPadding,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              9.0.s,
            ),
          ),
          content: Container(
            width: actualWidth,
            child: Stack(
              children: [
                Container(
                  child: childcontainer,
                ),
                if (crossIconRequired)
                  Positioned(
                    right: 0,
                    top: 0,
                    bottom: 0,
                    left: 0,
                    child: IconButton(
                      icon: Icon(
                        Icons.close,
                        color: iconColor,
                        size: crossIconSize.ics,
                      ),
                      onPressed: () {
                        if (closeClick != null) {
                          closeClick();
                        }
                        Navigations.pop(context);
                      },
                    ),
                  ),
              ],
            ),
          ),
        );
      },
      barrierDismissible: barrierDismissable,
      barrierColor: barrierColor,
    );
  }
}
