import 'package:flutter/material.dart';

import '../navigator/navigations.dart';
import '../responsiveness.dart';

class ModalPopUp {
  final Color backgroundColor;
  final Widget childcontainer;
  final Function closeClick;
  final double crossIconSize;
  final Color iconColor;
  final double iconPositionLeft;
  final double iconPositionRight;
  final double iconPositionTop;
  final double modalWidth;
  final double actualWidth;
  final bool crossIconRequired;
  final bool barrierDismissable;
  final Color barrierColor;
  ModalPopUp({
    required this.backgroundColor,
    required this.childcontainer,
    required this.closeClick,
    this.crossIconSize = 30.0,
    required this.iconColor,
    this.iconPositionLeft = 0.0,
    this.iconPositionTop = 0.0,
    this.iconPositionRight = 0.0,
    this.modalWidth = 0.8,
    this.actualWidth = 0,
    this.crossIconRequired = true,
    this.barrierDismissable = true,
    required this.barrierColor,
  });

  void showPopup(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) {
        return AlertDialog(
          backgroundColor: backgroundColor,
          contentPadding: EdgeInsets.all(0.0.s),
          insetPadding: EdgeInsets.all(0.0.s),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              16.0.s,
            ),
          ),
          content: IntrinsicHeight(
            child: Container(
              width: actualWidth,
              child: Stack(
                children: [
                  Container(
                    child: childcontainer,
                  ),
                  if (crossIconRequired)
                    Positioned(
                      right: iconPositionRight,
                      top: iconPositionTop,
                      left: iconPositionLeft,
                      child: IconButton(
                        icon: Icon(
                          Icons.close,
                          color: iconColor,
                          size: crossIconSize,
                        ),
                        onPressed: () {
                          if (closeClick != null) {
                            closeClick();
                          }
                          Navigations.pop(context);
                        },
                      ),
                    ),
                ],
              ),
            ),
          ),
        );
      },
      barrierDismissible: barrierDismissable,
      barrierColor: barrierColor,
    );
  }
}
