import 'package:flutter/material.dart';

import '../buttons/primary_button.dart';
import '../colors.dart';
import '../responsiveness.dart';
import '../theme/app_theme.dart';

class ConfirmPopUp extends StatefulWidget {
  final String message;
  final Function onClick;

  ConfirmPopUp({this.message = '', required this.onClick});
  @override
  _ConfirmPopUpState createState() => _ConfirmPopUpState();
}

class _ConfirmPopUpState extends State<ConfirmPopUp> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15.0.s, horizontal: 30.0.s),
      child: Column(
        children: [
          Text(
            widget.message,
            style: buildAppTextTheme().headline6!.copyWith(
                color: AppColors.lightBlack, fontWeight: FontWeight.w700),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 15.0.h),
          Divider(),
          SizedBox(height: 10.0.h),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              PrimaryButton(
                width: 110.0.w,
                height: 48.0.h,
                onPressed: () => widget.onClick(),
                text: 'Yes',
                disabled: false,
                color: AppColors.primaryBlue,
              ),
              PrimaryButton(
                width: 110.0.w,
                height: 48.0.h,
                onPressed: () {
                  Navigator.pop(
                    context,
                  );
                },
                text: 'No',
                disabled: false,
                color: AppColors.primaryBlue,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
