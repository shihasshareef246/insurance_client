import 'package:flutter/foundation.dart';

import 'enum.dart';

class AppEnvironment {
  static String devUrl = 'http://live.v2.app.fusionezee.com:5000/fz-api/';
  static String prodUrl = 'http://live.v2.app.fusionezee.com:5000/fz-api/';
  static String serverUrl = '';
  static String appVersion = '1.0.0';

  setEnvironment() {
    if (kReleaseMode) {
      print('release mode');
      serverUrl = prodUrl;
    } else {
      print('debug mode');
      serverUrl = devUrl;
    }
  }

  setEnvironmentOnTapEvent(AppEnvironments appEnvironments) {
    switch (appEnvironments) {
      case AppEnvironments.dev:
        serverUrl = devUrl;
        break;
      case AppEnvironments.prod:
        serverUrl = prodUrl;
        break;
      case AppEnvironments.none:
        serverUrl = devUrl;
        break;
    }
  }
}