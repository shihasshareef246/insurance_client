import 'package:flutter/material.dart';

import 'package:gt_test/common/theme/app_theme.dart';
import '../colors.dart';
import '../responsiveness.dart';

class SecondaryButton extends StatelessWidget {
  final Function onPressed;
  final Widget icon;
  final String text;
  final bool hasIcon;
  final bool disabled;
  final double height;
  final double width;
  final Color borderColor;

  SecondaryButton({
    required this.onPressed,
    required this.text,
    this.hasIcon = false,
    this.disabled = false,
    required this.icon,
    this.height = 50.0,
    this.width = 80.0,
    required this.borderColor,
  });

  @override
  Widget build(BuildContext context) {
    if (hasIcon) {
      return Container(
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(10.0.s),
        ),
        child: OutlinedButton.icon(
          onPressed: disabled ? null : onPressed(),
          icon: icon,
          label: Text(text,
              textAlign: TextAlign.center,
              style: buildAppTextTheme()
                  .button!
                  .copyWith(color: AppColors.primaryBlue)),
        ),
      );
    }
    return Container(
      child: Container(
        height: this.height ?? 50.0.h,
        width: this.width ?? 80.0.w,
        decoration: BoxDecoration(
          color: AppColors.white,
          border: Border.all(
              color: this.borderColor ?? AppColors.white, width: 1.0),
          borderRadius: BorderRadius.circular(
            10.0.s,
          ),
        ),
        child: OutlinedButton(
          onPressed: disabled ? null : onPressed(),
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: buildAppTextTheme()
                .button!
                .copyWith(color: AppColors.primaryBlue),
          ),
        ),
      ),
    );
  }
}
