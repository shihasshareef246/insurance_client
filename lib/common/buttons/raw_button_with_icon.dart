import 'package:flutter/material.dart';

import 'package:gt_test/common/theme/app_theme.dart';
import '../../widgets/common/circular_progress_bar.dart';
import '../responsiveness.dart';
import '../colors.dart';

class RawButtonWithIcon extends StatelessWidget {
  final String text;
  final Function onPressed;
  final bool disabled;
  final double width;
  final double height;
  final Color bgColor;
  final Color textColor;
  final double borderRadius;
  final bool isLoading;
  final IconData icon;

  const RawButtonWithIcon({
    required this.text,
    required this.onPressed,
    this.disabled = false,
    this.width = 120.0,
    this.height = 40.0,
    required this.bgColor,
    required this.textColor,
    this.borderRadius = 300.0,
    this.isLoading = false,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      width: this.width,
      padding: EdgeInsets.symmetric(horizontal: 2.0.s),
      decoration: BoxDecoration(
        color: disabled ? this.bgColor.withOpacity(0.6) : this.bgColor,
        borderRadius: BorderRadius.circular(this.borderRadius),
        border: Border.all(color: AppColors.primaryBlue),
      ),
      child: isLoading
          ? Center(child: CircularProgressBar())
          : TextButton(
              child: InkWell(
                child: Container(
                  constraints: BoxConstraints(),
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(this.text,
                          textAlign: TextAlign.center,
                          style: buildAppTextTheme()
                              .headline5!
                              .copyWith(color: this.textColor)),
                      Icon(
                        icon,
                        color: AppColors.textColor,
                        size: 22.0.ics,
                      ),
                    ],
                  ),
                ),
              ),
              onPressed: () {
                if (!disabled) {
                  onPressed();
                }
              },
            ),
    );
  }
}
