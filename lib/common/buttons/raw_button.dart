import 'package:flutter/material.dart';

import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/widgets/common/circular_progress_bar.dart';
import '../responsiveness.dart';
import '../colors.dart';

class RawButton extends StatelessWidget {
  final String text;
  final Function onPressed;
  final bool disabled;
  final double width;
  final double height;
  final Color bgColor;
  final Color textColor;
  final double borderRadius;
  final bool isLoading;

  const RawButton({
    required this.text,
    required this.onPressed,
    this.disabled = false,
    this.width = 100.0,
    this.height = 40.0,
    required this.bgColor,
    required this.textColor,
    this.borderRadius = 300.0,
    this.isLoading = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      width: this.width,
      decoration: BoxDecoration(
        color: this.bgColor,
        borderRadius: BorderRadius.circular(this.borderRadius),
        border: Border.all(color: AppColors.primaryBlue),
      ),
      child: TextButton(
        child: InkWell(
          child: Container(
            constraints: BoxConstraints(),
            alignment: Alignment.center,
            child: Text(this.text,
                textAlign: TextAlign.center,
                style: buildAppTextTheme()
                    .bodyText1!
                    .copyWith(color: this.textColor)),
          ),
        ),
        onPressed: () {
          onPressed();
        },
      ),
    );
  }
}
