import 'package:flutter/material.dart';

import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/widgets/common/circular_progress_bar.dart';
import '../responsiveness.dart';
import '../colors.dart';

class PrimaryButton extends StatelessWidget {
  final Function onPressed;
  final bool disabled;
  final String text;
  final double width;
  final double height;
  final double borderRadius;
  final Color color;
  final bool isLoading;

  const PrimaryButton({
    required this.onPressed,
    this.disabled = false,
    this.text = "",
    this.width = 180.0,
    this.height = 50.0,
    this.borderRadius = 300.0,
    required this.color,
    this.isLoading = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      width: this.width,
      decoration: BoxDecoration(
        color: disabled ? AppColors.primaryBlue.withOpacity(0.5) : this.color,
        borderRadius: BorderRadius.circular(this.borderRadius),
        //border: Border.all(color: AppColors.white),
      ),
      child: isLoading
          ? Center(child: CircularProgressBar())
          : TextButton(
              child: Ink(
                child: Container(
                  constraints: BoxConstraints(),
                  alignment: Alignment.center,
                  child: Text(text,
                      textAlign: TextAlign.center,
                      style: buildAppTextTheme().button),
                ),
              ),
              onPressed: () {
                if (!disabled) onPressed();
              },
            ),
    );
  }
}
