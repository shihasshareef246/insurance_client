import 'package:flutter/material.dart';

import 'package:gt_test/common/theme/app_theme.dart';
import '../responsiveness.dart';
import '../colors.dart';

class TertiaryButton extends StatelessWidget {
  final Widget child;
  final Function onPressed;
  final bool disabled;
  final String text;
  final double width;
  final double height;
  final double borderRadius;
  final List<BoxShadow> shadow;

  const TertiaryButton({
    required this.onPressed,
    required this.child,
    this.disabled = false,
    this.text = "",
    this.width = 180.0,
    this.height = 50.0,
    this.borderRadius = 300.0,
    required this.shadow,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height,
      width: this.width,
      decoration: BoxDecoration(
        color: disabled
            ? AppColors.green10.withOpacity(0.5)
            : AppColors.green10,
        borderRadius: BorderRadius.circular(this.borderRadius ?? 300.s),
        border: Border.all(color: AppColors.white),
        boxShadow: this.shadow,
      ),
      child: TextButton(
        child: Ink(
          child: Container(
            constraints: BoxConstraints(),
            alignment: Alignment.center,
            child: child ??
                Text(text,
                    textAlign: TextAlign.center,
                    style: buildAppTextTheme()
                        .button!
                        .copyWith(color: AppColors.green)),
          ),
        ),
        onPressed: disabled ? null : onPressed(),
      ),
    );
  }
}
