import 'dart:ui' as ui;

import 'package:flutter/material.dart';

class Responsiveness {
  static Responsiveness? _instance;
  static const Size defaultSize = Size(428, 926);

  Size uiSize = defaultSize;

  static late double _screenWidth;
  static late double _screenHeight;
  static late double _pixelRatio;
  static late double _statusBarHeight;
  static late double _bottomBarHeight;
  static late double _textScaleFactor;
  static late double _safeAreaHorizontal;
  static late double _safeAreaVertical;
  static late double _safeArea;
  static late double _safeWidth;
  static late double _safeHeight;

  Responsiveness._() {
    MediaQueryData mediaQuery = MediaQueryData.fromWindow(ui.window);
    _pixelRatio = mediaQuery.devicePixelRatio;
    _screenWidth = mediaQuery.size.width;
    _screenHeight = mediaQuery.size.height;
    _statusBarHeight = mediaQuery.padding.top;
    _bottomBarHeight = mediaQuery.padding.bottom;
    _textScaleFactor = mediaQuery.textScaleFactor;
    _safeAreaHorizontal = mediaQuery.padding.left + mediaQuery.padding.right;
    _safeAreaVertical = mediaQuery.padding.top + mediaQuery.padding.bottom;
    _safeArea = _safeAreaHorizontal + _safeAreaVertical;
    _safeWidth =
        _screenWidth - mediaQuery.padding.left - mediaQuery.padding.right;
    _safeHeight = _screenHeight - _statusBarHeight - _bottomBarHeight;
  }

  factory Responsiveness() {
    assert(
      _instance != null,
    );
    return _instance!;
  }

  static void init({
    Size designSize = defaultSize,
  }) {
    _instance ??= Responsiveness._();
    _instance!..uiSize = designSize;
  }

  static double get textScaleFactor => _textScaleFactor;
  static double get pixelRatio => _pixelRatio;
  static double get screenWidth => _screenWidth;
  static double get screenHeight => _screenHeight;
  static double get screenWidthPx => _screenWidth * _pixelRatio;
  static double get screenHeightPx => _screenHeight * _pixelRatio;
  static double get statusBarHeight => _statusBarHeight;
  static double get bottomBarHeight => _bottomBarHeight;
  static double get safeArea => _safeArea;
  static double get safeAreaHorizontal => _safeAreaHorizontal;
  static double get safeAreaVertical => _safeAreaVertical;
  double get scaleWidth => _screenWidth / uiSize.width;
  double get scaleHeight => _screenHeight / uiSize.height;
  double get scaleText => scaleWidth;
  double get scaleSafeAreaWidth => _safeWidth / uiSize.width;
  double get scaleSafeAreaHeight => _safeHeight / uiSize.height;
  double get scaleSafeAreaText => scaleSafeAreaWidth;

  double setWidth(num width) => width * scaleWidth;
  double setHeight(num height) => height * scaleHeight;
  double setSafeAreaWidth(num width) => width * scaleSafeAreaWidth;
  double setSafeAreaHeight(num height) => height * scaleSafeAreaHeight;
  double setSpacing(num spacing) => spacing * scaleWidth;
  double setHorizontalSpacing(num spacing) => spacing * scaleWidth;
  double setVerticalSpacing(num spacing) => spacing * scaleHeight;
  double setSafeAreaSpacing(num spacing) => spacing * scaleSafeAreaWidth;
  double setSafeAreaHorizontalSpacing(num spacing) =>
      spacing * scaleSafeAreaWidth;
  double setSafeAreaVerticalSpacing(num spacing) =>
      spacing * scaleSafeAreaHeight;
  double setIconSize(num size) => size * scaleWidth;
  double setSafeAreaIconSize(num size) => size * scaleSafeAreaWidth;
  double setFontSize(num fontSize) =>
      ((fontSize * scaleText) / _textScaleFactor);
  double setSafeAreaFontSize(num fontSize) =>
      ((fontSize * scaleSafeAreaText) / _textScaleFactor);
}

extension ResponsivenessExtension on num {
  double get w => Responsiveness().setWidth(this);
  double get h => Responsiveness().setHeight(this);
  double get f => Responsiveness().setFontSize(this);
  double get s => Responsiveness().setSpacing(this);
  double get vs => Responsiveness().setVerticalSpacing(this);
  double get hs => Responsiveness().setHorizontalSpacing(this);
  double get ics => Responsiveness().setIconSize(this);
  double get sw => Responsiveness().setSafeAreaWidth(this);
  double get sh => Responsiveness().setSafeAreaHeight(this);
  double get sf => Responsiveness().setSafeAreaFontSize(this);
  double get ss => Responsiveness().setSafeAreaSpacing(this);
  double get svs => Responsiveness().setSafeAreaVerticalSpacing(this);
  double get shs => Responsiveness().setHorizontalSpacing(this);
  double get sics => Responsiveness().setSafeAreaIconSize(this);
}
