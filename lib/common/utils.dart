import 'dart:io';
import 'package:intl/intl.dart';
import 'package:amazon_s3_cognito/amazon_s3_cognito.dart';
import 'package:amazon_s3_cognito/aws_region.dart';

import 'constants.dart';

class Utils {
  Utils._();

  static String getInsuranceType({
    required String? type,
  }) {
    switch (type) {
      case '1':
        return 'Motor Insurance';
      case '2':
        return 'Travel Insurance';
      case '3':
        return 'Medical Insurance';
    }
    return '';
  }

  static Future<String?> uploadDocument(
      File? file, String? name, String? extension, String folderName) async {
    String? uploadedImageUrl = '';
    final dateFormat = DateFormat("dd-MM-yyyy");
    //  var uuid = Uuid();
    try {
      // ImageData imageData = ImageData(name ?? '', file?.path ?? '',
      //     uniqueId: uuid.v1(), imageUploadFolder: folderName);
      // uploadedImageUrl = await AmazonS3Cognito.upload(
      //     AppConstants.S3_BUCKET,
      //     AppConstants.S3_POOL,
      //     AwsRegion.US_EAST_1,
      //     AwsRegion.US_EAST_1,
      //     imageData);

      uploadedImageUrl = await AmazonS3Cognito.upload(
          file?.path ?? '',
          AppConstants.S3_BUCKET,
          AppConstants.S3_POOL,
          '${folderName}_${name}_${dateFormat.format(DateTime.now()).toString()}.$extension' ??
              '',
          AwsRegion.US_EAST_1,
          AwsRegion.US_EAST_1);
      if (uploadedImageUrl != '') {
        return "https://${AppConstants.S3_BUCKET}.s3.amazonaws.com/${folderName}_${name}_${dateFormat.format(DateTime.now()).toString()}.$extension";
      }
    } catch (e) {
      print(e);
    }
    return '';
  }
}
