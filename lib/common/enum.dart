enum AppEnvironments { dev, prod, none }

enum RegistrationModule {
  verifyMobile,
  userType,
  uploadDocuments,
  profilePicture,
  consentAndSubmit,
  none
}
enum ApiResponseType { successful, failure, none }
enum TextFieldValidationType { success, error, enable, disable, none }

enum UploadDocumentType {
  aadharCard,
  panCard,
  // voterIdCard,
  gstCertificate,
  gstr3bCertificate,
  bankStatement,
  bankPassBook,
  cibilDetails,
  profileDetails,
  voterIdOrRationOrPanCard,
  completeAllDocs,
  registrationComplete,
  registrationPending,
  invoice,
  none
}

enum UploadOptions { camera, gallery, single, multiple, profilePhoto, none }
enum UploadDocumentsStatus {
  pending,
  uploaded,
  none,
}

enum CibilDetailsAckType {
  cibilDetails1,
  cibilDetails2,
  cibilAck,
  none,
}

enum RegistrationType { retailer, farmer, trader, none }
enum UserSessionStatus { freshUser, registeredUser, registrationPending, none }
