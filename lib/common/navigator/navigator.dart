import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class FeatureNavigator extends StatelessWidget {
  final Widget sectionChild;
  final bool maintainState;

  FeatureNavigator({required this.sectionChild, this.maintainState = false});

  @override
  Widget build(BuildContext context) {
    return Navigator(onGenerateRoute: (routeSettings) {
      return MaterialPageRoute(
          builder: (context) => sectionChild,
          maintainState: maintainState);
    });
  }
}
