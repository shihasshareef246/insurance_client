import 'package:flutter/material.dart';

class Navigations {
  Navigations._();
  static pushNamed(BuildContext context, String routeName,
      [Object? arguments]) {
    Navigator.of(context).pushNamed(routeName, arguments: arguments);
  }

  static pushReplacementNamed(BuildContext context, String routeName,
      [Object? arguments]) {
    Navigator.of(context).pushReplacementNamed(
      routeName,
      arguments: arguments,
    );
  }

  static pushNamedAndRemoveUntil(BuildContext context, String routeName,
      [Object? arguments]) {
    Navigator.pushNamedAndRemoveUntil(context, routeName, (route) => false,
        arguments: arguments);
  }

  static pop(BuildContext context) {
    Navigator.of(context).pop();
  }

  static bool canPop(BuildContext context) {
    return Navigator.of(context).canPop();
  }

  static final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();

  static final RouteObserver<PageRoute> routeObserver =
      RouteObserver<PageRoute>();

  static canPushNamed(
      {BuildContext? context,
      String? routeName,
      String routeType = 'pushNamed'}) {
    Route? route = ModalRoute.of(context!);
    final existingRouteName = route?.settings?.name;
    if (routeName != null && routeName != existingRouteName) {
      routeType == 'pushNamed'
          ? Navigator.of(context).pushNamed(routeName)
          : Navigator.of(context).pushReplacementNamed(routeName);
    }
  }
}
