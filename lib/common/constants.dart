class AppConstants {
  AppConstants._();

  static const APP_TITLE = 'Gulf Trust';
  static const MALE = 'Male';
  static const FEMALE = 'Female';
  static const OTHER = 'Other';
  static const S3_BUCKET = 'gulftrust-insurance-mobile';
  static const S3_POOL = 'us-east-1:cd2d14d9-8d7d-4a82-aa5a-e7e35e7df05e';
//static const S3_BUCKET = 'gulftrust-bucket';
  //static const S3_POOL = 'us-west-2:3022c3a7-7f11-4d13-8595-0dbc5550a078';
  //static const S3_POOL = 'me-south-1:dd10e542-ecde-4194-8683-ea8ee4a1fd06';
}
