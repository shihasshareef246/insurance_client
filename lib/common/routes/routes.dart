import 'package:flutter/material.dart';
import 'package:gt_test/screens/claim_details_screen.dart';
import 'package:gt_test/screens/claims_screen.dart';
import 'package:gt_test/screens/home_screen.dart';
import 'package:gt_test/screens/init_screen.dart';
import 'package:gt_test/screens/insurance_details_screen.dart';
import 'package:gt_test/screens/login_screen.dart';
import 'package:gt_test/screens/medical_insurances_screen.dart';
import 'package:gt_test/screens/medical_insurer_details_screen.dart';
import 'package:gt_test/screens/motor_insurances_screen.dart';
import 'package:gt_test/screens/motor_payment_details_screen.dart';
import 'package:gt_test/screens/policy_guidelines_screen.dart';
import 'package:gt_test/screens/preliminary_screen.dart';
import 'package:gt_test/screens/prelogin_screen.dart';
import 'package:gt_test/screens/profile_screen.dart';
import 'package:gt_test/screens/register_claim_screen.dart';
import 'package:gt_test/screens/register_screen.dart';
import 'package:gt_test/screens/search_insurances_screen.dart';
import 'package:gt_test/screens/travel_documents_upload_screen.dart';
import 'package:gt_test/screens/travel_insurances_screen.dart';
import 'package:gt_test/screens/travel_insurer_details_screen.dart';
import 'package:gt_test/screens/travel_payment_details_screen.dart';

import '../../screens/base.dart';
import '../../screens/medical_documents_upload_screen.dart';
import '../../screens/motor_documents_upload_screen.dart';
import '../../screens/insurances_screen.dart';
import '../../screens/motor_insurer_details_screen.dart';
import '../../screens/otp_screen.dart';

class AppRouter {
  static const initScreen = '/initScreen';
  static const homeScreen = '/homeScreen';
  static const insurancesScreen = '/insurancesScreen';
  static const searchInsurancesScreen = '/searchInsurancesScreen';
  static const motorInsurancesScreen = '/motorInsurancesScreen';
  static const travelInsurancesScreen = '/travelInsurancesScreen';
  static const medicalInsurancesScreen = '/medicalInsurancesScreen';
  static const claimsScreen = '/claimsScreen';
  static const profileScreen = '/profileScreen';
  static const loginScreen = '/loginScreen';
  static const registerScreen = '/registerScreen';
  static const otpScreen = '/otpScreen';
  static const preliminaryScreen = '/preliminaryScreen';
  static const preloginScreen = '/preloginScreen';
  static const motorDocumentsUploadScreen = '/motorDocumentsUploadScreen';
  static const travelDocumentsUploadScreen = '/travelDocumentsUploadScreen';
  static const medicalDocumentsUploadScreen = '/medicalDocumentsUploadScreen';
  static const motorInsurerDetailsScreen = '/motorInsurerDetailsScreen';
  static const travelInsurerDetailsScreen = '/travelInsurerDetailsScreen';
  static const medicalInsurerDetailsScreen = '/medicalInsurerDetailsScreen';
  static const registerClaimScreen = '/registerClaimScreen';
  static const insuranceDetailsScreen = '/insuranceDetailsScreen';
  static const claimDetailsScreen = '/claimDetailsScreen';
  static const policyGuidelineScreen = '/policyGuidelineScreen';
  static const motorPaymentDetailsScreen = '/motorPaymentDetailsScreen';
  static const travelPaymentDetailsScreen = '/travelPaymentDetailsScreen';

  static Map<String, WidgetBuilder> get setUpRoutes {
    return {
      initScreen: (BuildContext context) => InitScreen(),
      homeScreen: (BuildContext context) => BaseLayout(
            page: HomeScreen(),
            activePageIndex: 0,
          ),
      insurancesScreen: (BuildContext context) => BaseLayout(
            page: InsurancesScreen(),
            activePageIndex: 1,
          ),
      claimsScreen: (BuildContext context) => BaseLayout(
            page: ClaimsScreen(),
            activePageIndex: 2,
          ),
      profileScreen: (BuildContext context) => BaseLayout(
            page: ProfileScreen(),
            activePageIndex: 3,
          ),
      searchInsurancesScreen: (BuildContext context) => SearchInsurancesScreen(
            type: 'MOTOR',
          ),
      motorInsurancesScreen: (BuildContext context) => MotorInsurancesScreen(
            type: 'MOTOR',
          ),
      travelInsurancesScreen: (BuildContext context) => TravelInsurancesScreen(
            type: 'TRAVEL',
          ),
      medicalInsurancesScreen: (BuildContext context) =>
          MedicalInsurancesScreen(
            type: 'MEDICAL',
          ),
      loginScreen: (BuildContext context) => LoginScreen(),
      registerScreen: (BuildContext context) => RegisterScreen(),
      otpScreen: (BuildContext context) => OtpScreen(),
      preliminaryScreen: (BuildContext context) => PreliminaryScreen(),
      preloginScreen: (BuildContext context) => PreLoginScreen(),
      motorDocumentsUploadScreen: (BuildContext context) =>
          MotorDocumentsUploadScreen(),
      travelDocumentsUploadScreen: (BuildContext context) =>
          TravelDocumentsUploadScreen(),
      medicalDocumentsUploadScreen: (BuildContext context) =>
          MedicalDocumentsUploadScreen(),
      motorInsurerDetailsScreen: (BuildContext context) =>
          MotorInsurerDetailsScreen(),
      travelInsurerDetailsScreen: (BuildContext context) =>
          TravelInsurerDetailsScreen(),
      medicalInsurerDetailsScreen: (BuildContext context) =>
          MedicalInsurerDetailsScreen(),
      registerClaimScreen: (BuildContext context) => RegisterClaimScreen(),
      insuranceDetailsScreen: (BuildContext context) =>
          InsuranceDetailsScreen(),
      claimDetailsScreen: (BuildContext context) => ClaimDetailsScreen(),
      policyGuidelineScreen: (BuildContext context) => PolicyGuidelinesScreen(),
      motorPaymentDetailsScreen: (BuildContext context) => MotorPaymentDetailsScreen(),
      travelPaymentDetailsScreen: (BuildContext context) => TravelPaymentDetailsScreen(),
    };
  }

  /// Handler for Error and Unhandled pages.
  static Function get unknownRoute {
    return (settings) {
      MaterialPageRoute(
        builder: (ctx) => InitScreen(),
      );
    };
  }

  /// Configures the initial route.
  static String get initialRoute {
    return AppRouter.initScreen;
  }
}
