import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../common/colors.dart';
import '../../common/images.dart';
import '../../common/theme/app_theme.dart';
import '../models/insurance_details_model.dart';
import 'package:gt_test/common/responsiveness.dart';

class CustomerDetailsWidget extends StatelessWidget {
  final PersonalDetailsModel? personalDetails;
  final String mobileNo;
  final String status;
  const CustomerDetailsWidget({
    required this.personalDetails,
    required this.mobileNo,
    required this.status,
  });

  @override
  Widget build(BuildContext context) {
    final dateFormat = DateFormat("dd-MM-yyyy");
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 200.0.w,
          child: Text(
            'Insurer Details',
            style: buildAppTextTheme().headline6,
            textAlign: TextAlign.start,
          ),
        ),
        SizedBox(
          height: 10.0.h,
        ),
        Container(
          padding: EdgeInsets.all(20.0.s),
          decoration: BoxDecoration(
            color: AppColors.white,
            border: Border.all(color: AppColors.border, width: 1.0),
            borderRadius: BorderRadius.circular(
              6.0.s,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                        image: AssetImage(AppImages.NAME),
                        height: 18.0.h,
                        width: 18.0.w,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Name :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    personalDetails?.insurerName ?? '',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.mail_outline,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Email Id :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    personalDetails?.emailId ?? '',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                        image: AssetImage(AppImages.PHONE),
                        height: 18.0.h,
                        width: 18.0.w,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Mobile No :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    mobileNo ?? '',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                        image: AssetImage(AppImages.DATE),
                        height: 18.0.h,
                        width: 18.0.w,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'DOB :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    personalDetails?.dob.toString() != ''
                        ? dateFormat.format(DateTime.parse(
                            personalDetails?.dob.toString() ?? ''))
                        : '',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.flag_outlined,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Nationality :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    personalDetails?.nationality ?? '',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.male,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Gender :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    personalDetails?.gender ?? '',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.place_outlined,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Place :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    personalDetails?.area ?? '',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.book_outlined,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Civil Id No :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    personalDetails?.civilIdNo ?? '',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.note_outlined,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Passport No :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    personalDetails?.passportNo ?? '',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.attach_money,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Status :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    status!,
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
