import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import '../../common/colors.dart';
import '../../common/images.dart';
import '../../common/theme/app_theme.dart';
import '../providers/profile_provider.dart';
import '../screens/motor_insurer_details_screen.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/models/insurance_details_model.dart';

import '../screens/travel_insurer_details_screen.dart';

class InsurerDetailsWidget extends StatefulWidget {
  final List<PersonalDetailsModel?> data;

  const InsurerDetailsWidget({
    required this.data,
  });

  @override
  State<InsurerDetailsWidget> createState() => _InsurerDetailsWidgetState();
}

class _InsurerDetailsWidgetState extends State<InsurerDetailsWidget> {
  ProfileProvider profileProvider = ProfileProvider();

  @override
  void initState() {
    super.initState();
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 200.0.w,
              child: Text(
                'Insurer Details',
                style: buildAppTextTheme().headline6,
                textAlign: TextAlign.start,
              ),
            ),
            InkWell(
              onTap: () => Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.fade,
                    child: TravelInsurerDetailsScreen(
                      isEdit: true,
                    )),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image(
                    image: AssetImage(AppImages.EDIT),
                    height: 18.0.h,
                    width: 18.0.w,
                  ),
                  SizedBox(
                    width: 10.0.w,
                  ),
                  Text(
                    'Edit',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10.0.h,
        ),
        Container(
          padding: EdgeInsets.all(22.0.s),
          decoration: BoxDecoration(
            color: AppColors.white,
            border: Border.all(color: AppColors.border, width: 1.0),
            borderRadius: BorderRadius.circular(
              6.0.s,
            ),
          ),
          child: ListView.builder(
              padding: EdgeInsets.zero,
              reverse: false,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: widget.data.length,
              scrollDirection: Axis.vertical,
              itemBuilder: (context, index) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Member ${index + 1}',
                      style: buildAppTextTheme()
                          .overline!
                          .copyWith(color: AppColors.primaryBlue),
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(
                      height: 10.0.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image(
                          image: AssetImage(AppImages.NAME),
                          height: 24.0.h,
                          width: 24.0.w,
                          color: AppColors.primaryBlue,
                        ),
                        SizedBox(
                          width: 16.0.w,
                        ),
                        Text(
                          widget.data[index]?.insurerName ?? '',
                          style: buildAppTextTheme()
                              .caption!
                              .copyWith(color: AppColors.primaryBlue),
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 17.0.h,
                    ),
                    if (widget.data[index]?.dob != '')
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.date_range_rounded,
                            color: AppColors.primaryBlue,
                            size: 24.0.s,
                          ),
                          SizedBox(
                            width: 16.0.w,
                          ),
                          Container(
                            width: 260.0.w,
                            child: Text(
                              widget.data[index]?.dob ?? '',
                              style: buildAppTextTheme()
                                  .caption!
                                  .copyWith(color: AppColors.primaryBlue),
                              textAlign: TextAlign.start,
                              maxLines: 2,
                            ),
                          ),
                        ],
                      ),
                    if (widget.data[index]?.dob != '')
                      SizedBox(
                        height: 17.0.h,
                      ),
                    if (widget.data[index]?.mobileNo != '')
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image(
                            image: AssetImage(AppImages.PHONE),
                            height: 24.0.h,
                            width: 24.0.w,
                            color: AppColors.primaryBlue,
                          ),
                          SizedBox(
                            width: 16.0.w,
                          ),
                          Text(
                            '+965 ${profileProvider?.profileData?.mobileNo ?? ''}',
                            style: buildAppTextTheme()
                                .caption!
                                .copyWith(color: AppColors.primaryBlue),
                            textAlign: TextAlign.start,
                          ),
                        ],
                      ),
                    if (widget.data[index]?.mobileNo != '')
                      SizedBox(
                        height: 17.0.h,
                      ),
                    if (widget.data[index]?.civilIdNo != '')
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image(
                            image: AssetImage(AppImages.ID),
                            height: 24.0.h,
                            width: 24.0.w,
                            color: AppColors.primaryBlue,
                          ),
                          SizedBox(
                            width: 16.0.w,
                          ),
                          Text(
                            widget.data[index]?.civilIdNo ?? '',
                            style: buildAppTextTheme()
                                .caption!
                                .copyWith(color: AppColors.primaryBlue),
                            textAlign: TextAlign.start,
                          ),
                        ],
                      ),
                    if (widget.data[index]?.civilIdNo != '')
                      SizedBox(
                        height: 17.0.h,
                      ),
                  ],
                );
              }),
        ),
      ],
    );
  }
}
