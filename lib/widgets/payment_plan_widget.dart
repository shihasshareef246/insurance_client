import 'package:flutter/material.dart';

import 'package:gt_test/common/responsiveness.dart';
import '../../common/colors.dart';
import '../../common/images.dart';
import '../../common/theme/app_theme.dart';
import 'insurance_details_bottom_sheet_widget.dart';

class PaymentPlanWidget extends StatelessWidget {
  final String? name;
  final String year;
  final String amount;
  final String productType;
  final String promoDiscount;

  const PaymentPlanWidget({
    required this.name,
    required this.year,
    required this.amount,
    required this.productType,
    this.promoDiscount = '',
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 134.0.h,
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Stack(
            children: [
              Container(
                padding: EdgeInsets.only(
                    left: 20.0.s, top: 12.0.s, right: 6.0.s, bottom: 12.0.s),
                width: MediaQuery.of(context).size.width * 0.6,
                decoration: BoxDecoration(
                  color: AppColors.white,
                  border: Border.all(color: AppColors.border, width: 1.0),
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(6.0.s),
                    topLeft: Radius.circular(6.0.s),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage(productType == 'Warba'
                          ? AppImages.WARBA
                          : productType == 'Tazur'
                              ? AppImages.TAZUR
                              : productType == 'Burgan'
                                  ? AppImages.BURGAN
                                  : productType == 'NLGIC'
                                      ? AppImages.NATIONAL
                                      : AppImages.SPLASH_SCREEN),
                      height: 40.0.h,
                      width: 40.0.w,
                    ),
                    SizedBox(
                      width: 12.0.w,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 170.0.w,
                          child: Text(
                            name ?? '',
                            style: buildAppTextTheme()
                                .caption!
                                .copyWith(color: AppColors.primaryBlue),
                            textAlign: TextAlign.start,
                          ),
                        ),
                        SizedBox(
                          height: 4.0.h,
                        ),
                        Text(
                          year,
                          style: buildAppTextTheme()
                              .caption!
                              .copyWith(color: AppColors.primaryBlue),
                          textAlign: TextAlign.right,
                        ),
                        SizedBox(
                          height: 4.0.h,
                        ),
                        InkWell(
                          onTap: () => showModalBottomSheet(
                              isScrollControlled: true,
                              backgroundColor:
                                  AppColors.primaryBlue.withOpacity(0.9),
                              barrierColor:
                                  AppColors.primaryBlue.withOpacity(0.9),
                              context: context,
                              builder: (BuildContext context) {
                                return InsuranceDetailsBottomSheetWidget(
                                    index: productType == 'Tazur'
                                        ? 0
                                        : productType == 'Burgan'
                                            ? 1
                                            : productType == 'NLGIC'
                                                ? 2
                                                : productType == 'Warba'
                                                    ? 3
                                                    : 4);
                              }),
                          child: Row(
                            children: [
                              Text(
                                'Information',
                                style: buildAppTextTheme()
                                    .caption!
                                    .copyWith(color: AppColors.orange),
                                textAlign: TextAlign.right,
                              ),
                              SizedBox(
                                width: 8.0.w,
                              ),
                              Image(
                                image: AssetImage(AppImages.INFO),
                                height: 18.0.h,
                                width: 18.0.w,
                                color: AppColors.orange,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              if (promoDiscount != '' && promoDiscount != '0')
                Positioned(
                  bottom: 0.0.s,
                  right: 0.0.s,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Promo Code',
                        style: buildAppTextTheme()
                            .bodyText1!
                            .copyWith(color: AppColors.brownColor),
                        textAlign: TextAlign.start,
                      ),
                      SizedBox(
                        width: 40.0.w,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: AppColors.skyBlue,
                          borderRadius: BorderRadius.only(
                            bottomRight: Radius.circular(6.0.s),
                            topLeft: Radius.circular(8.0.s),
                          ),
                        ),
                        height: 40.0.h,
                        width: 50.0.w,
                        child: Center(
                          child: Text(
                            '$promoDiscount%',
                            style: buildAppTextTheme().caption,
                            textAlign: TextAlign.start,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
            ],
          ),
          Stack(
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.25,
                padding: EdgeInsets.all(10.0.s),
                decoration: BoxDecoration(
                  color: AppColors.primaryBlue,
                  border: Border.all(color: AppColors.border, width: 1.0),
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(6.0.s),
                    topRight: Radius.circular(6.0.s),
                  ),
                ),
                child: Center(
                  child: Text(
                    amount ?? '',
                    style: buildAppTextTheme()
                        .headline4!
                        .copyWith(color: AppColors.white),
                    textAlign: TextAlign.right,
                  ),
                ),
              ),
              Positioned(
                top: 0.0.s,
                right: 0.0.s,
                child: Container(
                  decoration: BoxDecoration(
                    color: AppColors.white,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(6.0.s),
                      topRight: Radius.circular(8.0.s),
                    ),
                  ),
                  height: 26.0.h,
                  width: 40.0.w,
                  child: Center(
                    child: Text(
                      'KWD',
                      style: buildAppTextTheme()
                          .bodyText1!
                          .copyWith(color: AppColors.primaryBlue),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
