import 'package:flutter/material.dart';
import 'package:gt_test/common/images.dart';

import 'package:gt_test/common/responsiveness.dart';
import '../../common/colors.dart';
import '../../common/theme/app_theme.dart';

class InsuranceDetailsBottomSheetWidget extends StatefulWidget {
  int index;

  InsuranceDetailsBottomSheetWidget({
    this.index = 0,
  });
  @override
  _InsuranceDetailsBottomSheetWidgetState createState() =>
      _InsuranceDetailsBottomSheetWidgetState();
}

class _InsuranceDetailsBottomSheetWidgetState
    extends State<InsuranceDetailsBottomSheetWidget> {
  var projectData = [
    {
      'image': AppImages.TAZUR_LOGO,
      'title': 'Tazur Insurance',
      'specialConditions':
          '• Nil Deductibles. Sports cars 100 KD (Sum insured up to 25,000 KD) & 150 KD (Sum insured from 25,001 KD to 45,000 KD)\n• No Depreciation on Spare Parts for latest 5 years old models. Thereafter 10% added every year.\n• Agency Repair for latest 5 years old models.',
      'unknownAccidentCover':
          '10% of sum insured & Maximum 1,000 KD in aggregate per year and above that Insurance Company will pay 75% and customer will pay 25%',
      'frontWindShield':
          '100% for the first time and 50% thereafter (At Agency for latest 5 years old models)',
      'ringsAndTyre': 'Known Accidents 100% and Unknown Accidents 50%',
      'specialConditionsArabic':
          '• بدون تحمل للسيارات الخصوصي\n • بالنسبة للسيارات السرعة يتم فتح ملف بقيمة (100د.ك) د.ك من 1 د.ك الي 25000 د.ك ، ومن 25001 د.ك الي 45000 (150د.ك)- بدون استهلاك على قطع الغيارلاول 5 سنوات بعد ذلك ، تتم إضافة 10٪ لكل سنة \n• تصليح وكالة لاول 5 سنوات',
      'unknownAccidentCoverArabic':
          'تغطيةاجمالي حوادث المجهول لغاية10% بحد اقصي 1000 د.ك ومايزيديتحمل المؤمن له25%',
      'frontWindShieldArabic':
          '- %100 لأول مرة و 50٪ بعد ذلك (في الوكالة لاخر 5 سنوات)',
      'ringsAndTyreArabic': '- حادث معلوم 100٪ وحوادث مجهوله 50٪',
    },
    {
      'image': AppImages.BURGAN_LOGO,
      'title': 'Burgan Takaful Insurance',
      'specialConditions':
          '• Nil Deductibles\n• No Depreciation on Spare Parts for latest 5 years old models. Thereafter 10% added every year.\n• Agency Repair for latest 5 years old models.',
      'unknownAccidentCover':
          '10% of sum insured & Maximum 1,000 KD in aggregate per year and above that Insurance Company will pay 75% and customer will pay 25%',
      'frontWindShield':
          '100% for the first time and 50% thereafter (At Agency for latest 5 years old models)',
      'ringsAndTyre': 'Known Accidents 100% and Unknown Accidents 50%',
      'specialConditionsArabic':
          '• بدون تحمل\n• بدون استهلاك على قطع الغيارلاول 5 سنوات بعد ذلك  تتم إضافة 10٪ لكل سنة\n• تصليح وكالة لاول 5 سنوات ',
      'unknownAccidentCoverArabic':
          'تغطيةاجمالي حوادث المجهول لغاية10% بحد اقصي 1000 د.ك ومايزيديتحمل المؤمن له25%',
      'frontWindShieldArabic':
          '%100 لأول مرة و 50٪ بعد ذلك (في الوكالة لاخر 5 سنوات)',
      'ringsAndTyreArabic': 'الحوادث  المعلومه 100٪ الحوادث مجهوله 50٪',
    },
    {
      'image': AppImages.NATIONAL_LOGO,
      'title': 'National Life & General Insurance',
      'specialConditions':
          '• Nil Deductibles\n• No Depreciation on Spare Parts for latest 5 years old models. Thereafter 10% added every year.\n• Agency Repair for latest 5 years old models.',
      'unknownAccidentCover':
          '10% of sum insured & Maximum 1,000 KD each claim and above that Insurance Company will pay 75% and customer will pay 25%',
      'frontWindShield':
          '100% for the first time and 50% thereafter (At Agency for latest 5 years old models)',
      'ringsAndTyre': 'Known Accidents 100% and Unknown Accidents 50%',
      'specialConditionsArabic':
          '• بدون تحمل\n• بدون استهلاك على قطع الغيارلاول 5 سنوات بعد ذلك  تتم إضافة 10٪ لكل سنة\n• تصليح وكالة لاول 5 سنوات',
      'unknownAccidentCoverArabic':
          'تغطيةاجمالي حوادث المجهول لغاية10% بحد اقصي 1000 د.ك ومايزيديتحمل المؤمن له25%',
      'frontWindShieldArabic':
          '%100 لأول مرة و 50٪ بعد ذلك (في الوكالة لاخر 5 سنوات)',
      'ringsAndTyreArabic': 'الحوادث  المعلومه 100٪ الحوادث مجهوله 50٪',
    },
    {
      'image': AppImages.WARBA_LOGO,
      'title': 'Warba Insurance',
      'specialConditions':
          '• Nil Deductibles\n• No Depreciation on Spare Parts for latest 5 years old models. Thereafter 10% added every year.\n• Agency Repair for latest 5 years old models.',
      'unknownAccidentCover':
          '10% of sum insured & Maximum 1,000 KD each claim and above that Insurance Company will pay 75% and customer will pay 25%',
      'frontWindShield':
          '100% for the first time and 50% thereafter (At Agency for latest 5 years old models)',
      'ringsAndTyre': 'Known Accidents 100% and Unknown Accidents 50%',
      'specialConditionsArabic':
          ' • بدون تحمل\n •بدون استهلاك على قطع الغيارلاول 5 سنوات بعد ذلك  تتم إضافة 10٪ لكل سنة \n• تصليح وكالة لاول 5 سنوات\n',
      'unknownAccidentCoverArabic':
          'تغطيةاجمالي حوادث المجهول لغاية10% بحد اقصي 1000 د.ك ومايزيديتحمل المؤمن له25%',
      'frontWindShieldArabic':
          '%100 لأول مرة و 50٪ بعد ذلك (في الوكالة لاخر 5 سنوات)',
      'ringsAndTyreArabic': 'الحوادث  المعلومه 100٪ الحوادث مجهوله 50٪',
    },
    {
      'image': AppImages.LOGO,
      'title': 'Gulf trust',
      'desc':
          'Third-party insurance is the basic insurance coverage that is compulsory for every Vehicle. It provides coverage against any property damages, physical injuries or death of third parties if the car owner is at fault.',
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.index == 4
          ? MediaQuery.of(context).size.height > 650
              ? 476.0.h
              : 500.0.h
          : MediaQuery.of(context).size.height > 650
              ? 676.0.h
              : 700.0.h,
      padding: EdgeInsets.only(
        left: 30.0.s,
        right: 30.0.s,
      ),
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.0.s),
            topRight: Radius.circular(10.0.s)),
        boxShadow: [
          BoxShadow(
            blurRadius: 4.0.s,
            offset: Offset(0, 2),
            color: AppColors.primaryBlue,
          ),
        ],
      ),
      child: widget.index == 4
          ? ListView(children: [
              SizedBox(
                height: 30.0.h,
              ),
              Image(
                image: AssetImage(
                  projectData[widget.index]['image']!,
                ),
                height: 60.0.h,
                width: 60.0.w,
                alignment: Alignment.centerLeft,
              ),
              SizedBox(
                height: 30.0.h,
              ),
              Text(
                projectData[widget.index]['title']!,
                style: buildAppTextTheme()
                    .headline4!
                    .copyWith(color: AppColors.black),
              ),
              SizedBox(
                height: 30.0.h,
              ),
              Text(
                projectData[widget.index]['desc']!,
                style: buildAppTextTheme()
                    .headline6!
                    .copyWith(color: AppColors.textColor),
                textAlign: TextAlign.start,
              ),
              SizedBox(
                height: 30.0.h,
              ),
            ])
          : ListView(
              children: [
                SizedBox(
                  height: 30.0.h,
                ),
                Image(
                  image: AssetImage(
                    projectData[widget.index]['image']!,
                  ),
                  height: 60.0.h,
                  width: 60.0.w,
                  alignment: Alignment.centerLeft,
                ),
                SizedBox(
                  height: 30.0.h,
                ),
                Text(
                  projectData[widget.index]['title']!,
                  style: buildAppTextTheme()
                      .headline4!
                      .copyWith(color: AppColors.black),
                ),
                SizedBox(
                  height: 30.0.h,
                ),
                Text(
                  'Special Conditions',
                  style: buildAppTextTheme().headline4,
                  textAlign: TextAlign.start,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  projectData[widget.index]['specialConditions']!,
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  'Unknown Accident Cover',
                  style: buildAppTextTheme().headline4,
                  textAlign: TextAlign.start,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  projectData[widget.index]['unknownAccidentCover']!,
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  'Front Wind Shield',
                  style: buildAppTextTheme().headline4,
                  textAlign: TextAlign.start,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  projectData[widget.index]['frontWindShield']!,
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  'Rings and Tyre',
                  style: buildAppTextTheme().headline4,
                  textAlign: TextAlign.start,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  projectData[widget.index]['ringsAndTyre']!,
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 30.0.h,
                ),
                Text(
                  widget.index == 0
                      ? 'شركة تأزر للتأمين التكافلي :'
                      : widget.index == 1
                          ? 'شركة برقان تكافل للتأمين التكافلي :'
                          : widget.index == 2
                              ? 'شركة الوطنية للتأمين علي الحياة والعام  :'
                              : widget.index == 3
                                  ? 'شركة وربة للتامين :'
                                  : 'شركة برقان تكافل للتأمين التكافلي :',
                  style: buildAppTextTheme().headline4,
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.rtl,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  projectData[widget.index]['specialConditionsArabic']!,
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.rtl,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  'تغطية الحوادث المجهوله:',
                  style: buildAppTextTheme().headline4,
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.rtl,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  projectData[widget.index]['unknownAccidentCoverArabic']!,
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.rtl,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  'الزجاج الامامي:',
                  style: buildAppTextTheme().headline4,
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.rtl,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  projectData[widget.index]['frontWindShieldArabic']!,
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.rtl,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  'الاطارات و الرنجات :',
                  style: buildAppTextTheme().headline4,
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.rtl,
                ),
                SizedBox(height: 8.0.h),
                Text(
                  projectData[widget.index]['ringsAndTyreArabic']!,
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.textColor),
                  textAlign: TextAlign.start,
                  textDirection: TextDirection.rtl,
                ),
                SizedBox(height: 30.0.h),
              ],
            ),
    );
  }
}
