import 'package:flutter/material.dart';

import 'package:gt_test/common/responsiveness.dart';
import '../../common/colors.dart';
import '../../common/theme/app_theme.dart';

class InsuranceStatusBottomSheetWidget extends StatefulWidget {
  int index;

  InsuranceStatusBottomSheetWidget({
    this.index = 0,
  });
  @override
  _InsuranceStatusBottomSheetWidgetState createState() =>
      _InsuranceStatusBottomSheetWidgetState();
}

class _InsuranceStatusBottomSheetWidgetState
    extends State<InsuranceStatusBottomSheetWidget> {
  var projectData = [
    {
      'status': 'Registered',
      'desc': 'Your insurance is registered successfully',
    },
    {
      'status': 'Processed',
      'desc': 'Your claim is processed successfully',
    },
    {
      'status': 'Rejected',
      'desc': 'Your claim is rejected',
    },
    {
      'status': 'In Progress',
      'desc': 'Your claim is in progress',
    },
    {
      'status': 'Pending',
      'desc': 'Your claim is pending',
    },
  ];
  String status = 'Registered :\nIn Progress :\nActive :\nCancelled :\nExpired :';
  String desc =
      'Insurance Registered\nReview In Progress\nInsurance Active\nInsurance Cancelled\nInsurance Expired';
  @override
  Widget build(BuildContext context) {
    return Container(
      //  height: MediaQuery.of(context).size.height > 650 ? 176.0.h : 200.0.h,
      height: MediaQuery.of(context).size.height > 650 ? 260.0.h : 280.0.h,

      padding: EdgeInsets.only(
          left: 30.0.s, right: 30.0.s, top: 30.0.s, bottom: 20.0.s),
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.0.s),
            topRight: Radius.circular(10.0.s)),
        boxShadow: [
          BoxShadow(
            blurRadius: 4.0.s,
            offset: Offset(0, 2),
            color: AppColors.primaryBlue,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 100.0.w,
            child: Text(
              status,
              style: buildAppTextTheme()
                  .headline6!
                  .copyWith(color: AppColors.greyishBlack),
              textAlign: TextAlign.start,
            ),
          ),
          SizedBox(
            width: 30.0.w,
          ),
          Container(
            width: 200.0.w,
            child: Text(
              desc,
              style: buildAppTextTheme()
                  .headline6!
                  .copyWith(color: AppColors.black),
              textAlign: TextAlign.start,
            ),
          ),
        ],
      ),
    );
  }
}
