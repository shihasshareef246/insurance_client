import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/colors.dart';
import '../../common/theme/app_theme.dart';
import '../common/buttons/primary_button.dart';
import '../models/additional_travel_insurance_model.dart';
import '../providers/customer_provider.dart';
import '../providers/dashboard_provider.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/models/additional_motor_insurance_model.dart';

class TravelAdditionalCoverBottomSheetWidget extends StatefulWidget {
  @override
  _TravelAdditionalCoverBottomSheetWidgetState createState() =>
      _TravelAdditionalCoverBottomSheetWidgetState();
}

class _TravelAdditionalCoverBottomSheetWidgetState
    extends State<TravelAdditionalCoverBottomSheetWidget> {
  DashboardProvider dashboardProvider = DashboardProvider();
  CustomerProvider customerProvider = CustomerProvider();
  // List<bool> isAccepted = [];
  int selectedCoverId = 0;
  // List<AdditionalMotorInsuranceModel> optionalList = [];
  // List<AdditionalMotorInsuranceModel> groupList = [];
  // List<AdditionalMotorInsuranceModel> freeList = [];

  @override
  void initState() {
    dashboardProvider = Provider.of<DashboardProvider>(context, listen: false);
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);

    // isAccepted = List.filled(
    //     dashboardProvider.additionalMotorInsuranceList.length, false);
    // dashboardProvider.additionalMotorInsuranceList.forEach((element) {
    //   if (element?.type == 'optional') {
    //     optionalList.add(element!);
    //   } else if (element?.type == 'Group') {
    //     groupList.add(element!);
    //   } else {
    //     freeList.add(element!);
    //   }
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    dashboardProvider = Provider.of<DashboardProvider>(context, listen: false);

    return StatefulBuilder(
        builder: (BuildContext orderContext, StateSetter setModalState) {
      return Container(
        height: MediaQuery.of(context).size.height > 650 ? 800.0.h : 780.0.h,
        padding: EdgeInsets.only(left: 20.0.s, right: 30.0.s),
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10.0.s),
              topRight: Radius.circular(10.0.s)),
          boxShadow: [
            BoxShadow(
              blurRadius: 4.0.s,
              offset: Offset(0, 2),
              color: AppColors.primaryBlue,
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 16.0.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Additional Cover',
                  style: buildAppTextTheme()
                      .headline2!
                      .copyWith(color: AppColors.black),
                ),
                InkWell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.replay_sharp,
                          size: 20.0.ics, color: AppColors.lightGrey),
                      SizedBox(
                        width: 6.0.w,
                      ),
                      Text(
                        'Reset',
                        style: buildAppTextTheme().headline6,
                      ),
                    ],
                  ),
                  onTap: () {
                    customerProvider
                        .resetSelectedAdditionalTravelInsuranceList();
                    setState(() {
                      setModalState(() {
                        selectedCoverId = 0;
                        dashboardProvider.additionalTravelInsuranceList
                            .forEach((element) {
                          element?.isSelected = false;
                        });
                      });
                    });
                    // isAccepted = List.filled(
                    //     dashboardProvider.additionalMotorInsuranceList.length,
                    //     false);
                  },
                ),
              ],
            ),
            SizedBox(
              height: 16.0.h,
            ),
            Container(
              height: 300.0.h,
              child: ListView.builder(
                  itemCount:
                      dashboardProvider.additionalTravelInsuranceList.length,
                  scrollDirection: Axis.vertical,
                  physics: AlwaysScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    AdditionalTravelInsuranceModel? element =
                        dashboardProvider.additionalTravelInsuranceList[index];
                    return Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Checkbox(
                          splashRadius: 8.0.s,
                          activeColor: AppColors.white,
                          value: element?.isSelected,
                          onChanged: (value) {
                            setState(() {
                              element?.isSelected = !element.isSelected;
                              selectedCoverId = 0;
                            });
                          },
                          checkColor: AppColors.primaryBlue,
                        ),
                        Container(
                          width: 320.0.w,
                          child: Text(
                            '${element?.coverDetails} (+${element?.premiumAmount}KWD)',
                            style: buildAppTextTheme().headline6,
                            textAlign: TextAlign.start,
                          ),
                        ),
                      ],
                    );
                  }),
            ),
            // Container(
            //  // height: freeList.length* 40.0.h,
            //   child: ListView.builder(
            //       itemCount:
            //       freeList.length,
            //       shrinkWrap: true,
            //       scrollDirection: Axis.vertical,
            //       physics: NeverScrollableScrollPhysics(),
            //       itemBuilder: (context, index) {
            //         return Row(
            //           mainAxisAlignment: MainAxisAlignment.start,
            //           crossAxisAlignment: CrossAxisAlignment.center,
            //           children: [
            //             Checkbox(
            //               splashRadius: 8.0.s,
            //               activeColor: AppColors.primaryGreen,
            //               value: isAccepted[index],
            //               onChanged: (value) {
            //                 setState(() {
            //                   isAccepted[index] = !isAccepted[index];
            //                 });
            //               },
            //               checkColor: AppColors.primaryBlue,
            //             ),
            //             Container(
            //               width: 320.0.w,
            //               child: Text(
            //                 '${freeList[index]?.coverDetails} (+${freeList[index]?.premiumAmount}KWD)',
            //                 style: buildAppTextTheme().headline6,
            //                 textAlign: TextAlign.start,
            //               ),
            //             ),
            //           ],
            //         );
            //       }),
            // ),
            // Container(
            //  // height:optionalList.length* 40.0.h,
            //   child: ListView.builder(
            //       itemCount:
            //       optionalList.length,
            //       shrinkWrap: true,
            //       scrollDirection: Axis.vertical,
            //       physics: AlwaysScrollableScrollPhysics(),
            //       itemBuilder: (context, index) {
            //         return Row(
            //           mainAxisAlignment: MainAxisAlignment.start,
            //           crossAxisAlignment: CrossAxisAlignment.center,
            //           children: [
            //             Checkbox(
            //               splashRadius: 8.0.s,
            //               activeColor: AppColors.primaryGreen,
            //               value: isAccepted[index],
            //               onChanged: (value) {
            //                 setState(() {
            //                   isAccepted[index] = !isAccepted[index];
            //                 });
            //               },
            //               checkColor: AppColors.primaryBlue,
            //             ),
            //             Container(
            //               width: 320.0.w,
            //               child: Text(
            //                 '${optionalList[index]?.coverDetails} (+${optionalList[index]?.premiumAmount}KWD)',
            //                 style: buildAppTextTheme().headline6,
            //                 textAlign: TextAlign.start,
            //               ),
            //             ),
            //           ],
            //         );
            //       }),
            // ),
            // Container(
            //  // height: groupList.length* 40.0.h,
            //   child: ListView.builder(
            //       itemCount:
            //       groupList.length,
            //       scrollDirection: Axis.vertical,
            //       shrinkWrap: true,
            //       physics: AlwaysScrollableScrollPhysics(),
            //       itemBuilder: (context, index) {
            //         return Row(
            //           mainAxisAlignment: MainAxisAlignment.start,
            //           crossAxisAlignment: CrossAxisAlignment.center,
            //           children: [
            //             Checkbox(
            //               splashRadius: 8.0.s,
            //               activeColor: AppColors.primaryGreen,
            //               value: isAccepted[index],
            //               onChanged: (value) {
            //                 setState(() {
            //                   isAccepted[index] = !isAccepted[index];
            //                 });
            //               },
            //               checkColor: AppColors.primaryBlue,
            //             ),
            //             Container(
            //               width: 320.0.w,
            //               child: Text(
            //                 '${groupList[index]?.coverDetails} (+${groupList[index]?.premiumAmount}KWD)',
            //                 style: buildAppTextTheme().headline6,
            //                 textAlign: TextAlign.start,
            //               ),
            //             ),
            //           ],
            //         );
            //       }),
            // ),
            SizedBox(height: 16.0.h),
            Center(
              child: PrimaryButton(
                  height: 65.0.h,
                  width: 216.0.w,
                  color: AppColors.primaryBlue,
                  //  disabled: !(this.otpValid),
                  // isLoading: isLoading,
                  onPressed: () async {
                    customerProvider
                        .resetSelectedAdditionalTravelInsuranceList();
                    dashboardProvider.additionalTravelInsuranceList
                        .forEach((element) {
                      if ((element?.isSelected ?? false)) {
                        customerProvider.selectedAdditionalTravelInsuranceList
                            .add(element);
                      }
                    });
                    Navigator.pop(context);
                    // if (!isLoading) {
                    //   setState(() {
                    //     isLoading = true;
                    //   });
                    //   Navigations.pushNamed(
                    //     context,
                    //     AppRouter.homeScreen,
                    //   );
                    //   setState(() {
                    //     isLoading = false;
                    //   });
                    // }
                  },
                  text: 'Add',
                  borderRadius: 80.0.s),
            ),
            SizedBox(
              height: 20.0.h,
            ),
            Center(
              child: InkWell(
                child: Text(
                  'Skip',
                  style: buildAppTextTheme()
                      .headline6!
                      .copyWith(color: AppColors.primaryBlue),
                  textAlign: TextAlign.center,
                ),
                onTap: () {
                  customerProvider.resetSelectedAdditionalTravelInsuranceList();
                  Navigator.pop(context);
                },
              ),
            ),
            SizedBox(
              height: 16.0.h,
            ),
          ],
        ),
      );
    });
  }
}
