import 'package:flutter/material.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:page_transition/page_transition.dart';

import '../../common/colors.dart';
import '../../common/navigator/navigations.dart';
import '../../common/routes/routes.dart';
import '../../common/theme/app_theme.dart';
import '../models/discount_plan_model.dart';
import '../screens/search_insurances_screen.dart';

class OffersWidget extends StatelessWidget {
  final String? text;
  final String? desc;
  final String? image;
  final String? discount;
  final DiscountPlanModel? discountData;
  const OffersWidget({
    required this.text,
    required this.desc,
    required this.image,
    required this.discount,
    required this.discountData,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.fade,
            child: SearchInsurancesScreen(
              discountData: discountData,
              type: 'MOTOR',
            ),
          ),
        );
        // Navigations.pushNamed(
        //   context,
        //   AppRouter.searchInsurancesScreen,
        // );
      },
      child: Stack(
        children: [
          Container(
            width: 270.0.w,
            decoration: BoxDecoration(
              color: AppColors.white,
              border: Border.all(color: AppColors.border, width: 1.0),
              borderRadius: BorderRadius.circular(
                12.0.s,
              ),
              boxShadow: [
                BoxShadow(
                  blurRadius: 3.0.s,
                  offset: Offset(0, 1),
                  color: AppColors.black.withOpacity(0.16),
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                if (image != null)
                  Image(
                    image: AssetImage(image!),
                    height: 93.0.h,
                    width: 93.0.w,
                    fit: BoxFit.fill,
                  ),
                SizedBox(
                  width: 10.0.w,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20.0.h,
                    ),
                    Container(
                      width: 160.0.w,
                      child: Text(
                        text ?? '',
                        style: buildAppTextTheme().headline5,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    Container(
                      width: 160.0.w,
                      child: Text(
                        desc ?? '',
                        style: buildAppTextTheme()
                            .bodyText2!
                            .copyWith(color: AppColors.black.withOpacity(0.6)),
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0.0.s,
            right: 0.0.s,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  'Promo Code',
                  style: buildAppTextTheme()
                      .bodyText1!
                      .copyWith(color: AppColors.brownColor),
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  width: 40.0.w,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: AppColors.skyBlue,
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(6.0.s),
                      topLeft: Radius.circular(8.0.s),
                    ),
                  ),
                  height: 40.0.h,
                  width: 50.0.w,
                  child: Center(
                    child: Text(
                      '$discount%',
                      style: buildAppTextTheme().caption,
                      textAlign: TextAlign.start,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
