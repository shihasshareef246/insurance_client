import 'package:flutter/material.dart';

import 'package:gt_test/common/responsiveness.dart';
import '../../common/colors.dart';
import '../../common/theme/app_theme.dart';

class ContactUsBottomSheetWidget extends StatefulWidget {
  @override
  _ContactUsBottomSheetWidgetState createState() =>
      _ContactUsBottomSheetWidgetState();
}

class _ContactUsBottomSheetWidgetState
    extends State<ContactUsBottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height > 650 ? 326.0.h : 350.0.h,
      padding: EdgeInsets.all(20.0.s),
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.0.s),
            topRight: Radius.circular(10.0.s)),
        boxShadow: [
          BoxShadow(
            blurRadius: 4.0.s,
            offset: Offset(0, 2),
            color: AppColors.primaryBlue,
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Contact us:',
            style: buildAppTextTheme()
                .headline6!
                .copyWith(color: AppColors.greyishBlack),
          ),
          SizedBox(
            height: 30.0.h,
          ),
          Text(
            'Gulf Trust Insurance Company  W.L.L',
            style: buildAppTextTheme().headline6,
            textAlign: TextAlign.start,
          ),
          SizedBox(height: 8.0.h),
          Text(
            'Kuwait Building Tower, 6th Floor (A)\nQebla, Fahad Al-Salem St. ,\nKuwait City, Kuwait',
            style: buildAppTextTheme().headline6,
            textAlign: TextAlign.start,
          ),
          SizedBox(height: 4.0.h),
          Text(
            'Tel : +965 2221 7090',
            style: buildAppTextTheme().headline6,
            textAlign: TextAlign.start,
          ),
          SizedBox(height: 4.0.h),
          Text(
            'Email : info@gulftrust-kw.com',
            style: buildAppTextTheme().headline6,
            textAlign: TextAlign.start,
          ),
          SizedBox(height: 4.0.h),
          Text(
            'Website : www.gulftrust-kw.com',
            style: buildAppTextTheme().headline6,
            textAlign: TextAlign.start,
          ),
          SizedBox(height: 8.0.h),
        ],
      ),
    );
  }
}
