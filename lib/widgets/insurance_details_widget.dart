import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:advance_pdf_viewer_fork/advance_pdf_viewer_fork.dart';
import 'package:provider/provider.dart';

import '../../common/colors.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/widgets/plan_details_widget.dart';
import 'package:gt_test/widgets/policy_document_widget.dart';
import '../../common/theme/app_theme.dart';
import '../common/buttons/primary_button.dart';
import '../common/buttons/raw_button.dart';
import '../common/images.dart';
import '../models/insurance_details_model.dart';
import '../providers/profile_provider.dart';
import '../screens/register_claim_screen.dart';
import 'customer_details_widget.dart';

class InsuranceDetailsWidget extends StatefulWidget {
  final InsuranceDetailsModel insuranceDetails;
  final String status;

  const InsuranceDetailsWidget({
    required this.insuranceDetails,
    required this.status,
  });

  @override
  _InsuranceDetailsWidgetState createState() => _InsuranceDetailsWidgetState();
}

class _InsuranceDetailsWidgetState extends State<InsuranceDetailsWidget> {
  ProfileProvider profileProvider = ProfileProvider();
  bool isLoading = false;
  late PDFDocument? document;
  bool isDocShown = false;

  @override
  void initState() {
    profileProvider = Provider.of<ProfileProvider>(context, listen: false);
    super.initState();
  }

  @override
  void dispose() {
    document = null;
    super.dispose();
  }

  loadDocument() async {
    document = await PDFDocument.fromAsset('assets/files/pdf/sample.pdf');
    setState(() {
      isLoading = true;
    });
    // ModalPopUp(
    //     crossIconRequired: false,
    //     crossIconSize: 16.0.s,
    //     iconColor: AppColors.greyishBlack,
    //     backgroundColor: AppColors.white,
    //     childcontainer: PDFViewer(document: document!, zoomSteps: 1,
    //       showPicker: false,
    //       showNavigation: true,),
    //     barrierDismissable: false,
    //     barrierColor: Colors.black,
    //     closeClick: () => {}).showPopup(context);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: isLoading
          ? Center(
              child: Container(
                height: MediaQuery.of(context).size.height * 0.8,
                width: MediaQuery.of(context).size.width * 0.8,
                child: PDFViewer(
                  document: document!,
                  zoomSteps: 1,
                  showPicker: false,
                  showNavigation: true,
                ),
              ),
            )
          : Container(
              padding: EdgeInsets.only(
                  left: 30.0.s, right: 30.0.s, top: 23.0.s, bottom: 60.0.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'ID : #${widget.insuranceDetails.MotorDetails?.transactionId}',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  CustomerDetailsWidget(
                    mobileNo: profileProvider?.profileData?.mobileNo ?? '',
                    personalDetails: widget.insuranceDetails.PersonalDetails,
                    status: widget.status,
                  ),
                  SizedBox(
                    height: 27.0.h,
                  ),
                  PlanDetailsWidget(
                    motorDetails: widget.insuranceDetails.MotorDetails,
                  ),
                  SizedBox(
                    height: 20.0.h,
                  ),
                  Container(
                    padding: EdgeInsets.all(10.0.s),
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      border: Border.all(color: AppColors.border, width: 1.0),
                      borderRadius: BorderRadius.circular(
                        6.0.s,
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Image(
                                  image: AssetImage(AppImages.PDF),
                                  height: 24.0.h,
                                  width: 24.0.w,
                                ),
                                SizedBox(
                                  width: 16.0.w,
                                ),
                                Container(
                                  width: 200.0.w,
                                  child: Text(
                                    'Policy document',
                                    style: buildAppTextTheme().headline3,
                                    textAlign: TextAlign.start,
                                  ),
                                ),
                              ],
                            ),
                            RawButton(
                              width: 102.0.w,
                              height: 48.0.h,
                              text: 'View',
                              borderRadius: 5.0.s,
                              onPressed: () => loadDocument(),
                              bgColor: AppColors.primaryBlue,
                              textColor: AppColors.white,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 90.0.h),
                  PrimaryButton(
                      height: 65.0.h,
                      width: 368.0.w,
                      color: AppColors.primaryBlue,
                      isLoading: isLoading,
                      onPressed: () async {
                        if (!isLoading) {
                          setState(() {
                            isLoading = true;
                          });
                          Navigator.push(
                            context,
                            PageTransition(
                              type: PageTransitionType.fade,
                              child: RegisterClaimScreen(
                                transactionId: widget.insuranceDetails
                                    .MotorDetails?.transactionId,
                                planId: widget
                                    .insuranceDetails.MotorDetails?.planId,
                              ),
                            ),
                          );
                          // Navigations.pushNamed(
                          //   context,
                          //   AppRouter.registerClaimScreen,
                          // );
                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      text: 'Claim',
                      borderRadius: 80.0.s),
                  SizedBox(
                    height: 30.0.h,
                  ),
                ],
              ),
            ),
    );
  }
}
