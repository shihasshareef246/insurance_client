import 'package:flutter/material.dart';

import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/models/motor_insurance_model.dart';
import 'package:provider/provider.dart';
import '../../common/colors.dart';
import '../../common/images.dart';
import '../../common/theme/app_theme.dart';
import '../providers/customer_provider.dart';
import 'insurance_details_bottom_sheet_widget.dart';

class PlanWidget extends StatefulWidget {
  final MotorInsuranceModel? plan;
  // final String? name;
  // final int? planId;
  // final String year;
  // final String amount;
  final double width;
  bool isSelected;

  PlanWidget({
    required this.plan,
    // required this.name,
    // required this.planId,
    // required this.year,
    // required this.amount,
    required this.width,
    this.isSelected = false,
  });
  @override
  _PlanWidgetState createState() => _PlanWidgetState();
}

class _PlanWidgetState extends State<PlanWidget> {
  CustomerProvider customerProvider = CustomerProvider();

  @override
  void initState() {
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 110.0.h,
      child: InkWell(
        onTap: () {
          setState(() {
            widget.isSelected = !widget.isSelected;
            if (widget.isSelected) {
              customerProvider.selectedMotorInsuranceList.add(widget.plan);
            }
          });
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              //  height: 96.0.h,
              width: 90.0.w,
              decoration: BoxDecoration(
                color: widget.isSelected ? AppColors.border : AppColors.white,
                border: Border(
                  top: BorderSide(color: AppColors.border, width: 1.0),
                  bottom: BorderSide(color: AppColors.border, width: 1.0),
                  left: BorderSide(color: AppColors.border, width: 1.0),
                  right: BorderSide(color: AppColors.border, width: 1.0),
                ),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10.0.s),
                  topLeft: Radius.circular(10.0.s),
                ),
              ),
              child: Center(
                child: Image(
                  image: AssetImage(widget.plan?.productType == 'Gulf Trust'
                          ? AppImages.SPLASH_SCREEN
                          : widget.plan?.productType == 'Tazur'
                              ? AppImages.TAZUR
                              : widget.plan?.productType == 'Burgan'
                                  ? AppImages.BURGAN
                                  : widget.plan?.productType == 'NLGIC'
                                      ? AppImages.NATIONAL
                                      : AppImages.WARBA
                      // widget.isSelected
                      // ? AppImages.SELECTED_PLAN
                      // : AppImages.PLAN
                      ),
                  height: 63.0.h,
                  width: 63.0.w,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Container(
              // height: 96.0.h,
              padding: EdgeInsets.only(
                  left: 10.0.s, top: 10.0.s, bottom: 6.0.s, right: 10.0.s),
              decoration: BoxDecoration(
                color: widget.isSelected ? AppColors.border : AppColors.white,
                border: Border(
                  top: BorderSide(color: AppColors.border, width: 1.0),
                  bottom: BorderSide(color: AppColors.border, width: 1.0),
                  left: BorderSide(
                      color: widget.isSelected
                          ? AppColors.primaryBlue
                          : AppColors.border,
                      width: 0.0),
                  right: BorderSide(color: AppColors.border, width: 1.0),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.plan?.productSummary.trim() ?? '',
                        style: buildAppTextTheme()
                            .subtitle2!
                            .copyWith(color: AppColors.primaryBlue),
                        textAlign: TextAlign.right,
                      ),
                      SizedBox(
                        height: 10.0.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${widget.plan?.validity} ${widget.plan?.validityPeriod}',
                            style: buildAppTextTheme()
                                .caption!
                                .copyWith(color: AppColors.primaryBlue),
                            textAlign: TextAlign.right,
                          ),
                          SizedBox(width: 5.0.w),
                          if (widget.plan?.productType != null &&
                              widget.plan?.productType.trim() != '')
                            Container(
                                height: 16.0.h,
                                child: VerticalDivider(
                                  width: 2.0.w,
                                  thickness: 2.0.w,
                                  color: AppColors.border,
                                )),
                          SizedBox(width: 7.0.w),
                          Text(
                            widget.plan?.productType ?? '',
                            style: buildAppTextTheme()
                                .caption!
                                .copyWith(color: AppColors.primaryBlue),
                            textAlign: TextAlign.right,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 9.0.h,
                      ),
                      InkWell(
                        onTap: () => showModalBottomSheet(
                            isScrollControlled: true,
                            backgroundColor:
                                AppColors.primaryBlue.withOpacity(0.9),
                            barrierColor:
                                AppColors.primaryBlue.withOpacity(0.9),
                            context: context,
                            builder: (BuildContext context) {
                              return InsuranceDetailsBottomSheetWidget(
                                  index: widget.plan?.productType == 'Tazur'
                                      ? 0
                                      : widget.plan?.productType == 'Burgan'
                                          ? 1
                                          : widget.plan?.productType ==
                                                  'NLGIC'
                                              ? 2
                                              : 3);
                            }),
                        child: Row(
                          children: [
                            Text(
                              'Information',
                              style: buildAppTextTheme()
                                  .caption!
                                  .copyWith(color: AppColors.orange),
                              textAlign: TextAlign.right,
                            ),
                            SizedBox(
                              width: 8.0.w,
                            ),
                            Image(
                              image: AssetImage(AppImages.INFO),
                              height: 18.0.h,
                              width: 18.0.w,
                              color: AppColors.orange,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Stack(
              children: [
                Container(
                  //height: 92.0.h,
                  width: 90.0.w,
                  padding: EdgeInsets.all(20.0.s),
                  decoration: BoxDecoration(
                    color: widget.isSelected
                        ? AppColors.primaryBlue
                        : AppColors.border,
                    border: Border.all(
                        color: widget.isSelected
                            ? AppColors.primaryBlue
                            : AppColors.border,
                        width: 1.0),
                    borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(10.0.s),
                      topRight: Radius.circular(10.0.s),
                    ),
                  ),
                  child: Center(
                    child: Text(
                      '${widget.plan?.premiumAmount}/-',
                      style: buildAppTextTheme().headline2!.copyWith(
                          color: widget.isSelected
                              ? AppColors.white
                              : AppColors.primaryBlue),
                      textAlign: TextAlign.right,
                    ),
                  ),
                ),
                Positioned(
                  top: 1.0.s,
                  right: 1.5.s,
                  child: Container(
                    decoration: BoxDecoration(
                      color: widget.isSelected
                          ? AppColors.white
                          : AppColors.primaryBlue,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10.0.s),
                        topRight: Radius.circular(10.0.s),
                      ),
                    ),
                    height: 26.0.h,
                    width: 40.0.w,
                    child: Center(
                      child: Text(
                        'KWD',
                        style: buildAppTextTheme().bodyText1!.copyWith(
                            color: widget.isSelected
                                ? AppColors.primaryBlue
                                : AppColors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
