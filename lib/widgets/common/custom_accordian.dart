import 'package:flutter/material.dart';

import '../../common/colors.dart';

class CustomAccordian extends StatefulWidget {
  final Widget childWidget;
  final Widget parentWidget;
  final EdgeInsets accordianPadding;
  final bool isExpanded;
  const CustomAccordian({
    required this.childWidget,
    required this.parentWidget,
    required this.accordianPadding,
    this.isExpanded = false,
  });

  @override
  _CustomAccordianState createState() => _CustomAccordianState();
}

class _CustomAccordianState extends State<CustomAccordian> {
  bool isExpanded = false;

  @override
  void initState() {
    super.initState();
    isExpanded = widget.isExpanded;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          padding: widget.accordianPadding,
          child: InkWell(
            onTap: () {
              setState(() {
                isExpanded = !isExpanded;
              });
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(child: widget.parentWidget),
                !isExpanded
                    ? Icon(Icons.expand_more,
                        size: 25.0, color: AppColors.darkBlue)
                    : Icon(Icons.expand_less,
                        size: 25.0, color: AppColors.darkBlue)
              ],
            ),
          ),
        ),
        Visibility(
          visible: isExpanded,
          child: Container(
            padding: widget.accordianPadding,
            child: widget.childWidget,
          ),
        ),
      ],
    );
  }
}
