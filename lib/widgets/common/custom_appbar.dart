import 'package:flutter/material.dart';
import 'package:gt_test/common/responsiveness.dart';

import '../../common/colors.dart';
import '../../common/images.dart';
import '../../common/theme/app_theme.dart';

class CustomAppBar extends StatelessWidget {
  final String title;
  final String desc;

  const CustomAppBar({
    required this.title,
    required this.desc,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: AppColors.primaryBlue,
      leading: Icon(
        Icons.arrow_back,
        color: AppColors.primaryBlue,
        size: 24.0.ics,
      ),
      title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 200.0.w,
              child: Text(
                title,
                style: buildAppTextTheme().headline2,
                textAlign: TextAlign.start,
              ),
            ),
            SizedBox(
              height: 5.0.h,
            ),
            Container(
              width: 200.0.w,
              child: Text(
                desc,
                style: buildAppTextTheme().bodyText2,
                textAlign: TextAlign.start,
              ),
            ),
          ]),
    );
  }
}
