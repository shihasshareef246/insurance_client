import 'package:flutter/material.dart';

import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/utils.dart';
import 'package:gt_test/screens/search_insurances_screen.dart';
import 'package:gt_test/widgets/common/product_widget.dart';
import 'package:gt_test/widgets/offers_widget.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import '../../common/colors.dart';
import '../../common/images.dart';
import '../../common/navigator/navigations.dart';
import '../../common/routes/routes.dart';
import '../../common/theme/app_theme.dart';
import '../../providers/dashboard_provider.dart';

class ProductOffersWidget extends StatefulWidget {
  @override
  _ProductOffersWidgetState createState() => _ProductOffersWidgetState();
}

class _ProductOffersWidgetState extends State<ProductOffersWidget> {
  DashboardProvider dashboardProvider = DashboardProvider();

  @override
  void initState() {
    dashboardProvider = Provider.of<DashboardProvider>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Our Insurance Products',
          style: buildAppTextTheme().headline4,
          textAlign: TextAlign.start,
        ),
        SizedBox(
          height: 16.0.h,
        ),
        ProductWidget(
          text: 'Motor Insurance',
          desc: 'In publishing and graphic design, a placeholder',
          image: AppImages.CAR,
          type: 'MOTOR',
        ),
        SizedBox(
          height: 16.0.h,
        ),
        ProductWidget(
          text: 'Travel Insurance',
          desc: 'In publishing and graphic design, a placeholder',
          image: AppImages.TRAVEL,
          type: 'TRAVEL',
        ),
        SizedBox(
          height: 16.0.h,
        ),
        ProductWidget(
          text: 'Medical Insurance',
          desc: 'In publishing and graphic design, a placeholder',
          image: AppImages.MEDICAL,
          type: 'MEDICAL',
        ),
        SizedBox(
          height: 24.0.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Special Offers for you',
              style: buildAppTextTheme().headline3,
              textAlign: TextAlign.center,
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  PageTransition(
                    type: PageTransitionType.fade,
                    child: SearchInsurancesScreen(type: 'OFFER'), //TODO
                  ),
                );
                // Navigations.pushNamed(
                //   context,
                //   AppRouter.searchInsurancesScreen,
                // );
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'See All',
                    style: buildAppTextTheme().subtitle2,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    width: 7.0.w,
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    color: AppColors.primaryBlue,
                    size: 14.0.ics,
                  ),
                ],
              ),
            ),
          ],
        ),
        SizedBox(
          height: 16.0.h,
        ),
        Container(
          height: 120.0.h,
          child: dashboardProvider.discountPlanList.isNotEmpty
              ? ListView.builder(
                  padding: EdgeInsets.zero,
                  reverse: false,
                  itemCount: dashboardProvider.discountPlanList.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.only(right: 15.0.s),
                      child: OffersWidget(
                        discountData:
                            dashboardProvider?.discountPlanList[index],
                        discount: dashboardProvider
                            .discountPlanList[index]?.promoDiscount,
                        text: Utils.getInsuranceType(
                            type: dashboardProvider
                                .discountPlanList[index]?.insuranceId),
                        desc:
                            '${dashboardProvider.discountPlanList[index]?.planType} : ${dashboardProvider.discountPlanList[index]?.promoDiscountText}',
                        image: AppImages.CAR1,
                      ),
                    );
                  })
              : Center(
                  child: Text(
                    'No offers available currently',
                    style: buildAppTextTheme().headline6,
                    textAlign: TextAlign.start,
                  ),
                ),
        ),
      ],
    );
  }
}
