import 'package:flutter/material.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:page_transition/page_transition.dart';

import '../../common/buttons/raw_button.dart';
import '../../common/colors.dart';
import '../../common/images.dart';
import '../../common/navigator/navigations.dart';
import '../../common/routes/routes.dart';
import '../../common/theme/app_theme.dart';
import '../../screens/register_claim_screen.dart';
import '../insurance_status_bottom_sheet_widget.dart';

class InsuranceWidget extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final String? status;
  final String? desc;
  final String? amount;
  final String? productType;
  final String? paymentMode;
  final int? transactionId;
  final int? insuranceId;
  final int? planId;
  final String? transactionDate;
  const InsuranceWidget({
    required this.title,
    required this.subTitle,
    required this.status,
    required this.desc,
    required this.amount,
    required this.productType,
    required this.paymentMode,
    required this.transactionId,
    required this.insuranceId,
    required this.planId,
    required this.transactionDate,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0.s),
      decoration: BoxDecoration(
        color: AppColors.white,
        border: Border.all(color: AppColors.border, width: 1.0),
        borderRadius: BorderRadius.circular(
          6.0.s,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0.s),
                  ),
                ),
                padding: EdgeInsets.all(17.0.s),
                child: Image(
                  image: AssetImage(productType == 'Gulf Trust'
                      ? AppImages.SPLASH_SCREEN
                      : productType == 'Tazur'
                          ? AppImages.TAZUR
                          : productType == 'Burgan'
                              ? AppImages.BURGAN
                              : productType == 'NLGIC'
                                  ? AppImages.NATIONAL
                                  : AppImages.WARBA),
                  height: 52.0.h,
                  width: 52.0.w,
                ),
              ),
              SizedBox(
                width: 10.0.w,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 170.0.w,
                        child: Text(
                          title ?? '',
                          style: buildAppTextTheme().headline5,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            status ?? '',
                            style: buildAppTextTheme().caption!.copyWith(
                                color: status == 'Expired'
                                    ? AppColors.red
                                    : AppColors.specialGreen),
                            textAlign: TextAlign.end,
                          ),
                          SizedBox(
                            width: 5.0.w,
                          ),
                          InkWell(
                            onTap: () => showModalBottomSheet(
                                isScrollControlled: true,
                                backgroundColor:
                                    AppColors.primaryBlue.withOpacity(0.9),
                                barrierColor:
                                    AppColors.primaryBlue.withOpacity(0.9),
                                context: context,
                                builder: (BuildContext context) {
                                  return InsuranceStatusBottomSheetWidget(
                                      index: status == 'Registered'
                                          ? 0
                                          : status == 'Processed'
                                              ? 1
                                              : status == 'Rejected'
                                                  ? 2
                                                  : status == 'In Progress'
                                                      ? 3
                                                      : status == 'Pending'
                                                          ? 4
                                                          : 4);
                                }),
                            child: Image(
                              image: AssetImage(AppImages.INFO),
                              height: 18.0.h,
                              width: 18.0.w,
                              color: AppColors.orange,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 7.0.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 180.0.w,
                        child: Text(
                          "${subTitle ?? ''} | Paid as ${paymentMode ?? ''}",
                          style: buildAppTextTheme().bodyText1,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Text(
                        '#${transactionId.toString()}',
                        style: buildAppTextTheme()
                            .caption!
                            .copyWith(color: AppColors.primaryBlue),
                        textAlign: TextAlign.end,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 9.0.h,
                  ),
                  Text(
                    transactionDate ?? '',
                    style: buildAppTextTheme().subtitle2,
                    textAlign: TextAlign.start,
                  ),
                ],
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 120.0.w,
                child: Text(
                  amount ?? '',
                  style: buildAppTextTheme().headline2,
                  textAlign: TextAlign.start,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RawButton(
                    width: 73.0.w,
                    height: 40.0.h,
                    text: 'View',
                    borderRadius: 50.0.s,
                    onPressed: () => Navigations.pushNamed(
                        context,
                        AppRouter.insuranceDetailsScreen,
                        {"transactionId": transactionId, "status": status}),
                    bgColor: AppColors.white,
                    textColor: AppColors.primaryBlue,
                  ),
                  SizedBox(
                    width: 9.0.w,
                  ),
                  RawButton(
                    width: 73.0.w,
                    height: 40.0.h,
                    text: 'Claim',
                    borderRadius: 50.0.s,
                    onPressed: () {
                      Navigator.push(
                        context,
                        PageTransition(
                            type: PageTransitionType.fade,
                            child: RegisterClaimScreen(
                              planId: planId,
                            )),
                      );
                      // Navigations.pushNamed(
                      //   context,
                      //   AppRouter.registerClaimScreen,
                      // );
                    },
                    bgColor: AppColors.primaryBlue,
                    textColor: AppColors.white,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
