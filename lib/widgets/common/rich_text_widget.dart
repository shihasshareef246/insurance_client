import 'package:flutter/material.dart';

class RichTextWidget extends StatelessWidget {
  final TextStyle style1;
  final String text1;
  final TextStyle style2;
  final String text2;

  const RichTextWidget({
    required this.style1,
    required this.text1,
    required this.style2,
    required this.text2,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Text.rich(
          TextSpan(style: style1, children: <TextSpan>[
            TextSpan(text: text1),
            TextSpan(text: text2, style: style2),
          ]),
        ));
  }
}
