import 'package:flutter/material.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/models/customer_claims_model.dart';
import 'package:gt_test/screens/claim_details_screen.dart';
import 'package:page_transition/page_transition.dart';

import '../../common/buttons/raw_button.dart';
import '../../common/colors.dart';
import '../../common/images.dart';
import '../../common/theme/app_theme.dart';
import '../insurance_status_bottom_sheet_widget.dart';

class ClaimsWidget extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final String? status;
  final String? id;
  final String? desc;
  final String? image;
  final String? claimAmount;
  final String? productType;
  final CustomerClaimsModel? claimDetails;
  const ClaimsWidget({
    required this.title,
    required this.subTitle,
    required this.status,
    required this.id,
    required this.desc,
    required this.image,
    required this.claimAmount,
    required this.productType,
    required this.claimDetails,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0.s),
      decoration: BoxDecoration(
        color: AppColors.white,
        border: Border.all(color: AppColors.border, width: 1.0),
        borderRadius: BorderRadius.circular(
          6.0.s,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.all(17.0.s),
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(10.0.s),
                  ),
                ),
                child: Image(
                  image: AssetImage(productType == 'Gulf Trust'
                      ? AppImages.SPLASH_SCREEN
                      : productType == 'Tazur'
                          ? AppImages.TAZUR
                          : productType == 'Burgan'
                              ? AppImages.BURGAN
                              : productType == 'NLGIC'
                                  ? AppImages.NATIONAL
                                  : AppImages.WARBA),
                  height: 52.0.h,
                  width: 52.0.w,
                ),
              ),
              SizedBox(
                width: 26.0.w,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 160.0.w,
                        child: Text(
                          title ?? '',
                          style: buildAppTextTheme().headline3,
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            width: 60.0.w,
                            child: Text(
                              '#${id ?? ''}',
                              style: buildAppTextTheme().bodyText1,
                              textAlign: TextAlign.right,
                            ),
                          ),
                          SizedBox(
                            width: 5.0.w,
                          ),
                          InkWell(
                            onTap: () => showModalBottomSheet(
                                isScrollControlled: true,
                                backgroundColor:
                                AppColors.primaryBlue.withOpacity(0.9),
                                barrierColor:
                                AppColors.primaryBlue.withOpacity(0.9),
                                context: context,
                                builder: (BuildContext context) {
                                  return InsuranceStatusBottomSheetWidget(
                                      index: status == 'Registered'
                                          ? 0
                                          : status == 'Processed'
                                          ? 1
                                          : status == 'Rejected'
                                          ? 2
                                          : status == 'In Progress'
                                          ? 3
                                          : status == 'Pending'
                                          ? 4
                                          : 4);
                                }),
                            child: Image(
                              image: AssetImage(AppImages.INFO),
                              height: 18.0.h,
                              width: 18.0.w,
                              color: AppColors.orange,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 7.0.h,
                  ),
                  Container(
                    width: 160.0.w,
                    child: Text(
                      subTitle ?? '',
                      style: buildAppTextTheme().bodyText1,
                      textAlign: TextAlign.start,
                    ),
                  ),
                  SizedBox(
                    height: 9.0.h,
                  ),
                  Container(
                    width: 160.0.w,
                    child: Text(
                      desc ?? '',
                      style: buildAppTextTheme().subtitle2,
                      textAlign: TextAlign.start,
                    ),
                  ),
                ],
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: 120.0.w,
                child: Text(
                  claimAmount!,
                  style: buildAppTextTheme().headline2,
                  textAlign: TextAlign.start,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RawButton(
                    width: 73.0.w,
                    height: 40.0.h,
                    text: 'View',
                    borderRadius: 50.0.s,
                    onPressed: () {
                      Navigator.push(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: ClaimDetailsScreen(claimDetails: claimDetails),
                        ),
                      );
                    },
                    bgColor: AppColors.white,
                    textColor: AppColors.primaryBlue,
                  ),
                  SizedBox(
                    width: 9.0.w,
                  ),
                  Container(
                    width: 73.0.w,
                    height: 40.0.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50.0.s),
                      color: status == 'Processed'
                          ? AppColors.green
                          : status == 'Rejected'
                              ? AppColors.violentRed
                              : status == 'Pending'
                                  ? AppColors.paleYellow
                                  : status == 'In Progress'
                                      ? AppColors.paleOrange
                                      : AppColors.red,
                    ),
                    child: Center(
                      child: Text(status ?? '',
                          textAlign: TextAlign.center,
                          style: buildAppTextTheme().bodyText1),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
