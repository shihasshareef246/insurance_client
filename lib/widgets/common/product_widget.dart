import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import '../../common/colors.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/screens/motor_insurances_screen.dart';
import 'package:gt_test/screens/travel_insurances_screen.dart';
import '../../common/theme/app_theme.dart';
import '../../providers/customer_provider.dart';
import '../../screens/search_insurances_screen.dart';

class ProductWidget extends StatefulWidget {
  final String text;
  final String desc;
  final String image;
  final String type;

  const ProductWidget({
    required this.text,
    required this.desc,
    required this.image,
    required this.type,
  });
  @override
  _ProductWidgetState createState() => _ProductWidgetState();
}

class _ProductWidgetState extends State<ProductWidget> {
  CustomerProvider customerProvider = CustomerProvider();

  @override
  void initState() {
    customerProvider = Provider.of<CustomerProvider>(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        customerProvider.setInsuranceType = widget.type;
        Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.fade,
            child: widget.type == 'MOTOR'
                ? MotorInsurancesScreen(type: widget.type)
                : widget.type == 'TRAVEL'
                    ? TravelInsurancesScreen(type: widget.type)
                    : widget.type == 'MEDICAL'
                        //TODO  ? MedicalInsurancesScreen(type: type)
                        ? MotorInsurancesScreen(type: widget.type)
                        : SearchInsurancesScreen(type: widget.type),
          ),
        );
      },
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              color: AppColors.white,
              border: Border.all(color: AppColors.border, width: 1.0),
              borderRadius: BorderRadius.circular(
                9.0.s,
              ),
              boxShadow: [
                BoxShadow(
                  blurRadius: 8.0.s,
                  offset: Offset(0, 4),
                  color: AppColors.black.withOpacity(0.15),
                ),
              ],
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage(widget.image),
                  height: 124.0.h,
                  width: 140.0.w,
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  width: 13.0.w,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 200.0.w,
                      child: Text(
                        widget.text,
                        style: buildAppTextTheme().headline2,
                        textAlign: TextAlign.start,
                      ),
                    ),
                    SizedBox(
                      height: 5.0.h,
                    ),
                    Container(
                      width: 200.0.w,
                      child: Text(
                        widget.desc,
                        style: buildAppTextTheme()
                            .bodyText2
                            ?.copyWith(color: AppColors.primaryBlue),
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Positioned(
            bottom: 0.0.s,
            right: 0.0.s,
            child: Container(
              decoration: BoxDecoration(
                color: AppColors.primaryBlue,
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(6.0.s),
                  topLeft: Radius.circular(8.0.s),
                ),
              ),
              height: 27.0.h,
              width: 35.0.w,
              child: Icon(
                Icons.arrow_forward_ios,
                color: AppColors.white,
                size: 10.0.ics,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
