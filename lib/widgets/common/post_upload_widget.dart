import 'package:flutter/material.dart';
import 'package:gt_test/common/responsiveness.dart';

import '../../common/colors.dart';
import '../../common/images.dart';
import '../../common/theme/app_theme.dart';

class PostUploadWidget extends StatelessWidget {
  final String text;
  final String type;

  const PostUploadWidget({
    this.text = '',
    this.type = '',
  });

  void viewSample(String name, String type, BuildContext context) {
    showModalBottomSheet<dynamic>(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Container(
            decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20.0.s),
                topRight: Radius.circular(20.0.s),
              ),
            ),
            child: Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(30.0.s),
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0.s),
                      topRight: Radius.circular(20.0.s),
                    ),
                    child: Image(
                      image: AssetImage(AppImages.GALLERY),
                      height: 360.0.h,
                      width: 320.0.w,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Positioned(
                  top: 20.0.s,
                  right: 20.0.s,
                  child: InkWell(
                    child: CircleAvatar(
                      backgroundColor: AppColors.brownish,
                      child: Icon(Icons.close,
                          size: 20.0.ics, color: AppColors.primaryBlue),
                    ),
                    onTap: () => Navigator.pop(context),
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 14.0.s, vertical: 18.0.s),
      decoration: BoxDecoration(
        color: AppColors.white,
        border: Border.all(color: AppColors.border, width: 1.0),
        borderRadius: BorderRadius.circular(
          6.0.s,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image(
                image: AssetImage(AppImages.DOCUMENT),
                height: 20.0.h,
                width: 16.0.w,
              ),
              SizedBox(
                width: 15.0.w,
              ),
              Text(
                text,
                style: buildAppTextTheme().headline6,
                textAlign: TextAlign.center,
              ),
            ],
          ),
          type == 'Claim_Document'
              ? Icon(
                  Icons.done,
                  color: AppColors.primaryGreen,
                  size: 24.0.ics,
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () => viewSample(text, type, context),
                      child: Image(
                        image: AssetImage(AppImages.DOCUMENT),
                        height: 20.0.h,
                        width: 16.0.w,
                      ),
                    ),
                    SizedBox(
                      width: 40.0.w,
                    ),
                    Icon(
                      Icons.done,
                      color: AppColors.primaryGreen,
                      size: 24.0.ics,
                    ),
                  ],
                ),

          // Image(
          //   image: AssetImage(AppImages.DONE),
          //   height: 24.0.h,
          //   width: 24.0.w,
          //   color: AppColors.primaryGreen,
          // ),
        ],
      ),
    );
  }
}
