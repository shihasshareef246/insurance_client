import 'package:flutter/material.dart';
import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/responsiveness.dart';

class CircularProgressBar extends StatelessWidget {
  final bool isButtonLoader;
  final double strokeWidth;

  CircularProgressBar({
    this.isButtonLoader = true,
    this.strokeWidth = 4.0,
  });
  @override
  Widget build(BuildContext context) {
    final Widget loader = CircularProgressIndicator(
      backgroundColor: AppColors.primaryBlue,
      valueColor: AlwaysStoppedAnimation<Color>(
        AppColors.white,
      ),
      strokeWidth: strokeWidth,
    );
    return isButtonLoader
        ? SizedBox(
            height: 24.0.h,
            width: 24.0.w,
            child: loader,
          )
        : loader;
  }
}
