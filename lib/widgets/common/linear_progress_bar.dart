import 'package:flutter/material.dart';

class LinearProgressBar extends StatelessWidget {
  final double value;
  final Color backgroundColor;
  final Color valueColor;
  final double minHeight;
  LinearProgressBar({
    required this.value,
    required this.backgroundColor,
    required this.valueColor,
    required this.minHeight,
  });

  @override
  Widget build(BuildContext context) {
    return LinearProgressIndicator(
      value: value,
      backgroundColor: backgroundColor,
      valueColor: AlwaysStoppedAnimation<Color>(
        valueColor,
      ),
      minHeight: minHeight,
    );
  }
}
