import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

import 'package:gt_test/common/colors.dart';
import 'package:gt_test/common/responsiveness.dart';
import 'package:gt_test/common/theme/app_theme.dart';
import 'package:gt_test/screens/home_screen.dart';
import 'package:page_transition/page_transition.dart';
import '../../common/images.dart';
import '../../screens/base.dart';
import '../../screens/claims_screen.dart';
import '../../screens/insurances_screen.dart';
import '../../screens/profile_screen.dart';

class BottomNavigation extends StatefulWidget {
  final int activePageIndex;
  BottomNavigation({
    this.activePageIndex = 0,
  });
  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CurvedNavigationBar(
          height: 50.0.h,
          backgroundColor: Colors.transparent,
          color: AppColors.primaryBlue,
          index: widget.activePageIndex,
          items: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image(
                      image: AssetImage(AppImages.HOME),
                      width: 23.0.w,
                      height: 21.0.h),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image(
                      image: AssetImage(AppImages.INSURANCES),
                      width: 23.0.w,
                      height: 21.0.h),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image(
                      image: AssetImage(AppImages.CLAIMS),
                      width: 23.0.w,
                      height: 21.0.h),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0.s),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image(
                      image: AssetImage(AppImages.PROFILE),
                      width: 23.0.w,
                      height: 21.0.h),
                ],
              ),
            )
          ],
          onTap: (index) {
            switch (index) {
              case 0:
                Navigator.push(
                  context,
                  PageTransition(
                    type: PageTransitionType.fade,
                    child: BaseLayout(
                      page: HomeScreen(),
                      activePageIndex: 0,
                    ),
                  ),
                );
                // Navigations.pushNamed(
                //   context,
                //   AppRouter.homeScreen,
                // );
                break;
              case 1:
                Navigator.push(
                  context,
                  PageTransition(
                    type: PageTransitionType.fade,
                    child: BaseLayout(
                      page: InsurancesScreen(),
                      activePageIndex: 1,
                    ),
                  ),
                );
                // Navigations.pushNamed(
                //   context,
                //   AppRouter.insurancesScreen,
                // );
                break;
              case 2:
                Navigator.push(
                  context,
                  PageTransition(
                    type: PageTransitionType.fade,
                    child: BaseLayout(
                      page: ClaimsScreen(),
                      activePageIndex: 2,
                    ),
                  ),
                );
                // Navigations.pushNamed(
                //   context,
                //   AppRouter.claimsScreen,
                // );
                break;
              case 3:
                Navigator.push(
                  context,
                  PageTransition(
                    type: PageTransitionType.fade,
                    child: BaseLayout(
                      page: ProfileScreen(),
                      activePageIndex: 3,
                    ),
                  ),
                );
                // Navigations.pushNamed(
                //   context,
                //   AppRouter.profileScreen,
                // );
                break;
            }
          },
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              margin: EdgeInsets.only(top: 52.0.s),
              color: AppColors.primaryBlue,
              padding:
                  EdgeInsets.only(left: 15.0.s, right: 15.0.s, bottom: 10.0.s),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: BaseLayout(
                            page: HomeScreen(),
                            activePageIndex: 0,
                          ),
                        ),
                      );
                      // Navigations.pushNamed(
                      //   context,
                      //   AppRouter.homeScreen,
                      // );
                    },
                    child: Container(
                      width: 80.0.w,
                      child: Text(
                        'Home',
                        style: buildAppTextTheme()
                            .caption!
                            .copyWith(color: AppColors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: BaseLayout(
                            page: InsurancesScreen(),
                            activePageIndex: 1,
                          ),
                        ),
                      );
                      // Navigations.pushNamed(
                      //   context,
                      //   AppRouter.insurancesScreen,
                      // );
                    },
                    child: Container(
                      width: 80.0.w,
                      child: Text(
                        'Insurances',
                        style: buildAppTextTheme()
                            .caption!
                            .copyWith(color: AppColors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: BaseLayout(
                            page: ClaimsScreen(),
                            activePageIndex: 2,
                          ),
                        ),
                      );
                      // Navigations.pushNamed(
                      //   context,
                      //   AppRouter.claimsScreen,
                      // );
                    },
                    child: Container(
                      width: 80.0.w,
                      child: Text(
                        'Claims',
                        style: buildAppTextTheme()
                            .caption!
                            .copyWith(color: AppColors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: BaseLayout(
                            page: ProfileScreen(),
                            activePageIndex: 3,
                          ),
                        ),
                      );
                      // Navigations.pushNamed(
                      //   context,
                      //   AppRouter.profileScreen,
                      // );
                    },
                    child: Container(
                      width: 80.0.w,
                      child: Text(
                        'Profile',
                        style: buildAppTextTheme()
                            .caption!
                            .copyWith(color: AppColors.white),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
