import 'package:flutter/material.dart';

import '../../common/colors.dart';
import '../../common/theme/app_theme.dart';
import 'package:gt_test/common/responsiveness.dart';

class UploadBottomSheetWidget extends StatelessWidget {
  final String title;
  final String listTitle;
  final Function onClick;
  final String image;
  final bool isProfile;
  final String listTitle1;
  final Function onClick1;
  final String image1;
  final String listTitle2;
  final Function onClick2;
  final IconData? image2;
  UploadBottomSheetWidget({
    this.title = '',
    this.listTitle = '',
    required this.onClick,
    this.image = '',
    this.isProfile = false,
    this.listTitle1 = '',
    required this.onClick1,
    this.image1 = '',
    this.listTitle2 = '',
    required this.onClick2,
    this.image2,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height > 650 ? 320.0.h : 350.0.h,
      padding: EdgeInsets.all(20.0.s),
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10.0.s),
            topRight: Radius.circular(10.0.s)),
        boxShadow: [
          BoxShadow(
            blurRadius: 4.0.s,
            offset: Offset(0, 2),
            color: AppColors.primaryBlue,
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(bottom: 10.0.s),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  this.title,
                  style: buildAppTextTheme()
                      .headline3!
                      .copyWith(color: AppColors.black),
                ),
                InkWell(
                  child: Icon(Icons.close,
                      size: 20.0.ics, color: AppColors.primaryBlue),
                  onTap: () => Navigator.pop(context),
                ),
              ],
            ),
          ),
          // SizedBox(height: 10.0.h),
          InkWell(
            child: Container(
              padding: EdgeInsets.all(10.0.s),
              color: AppColors.lightGreen,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      // SvgPicture.asset(this.image,
                      //     width: 38.0.w, height: 38.0.h),
                      Image(
                        image: AssetImage(this.image),
                        width: 24.0.w,
                        height: 24.0.h,
                        color: AppColors.primaryBlue,
                      ),
                      SizedBox(width: 10.0.w),
                      Text(
                        this.listTitle,
                        style: buildAppTextTheme().headline4!.copyWith(
                              color: AppColors.lightBlack,
                            ),
                      ),
                    ],
                  ),
                  SizedBox(width: 10.0.w),
                  Icon(
                    Icons.keyboard_arrow_right,
                    size: 25.0.ics,
                    color: AppColors.primaryBlue,
                  ),
                ],
              ),
            ),
            onTap: () => this.onClick(),
          ),
          SizedBox(height: 16.0.h),
          InkWell(
            child: Container(
              padding: EdgeInsets.all(10.0.s),
              color: AppColors.lightGreen,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      // SvgPicture.asset(this.image1,
                      //     width: 38.0.w, height: 38.0.h),
                      Image(
                        image: AssetImage(this.image1),
                        width: 24.0.w,
                        height: 24.0.h,
                        color: AppColors.primaryBlue,
                      ),
                      SizedBox(width: 10.0.w),
                      Text(
                        this.listTitle1,
                        style: buildAppTextTheme().headline4!.copyWith(
                              color: AppColors.primaryBlue,
                            ),
                      ),
                    ],
                  ),
                  SizedBox(width: 10.0.w),
                  Icon(
                    Icons.keyboard_arrow_right,
                    size: 25.0.ics,
                    color: AppColors.primaryBlue,
                  ),
                ],
              ),
            ),
            onTap: () => this.onClick1(),
          ),
          SizedBox(height: 16.0.h),
          InkWell(
            child: Container(
              padding: EdgeInsets.all(10.0.s),
              color: AppColors.lightGreen,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      // SvgPicture.asset(this.image1,
                      //     width: 38.0.w, height: 38.0.h),
                      InkWell(
                        child: Icon(this.image2,
                            size: 24.0.ics, color: AppColors.primaryBlue),
                      ),
                      // Image(
                      //   image: AssetImage(this.image2),
                      //   width: 24.0.w,
                      //   height: 24.0.h,
                      //   color: AppColors.primaryBlue,
                      // ),
                      SizedBox(width: 10.0.w),
                      Text(
                        this.listTitle2,
                        style: buildAppTextTheme().headline4!.copyWith(
                              color: AppColors.primaryBlue,
                            ),
                      ),
                    ],
                  ),
                  SizedBox(width: 10.0.w),
                  Icon(
                    Icons.keyboard_arrow_right,
                    size: 25.0.ics,
                    color: AppColors.primaryBlue,
                  ),
                ],
              ),
            ),
            onTap: () => this.onClick2(),
          ),
        ],
      ),
    );
  }
}
