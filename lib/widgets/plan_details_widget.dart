import 'package:flutter/material.dart';
import 'package:gt_test/common/responsiveness.dart';

import '../../common/buttons/raw_button.dart';
import '../../common/colors.dart';
import '../../common/images.dart';
import '../../common/theme/app_theme.dart';
import '../models/insurance_details_model.dart';

class PlanDetailsWidget extends StatelessWidget {
  final MotorDetailsModel? motorDetails;
  const PlanDetailsWidget({
    required this.motorDetails,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: 200.0.w,
          child: Text(
            'Insurance Details',
            style: buildAppTextTheme().headline6,
            textAlign: TextAlign.start,
          ),
        ),
        SizedBox(
          height: 10.0.h,
        ),
        Container(
          padding: EdgeInsets.all(20.0.s),
          decoration: BoxDecoration(
            color: AppColors.white,
            border: Border.all(color: AppColors.border, width: 1.0),
            borderRadius: BorderRadius.circular(
              6.0.s,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                        image: AssetImage(AppImages.MOTOR),
                        height: 18.0.h,
                        width: 18.0.w,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Insurance type :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    'Motor',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ), Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                        image: AssetImage(AppImages.BRAND),
                        height: 18.0.h,
                        width: 18.0.w,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Make & Model :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    motorDetails?.makeModel ?? '',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image(
                        image: AssetImage(AppImages.YEAR),
                        height: 18.0.h,
                        width: 18.0.w,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Year of manufacture :',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    motorDetails?.makeYear ?? '',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.attach_money,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Car value : ',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    '${motorDetails?.carValue} KWD',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.airline_seat_legroom_extra,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'No of seats : ',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    motorDetails?.noOfSeats??'',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.confirmation_number_outlined,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Plate No : ',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    motorDetails?.plateNo??'',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.tablet,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'Chasis No : ',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    motorDetails?.vinNo??'',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
              SizedBox(
                height: 8.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.file_copy_outlined,
                        color: AppColors.primaryBlue,
                        size: 18.0.ics,
                      ),
                      SizedBox(
                        width: 16.0.w,
                      ),
                      Text(
                        'License No : ',
                        style: buildAppTextTheme().caption,
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                  Text(
                    motorDetails?.licenseNo??'',
                    style: buildAppTextTheme()
                        .caption!
                        .copyWith(color: AppColors.primaryBlue),
                    textAlign: TextAlign.right,
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
