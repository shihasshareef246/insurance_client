import 'package:flutter/material.dart';
//import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:advance_pdf_viewer_fork/advance_pdf_viewer_fork.dart';
//import 'package:flutter_pdfview/flutter_pdfview.dart';

import '../../common/buttons/raw_button.dart';
import '../../common/colors.dart';
import '../../common/images.dart';
import '../../common/theme/app_theme.dart';
import '../common/popup/modal_popup.dart';
import 'package:gt_test/common/responsiveness.dart';

class PolicyDocumentWidget extends StatefulWidget {
  final String name;

  const PolicyDocumentWidget({
    required this.name,
  });
  @override
  _PolicyDocumentWidgetState createState() => _PolicyDocumentWidgetState();
}

class _PolicyDocumentWidgetState extends State<PolicyDocumentWidget> {
  late PDFDocument? document;
  bool isLoading = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    document = null;
    super.dispose();
  }

  loadDocument() async {
    document = await PDFDocument.fromAsset('assets/files/pdf/sample.pdf');
    setState(() {
      isLoading = true;
    });
    // ModalPopUp(
    //     crossIconRequired: false,
    //     crossIconSize: 16.0.s,
    //     iconColor: AppColors.greyishBlack,
    //     backgroundColor: AppColors.white,
    //     childcontainer: PDFViewer(document: document!, zoomSteps: 1,
    //       showPicker: false,
    //       showNavigation: true,),
    //     barrierDismissable: false,
    //     barrierColor: Colors.black,
    //     closeClick: () => {}).showPopup(context);
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Container(
            height: 200.0.h,
            width: 200.0.w,
            child: PDFViewer(
              document: document!,
              zoomSteps: 1,
              showPicker: false,
              showNavigation: true,
            ),
          )
        : Container(
            padding: EdgeInsets.all(10.0.s),
            decoration: BoxDecoration(
              color: AppColors.white,
              border: Border.all(color: AppColors.border, width: 1.0),
              borderRadius: BorderRadius.circular(
                6.0.s,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Image(
                          image: AssetImage(AppImages.PDF),
                          height: 24.0.h,
                          width: 24.0.w,
                        ),
                        SizedBox(
                          width: 16.0.w,
                        ),
                        Container(
                          width: 200.0.w,
                          child: Text(
                            widget.name,
                            style: buildAppTextTheme().headline3,
                            textAlign: TextAlign.start,
                          ),
                        ),
                      ],
                    ),
                    RawButton(
                      width: 102.0.w,
                      height: 48.0.h,
                      text: 'View',
                      borderRadius: 5.0.s,
                      onPressed: () => loadDocument(),
                      bgColor: AppColors.primaryBlue,
                      textColor: AppColors.white,
                    ),
                  ],
                ),
              ],
            ),
          );
  }
}
